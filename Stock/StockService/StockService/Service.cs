﻿using System.ServiceProcess;
using StockServiceCore;

namespace StockService
{
    public partial class Service : ServiceBase
    {
        public Service()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            Model.Instance.Start();
        }

        protected override void OnStop()
        {
            Model.Instance.Stop();
        }
    }
}