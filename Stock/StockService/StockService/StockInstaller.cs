﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;

namespace StockService
{
    [RunInstaller(true)]
    public partial class StockInstaller : System.Configuration.Install.Installer
    {
        public StockInstaller()
        {
            InitializeComponent();
        }
    }
}
