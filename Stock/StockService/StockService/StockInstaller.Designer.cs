﻿namespace StockService
{
    partial class StockInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            this.ServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            // 
            // ServiceInstaller
            // 
            this.ServiceInstaller.Description = "Служба печати Stock Inventory";
            this.ServiceInstaller.DisplayName = "StockService";
            this.ServiceInstaller.ServiceName = "StockService";
            // 
            // ServiceProcessInstaller
            // 
            this.ServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.ServiceProcessInstaller.Password = null;
            this.ServiceProcessInstaller.Username = null;
            // 
            // StockInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.ServiceInstaller,
            this.ServiceProcessInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceInstaller ServiceInstaller;
        private System.ServiceProcess.ServiceProcessInstaller ServiceProcessInstaller;
    }
}