﻿using System;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using StockCore;
using StockCore.Enums;
using StockCore.Factories;
using StockCore.Handlers.Exchange.Configuration.CSV;
using StockCore.Handlers.Exchange.Exports;
using StockInventoryMaster.Handlers;

namespace StockInventoryMaster
{
    public partial class ExportControl : MasterControl
    {
        protected ExchangeControl Exchange { get; set; }

        public ExportControl()
        {
            InitializeComponent();

            Exchange = new ExchangeControl {Dock = DockStyle.Fill};

            ExchangePanelControl.Controls.Add(Exchange);
        }

        private void DirectoryButtonEdit_ButtonClick(object sender,
                                                     DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            using (var dialog = new FolderBrowserDialog())
            {
                if (dialog.ShowDialog() == DialogResult.Cancel)
                    return;

                if (Directory.Exists(dialog.SelectedPath))
                {
                    Invoker(() => DirectoryButtonEdit.Text = dialog.SelectedPath);
                }
            }
        }

        private void ExportControl_Load(object sender, EventArgs e)
        {
            Invoker(() =>
                {
                    var path = Path.Combine(Model.Instance.Common.CurrentDirectory, "Export");

                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                    DirectoryButtonEdit.Text = path;

                });
        }

        private void CopyDatabaseBtn_Click(object sender, EventArgs e)
        {
            CopyDatabaseFromTerminal();
        }

        protected void CopyDatabaseFromTerminal()
        {
            Model.Instance.Invoker.DoIndependentThread("Копирование базы данных c мобильного устройства", () =>
                {
                    var from = Path.Combine(DirectoryButtonEdit.Text, Constants.DatabaseName);

                    WaitShow("Копирование с ТСД ...", "Пожалуйста, подождите");

                    var copy =
                        RapiHandler.Instance.CopyFromDevice(
                            new[]
                                {
                                    Path.Combine(TerminalDirectoryEdit.Text, Constants.DatabaseName),
                                    Path.Combine(@"\..\Storage Card", Constants.DatabaseName),
                                    Path.Combine(@"\..\Program Files\stock", Constants.DatabaseName)
                                }, from);

                    WaitClose();

                    if (!copy)
                    {
                        if (RapiHandler.Instance.LastException != null)
                        {
                            MessageHandler.Instance.ShowExclamation(RapiHandler.Instance.LastException.Message);
                            return;
                        }
                    }

                    // выгружаем из скопированой базы данные
                    Export(from);

                },
            () =>
                {
                    WaitClose();

                }, true);
        }

        private void ExportBtn_Click(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("Копирование базы данных c мобильного устройства", () =>
                {
                    var from = Path.Combine(DirectoryButtonEdit.Text, Constants.DatabaseName);

                    if (!File.Exists(from))
                    {
                        throw new Exception(string.Format("Не удалось найти базу данных по указанному пути \"{0}\"", from));
                    }

                    Export(from);
                },

            false, true);
        }

        protected void Export(string database)
        {
            var settings = CsvExchangeSettings.Instance;

            var export = ExportFactory.Instance.GetInstance(
                ExportEnum.Cvs,
                new CsvExportProtocol(false, false, database, OnlyFoundСheck.Checked, OnlyNewСheck.Checked),
                settings);

            Model.Instance.Presenters.ExchangeProcess.Exchanger = export;
            Model.Instance.Presenters.ExchangeProcess.Exchanger.MoveNextRecord += ExchangerMoveNextRecord;
            Model.Instance.Presenters.ExchangeProcess.Exchanger.ExchangeFinishedSuccessfully += ExchangerExchangeFinishedSuccessfully;
            Model.Instance.Presenters.ExchangeProcess.Exchanger.ExchangeFinishedException += ExchangerExchangeFinishedException;

            Model.Instance.Presenters.ExchangeProcess.ShowView();
        }

        protected void FinishExchange()
        {
            Exchange.ProcessText = "Выгрузка данных завершена";
            if (Model.Instance.Presenters.ExchangeProcess.Exchanger != null)
            {
                Model.Instance.Presenters.ExchangeProcess.Exchanger.MoveNextRecord -= ExchangerMoveNextRecord;
                Model.Instance.Presenters.ExchangeProcess.Exchanger.ExchangeFinishedException -= ExchangerExchangeFinishedException;
                Model.Instance.Presenters.ExchangeProcess.Exchanger.ExchangeFinishedSuccessfully -= ExchangerExchangeFinishedSuccessfully;
            }
        }

        protected void ExchangerExchangeFinishedException(object sender, StockCore.Events.StockEventArgs e)
        {
            FinishExchange();
        }

        protected void ExchangerExchangeFinishedSuccessfully(object sender, EventArgs e)
        {
            FinishExchange();
        }

        protected void ExchangerMoveNextRecord(object sender, EventArgs e)
        {
            Exchange.ProcessText = Model.Instance.Presenters.ExchangeProcess.Exchanger.ExchangeProcessStatus;
            Exchange.ExchangeTime = string.Format("{0}:{1}:{2}", Model.Instance.Presenters.ExchangeProcess.Exchanger.Watcher.Elapsed.Hours, Model.Instance.Presenters.ExchangeProcess.Exchanger.Watcher.Elapsed.Minutes, Model.Instance.Presenters.ExchangeProcess.Exchanger.Watcher.Elapsed.Seconds);
            Exchange.Progress = Model.Instance.Presenters.ExchangeProcess.Exchanger.Persent;
            Exchange.Received = Model.Instance.Presenters.ExchangeProcess.Exchanger.Received.ToString(CultureInfo.InvariantCulture);
            Exchange.Prepared = Model.Instance.Presenters.ExchangeProcess.Exchanger.Prepared.ToString(CultureInfo.InvariantCulture);
            Exchange.Processed = Model.Instance.Presenters.ExchangeProcess.Exchanger.Processed.ToString(CultureInfo.InvariantCulture);
            Exchange.NotProcessed = Model.Instance.Presenters.ExchangeProcess.Exchanger.NotProcessed.ToString(CultureInfo.InvariantCulture);
        }

        private void OnlyFoundСheck_CheckedChanged(object sender, EventArgs e)
        {
            if (OnlyFoundСheck.Checked)
            {
                OnlyNewСheck.Checked = false;
            }
        }

        private void OnlyNewСheck_CheckedChanged(object sender, EventArgs e)
        {
            if (OnlyNewСheck.Checked)
            {
                OnlyFoundСheck.Checked = false;
            }
        }
    }
}