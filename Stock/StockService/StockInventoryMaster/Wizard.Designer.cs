﻿using StockInventoryMaster.Properties;

namespace StockInventoryMaster
{
    partial class Wizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Wizard));
            this.InventoryWizardControl = new DevExpress.XtraWizard.WizardControl();
            this.WelcomeWizardPage = new DevExpress.XtraWizard.WelcomeWizardPage();
            this.WizardSelectActionPage = new DevExpress.XtraWizard.WizardPage();
            this.ActionPanel = new DevExpress.XtraEditors.PanelControl();
            this.ActionDescriptionEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ActionsGroup = new DevExpress.XtraEditors.RadioGroup();
            this.completionWizardPage1 = new DevExpress.XtraWizard.CompletionWizardPage();
            this.wizardPage2 = new DevExpress.XtraWizard.WizardPage();
            ((System.ComponentModel.ISupportInitialize)(this.InventoryWizardControl)).BeginInit();
            this.InventoryWizardControl.SuspendLayout();
            this.WizardSelectActionPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ActionPanel)).BeginInit();
            this.ActionPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ActionDescriptionEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActionsGroup.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // InventoryWizardControl
            // 
            this.InventoryWizardControl.AccessibleName = "Actions";
            this.InventoryWizardControl.CancelText = "Отмена";
            this.InventoryWizardControl.Controls.Add(this.WelcomeWizardPage);
            this.InventoryWizardControl.Controls.Add(this.WizardSelectActionPage);
            this.InventoryWizardControl.Controls.Add(this.completionWizardPage1);
            this.InventoryWizardControl.Controls.Add(this.wizardPage2);
            this.InventoryWizardControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InventoryWizardControl.FinishText = "&Завершить";
            this.InventoryWizardControl.HelpText = "&Помощь";
            this.InventoryWizardControl.Image = global::StockInventoryMaster.Properties.Resources.Logo1;
            this.InventoryWizardControl.Location = new System.Drawing.Point(0, 0);
            this.InventoryWizardControl.Name = "InventoryWizardControl";
            this.InventoryWizardControl.NextText = "&Далее >";
            this.InventoryWizardControl.Pages.AddRange(new DevExpress.XtraWizard.BaseWizardPage[] {
            this.WelcomeWizardPage,
            this.WizardSelectActionPage,
            this.wizardPage2,
            this.completionWizardPage1});
            this.InventoryWizardControl.PreviousText = "< &Назад";
            this.InventoryWizardControl.Size = new System.Drawing.Size(670, 554);
            this.InventoryWizardControl.SelectedPageChanging += new DevExpress.XtraWizard.WizardPageChangingEventHandler(this.InventoryWizardControlSelectedPageChanging);
            this.InventoryWizardControl.CancelClick += new System.ComponentModel.CancelEventHandler(this.InventoryWizardControl_CancelClick);
            this.InventoryWizardControl.FinishClick += new System.ComponentModel.CancelEventHandler(this.InventoryWizardControl_FinishClick);
            // 
            // WelcomeWizardPage
            // 
            this.WelcomeWizardPage.AccessibleName = "Wellcome";
            this.WelcomeWizardPage.IntroductionText = resources.GetString("WelcomeWizardPage.IntroductionText");
            this.WelcomeWizardPage.Name = "WelcomeWizardPage";
            this.WelcomeWizardPage.ProceedText = "Чтобы продолжить нажмине Далее";
            this.WelcomeWizardPage.Size = new System.Drawing.Size(453, 398);
            this.WelcomeWizardPage.Text = "Добро пожаловать в \"Мастер инвентаризации\"";
            // 
            // WizardSelectActionPage
            // 
            this.WizardSelectActionPage.Controls.Add(this.ActionPanel);
            this.WizardSelectActionPage.DescriptionText = "Пожалуйста, выберите действие, которое вы хотите выполнить";
            this.WizardSelectActionPage.Name = "WizardSelectActionPage";
            this.WizardSelectActionPage.Size = new System.Drawing.Size(638, 409);
            this.WizardSelectActionPage.Text = "Выбор операции";
            // 
            // ActionPanel
            // 
            this.ActionPanel.Controls.Add(this.ActionDescriptionEdit);
            this.ActionPanel.Controls.Add(this.ActionsGroup);
            this.ActionPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ActionPanel.Location = new System.Drawing.Point(0, 0);
            this.ActionPanel.Name = "ActionPanel";
            this.ActionPanel.Size = new System.Drawing.Size(638, 409);
            this.ActionPanel.TabIndex = 0;
            // 
            // ActionDescriptionEdit
            // 
            this.ActionDescriptionEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ActionDescriptionEdit.EditValue = global::StockInventoryMaster.Properties.Resources.ActionImportDescription;
            this.ActionDescriptionEdit.Location = new System.Drawing.Point(5, 70);
            this.ActionDescriptionEdit.Name = "ActionDescriptionEdit";
            this.ActionDescriptionEdit.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.ActionDescriptionEdit.Size = new System.Drawing.Size(628, 334);
            this.ActionDescriptionEdit.TabIndex = 1;
            // 
            // ActionsGroup
            // 
            this.ActionsGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.ActionsGroup.EditValue = 0;
            this.ActionsGroup.Location = new System.Drawing.Point(2, 2);
            this.ActionsGroup.Name = "ActionsGroup";
            this.ActionsGroup.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Загрузка данных"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Выгрузка данных")});
            this.ActionsGroup.Size = new System.Drawing.Size(634, 62);
            this.ActionsGroup.TabIndex = 0;
            this.ActionsGroup.SelectedIndexChanged += new System.EventHandler(this.ActionsGroupSelectedIndexChanged);
            // 
            // completionWizardPage1
            // 
            this.completionWizardPage1.AccessibleName = "Finish";
            this.completionWizardPage1.FinishText = "Работа мастера завершена.";
            this.completionWizardPage1.Name = "completionWizardPage1";
            this.completionWizardPage1.ProceedText = "Для выхода из приложения нажмите \"Завершить\"";
            this.completionWizardPage1.Size = new System.Drawing.Size(453, 421);
            this.completionWizardPage1.Text = "Завершение работы";
            // 
            // wizardPage2
            // 
            this.wizardPage2.AccessibleName = "Exchange";
            this.wizardPage2.DescriptionText = "Настройка и выполнение обмена";
            this.wizardPage2.Name = "wizardPage2";
            this.wizardPage2.Size = new System.Drawing.Size(638, 409);
            this.wizardPage2.Text = "Обмен данными";
            // 
            // Wizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(670, 554);
            this.ControlBox = false;
            this.Controls.Add(this.InventoryWizardControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Wizard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Мастер инвентаризации";
            ((System.ComponentModel.ISupportInitialize)(this.InventoryWizardControl)).EndInit();
            this.InventoryWizardControl.ResumeLayout(false);
            this.WizardSelectActionPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ActionPanel)).EndInit();
            this.ActionPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ActionDescriptionEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActionsGroup.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraWizard.WizardControl InventoryWizardControl;
        private DevExpress.XtraWizard.WelcomeWizardPage WelcomeWizardPage;
        private DevExpress.XtraWizard.WizardPage WizardSelectActionPage;
        private DevExpress.XtraWizard.CompletionWizardPage completionWizardPage1;
        private DevExpress.XtraWizard.WizardPage wizardPage2;
        private DevExpress.XtraEditors.PanelControl ActionPanel;
        private DevExpress.XtraEditors.RadioGroup ActionsGroup;
        private DevExpress.XtraEditors.MemoEdit ActionDescriptionEdit;
    }
}

