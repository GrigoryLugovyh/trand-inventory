﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace StockInventoryMaster
{
    public partial class MasterControl : UserControl
    {
        public MasterControl()
        {
            InitializeComponent();
        }

        protected void Invoker(Action action)
        {
            if (!InvokeRequired)
            {
                action.Invoke();
            }
            else
            {
                Invoke(new Action(action.Invoke));
            }
        }

        protected virtual void ParentFormActivated(object sender, EventArgs e)
        {
            if (_wait != null && _thread != null)
            {
                Invoker(() => _wait.BringToFront());
                Application.DoEvents();
            }
        }

        private Thread _thread;
        private Wait _wait;

        public void WaitShow(string description, string caption)
        {
            if (_thread == null)
            {
                _thread = new Thread(() =>
                {
                    _wait = new Wait(description, caption);
                    Invoker(() => _wait.ShowDialog(this));

                    Application.DoEvents();

                }) { IsBackground = true, Name = "WaitThread" };

                _thread.Start();
            }

            while (_wait == null)
            {
                Thread.Sleep(2);
            }

            if (IsHandleCreated)
                Invoker(() =>
                    {
                        if (_wait != null)
                        {
                            _wait.SetDescription(description);
                            _wait.SetCaption(caption);
                            Application.DoEvents();
                        }
                    });
        }

        public void WaitClose()
        {
            if (_wait != null && _thread != null)
            {
                _wait.Invoke(new Action(() =>
                {
                    _wait.Close();
                    _wait = null;
                }));

                _thread = null;

                GC.WaitForPendingFinalizers();
            }
        }
    }
}
