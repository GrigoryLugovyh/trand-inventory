﻿using System;
using System.IO;
using System.Threading;
using OpenNETCF.Desktop.Communication;

namespace StockInventoryMaster.Handlers
{
    public sealed class RapiHandler
    {
        private static readonly object Sync = new object();

        private static volatile RapiHandler _instance;

        public static RapiHandler Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (Sync)
                    {
                        if (_instance == null)
                        {
                            _instance = new RapiHandler();
                        }
                    }
                }

                return _instance;
            }
        }

        private void Sleep()
        {
            Thread.Sleep(500);
        }

        public void RapiInit()
        {
            Sleep();

            Rapi = new RAPI();

            Sleep();
            Rapi.Connect(false);
        }

        public const int CountTryConnection = 3;

        public RAPI Rapi { get; protected set; }

        public Exception LastException { get; protected set; }

        public bool CopyToDevice(string from, string[] to)
        {
            try
            {
                if (to.Length < 1)
                    throw new Exception("Не указано место хранения базы данных на ТСД");

                if (Rapi == null)
                {
                    RapiInit();
                }

                if (!File.Exists(from))
                    throw new Exception(string.Format("Не удается найти указанный файл: \"{0}\"", from));

                if (Rapi != null)
                {
                    if (!Rapi.Connected)
                    {
                        Sleep();
                        Rapi.Connect(false);
                    }

                    var too = string.Empty;

                    foreach (var t in to)
                    {
                        too = t;
                        Sleep();
                        if (Rapi.DeviceFileExists(Path.GetDirectoryName(too)))
                        {
                            break;
                        }
                    }

                    if (string.IsNullOrWhiteSpace(too))
                    {
                        throw new Exception(
                            "Не удалось установить путь для копирования на ТСД. Возможно, на ТСД нет карты памяти или приложение установлено не в директорию по-умолчанию.");
                    }

                    int count = 0;

                    if (Rapi == null || !Rapi.Connected)
                    {
                        if (Rapi != null)
                        {
                            Sleep();
                            Rapi.Disconnect();
                            Rapi.Dispose();
                            Rapi = null;

                            RapiInit();
                        }

                        while (Rapi != null && (!Rapi.Connected || count < 3))
                        {
                            Sleep();
                            Rapi.Connect(false);

                            count++;
                        }

                        if (Rapi == null || !Rapi.Connected)
                        {
                            throw new Exception("Не удалось подключиться к устройству");
                        }
                    }

                    Sleep();
                    if (Rapi.DeviceFileExists(too))
                    {
                        Sleep();
                        Rapi.DeleteDeviceFile(too);
                    }

                    Sleep();
                    Rapi.CopyFileToDevice(from, too, true);
                }
                else
                {
                    throw new Exception("Не удалось инициализировать взаимодействие с ТСД");
                }

                return true;
            }
            catch (Exception ex)
            {
                LastException = ex;
                return false;
            }
        }

        public bool CopyFromDevice(string[] from, string to)
        {
            try
            {
                if (from.Length < 1)
                    throw new Exception("Не указано место хранения базы данных на ТСД");

                if (Rapi == null)
                {
                    RapiInit();
                }

                if (File.Exists(to))
                {
                    File.Delete(to);
                }

                if (Rapi != null)
                {
                    if (!Rapi.Connected)
                    {
                        Rapi.Connect(false);
                    }

                    var foo = string.Empty;

                    foreach (var f in from)
                    {
                        foo = f;
                        if (Rapi.DeviceFileExists(Path.GetDirectoryName(foo)))
                        {
                            break;
                        }
                    }

                    if (string.IsNullOrWhiteSpace(foo))
                    {
                        throw new Exception("Не удалось установить путь для копирования с ТСД. Возможно, на ТСД нет карты памяти или приложение установлено не в директорию по-умолчанию.");
                    }

                    if (Rapi.Connected)
                    {
                        Rapi.CopyFileFromDevice(to, foo, true);
                    }
                    else
                    {
                        throw new Exception("Не удалось подключиться к устройству");
                    }
                }
                else
                {
                    throw new Exception("Не удалось инициализировать взаимодействие с ТСД");
                }

                return true;
            }
            catch (Exception ex)
            {
                LastException = ex;
                return false;
            }
        }
    }
}