namespace StockInventoryMaster.Handlers
{
    public sealed class DialogHandler
    {
        private static readonly object Sync = new object();

        private static volatile DialogHandler _instance;

        public static DialogHandler Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (Sync)
                    {
                        if (_instance == null)
                        {
                            _instance = new DialogHandler();
                        }
                    }
                }

                return _instance;
            }
        }
    }
}