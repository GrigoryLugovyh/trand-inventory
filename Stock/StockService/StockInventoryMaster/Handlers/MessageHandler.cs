using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace StockInventoryMaster.Handlers
{
    public sealed class MessageHandler
    {
        private static readonly object Sync = new object();

        private static volatile MessageHandler _instance;

        public static MessageHandler Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (Sync)
                    {
                        if (_instance == null)
                        {
                            _instance = new MessageHandler();
                        }
                    }
                }

                return _instance;
            }
        }

        public DialogResult ShowExclamation(string message)
        {
            return XtraMessageBox.Show(message, string.Empty, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }

        public DialogResult ShowQuestion(MessageBoxIcon icon, string message, DialogResult result, string btnYes, string bntNo)
        {
            return XtraMessageBox.Show(message, string.Empty, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }
    }
}