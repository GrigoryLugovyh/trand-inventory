﻿using System;
using System.Windows.Forms;
using StockCore;
using StockCore.Enums;
using StockCore.Presenters;
using StockInventoryMaster.Common;
using StockInventoryMaster.Properties;
using StockInventoryMaster.Views;

namespace StockInventoryMaster
{
    public partial class Wizard : Form
    {
        protected ImportControl Import = new ImportControl {Dock = DockStyle.Fill};

        protected ExportControl Export = new ExportControl {Dock = DockStyle.Fill};

        public Wizard()
        {
            InitializeComponent();

            Init();

            Model.Instance.Messages = new MessageDistributor();
            Model.Instance.Dialogs = new DialogDistributor();
            Model.Instance.Wait = new WaitDistributor();

            var presenters = new Presenters(new SplashPresenter(new SplashView()));

            presenters.InitPresenters(
                new MainPresenter(new MainView()),
                new ObjectsPresenter(new ObjectsView()),
                new ExchangePresenter(new ExchangeView()),
                new ExchangeProcessPresenter(new ExchangeProcessView()),
                new ObjectParametersPresenter(new ObjectsParametersView()),
                new FindUsersPresenter(new FindUserView()),
                new FindMolsPresenter(new FindMolsView()),
                new SelectUserPresenter(new SelectUserView()),
                new SelectMolPresenter(new SelectMolView()),
                new InventoryPresenter(new InventoryView()),
                new InformationPresenter(new InformationView()),
                new EnterBarcodePresenter(new EnterBarcodeView()),
                new SeleectItemStatusPresenter(new SelectItemStatusView()),
                new AddItemPresenter(new AddItemView()),
                new SerialNumberInputPresenter(new SerialNumberInpitView()),
                new AddObjectPresenter(new AddObjectView()),
                new FindObjectsPresenter(new FindObjectView()),
                new ObjectItemsPresenter(new ObjectItemsView()),
                new InformationObjectsPresenter(new InformationObjectsView()),
                new PrintingPresenter(new PrintingView()),
                new SearchMobilePrinterPresenter(new SearchMobilePRinterView()));

            Model.Instance.Presenters = presenters;

            Model.Instance.Start();

            Action = (int) ActionEnum.Import;
        }

        private void Init()
        {
            Text = string.Format("Мастер инвентаризации версии {0}", Model.ApplicationVersion);
        }

/*        protected void RAPIConnected()
        {
            Invoke(new Action(() =>
                {
                    Text = string.Format("Мастер инвентаризации ({0})", "ТСД подключен");
                    Application.DoEvents();
                }));
        }

        protected void RAPIDisconnected()
        {
            Invoke(new Action(() =>
                {
                    Text = string.Format("Мастер инвентаризации ({0})", "ТСД отключен");
                    Application.DoEvents();
                }));
        }*/

        private ActionEnum Action { get; set; }

        private void ActionsGroupSelectedIndexChanged(object sender, EventArgs e)
        {
            Action = (ActionEnum) ActionsGroup.SelectedIndex;

            if (ActionsGroup.SelectedIndex == (int) ActionEnum.Import)
            {
                ActionDescriptionEdit.Text = Resources.ActionImportDescription;
            }
            else if (ActionsGroup.SelectedIndex == (int) ActionEnum.Export)
            {
                ActionDescriptionEdit.Text = Resources.ActionExportDescription;
            }
        }

        private void InventoryWizardControlSelectedPageChanging(object sender, DevExpress.XtraWizard.WizardPageChangingEventArgs e)
        {
            if (e.Page.AccessibleName == Resources.WizardInventoryWizardControlSelectedPageChangingExchange)
            {
                e.Page.Controls.Clear();

                if (Action == ActionEnum.Import)
                {
                    e.Page.Controls.Add(Import);
                }
                else if (Action == ActionEnum.Export)
                {
                    e.Page.Controls.Add(Export);
                }
            }
        }

        private void InventoryWizardControl_CancelClick(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (
                Model.Instance.Messages.ShowQuestion(
                    "Вы действительно хотите завершить работу мастера и выйти из приложения?", DialogEnum.No,
                    string.Empty, string.Empty) == DialogEnum.Yes)
            {
                Exit();
            }
        }

        private void InventoryWizardControl_FinishClick(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Exit();
        }

        protected void Exit()
        {
            Model.Instance.Stop();
            Close();
        }
    }
}