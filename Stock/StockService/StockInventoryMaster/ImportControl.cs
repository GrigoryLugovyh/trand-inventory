﻿using System;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using StockCore;
using StockCore.Enums;
using StockCore.Factories;
using StockCore.Handlers.Exchange.Configuration.CSV;
using StockCore.Handlers.Exchange.Imports;
using StockInventoryMaster.Handlers;
using StockInventoryMaster.Properties;

namespace StockInventoryMaster
{
    public partial class ImportControl : MasterControl
    {
        protected ExchangeControl Exchange { get; set; }

        public ImportControl()
        {
            InitializeComponent();

            Exchange = new ExchangeControl {Dock = DockStyle.Fill};

            ExchangePanelControl.Controls.Add(Exchange);
        }

        private void ImportBtnClick(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("Загрузка данных", () =>
            {
                if (Model.Instance.Messages.ShowQuestion("Вы действительно хотите загрузить данные?", DialogEnum.No, "Да", "Нет") == DialogEnum.Yes)
                {
                    var from = Path.Combine(Model.Instance.Common.CurrentDirectory, Constants.DatabaseName);

                    if (File.Exists(from))
                    {
                        File.Delete(from);
                    }

                    File.WriteAllBytes(from, Resources.stock);

                    CsvExchangeSettings settings = CsvExchangeSettings.Instance;

                    var filename = Model.Instance.Dialogs.OpenFileDialog(settings.ImportDirectory);

                    if (string.IsNullOrEmpty(filename))
                    {
                        return;
                    }

                    var import = ImportFactory.Instance.GetInstance(
                        ImportEnum.Cvs,
                        new CsvImportProtocol(false, false, filename, true, 5000, false),
                        settings);

                    
                    Model.Instance.Presenters.ExchangeProcess.Exchanger = import;
                    Model.Instance.Presenters.ExchangeProcess.Exchanger.MoveNextRecord += ExchangerMoveNextRecord;
                    Model.Instance.Presenters.ExchangeProcess.Exchanger.ExchangeFinishedSuccessfully += ExchangerExchangeFinishedSuccessfully;
                    Model.Instance.Presenters.ExchangeProcess.Exchanger.ExchangeFinishedException += ExchangerExchangeFinishedException;

                    Model.Instance.Presenters.ExchangeProcess.ShowView();
                }

            }, false, true);
        }

        protected void FinishExchange()
        {
            Exchange.ProcessText = "Загрузка данных завершена";
            if (Model.Instance.Presenters.ExchangeProcess.Exchanger != null)
            {
                Model.Instance.Presenters.ExchangeProcess.Exchanger.MoveNextRecord -= ExchangerMoveNextRecord;
                Model.Instance.Presenters.ExchangeProcess.Exchanger.ExchangeFinishedException -= ExchangerExchangeFinishedException;
                Model.Instance.Presenters.ExchangeProcess.Exchanger.ExchangeFinishedSuccessfully -= ExchangerExchangeFinishedSuccessfully;
            }
        }

        protected void ExchangerExchangeFinishedException(object sender, StockCore.Events.StockEventArgs e)
        {
            FinishExchange();
        }

        protected void ExchangerExchangeFinishedSuccessfully(object sender, EventArgs e)
        {
            FinishExchange();
        }

        protected void ExchangerMoveNextRecord(object sender, EventArgs e)
        {
            Exchange.ProcessText = Model.Instance.Presenters.ExchangeProcess.Exchanger.ExchangeProcessStatus;
            Exchange.ExchangeTime = string.Format("{0}:{1}:{2}", Model.Instance.Presenters.ExchangeProcess.Exchanger.Watcher.Elapsed.Hours, Model.Instance.Presenters.ExchangeProcess.Exchanger.Watcher.Elapsed.Minutes, Model.Instance.Presenters.ExchangeProcess.Exchanger.Watcher.Elapsed.Seconds);
            Exchange.Progress = Model.Instance.Presenters.ExchangeProcess.Exchanger.Persent;
            Exchange.Received = Model.Instance.Presenters.ExchangeProcess.Exchanger.Received.ToString(CultureInfo.InvariantCulture);
            Exchange.Prepared = Model.Instance.Presenters.ExchangeProcess.Exchanger.Prepared.ToString(CultureInfo.InvariantCulture);
            Exchange.Processed = Model.Instance.Presenters.ExchangeProcess.Exchanger.Processed.ToString(CultureInfo.InvariantCulture);
            Exchange.NotProcessed = Model.Instance.Presenters.ExchangeProcess.Exchanger.NotProcessed.ToString(CultureInfo.InvariantCulture);
        }

        private void LoadTerminalBtn_Click(object sender, EventArgs e)
        {
            CopyDatabaseToTerminal();
        }

        protected void CopyDatabaseToTerminal()
        {
            Model.Instance.Invoker.DoIndependentThread("Копирование базы данных на мобильное устройство", () =>
                {
                    var from = Path.Combine(Model.Instance.Common.CurrentDirectory, Constants.DatabaseName);

                    if (!File.Exists(from))
                    {
                        File.WriteAllBytes(from, Resources.stock);
                    }

                    var repit = true;

                    while (repit)
                    {
                        WaitShow("Копирование на ТСД ...", "Пожалуйста, подождите");

                        var copy =
                            RapiHandler.Instance.CopyToDevice(from,
                                new []
                                    {
                                        Path.Combine(MobileDirectoryEdit.Text, Constants.DatabaseName),
                                        Path.Combine(@"\..\Storage Card", Constants.DatabaseName),
                                        Path.Combine(@"\..\Program Files\stock", Constants.DatabaseName)
                                    });

                        WaitClose();

                        if (!copy)
                        {
                            if (RapiHandler.Instance.LastException != null)
                            {
                                MessageHandler.Instance.ShowExclamation(RapiHandler.Instance.LastException.Message);
                            }

                            repit = false;
                        }
                        else
                        {
                            repit = Model.Instance.Messages.ShowQuestion("Данные были успешно скопированы на мобильное устройство. Если вы хотите повторить процесс копирования, то подключите следующий ТСД и нажмине \"ДА\"", DialogEnum.No, string.Empty, string.Empty) == DialogEnum.Yes;
                        }
                    }

                }, 
                () =>
                    {
                        WaitClose();

                    }, true);
        }
    }
}