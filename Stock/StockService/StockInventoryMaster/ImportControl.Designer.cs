﻿namespace StockInventoryMaster
{
    partial class ImportControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ImportPanel = new DevExpress.XtraEditors.PanelControl();
            this.ImportLayoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.LoadTerminalBtn = new DevExpress.XtraEditors.SimpleButton();
            this.ImportBtn = new DevExpress.XtraEditors.SimpleButton();
            this.ExchangePanelControl = new DevExpress.XtraEditors.PanelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.ImportLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.MobileDirectoryEdit = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.ImportPanel)).BeginInit();
            this.ImportPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ImportLayoutControl)).BeginInit();
            this.ImportLayoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ExchangePanelControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImportLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MobileDirectoryEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // ImportPanel
            // 
            this.ImportPanel.Controls.Add(this.ImportLayoutControl);
            this.ImportPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ImportPanel.Location = new System.Drawing.Point(0, 0);
            this.ImportPanel.Name = "ImportPanel";
            this.ImportPanel.Size = new System.Drawing.Size(529, 405);
            this.ImportPanel.TabIndex = 0;
            // 
            // ImportLayoutControl
            // 
            this.ImportLayoutControl.Controls.Add(this.MobileDirectoryEdit);
            this.ImportLayoutControl.Controls.Add(this.LoadTerminalBtn);
            this.ImportLayoutControl.Controls.Add(this.ImportBtn);
            this.ImportLayoutControl.Controls.Add(this.ExchangePanelControl);
            this.ImportLayoutControl.Controls.Add(this.labelControl1);
            this.ImportLayoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ImportLayoutControl.Location = new System.Drawing.Point(2, 2);
            this.ImportLayoutControl.Name = "ImportLayoutControl";
            this.ImportLayoutControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(605, 198, 250, 350);
            this.ImportLayoutControl.Root = this.ImportLayoutControlGroup;
            this.ImportLayoutControl.Size = new System.Drawing.Size(525, 401);
            this.ImportLayoutControl.TabIndex = 0;
            this.ImportLayoutControl.Text = "layoutControl1";
            // 
            // LoadTerminalBtn
            // 
            this.LoadTerminalBtn.Location = new System.Drawing.Point(383, 367);
            this.LoadTerminalBtn.MaximumSize = new System.Drawing.Size(130, 40);
            this.LoadTerminalBtn.Name = "LoadTerminalBtn";
            this.LoadTerminalBtn.Size = new System.Drawing.Size(130, 22);
            this.LoadTerminalBtn.StyleController = this.ImportLayoutControl;
            this.LoadTerminalBtn.TabIndex = 8;
            this.LoadTerminalBtn.Text = "Загрузить базу на ТСД";
            this.LoadTerminalBtn.ToolTip = "Позволяет скопировать сформированную базу на терминал сбора данных";
            this.LoadTerminalBtn.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LoadTerminalBtn.ToolTipTitle = "Загрузка базы на ТСД";
            this.LoadTerminalBtn.Click += new System.EventHandler(this.LoadTerminalBtn_Click);
            // 
            // ImportBtn
            // 
            this.ImportBtn.Location = new System.Drawing.Point(225, 367);
            this.ImportBtn.MaximumSize = new System.Drawing.Size(150, 40);
            this.ImportBtn.Name = "ImportBtn";
            this.ImportBtn.Size = new System.Drawing.Size(150, 22);
            this.ImportBtn.StyleController = this.ImportLayoutControl;
            this.ImportBtn.TabIndex = 7;
            this.ImportBtn.Text = "Сформировать базу данных";
            this.ImportBtn.ToolTip = "Позволяет загрузить данные из текстового файла";
            this.ImportBtn.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ImportBtn.ToolTipTitle = "Формирование базы данных";
            this.ImportBtn.Click += new System.EventHandler(this.ImportBtnClick);
            // 
            // ExchangePanelControl
            // 
            this.ExchangePanelControl.Location = new System.Drawing.Point(12, 75);
            this.ExchangePanelControl.Name = "ExchangePanelControl";
            this.ExchangePanelControl.Size = new System.Drawing.Size(501, 288);
            this.ExchangePanelControl.TabIndex = 6;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl1.Location = new System.Drawing.Point(12, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(144, 19);
            this.labelControl1.StyleController = this.ImportLayoutControl;
            this.labelControl1.TabIndex = 4;
            this.labelControl1.Text = "Загрузка данных";
            // 
            // ImportLayoutControlGroup
            // 
            this.ImportLayoutControlGroup.CustomizationFormText = "Root";
            this.ImportLayoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.ImportLayoutControlGroup.GroupBordersVisible = false;
            this.ImportLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.emptySpaceItem1,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem2});
            this.ImportLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.ImportLayoutControlGroup.Name = "ImportLayoutControlGroup";
            this.ImportLayoutControlGroup.Size = new System.Drawing.Size(525, 401);
            this.ImportLayoutControlGroup.Text = "ImportLayoutControlGroup";
            this.ImportLayoutControlGroup.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.labelControl1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(505, 23);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.ExchangePanelControl;
            this.layoutControlItem3.CustomizationFormText = "Информация о загрузке";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 47);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(505, 308);
            this.layoutControlItem3.Text = "Информация о загрузке";
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(174, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 355);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(213, 26);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.ImportBtn;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(213, 355);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(158, 26);
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.LoadTerminalBtn;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(371, 355);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(134, 26);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // MobileDirectoryEdit
            // 
            this.MobileDirectoryEdit.EditValue = "\\..\\Storage Card";
            this.MobileDirectoryEdit.Location = new System.Drawing.Point(189, 35);
            this.MobileDirectoryEdit.Name = "MobileDirectoryEdit";
            this.MobileDirectoryEdit.Size = new System.Drawing.Size(324, 20);
            this.MobileDirectoryEdit.StyleController = this.ImportLayoutControl;
            this.MobileDirectoryEdit.TabIndex = 9;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.MobileDirectoryEdit;
            this.layoutControlItem2.CustomizationFormText = "Путь к базе данных на терминале";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 23);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(505, 24);
            this.layoutControlItem2.Text = "Путь к базе данных на терминале";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(174, 13);
            // 
            // ImportControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ImportPanel);
            this.Name = "ImportControl";
            this.Size = new System.Drawing.Size(529, 405);
            ((System.ComponentModel.ISupportInitialize)(this.ImportPanel)).EndInit();
            this.ImportPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ImportLayoutControl)).EndInit();
            this.ImportLayoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ExchangePanelControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImportLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MobileDirectoryEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl ImportPanel;
        private DevExpress.XtraLayout.LayoutControl ImportLayoutControl;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlGroup ImportLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.PanelControl ExchangePanelControl;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.SimpleButton ImportBtn;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.SimpleButton LoadTerminalBtn;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.TextEdit MobileDirectoryEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
    }
}
