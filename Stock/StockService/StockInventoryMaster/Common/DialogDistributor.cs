using System.Windows.Forms;
using StockCore.Interfaces.Common;

namespace StockInventoryMaster.Common
{
    public class DialogDistributor : IDialoger
    {
        public string OpenFileDialog(string directory)
        {
            string filename = string.Empty;
            using (var dlg = new OpenFileDialog { InitialDirectory = directory })
            {
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    filename = dlg.FileName;
                }
            }

            return filename;
        }
    }
}