using StockCore.Interfaces.Common;
using StockInventoryMaster.Handlers;

namespace StockInventoryMaster.Common
{
    public class WaitDistributor : IWait
    {
        public void ShowWait()
        {
            WaitHandler.Instance.Show();
        }

        public void CloseWait()
        {
            WaitHandler.Instance.Close();
        }
    }
}