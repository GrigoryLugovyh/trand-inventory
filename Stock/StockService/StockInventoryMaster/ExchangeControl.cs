﻿
namespace StockInventoryMaster
{
    public partial class ExchangeControl : MasterControl
    {
        public ExchangeControl()
        {
            InitializeComponent();
        }

        public string Received
        {
            get { return RecivedEdit.Text; }
            set { Invoker(() => RecivedEdit.Text = value); }
        }

        public string Prepared
        {
            get { return PreparedEdit.Text; }
            set { Invoker(() => PreparedEdit.Text = value); }
        }

        public string Processed
        {
            get { return ProcessedEdit.Text; }
            set { Invoker(() => ProcessedEdit.Text = value); }
        }

        public string NotProcessed
        {
            get { return NotProcessedEdit.Text; }
            set { Invoker(() => NotProcessedEdit.Text = value); }
        }

        public string ExchangeTime
        {
            get { return ProcessTimeEdit.Text; }
            set { Invoker(() => ProcessTimeEdit.Text = value); }
        }

        public string ProcessText
        {
            get { return ProcessStatusEdit.Text; }
            set { Invoker(() => ProcessStatusEdit.Text = value); }
        }

        public int Progress
        {
            get { return ProgressBar.Position; }
            set { Invoker(() => ProgressBar.Position = value); }
        }
    }
}
