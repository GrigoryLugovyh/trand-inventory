﻿using DevExpress.XtraWaitForm;
using System;

namespace StockInventoryMaster
{
    public partial class Wait : WaitForm
    {
        public Wait(string caption, string description)
        {
            InitializeComponent();
           
            Progress.AutoHeight = true;

            if (!string.IsNullOrEmpty(caption))
                Progress.Caption = caption;

            if (!string.IsNullOrEmpty(description))
                Progress.Description = description;

            ViewHandle = this;

            ShowOnTopMode = ShowFormOnTopMode.AboveParent;
            
        }

        public override void SetCaption(string caption)
        {
            base.SetCaption(caption);
            Progress.Caption = caption;
        }

        public override void SetDescription(string description)
        {
            base.SetDescription(description);
            Progress.Description = description;
        }

        public int ViewHeight
        {
            get { return Height; }
            set { Height = value; }
        }

        public int ViewWidth
        {
            get { return Width; }
            set { Width = value; }
        }

        public bool IsSlave { get; set; }

        public object ViewHandle { get; protected set; }

        public void WaitShow()
        {
            ShowDialog();
        }

        public void WaitShow(string description, string caption)
        {

        }

        public void WaitClose()
        {

        }

        public void UpdateView()
        {

        }

        public void EstablishCaption(string caption)
        {

        }

        public void EstablishDescription(string description)
        {

        }

        public void ProcessSupplyCaption(string caption)
        {
            Invoke(new Action(delegate { Progress.Caption = caption; }));
        }

        public void ProcessSupplyDescription(string description)
        {
            Invoke(new Action(delegate { Progress.Description = description; }));
        }

        public object Clone()
        {
            return this;
        }

        private void WaitViewShown(object sender, EventArgs e)
        {
            BringToFront();
        }
    }
}