﻿namespace StockInventoryMaster
{
    partial class ExportControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ImportLayoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.ExportBtn = new DevExpress.XtraEditors.SimpleButton();
            this.CopyDatabaseBtn = new DevExpress.XtraEditors.SimpleButton();
            this.ExchangePanelControl = new DevExpress.XtraEditors.PanelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.TerminalDirectoryEdit = new DevExpress.XtraEditors.TextEdit();
            this.OnlyNewСheck = new DevExpress.XtraEditors.CheckEdit();
            this.OnlyFoundСheck = new DevExpress.XtraEditors.CheckEdit();
            this.DirectoryButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.ExportlayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ImportPanel = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.ImportLayoutControl)).BeginInit();
            this.ImportLayoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ExchangePanelControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TerminalDirectoryEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OnlyNewСheck.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OnlyFoundСheck.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DirectoryButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExportlayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImportPanel)).BeginInit();
            this.ImportPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ImportLayoutControl
            // 
            this.ImportLayoutControl.Controls.Add(this.ExportBtn);
            this.ImportLayoutControl.Controls.Add(this.CopyDatabaseBtn);
            this.ImportLayoutControl.Controls.Add(this.ExchangePanelControl);
            this.ImportLayoutControl.Controls.Add(this.panelControl1);
            this.ImportLayoutControl.Controls.Add(this.labelControl1);
            this.ImportLayoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ImportLayoutControl.Location = new System.Drawing.Point(2, 2);
            this.ImportLayoutControl.Name = "ImportLayoutControl";
            this.ImportLayoutControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(605, 198, 250, 350);
            this.ImportLayoutControl.Root = this.ExportlayoutControlGroup;
            this.ImportLayoutControl.Size = new System.Drawing.Size(589, 418);
            this.ImportLayoutControl.TabIndex = 0;
            this.ImportLayoutControl.Text = "layoutControl1";
            // 
            // ExportBtn
            // 
            this.ExportBtn.Location = new System.Drawing.Point(470, 384);
            this.ExportBtn.MaximumSize = new System.Drawing.Size(107, 22);
            this.ExportBtn.Name = "ExportBtn";
            this.ExportBtn.Size = new System.Drawing.Size(107, 22);
            this.ExportBtn.StyleController = this.ImportLayoutControl;
            this.ExportBtn.TabIndex = 9;
            this.ExportBtn.Text = "Выгрузить данные";
            this.ExportBtn.ToolTip = "Позволяет выгрузить результаты инвентаризации в текстовый файл";
            this.ExportBtn.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ExportBtn.ToolTipTitle = "Выгрузка данных";
            this.ExportBtn.Click += new System.EventHandler(this.ExportBtn_Click);
            // 
            // CopyDatabaseBtn
            // 
            this.CopyDatabaseBtn.Location = new System.Drawing.Point(222, 384);
            this.CopyDatabaseBtn.MaximumSize = new System.Drawing.Size(244, 22);
            this.CopyDatabaseBtn.Name = "CopyDatabaseBtn";
            this.CopyDatabaseBtn.Size = new System.Drawing.Size(244, 22);
            this.CopyDatabaseBtn.StyleController = this.ImportLayoutControl;
            this.CopyDatabaseBtn.TabIndex = 8;
            this.CopyDatabaseBtn.Text = "Скопировать базу с ТСД и выгрузить данные";
            this.CopyDatabaseBtn.ToolTip = "Позволяет скопировать базу с терминала сбора данных и выгрузить результаты инвент" +
    "аризации в текстовый файл";
            this.CopyDatabaseBtn.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.CopyDatabaseBtn.ToolTipTitle = "Выгрузка данных";
            this.CopyDatabaseBtn.Click += new System.EventHandler(this.CopyDatabaseBtn_Click);
            // 
            // ExchangePanelControl
            // 
            this.ExchangePanelControl.Location = new System.Drawing.Point(12, 204);
            this.ExchangePanelControl.Name = "ExchangePanelControl";
            this.ExchangePanelControl.Size = new System.Drawing.Size(565, 176);
            this.ExchangePanelControl.TabIndex = 6;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.layoutControl1);
            this.panelControl1.Location = new System.Drawing.Point(12, 51);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(565, 133);
            this.panelControl1.TabIndex = 5;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.TerminalDirectoryEdit);
            this.layoutControl1.Controls.Add(this.OnlyNewСheck);
            this.layoutControl1.Controls.Add(this.OnlyFoundСheck);
            this.layoutControl1.Controls.Add(this.DirectoryButtonEdit);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup2;
            this.layoutControl1.Size = new System.Drawing.Size(561, 129);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // TerminalDirectoryEdit
            // 
            this.TerminalDirectoryEdit.EditValue = "\\..\\Storage Card";
            this.TerminalDirectoryEdit.Location = new System.Drawing.Point(189, 36);
            this.TerminalDirectoryEdit.Name = "TerminalDirectoryEdit";
            this.TerminalDirectoryEdit.Size = new System.Drawing.Size(360, 20);
            this.TerminalDirectoryEdit.StyleController = this.layoutControl1;
            this.TerminalDirectoryEdit.TabIndex = 7;
            // 
            // OnlyNewСheck
            // 
            this.OnlyNewСheck.Location = new System.Drawing.Point(12, 83);
            this.OnlyNewСheck.Name = "OnlyNewСheck";
            this.OnlyNewСheck.Properties.Caption = "Только новые ТМЦ";
            this.OnlyNewСheck.Size = new System.Drawing.Size(537, 19);
            this.OnlyNewСheck.StyleController = this.layoutControl1;
            this.OnlyNewСheck.TabIndex = 6;
            this.OnlyNewСheck.CheckedChanged += new System.EventHandler(this.OnlyNewСheck_CheckedChanged);
            // 
            // OnlyFoundСheck
            // 
            this.OnlyFoundСheck.Location = new System.Drawing.Point(12, 60);
            this.OnlyFoundСheck.Name = "OnlyFoundСheck";
            this.OnlyFoundСheck.Properties.Caption = "Только найденные ТМЦ";
            this.OnlyFoundСheck.Size = new System.Drawing.Size(537, 19);
            this.OnlyFoundСheck.StyleController = this.layoutControl1;
            this.OnlyFoundСheck.TabIndex = 5;
            this.OnlyFoundСheck.CheckedChanged += new System.EventHandler(this.OnlyFoundСheck_CheckedChanged);
            // 
            // DirectoryButtonEdit
            // 
            this.DirectoryButtonEdit.Location = new System.Drawing.Point(189, 12);
            this.DirectoryButtonEdit.Name = "DirectoryButtonEdit";
            this.DirectoryButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DirectoryButtonEdit.Properties.ReadOnly = true;
            this.DirectoryButtonEdit.Size = new System.Drawing.Size(360, 20);
            this.DirectoryButtonEdit.StyleController = this.layoutControl1;
            this.DirectoryButtonEdit.TabIndex = 4;
            this.DirectoryButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.DirectoryButtonEdit_ButtonClick);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem7,
            this.layoutControlItem9});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(561, 129);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.DirectoryButtonEdit;
            this.layoutControlItem4.CustomizationFormText = "Директория файла базы данных";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(541, 24);
            this.layoutControlItem4.Text = "Директория файлов выгрузки";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(174, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.OnlyFoundСheck;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(541, 23);
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.OnlyNewСheck;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 71);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(541, 38);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.TerminalDirectoryEdit;
            this.layoutControlItem9.CustomizationFormText = "Путь к базе данных на терминале";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(541, 24);
            this.layoutControlItem9.Text = "Путь к базе данных на терминале";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(174, 13);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl1.Location = new System.Drawing.Point(12, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(148, 19);
            this.labelControl1.StyleController = this.ImportLayoutControl;
            this.labelControl1.TabIndex = 4;
            this.labelControl1.Text = "Выгрузка данных";
            // 
            // ExportlayoutControlGroup
            // 
            this.ExportlayoutControlGroup.CustomizationFormText = "Root";
            this.ExportlayoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.ExportlayoutControlGroup.GroupBordersVisible = false;
            this.ExportlayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.emptySpaceItem1,
            this.layoutControlItem8,
            this.layoutControlItem6});
            this.ExportlayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.ExportlayoutControlGroup.Name = "ExportlayoutControlGroup";
            this.ExportlayoutControlGroup.Size = new System.Drawing.Size(589, 418);
            this.ExportlayoutControlGroup.Text = "ExportlayoutControlGroup";
            this.ExportlayoutControlGroup.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.labelControl1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(569, 23);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.panelControl1;
            this.layoutControlItem2.CustomizationFormText = "Настройка загрузки";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 23);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(569, 153);
            this.layoutControlItem2.Text = "Настройка загрузки";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(123, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.ExchangePanelControl;
            this.layoutControlItem3.CustomizationFormText = "Информация о загрузке";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 176);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(569, 196);
            this.layoutControlItem3.Text = "Информация о выгрузке";
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(123, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 372);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(210, 26);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.CopyDatabaseBtn;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(210, 372);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(248, 26);
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.ExportBtn;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(458, 372);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(111, 26);
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // ImportPanel
            // 
            this.ImportPanel.Controls.Add(this.ImportLayoutControl);
            this.ImportPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ImportPanel.Location = new System.Drawing.Point(0, 0);
            this.ImportPanel.Name = "ImportPanel";
            this.ImportPanel.Size = new System.Drawing.Size(593, 422);
            this.ImportPanel.TabIndex = 1;
            // 
            // ExportControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ImportPanel);
            this.Name = "ExportControl";
            this.Size = new System.Drawing.Size(593, 422);
            this.Load += new System.EventHandler(this.ExportControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ImportLayoutControl)).EndInit();
            this.ImportLayoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ExchangePanelControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TerminalDirectoryEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OnlyNewСheck.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OnlyFoundСheck.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DirectoryButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExportlayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImportPanel)).EndInit();
            this.ImportPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl ImportLayoutControl;
        private DevExpress.XtraEditors.PanelControl ExchangePanelControl;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.CheckEdit OnlyFoundСheck;
        private DevExpress.XtraEditors.ButtonEdit DirectoryButtonEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlGroup ExportlayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.PanelControl ImportPanel;
        private DevExpress.XtraEditors.CheckEdit OnlyNewСheck;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.SimpleButton CopyDatabaseBtn;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.SimpleButton ExportBtn;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.TextEdit TerminalDirectoryEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;

    }
}
