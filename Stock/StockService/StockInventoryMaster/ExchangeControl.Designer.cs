﻿namespace StockInventoryMaster
{
    partial class ExchangeControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.ProgressBar = new DevExpress.XtraEditors.ProgressBarControl();
            this.ProcessStatusEdit = new DevExpress.XtraEditors.LabelControl();
            this.ProcessTimeEdit = new DevExpress.XtraEditors.LabelControl();
            this.NotProcessedEdit = new DevExpress.XtraEditors.LabelControl();
            this.ProcessedEdit = new DevExpress.XtraEditors.LabelControl();
            this.PreparedEdit = new DevExpress.XtraEditors.LabelControl();
            this.RecivedEdit = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ProgressBar.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.ProgressBar);
            this.layoutControl2.Controls.Add(this.ProcessStatusEdit);
            this.layoutControl2.Controls.Add(this.ProcessTimeEdit);
            this.layoutControl2.Controls.Add(this.NotProcessedEdit);
            this.layoutControl2.Controls.Add(this.ProcessedEdit);
            this.layoutControl2.Controls.Add(this.PreparedEdit);
            this.layoutControl2.Controls.Add(this.RecivedEdit);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup3;
            this.layoutControl2.Size = new System.Drawing.Size(293, 163);
            this.layoutControl2.TabIndex = 1;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // ProgressBar
            // 
            this.ProgressBar.Location = new System.Drawing.Point(12, 129);
            this.ProgressBar.Name = "ProgressBar";
            this.ProgressBar.Size = new System.Drawing.Size(269, 18);
            this.ProgressBar.StyleController = this.layoutControl2;
            this.ProgressBar.TabIndex = 10;
            // 
            // ProcessStatusEdit
            // 
            this.ProcessStatusEdit.Location = new System.Drawing.Point(120, 112);
            this.ProcessStatusEdit.Name = "ProcessStatusEdit";
            this.ProcessStatusEdit.Size = new System.Drawing.Size(53, 13);
            this.ProcessStatusEdit.StyleController = this.layoutControl2;
            this.ProcessStatusEdit.TabIndex = 9;
            this.ProcessStatusEdit.Text = "Ожидание";
            // 
            // ProcessTimeEdit
            // 
            this.ProcessTimeEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ProcessTimeEdit.Location = new System.Drawing.Point(98, 92);
            this.ProcessTimeEdit.Name = "ProcessTimeEdit";
            this.ProcessTimeEdit.Size = new System.Drawing.Size(7, 16);
            this.ProcessTimeEdit.StyleController = this.layoutControl2;
            this.ProcessTimeEdit.TabIndex = 8;
            this.ProcessTimeEdit.Text = "0";
            // 
            // NotProcessedEdit
            // 
            this.NotProcessedEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NotProcessedEdit.Location = new System.Drawing.Point(98, 72);
            this.NotProcessedEdit.Name = "NotProcessedEdit";
            this.NotProcessedEdit.Size = new System.Drawing.Size(7, 16);
            this.NotProcessedEdit.StyleController = this.layoutControl2;
            this.NotProcessedEdit.TabIndex = 7;
            this.NotProcessedEdit.Text = "0";
            // 
            // ProcessedEdit
            // 
            this.ProcessedEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ProcessedEdit.Location = new System.Drawing.Point(98, 52);
            this.ProcessedEdit.Name = "ProcessedEdit";
            this.ProcessedEdit.Size = new System.Drawing.Size(7, 16);
            this.ProcessedEdit.StyleController = this.layoutControl2;
            this.ProcessedEdit.TabIndex = 6;
            this.ProcessedEdit.Text = "0";
            // 
            // PreparedEdit
            // 
            this.PreparedEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PreparedEdit.Location = new System.Drawing.Point(98, 32);
            this.PreparedEdit.Name = "PreparedEdit";
            this.PreparedEdit.Size = new System.Drawing.Size(7, 16);
            this.PreparedEdit.StyleController = this.layoutControl2;
            this.PreparedEdit.TabIndex = 5;
            this.PreparedEdit.Text = "0";
            // 
            // RecivedEdit
            // 
            this.RecivedEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RecivedEdit.Location = new System.Drawing.Point(98, 12);
            this.RecivedEdit.Name = "RecivedEdit";
            this.RecivedEdit.Size = new System.Drawing.Size(7, 16);
            this.RecivedEdit.StyleController = this.layoutControl2;
            this.RecivedEdit.TabIndex = 4;
            this.RecivedEdit.Text = "0";
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem13});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(293, 163);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.RecivedEdit;
            this.layoutControlItem7.ControlAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.layoutControlItem7.CustomizationFormText = "Получено:";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(273, 20);
            this.layoutControlItem7.Text = "Получено:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.PreparedEdit;
            this.layoutControlItem8.ControlAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.layoutControlItem8.CustomizationFormText = "Подготовлено:";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 20);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(273, 20);
            this.layoutControlItem8.Text = "Подготовлено:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.ProcessedEdit;
            this.layoutControlItem9.ControlAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.layoutControlItem9.CustomizationFormText = "Обработано:";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 40);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(273, 20);
            this.layoutControlItem9.Text = "Обработано:";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.NotProcessedEdit;
            this.layoutControlItem10.ControlAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.layoutControlItem10.CustomizationFormText = "Не обработано:";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 60);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(273, 20);
            this.layoutControlItem10.Text = "Не обработано:";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.ProcessTimeEdit;
            this.layoutControlItem11.ControlAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.layoutControlItem11.CustomizationFormText = "Время процесса:";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 80);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(273, 20);
            this.layoutControlItem11.Text = "Время процесса:";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.ProcessStatusEdit;
            this.layoutControlItem12.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.layoutControlItem12.CustomizationFormText = "layoutControlItem12";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 100);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(273, 17);
            this.layoutControlItem12.Text = "layoutControlItem12";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextToControlDistance = 0;
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.ProgressBar;
            this.layoutControlItem13.CustomizationFormText = "layoutControlItem13";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 117);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(273, 26);
            this.layoutControlItem13.Text = "layoutControlItem13";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextToControlDistance = 0;
            this.layoutControlItem13.TextVisible = false;
            // 
            // ExchangeControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl2);
            this.Name = "ExchangeControl";
            this.Size = new System.Drawing.Size(293, 163);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ProgressBar.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.ProgressBarControl ProgressBar;
        private DevExpress.XtraEditors.LabelControl ProcessStatusEdit;
        private DevExpress.XtraEditors.LabelControl ProcessTimeEdit;
        private DevExpress.XtraEditors.LabelControl NotProcessedEdit;
        private DevExpress.XtraEditors.LabelControl ProcessedEdit;
        private DevExpress.XtraEditors.LabelControl PreparedEdit;
        private DevExpress.XtraEditors.LabelControl RecivedEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
    }
}
