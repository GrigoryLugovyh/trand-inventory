﻿using System;
using System.Collections.Generic;
using StockCore.Entity;
using StockCore.Interfaces.Views;

namespace StockInventoryMaster.Views
{
    public class FindUserView : IFindPersonalView<Users>
    {
        public void ShowView()
        {
            
        }

        public void CloseView()
        {
            
        }

        public void BackView()
        {
            
        }

        public string StatusText { get; set; }
        public event EventHandler Show;
        public event EventHandler Close;
        public List<Users> Personals { get; set; }
        public Users SelectPersonal { get; private set; }
        public event EventHandler Select;
        public event EventHandler SelectedChange;
    }
}