﻿using System;
using System.Collections.Generic;
using StockCore.Entity;
using StockCore.Interfaces.Views;

namespace StockInventoryMaster.Views
{
    public class AddItemView : IAddItemView
    {
        public void ShowView()
        {
            
        }

        public void CloseView()
        {
           
        }

        public void BackView()
        {
            
        }

        public string StatusText { get; set; }
        public event EventHandler Show;
        public event EventHandler Close;
        public string ItemBarcode { get; set; }
        public string ItemName { get; set; }
        public List<Statuses> Statuses { get; set; }
        public Statuses SelectedStatus { get; private set; }
        public DateTime ItemBeginDate { get; set; }
        public bool BeginDateEnable { get; set; }
        public event EventHandler Add;
    }
}