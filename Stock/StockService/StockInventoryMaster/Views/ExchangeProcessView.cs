﻿using System;
using StockCore.Interfaces.Views;

namespace StockInventoryMaster.Views
{
    public class ExchangeProcessView : IExchangeProcessView
    {
        public void ShowView()
        {
            
        }

        public void CloseView()
        {
            
        }

        public void BackView()
        {
            
        }

        public string StatusText { get; set; }
        public event EventHandler Show;
        public event EventHandler Close;
        public string Header { get; set; }
        public string Received { get; set; }
        public string Prepared { get; set; }
        public string Processed { get; set; }
        public string NotProcessed { get; set; }
        public string ExchangeTime { get; set; }
        public string ProcessText { get; set; }
        public int Progress { get; set; }
        public event EventHandler Abort;
    }
}