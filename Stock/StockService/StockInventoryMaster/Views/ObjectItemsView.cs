﻿using System;
using System.Collections.Generic;
using StockCore.Entity;
using StockCore.Interfaces.Views;

namespace StockInventoryMaster.Views
{
    public class ObjectItemsView : IObjectItemsView
    {
        public void ShowView()
        {
            
        }

        public void CloseView()
        {
            
        }

        public void BackView()
        {
            
        }

        public string StatusText { get; set; }
        public event EventHandler Show;
        public event EventHandler Close;
        public void ItemsRefresh()
        {
            
        }

        public string ObjectName { get; set; }
        public bool SelectedCheck { get; set; }
        public List<Items> Items { get; set; }
        public event EventHandler SelectedChecked;
        public event EventHandler Cancel;
        public event EventHandler Print;
    }
}