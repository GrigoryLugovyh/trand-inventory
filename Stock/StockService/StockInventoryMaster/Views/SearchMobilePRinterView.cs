﻿using System;
using StockCore.Interfaces.Views;

namespace StockInventoryMaster.Views
{
    public class SearchMobilePRinterView : ISearchMobilePrinterView
    {
        public void ShowView()
        {

        }

        public void CloseView()
        {

        }

        public void BackView()
        {

        }

        public string StatusText { get; set; }
        public event EventHandler Show;
        public event EventHandler Close;
        public object Printers { get; set; }
        public object SelectedPrinter { get; private set; }
        public bool ViewEnable { set; private get; }
        public event EventHandler Use;
    }
}