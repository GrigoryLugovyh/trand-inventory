﻿using System;
using System.Collections.Generic;
using StockCore.Entity;
using StockCore.Handlers.Exchange.Configuration.CSV;
using StockCore.Interfaces.Views;

namespace StockInventoryMaster.Views
{
    public class EnterBarcodeView : IEnterBarcodeView
    {
        public void ShowView()
        {
            
        }

        public void CloseView()
        {
            
        }

        public void BackView()
        {
            
        }

        public string StatusText { get; set; }
        public event EventHandler Show;
        public event EventHandler Close;
        public void CloseToInventory()
        {
            
        }

        public void CloseToInformation()
        {
            
        }

        public string Barcode { get; set; }
        public bool PrefixesVisible { get; set; }
        public bool PostfixesVisible { get; set; }
        public List<CsvPrefix> Prefixes { get; set; }
        public List<Codes> Postfixes { get; set; }
        public CsvPrefix SelectedPrefix { get; private set; }
        public Codes SelectedPostfix { get; private set; }
        public event EventHandler Enter;
    }
}