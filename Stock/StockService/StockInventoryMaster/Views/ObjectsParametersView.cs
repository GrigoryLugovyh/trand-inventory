﻿using System;
using StockCore.Interfaces.Views;

namespace StockInventoryMaster.Views
{
    public class ObjectsParametersView : IObjectParameters
    {
        public void ShowView()
        {
            
        }

        public void CloseView()
        {
            
        }

        public void BackView()
        {
            
        }

        public string StatusText { get; set; }
        public event EventHandler Show;
        public event EventHandler Close;
        public string ObjectName { get; set; }
        public string MolName { get; set; }
        public string MolFilter { get; set; }
        public string UserName { get; set; }
        public string UserFilter { get; set; }
        public event EventHandler FindMol;
        public event EventHandler SelectMol;
        public event EventHandler FindUser;
        public event EventHandler SelectUser;
        public event EventHandler Next;
    }
}