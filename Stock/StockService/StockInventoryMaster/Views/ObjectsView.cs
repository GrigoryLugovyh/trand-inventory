﻿using System;
using System.Collections.Generic;
using StockCore.Entity;
using StockCore.Interfaces.Views;

namespace StockInventoryMaster.Views
{
    public class ObjectsView : IObjectsView
    {
        public void ShowView()
        {
            
        }

        public void CloseView()
        {
            
        }

        public void BackView()
        {
            
        }

        public string StatusText { get; set; }
        public event EventHandler Show;
        public event EventHandler Close;
        public string SelectedObjectName { get; set; }
        public string FilterEdit { get; set; }
        public Objects SelectedObject { get; private set; }
        public List<Objects> Objects { get; set; }
        public event EventHandler Filter;
        public event EventHandler Add;
        public event EventHandler Select;
        public event EventHandler SelectedChanging;
    }
}