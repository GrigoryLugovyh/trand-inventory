﻿using System;
using System.Collections.Generic;
using StockCore.Handlers.Printing;
using StockCore.Interfaces.Views;

namespace StockInventoryMaster.Views
{
    public class PrintingView : IPrintingView
    {
        public void ShowView()
        {
            
        }

        public void CloseView()
        {
            
        }

        public void BackView()
        {
            
        }

        public string StatusText { get; set; }
        public event EventHandler Show;
        public event EventHandler Close;
        public List<PrinterEntry> Printers { get; set; }
        public PrinterEntry SelectedPrinter { get; private set; }
        public bool SearchSettingVisible { set; private get; }
        public bool ViewEnable { set; private get; }
        public string Information { set; private get; }
        public event EventHandler Print;
        public event EventHandler Search;
        public event EventHandler Cancel;
        public event EventHandler PrintedChanged;
    }
}