﻿using System;
using System.Collections.Generic;
using StockCore.Entity;
using StockCore.Interfaces.Views;

namespace StockInventoryMaster.Views
{
    public class FindObjectView : IFindObjectView
    {
        public void ShowView()
        {
            
        }

        public void CloseView()
        {
           
        }

        public void BackView()
        {
            
        }

        public string StatusText { get; set; }
        public event EventHandler Show;
        public event EventHandler Close;
        public string FilterEdit { get; set; }
        public Objects SelectedObject { get; set; }
        public List<Objects> Objects { get; set; }
        public event EventHandler Filter;
        public event EventHandler Select;
    }
}