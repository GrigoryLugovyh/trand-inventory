﻿using System;
using StockCore.Interfaces.Views;

namespace StockInventoryMaster.Views
{
    public class ExchangeView : IExchangeView
    {
        public void ShowView()
        {
            
        }

        public void CloseView()
        {
            
        }

        public void BackView()
        {
            
        }

        public string StatusText { get; set; }
        public event EventHandler Show;
        public event EventHandler Close;
        public bool ImportUpdate { get; set; }
        public bool ExportFound { get; set; }
        public bool ExportNew { get; set; }
        public event EventHandler Import;
        public event EventHandler Export;
        public event EventHandler Exit;
    }
}