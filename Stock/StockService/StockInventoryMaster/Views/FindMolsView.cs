﻿using System;
using System.Collections.Generic;
using StockCore.Entity;
using StockCore.Interfaces.Views;

namespace StockInventoryMaster.Views
{
    public class FindMolsView : IFindPersonalView<Mols>
    {
        public void ShowView()
        {
            
        }

        public void CloseView()
        {
            
        }

        public void BackView()
        {
            
        }

        public string StatusText { get; set; }
        public event EventHandler Show;
        public event EventHandler Close;
        public List<Mols> Personals { get; set; }
        public Mols SelectPersonal { get; private set; }
        public event EventHandler Select;
        public event EventHandler SelectedChange;
    }
}