﻿using System;
using System.Collections.Generic;
using StockCore.Entity;
using StockCore.Interfaces.Views;

namespace StockInventoryMaster.Views
{
    public class SelectMolView : ISelectPersonalView<Mols>
    {
        public void ShowView()
        {
            
        }

        public void CloseView()
        {
            
        }

        public void BackView()
        {
            
        }

        public string StatusText { get; set; }
        public event EventHandler Show;
        public event EventHandler Close;

        public void CloseToObjects()
        {
            
        }

        public void CloseToInventory()
        {
            
        }

        public string FilterValue { get; set; }
        public string PersonalName { get; set; }
        public List<Mols> Personals { get; set; }
        public Mols SelectPersonal { get; private set; }
        public event EventHandler Filter;
        public event EventHandler Select;
        public event EventHandler SelectedChange;
    }
}