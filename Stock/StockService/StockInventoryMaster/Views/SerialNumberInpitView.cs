﻿using System;
using StockCore.Interfaces.Views;

namespace StockInventoryMaster.Views
{
    public class SerialNumberInpitView : ISerialNumberInputView
    {
        public void ShowView()
        {
            
        }

        public void CloseView()
        {
            
        }

        public void BackView()
        {
            
        }

        public string StatusText { get; set; }
        public event EventHandler Show;
        public event EventHandler Close;
        public string SerialNumber { get; set; }
        public string CurrentSerialNumber { get; set; }
        public event EventHandler Input;
    }
}