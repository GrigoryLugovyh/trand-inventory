﻿using System;
using StockCore.Interfaces.Views;

namespace StockInventoryMaster.Views
{
    public class AddObjectView : IAddObjectView
    {
        public void ShowView()
        {
            
        }

        public void CloseView()
        {
            
        }

        public void BackView()
        {
            
        }

        public string StatusText { get; set; }
        public event EventHandler Show;
        public event EventHandler Close;
        public string ObjectBarcode { get; set; }
        public string ObjectCode { get; set; }
        public string ObjectName { get; set; }
        public string ObjectParent { get; set; }
        public event EventHandler Add;
        public event EventHandler Create;
        public event EventHandler Delete;
    }
}