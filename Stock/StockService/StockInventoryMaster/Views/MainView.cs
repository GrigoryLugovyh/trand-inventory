﻿using System;
using StockCore.Interfaces.Views;

namespace StockInventoryMaster.Views
{
    public class MainView : IMainView
    {
        public void ShowView()
        {
            
        }

        public void CloseView()
        {
            
        }

        public void BackView()
        {
            
        }

        public string StatusText { get; set; }
        public event EventHandler Show;
        public event EventHandler Close;
        public string ItemsAll { get; set; }
        public string ObjectsAll { get; set; }
        public string ItemsProcessed { get; set; }
        public string ObjectsProcessed { get; set; }
        public event EventHandler InventoryClick;
        public event EventHandler InformationClick;
        public event EventHandler ObjectsClick;
        public event EventHandler ExchangeClick;
        public event EventHandler ExitClick;
    }
}