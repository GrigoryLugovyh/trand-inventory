﻿using System;
using System.Collections.Generic;
using StockCore.Entity;
using StockCore.Interfaces.Views;

namespace StockInventoryMaster.Views
{
    public class SelectItemStatusView : ISeleectItemStatusView
    {
        public void ShowView()
        {
            
        }

        public void CloseView()
        {
            
        }

        public void BackView()
        {
            
        }

        public string StatusText { get; set; }
        public event EventHandler Show;
        public event EventHandler Close;
        public List<Statuses> Statuses { get; set; }
        public Statuses SelectedStatus { get; private set; }
        public event EventHandler Change;
    }
}