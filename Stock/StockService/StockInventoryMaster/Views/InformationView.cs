﻿using System;
using System.Collections.Generic;
using StockCore.Entity;
using StockCore.Interfaces.Views;

namespace StockInventoryMaster.Views
{
    public class InformationView : IInformationView
    {
        public void ShowView()
        {
            
        }

        public void CloseView()
        {
            
        }

        public void BackView()
        {
            
        }

        public string StatusText { get; set; }
        public event EventHandler Show;
        public event EventHandler Close;

        public void ObjectsRefresh()
        {
            
        }

        public string Status { get; set; }
        public string Barcode { get; set; }
        public string ItemName { get; set; }
        public string Ibso { get; set; }
        public string UserName { get; set; }
        public string MolName { get; set; }
        public string Serial { get; set; }
        public List<Objects> Objects { get; set; }
        public Objects SelectedObject { get; private set; }
        public bool SelectAll { get; private set; }
        public string Filter { get; set; }
        public event EventHandler BarcodeEnterHand;
        public event EventHandler ToInformation;
        public event EventHandler ToAll;
        public event EventHandler SelectAllChanged;
        public event EventHandler Find;
        public event EventHandler Open;
        public event EventHandler PrintItem;
        public event EventHandler PrintObjects;
    }
}