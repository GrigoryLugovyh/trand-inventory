﻿using System;
using StockServiceCore;

namespace StockServiceConsole
{
    class Program
    {
        static void Main()
        {
            Model.Instance.Start();

            Console.ReadLine();

            Model.Instance.Stop();
        }
    }
}
