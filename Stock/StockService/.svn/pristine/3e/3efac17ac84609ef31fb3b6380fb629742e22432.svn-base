﻿using System;
using System.Diagnostics;
using System.IO;
using System.Management;
using System.Reflection;
using System.Text;
using System.Threading;
using StockServiceCore.Interfaces;
using StockServiceCore.Properties;

namespace StockServiceCore.Handlers
{
    public class CmdPrinter : IPrinntable
    {
        private string _directory;

        public void Print(byte[] label, Encoding encoding, string printer, int timeout)
        {
            if (string.IsNullOrWhiteSpace(_directory))
            {
                _directory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            }

            int id = 0;

            var t = 0;

            var text = encoding.GetString(label);

            var filename = string.Format("~{0}.txt", Guid.NewGuid());

            try
            {
                File.WriteAllText(filename, text);

                if (_directory != null)
                {
                    var procStartInfo = new ProcessStartInfo("cmd", string.Format(@"print /d:{0} {1}", printer, Path.Combine(_directory, filename)))
                        {
                            RedirectStandardOutput = true,
                            UseShellExecute = false,
                            CreateNoWindow = true
                        };

                    var proc = new Process {StartInfo = procStartInfo};

                    proc.Start();

                    id = proc.Id;

                    while (t < timeout)
                    {
                        if (proc.HasExited)
                        {
                            return;
                        }

                        Thread.Sleep(5);
                        t += 5;
                    }
                }

                throw new TimeoutException("Превышено время ожидания выполнения операции печати");
            }
            finally
            {
                if (id > 0)
                {
                    ProcessKill(id);
                }

                if (File.Exists(filename))
                {
                    File.Delete(filename);
                }
            }
        }

        private void ProcessKill(int pid)
        {
            if (pid != 0)
            {
                var searcher = new ManagementObjectSearcher(string.Format(Resources.QueryProcess, pid));
                var moc = searcher.Get();
                foreach (ManagementObject mo in moc)
                {
                    ProcessKill(Convert.ToInt32(mo["ProcessID"]));
                }
                try
                {
                    var proc = Process.GetProcessById(pid);
                    proc.Kill();
                }
                catch (ArgumentException)
                {
                    // сам завершился
                }
            }
        }
    }
}