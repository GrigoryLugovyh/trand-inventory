﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using StockServiceCore.Enums;
using StockServiceCore.Factories;
using StockServiceCore.Interfaces;

namespace StockServiceCore.Handlers
{
    public class PrintingQueueDistributor : IStartable, IExceptionable
    {
        public PrintingQueueDistributor(IPrinntable handler)
        {
            Encoding = EncodingFactory.Instance.GetInstance(Model.Instance.Config.CurrentConfig.Encoding);

            PrinterTimeout = Model.Instance.Config.CurrentConfig.PrintTimeout*1000;

            PrinterName = Model.Instance.Config.CurrentConfig.Printer;

            LabelsQueue = new Queue<byte[]>();

            Handler = handler;
            
            Status = StartableEnum.Stopped;
        }

        public Queue<byte[]> LabelsQueue { get; protected set; }

        public Exception LastException { get; protected set; }

        public IPrinntable Handler { get; protected set; }

        public string PrinterName { get; protected set; }

        public int PrinterTimeout { get; protected set; }

        public Encoding Encoding { get; protected set; }

        public virtual void Push(byte[] label)
        {
            LabelsQueue.Enqueue(label);
        }

        public StartableEnum Status { get; private set; }

        public void Start()
        {
            if (Status == StartableEnum.Stopped)
            {
                Printer = new Task(Printing);
                Printer.Start();

                Status = StartableEnum.Started;
            }
        }

        public void Stop()
        {
            if (Status == StartableEnum.Started)
            {
                Status = StartableEnum.Stopped;
                Printer = null;
            }
        }

        public void Restart()
        {
            Stop();
            Start();
        }

        protected Task Printer;

        protected virtual void Printing()
        {
            while (Status == StartableEnum.Started)
            {
                if (LabelsQueue.Count == 0)
                {
                    Thread.Sleep(100);
                    continue;
                }

                try
                {
                    // печатаем
                    Handler.Print(LabelsQueue.Dequeue(), Encoding, PrinterName, PrinterTimeout);
                }
                catch (Exception ex)
                {
                    LastException = ex;
                }
            }
        }
    }
}