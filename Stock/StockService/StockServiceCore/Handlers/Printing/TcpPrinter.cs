﻿using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using StockServiceCore.Interfaces;

namespace StockServiceCore.Handlers.Printing
{
    public class TcpPrinter : IPrintable
    {
        public void Print(byte[] label, Encoding encoding, string printer, int port, int timeout)
        {
            using (var clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
            {
                clientSocket.NoDelay = true;

                var ip = IPAddress.Parse(printer);

                var point = new IPEndPoint(ip, port);

                Log.Info("Печать этикетки:\r\n{0}", encoding.GetString(label));

                clientSocket.Connect(point);

                clientSocket.Send(label);

                clientSocket.Close();

                Thread.Sleep(timeout);
            }
        }
    }
}