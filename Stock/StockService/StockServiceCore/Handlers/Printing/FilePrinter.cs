﻿using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;
using StockServiceCore.Interfaces;

namespace StockServiceCore.Handlers.Printing
{
    public class FilePrinter : IPrintable
    {
        private string _directory;

        public void Print(byte[] label, Encoding encoding, string printer, int port, int timeout)
        {
            if (string.IsNullOrWhiteSpace(_directory))
            {
                _directory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            }

            var text = encoding.GetString(label);

            var filename = string.Format("~{0}.txt", Guid.NewGuid());

            File.WriteAllText(filename, text, encoding);

            Log.Info("Печать этикетки:\r\n{0}", text);

            File.Copy(filename, printer, true);

            Thread.Sleep(timeout);
        }
    }
}