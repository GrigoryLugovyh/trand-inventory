﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using StockServiceCore.Enums;
using StockServiceCore.Factories;
using StockServiceCore.Interfaces;

namespace StockServiceCore.Handlers
{
    public class PrintingQueueDistributor : IStartable, IExceptionable
    {
        public PrintingQueueDistributor(IPrintable handler)
        {
            Encoding = EncodingFactory.Instance.GetInstance(Model.Instance.Config.CurrentConfig.Encoding);

            PrinterTimeout = Model.Instance.Config.CurrentConfig.PrintTimeout*1000;

            PrinterName = Model.Instance.Config.CurrentConfig.Printer;

            PrinterPort = Model.Instance.Config.CurrentConfig.PrinterPort;

            LabelsQueue = new Queue<byte[]>();

            Handler = handler;

            Status = StartableEnum.Stopped;
        }

        public Queue<byte[]> LabelsQueue { get; protected set; }

        public Exception LastException { get; protected set; }

        public IPrintable Handler { get; protected set; }

        public string PrinterName { get; protected set; }

        public int PrinterPort { get; protected set; }

        public int PrinterTimeout { get; protected set; }

        public Encoding Encoding { get; protected set; }

        public virtual void Push(byte[] label)
        {
            LabelsQueue.Enqueue(label);
        }

        public StartableEnum Status { get; private set; }

        public void Start()
        {
            if (Status == StartableEnum.Stopped)
            {
                Printer = new Task(Printing);
                Printer.Start();

                Status = StartableEnum.Started;

                Log.Info("Обработчик очереди печати успешно запущен. Сетевое имя принтера \"{0}\", кодировка \"{1}\", время ожидания завершения печати {2} мс", PrinterName, Encoding.HeaderName, PrinterTimeout);
            }
        }

        public void Stop()
        {
            if (Status == StartableEnum.Started)
            {
                Status = StartableEnum.Stopped;
                Printer = null;

                Log.Info("Обработчик очереди печати остановлен");
            }
        }

        public void Restart()
        {
            Stop();
            Start();
        }

        protected Task Printer;

        protected virtual void Printing()
        {
            while (Status == StartableEnum.Started)
            {
                if (LabelsQueue.Count == 0)
                {
                    Thread.Sleep(100);
                    continue;
                }

                try
                {
                    Handler.Print(LabelsQueue.Dequeue(), Encoding, PrinterName, PrinterPort, PrinterTimeout);
                }
                catch (Exception ex)
                {
                    LastException = ex;
                    Log.Warn("В процессе печати произошла ошибка: {0}", ex.Message);
                }
                finally
                {
                    Log.Info("В очереди на печать осталось {0} этикеток ...", LabelsQueue.Count);
                }
            }
        }
    }
}