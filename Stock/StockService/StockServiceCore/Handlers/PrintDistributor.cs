﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using StockServiceCore.Enums;
using StockServiceCore.Interfaces;

namespace StockServiceCore.Handlers
{
    /// <summary>
    /// Будет слушать порт и ловить этикетки для печати
    /// </summary>
    public class PrintDistributor : IStartable, IExceptionable
    {
        public PrintDistributor(PrintingQueueDistributor queue)
        {
            QueueDistributor = queue;
            Status = StartableEnum.Stopped;
        }

        protected IPAddress Host 
        {
            get { return Dns.GetHostAddresses(Model.Instance.Config.CurrentConfig.Host)[0]; }
        }

        protected int Port
        {
            get { return Model.Instance.Config.CurrentConfig.Port; }
        }

        public PrintingQueueDistributor QueueDistributor { get; protected set; }

        public Exception LastException { get; protected set; }

        public StartableEnum Status { get; protected set; }

        public void Start()
        {
            if (Status == StartableEnum.Stopped)
            {
                Listener = new Task(Listening, Port);
                Listener.Start();

                Status = StartableEnum.Started;

                Log.Info("Дистрибьютор печати успешно запущен. Адрес \"{0}\", порт \"{1}\"", Host, Port);
                Log.Info("Ожидание входящих соединений ...");
            }
        }

        public void Stop()
        {
            if (Status == StartableEnum.Started)
            {
                if (Listener != null)
                {
                    Status = StartableEnum.Stopped;
                }

                if (Server != null)
                {
                    Server.Server.Close();
                    Server.Stop();
                }

                Listener = null;

                Log.Info("Дистрибьютор печати остановлен");
            }
        }

        public void Restart()
        {
            Stop();
            Start();
        }

        protected TcpListener Server;

        protected Task Listener;

        protected object Address;

        protected IPEndPoint LocalEndPoint { get; set; }

        protected bool ClientConnected { get; set; }

        protected void Listening(object port)
        {
            LocalEndPoint = new IPEndPoint(IPAddress.Any, int.Parse(port.ToString()));
            Server = new TcpListener(LocalEndPoint);
            Server.Start();

            while (Status == StartableEnum.Started)
            {
                Socket client = Server.AcceptSocket();
                ClientConnected = true;

                Log.Info("Прием данных с адреса \"{0}\" ...", client.RemoteEndPoint);

                try
                {
                    var buffer = new List<byte>();
                    var readBuffer = new byte[1024];
                    int numberOfBytesRead;

                    do
                    {
                        numberOfBytesRead = client.Receive(readBuffer);
                        if (numberOfBytesRead == 1024)
                        {
                            buffer.AddRange(readBuffer);
                        }
                        else
                        {
                            var tmp = new byte[numberOfBytesRead];
                            Array.Copy(readBuffer, 0, tmp, 0, numberOfBytesRead);
                            buffer.AddRange(tmp);
                        }
                    } while (numberOfBytesRead > 0);

                    if (buffer.Count != 0)
                    {
                        Log.Info("Данные успешно получены и поставлены в очередь печати [{0} байт]", buffer.Count);

                        QueueDistributor.Push(buffer.ToArray());
                    }
                }
                catch (Exception ex)
                {
                    LastException = ex;
                    Log.Warn("Ошибка в процессе получения данных: {0}", ex.Message);
                }
                finally
                {
                    if (client.Connected)
                    {
                        client.Shutdown(SocketShutdown.Both);
                        client.Close();
                    }

                    ClientConnected = false;
                }
            }
        }
    }
}