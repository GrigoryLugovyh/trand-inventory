﻿using System;
using System.Collections.Generic;
using System.Linq;
using StockServiceCore.Enums;
using StockServiceCore.Environments;

namespace StockServiceCore
{
    public sealed class Model
    {
        public Model()
        {
            Status = StartableEnum.Stopped;
        }

        private static readonly object Sync = new object();

        private static volatile Model _instance;

        public static Model Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (Sync)
                    {
                        if (_instance == null)
                        {
                            _instance = new Model();
                        }
                    }
                }

                return _instance;
            }
        }

        public StartableEnum Status { get; private set; }

        public List<ServiceEnvironment> Environments { get; private set; }

        public ConfigEnvironment Config
        {
            get { return Environments.FirstOrDefault(e => e is ConfigEnvironment) as ConfigEnvironment; }
        }

        public PrintingEnvironment Printing
        {
            get { return Environments.FirstOrDefault(e => e is PrintingEnvironment) as PrintingEnvironment; }
        }

        public void Start()
        {
            if (Status == StartableEnum.Stopped)
            {
                Log.Info("Запуск ...");

                Log.Info("Среда запуска: {0} {1}", Environment.OSVersion.Platform, Environment.OSVersion.Version);
                Log.Info("Версия CLR: {0}", Environment.Version);

                if (Environments == null)
                {
                    Environments = new List<ServiceEnvironment>();
                }

                Environments.Add(new ConfigEnvironment());
                Environments.Add(new PrintingEnvironment());
 
                foreach (var environment in Environments)
                {
                    try
                    {
                        environment.Start();
                        Log.Info("Окружение \"{0}\" успешно запущено", environment.FriendlyName);
                    }
                    catch (Exception ex)
                    {
                        Log.Warn("В процессе запуска окружения \"{0}\" возникла ошибка: \"{1}\" \r\n{2}", environment.FriendlyName, ex.Message, ex.StackTrace);
                    }
                }

                Status = StartableEnum.Started;

                Log.Info("Модель успешно запущена");
            }
            else
            {
                Log.Warn("Модель уже находится в состоянии \"Started\"");
            }
        }

        public void Stop()
        {
            if (Status == StartableEnum.Started)
            {
                Log.Info("Остановка ...");

                foreach (var environment in Environments)
                {
                    environment.Stop();
                }

                Environments.Clear();

                Status = StartableEnum.Stopped;

                Log.Info("Модель успешно остановлена");
            }
            else
            {
                Log.Warn("Модель уже находится в состоянии \"Stopped\"");
            }
        }

        public void Restart()
        {
            Stop();
            Start();
        }
    }
}