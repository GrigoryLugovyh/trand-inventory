﻿using System;
using System.IO;
using System.Reflection;
using StockServiceCore.Common;
using StockServiceCore.Handlers;

namespace StockServiceCore.Environments
{
    public class ConfigEnvironment : ServiceEnvironment
    {
        public override string FriendlyName
        {
            get { return "Конфигурация"; }
        }

        public ServiceConfig CurrentConfig { get; protected set; }

        public Configurator<ServiceConfig> Configurator { get; protected set; }

        public string ConfigPath
        {
            get { return Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "StockService.cfg"); }
        }

        public void Save()
        {
            Configurator.Object = CurrentConfig;
            Configurator.Save();
        }

        protected override Action StartAction
        {
            get
            {
                return () =>
                    {
                        if (Configurator == null)
                        {
                            Configurator = new Configurator<ServiceConfig> { ObjectPath = ConfigPath };
                        }

                        if (!File.Exists(Configurator.ObjectPath))
                        {
                            CurrentConfig = Configurator.Object = ServiceConfig.Default;
                            Configurator.Save();
                        }
                        else
                        {
                            Configurator.Load();
                            CurrentConfig = Configurator.Object;
                        }
                    };
            }
        }

        protected override Action StopAction
        {
            get
            {
                return () =>
                    {
                        Configurator = null;
                    };
            }
        }
    }
}