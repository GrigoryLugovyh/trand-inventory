﻿using System;

namespace StockServiceCore.Environments
{
    public abstract class ServiceEnvironment
    {
        public bool IsActive { get; protected set; }

        public abstract string FriendlyName { get; }

        protected abstract Action StartAction { get; }

        protected abstract Action StopAction { get; }

        public virtual void Start()
        {
            if (StartAction == null)
                throw new Exception("Нет реализации действия запуска");

            StartAction.Invoke();

            IsActive = true;
        }

        public virtual void Stop()
        {
            if (StopAction == null)
                throw new Exception("Нет реализации действия остановки");

            StopAction.Invoke();

            IsActive = false;
        }

        public override string ToString()
        {
            return FriendlyName;
        }
    }
}