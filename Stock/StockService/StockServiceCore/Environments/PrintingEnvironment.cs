﻿using System;
using StockServiceCore.Factories;
using StockServiceCore.Handlers;

namespace StockServiceCore.Environments
{
    public class PrintingEnvironment : ServiceEnvironment
    {
        public override string FriendlyName
        {
            get { return "Печать"; }
        }

        public PrintDistributor Distributor { get; protected set; }

        public PrintingQueueDistributor Queue { get; protected set; }

        protected override Action StartAction
        {
            get
            {
                return () =>
                    {
                        Queue = new PrintingQueueDistributor(PrinterFactory.Instance.GetPrinter(Model.Instance.Config.CurrentConfig.PrinterType));
                        Queue.Start();

                        Distributor = new PrintDistributor(Queue);
                        Distributor.Start();
                    };
            }
        }

        protected override Action StopAction
        {
            get
            {
                return () =>
                    {
                        if (Queue != null)
                        {
                            Queue.Stop();
                        }

                        if (Distributor != null)
                        {
                            Distributor.Stop();
                        }

                        Queue = null;
                        Distributor = null;
                    };

            }
        }
    }
}