﻿using System;
using System.Xml.Serialization;

namespace StockServiceCore.Enums
{
    [Serializable]
    public enum PrinterTypeEnum
    {
        [XmlEnum]
        Cmd,

        [XmlEnum]
        File,

        [XmlEnum]
        Tcp
    }
}