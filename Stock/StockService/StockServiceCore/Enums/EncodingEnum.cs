﻿using System;
using System.Xml.Serialization;

namespace StockServiceCore.Enums
{
    [Serializable]
    public enum EncodingEnum
    {
        [XmlEnum]
        Win,

        [XmlEnum]
        Dos,

        [XmlEnum]
        Unicode,

        [XmlEnum]
        Utf
    }
}