﻿using System;
using NLog;

namespace StockServiceCore
{
    public class Log
    {
        private static Logger _log;
        private static LogFactory _factory;

        private static void Init()
        {
            if (_log == null)
            {
                if (_factory == null)
                {
                    _factory = new LogFactory();
                }

                _log = _factory.GetCurrentClassLogger();
            }
        }

        public static void Info(string message, params object[] args)
        {
            Init();
            _log.Info(message, args);
        }

        public static void Warn(string message, params object[] args)
        {
            Init();
            _log.Warn(message, args);
        }

        public static void Warn(Exception ex)
        {
            Init();
            _log.Warn(ex.Message);
        }
    }
}