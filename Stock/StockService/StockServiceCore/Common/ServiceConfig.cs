﻿using System;
using System.Xml.Serialization;
using StockServiceCore.Enums;

namespace StockServiceCore.Common
{
    [Serializable]
    public class ServiceConfig
    {
        [XmlElement]
        public int Port { get; set; }

        [XmlElement]
        public PrinterTypeEnum PrinterType { get; set; }

        [XmlElement]
        public string Host { get; set; }

        [XmlElement]
        public string Printer { get; set; }

        [XmlElement]
        public int PrinterPort { get; set; }

        [XmlElement]
        public int PrintTimeout { get; set; }

        [XmlElement]
        public EncodingEnum Encoding { get; set; }

        public static ServiceConfig Default
        {
            get
            {
                return new ServiceConfig
                    {
                        Host = "10.1.12.133",
                        Port = 8989,
                        PrinterType = PrinterTypeEnum.Tcp,
                        PrinterPort = 9100,
                        Printer = "10.1.12.15",
                        PrintTimeout = 3,
                        Encoding = EncodingEnum.Dos,
                    };
            }
        }
    }
}