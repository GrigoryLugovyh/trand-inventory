﻿using System;

namespace StockServiceCore.Interfaces
{
    public interface IExceptionable
    {
        Exception LastException { get; } 
    }
}