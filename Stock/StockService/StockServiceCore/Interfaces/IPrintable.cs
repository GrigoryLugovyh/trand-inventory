﻿using System.Text;

namespace StockServiceCore.Interfaces
{
    public interface IPrintable
    {
        void Print(byte[] label, Encoding encoding, string printer, int port, int timeout);
    }
}