﻿using StockServiceCore.Enums;

namespace StockServiceCore.Interfaces
{
    public interface IStartable
    {
        StartableEnum Status { get; }

        void Start();

        void Stop();

        void Restart();
    }
}