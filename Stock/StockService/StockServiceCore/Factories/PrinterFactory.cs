﻿using System;
using StockServiceCore.Enums;
using StockServiceCore.Handlers.Printing;
using StockServiceCore.Interfaces;

namespace StockServiceCore.Factories
{
    public sealed class PrinterFactory
    {
        private static readonly object Sync = new object();

        private static volatile PrinterFactory _instance;

        /// <summary>
        /// Инициализация
        /// </summary>
        public static PrinterFactory Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (Sync)
                    {
                        if (_instance == null)
                        {
                            _instance = new PrinterFactory();
                        }
                    }
                }

                return _instance;
            }
        }

        public IPrintable GetPrinter(PrinterTypeEnum @type)
        {
            IPrintable printer = null;

            switch (type)
            {
                case PrinterTypeEnum.Cmd:
                    {
                        printer = new CmdPrinter();
                        break;
                    }
                case PrinterTypeEnum.File:
                    {
                        printer = new FilePrinter();
                        break;
                    }
                case PrinterTypeEnum.Tcp:
                    {
                        printer = new TcpPrinter();
                        break;
                    }
            }

            if (printer == null)
            {
                throw new ArgumentException("@type");
            }

            return printer;
        }
    }
}