﻿using System.Text;
using StockServiceCore.Enums;

namespace StockServiceCore.Factories
{
    /// <summary>
    /// Фабрика кодировок
    /// </summary>
    public sealed class EncodingFactory
    {
        private static readonly object Sync = new object();

        private static volatile EncodingFactory _instance;

        /// <summary>
        /// Инициализация
        /// </summary>
        public static EncodingFactory Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (Sync)
                    {
                        if (_instance == null)
                        {
                            _instance = new EncodingFactory();
                        }
                    }
                }

                return _instance;
            }
        }

        /// <summary>
        /// Релизация
        /// </summary>
        /// <param name="encoding"></param>
        /// <returns></returns>
        public Encoding GetInstance(EncodingEnum encoding)
        {
            Encoding code;

            switch (encoding)
            {
                case EncodingEnum.Dos:
                    {
                        code = Encoding.GetEncoding(866);
                        break;
                    }
                case EncodingEnum.Unicode:
                    {
                        code = Encoding.Unicode;
                        break;
                    }
                case EncodingEnum.Utf:
                    {
                        code = Encoding.UTF8;
                        break;
                    }
                default:
                    {
                        code = Encoding.GetEncoding(1251);
                        break;
                    }
            }

            return code;
        }
    }
}