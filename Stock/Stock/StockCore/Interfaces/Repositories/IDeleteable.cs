using System.Collections.Generic;
using StockCore.Entity;

namespace StockCore.Interfaces.Repositories
{
    public interface IDeleteable<T> where T : StockEntity
    {
        bool Delete(T entity);

        bool Delete(List<T> entities);
    }
}