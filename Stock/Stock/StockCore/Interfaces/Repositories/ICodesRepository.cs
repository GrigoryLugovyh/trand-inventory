using StockCore.Entity;

namespace StockCore.Interfaces.Repositories
{
    public interface ICodesRepository : IRepository<Codes>
    {
        
    }
}