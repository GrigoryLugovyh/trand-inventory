using StockCore.Entity;

namespace StockCore.Interfaces.Repositories
{
    public interface IRepository<T> : ILoadable<T>, ISaveable<T>, IDeleteable<T> where T : StockEntity
    {
    }
}