using System;
using System.Data;
using System.Data.Common;

namespace StockCore.Interfaces.Repositories
{
    /// <summary>
    /// ���������� ���������� � ����� ������
    /// </summary>
    public interface IDbConnection
    {
        /// <summary>
        /// ������������� �����������
        /// </summary>
        Guid Id { get; }

        /// <summary>
        /// �������� ����������
        /// </summary>
        string NameConnection { get; set; }

        /// <summary>
        /// ���������� �����������
        /// </summary>
        bool IsActive { get; set; }

        /// <summary>
        /// ������� ������� ��������
        /// </summary>
        int CmdTimeout { get; }

        /// <summary>
        /// ����������
        /// </summary>
        DbTransaction Transaction { get; }

        /// <summary>
        /// �����������
        /// </summary>
        DbConnection Connection { get; }

        /// <summary>
        /// ��������� ������
        /// </summary>
        Exception LastException { get; }

        /// <summary>
        /// �������� ���������� 
        /// </summary>
        void CheckConnection();

        /// <summary>
        /// �������� ����������
        /// </summary>
        /// <param name="isolation"></param>
        void BeginTransaction(IsolationLevel isolation);

        /// <summary>
        /// ��������� ����������
        /// </summary>
        void CommitTransaction();

        /// <summary>
        /// ���������� ����������
        /// </summary>
        void RollbackTransaction();

        /// <summary>
        /// ��������� ����������
        /// </summary>
        void Close();

        /// <summary>
        /// ��������� ������� ����� ������
        /// </summary>
        /// <param name="query"></param>
        /// <param name="table"></param>
        /// <returns></returns>
        int Fill(string query, ref DataTable table);

        /// <summary>
        /// ��������� ������� ����� �������
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="table"></param>
        /// <returns></returns>
        int Fill(IDbCommand cmd, ref DataTable table);

        /// <summary>
        /// ��������� ������ 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        int ExecuteNonQuery(string query);

        /// <summary>
        /// ��������� ��������� ������
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        object ExecuteScalar(string query);

        /// <summary>
        /// ������� ��������
        /// </summary>
        /// <returns></returns>
        IDbDataParameter CreateParameter();

        /// <summary>
        /// ��������� �������
        /// </summary>
        /// <param name="cmd"></param>
        /// <returns></returns>
        int ExecuteNonQuery(DbCommand cmd);


        /// <summary>
        /// ������� ������� �� ������ �������
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        IDbCommand CreateCommand(string query);

        int ExecuteNonQueryOrError(string query, string[] paramNames, object[] paramValues);

        int ExecuteNonQuery(string query, DbParameter[] parameters);

        int ExecuteNonQueryOrError(DbCommand command, string[] paramNames, object[] paramValues);

        object ExecuteScalar(DbCommand cmd);

        object ExecuteScalar(string query, string[] paramNames, object[] paramValues);

        object ExecuteScalar(string query, DbParameter[] parameters);

        object ExecuteScalar(DbCommand command, string[] paramNames, object[] paramValues);
    }
}