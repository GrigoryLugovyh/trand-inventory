using StockCore.Entity;

namespace StockCore.Interfaces.Repositories
{
    public interface IMolsRepository : IRepository<Mols>
    {
        
    }
}