using StockCore.Entity;

namespace StockCore.Interfaces.Repositories
{
    public interface IStatusesRepository : IRepository<Statuses>
    {
        
    }
}