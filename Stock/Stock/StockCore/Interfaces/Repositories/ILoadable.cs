using System.Collections.Generic;
using StockCore.Entity;

namespace StockCore.Interfaces.Repositories
{
    public interface ILoadable<T> where T : StockEntity
    {
        List<T> Load();

        List<T> Load(string where);

        T LoadSingle(string where);
    }
}