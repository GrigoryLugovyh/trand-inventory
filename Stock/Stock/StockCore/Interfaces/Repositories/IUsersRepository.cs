using StockCore.Entity;

namespace StockCore.Interfaces.Repositories
{
    public interface IUsersRepository : IRepository<Users>
    {
        
    }
}