using StockCore.Entity;

namespace StockCore.Interfaces.Repositories
{
    public interface IObjectsRepository : IRepository<Objects>
    {
        
    }
}