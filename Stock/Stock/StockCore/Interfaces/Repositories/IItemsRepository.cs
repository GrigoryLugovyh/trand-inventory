using StockCore.Entity;

namespace StockCore.Interfaces.Repositories
{
    public interface IItemsRepository : IRepository<Items>
    {
        
    }
}