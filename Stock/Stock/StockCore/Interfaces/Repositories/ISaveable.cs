using System.Collections.Generic;
using StockCore.Entity;

namespace StockCore.Interfaces.Repositories
{
    public interface ISaveable<T> where T : StockEntity
    {
        bool Save(T entity);

        bool Save(List<T> entities);
    }
}