using StockCore.Entity;

namespace StockCore.Interfaces.Repositories
{
    public interface IDoublesRepository : IRepository<Doubles>
    {
        
    }
}