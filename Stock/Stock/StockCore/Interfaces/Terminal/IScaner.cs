using System;

namespace StockCore.Interfaces.Terminal
{
    public delegate void ScanDelegate(string barcode);

    public interface IScaner : IDisposable
    {
        bool IsActive { get; }

        void Start();

        void Stop();

        event ScanDelegate Scanned;
    }
}