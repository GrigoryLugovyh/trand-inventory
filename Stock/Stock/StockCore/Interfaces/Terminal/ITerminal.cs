using System;

namespace StockCore.Interfaces.Terminal
{
    public interface ITerminal : IDisposable
    {
        void Init();

        IScaner Scaner { get; }
    }
}