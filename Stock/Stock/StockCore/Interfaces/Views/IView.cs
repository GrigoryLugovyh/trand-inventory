using System;

namespace StockCore.Interfaces.Views
{
    public interface IView
    {
        void ShowView();

        void CloseView();

        void BackView();

        event EventHandler Show;

        event EventHandler Close;

        string StatusText { get; set; }
    }
}