using System;
using System.Collections.Generic;
using StockCore.Entity;
using StockCore.Handlers.Exchange.Configuration.CSV;

namespace StockCore.Interfaces.Views
{
    public interface IEnterBarcodeView : IView
    {
        event EventHandler Enter;

        string Barcode { get; set; }

        bool PrefixesVisible { get; set; }

        bool PostfixesVisible { get; set; }

        List<CsvPrefix> Prefixes { get; set; }

        List<Codes> Postfixes { get; set; }

        CsvPrefix SelectedPrefix { get; }

        Codes SelectedPostfix { get; }

        void CloseToInventory();

        void CloseToInformation();
    }
}