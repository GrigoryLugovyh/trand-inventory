using System;
using System.Collections.Generic;
using StockCore.Entity;

namespace StockCore.Interfaces.Views
{
    public interface ISelectPersonalView<T> : IView where T : Personal
    {
        event EventHandler Filter;

        event EventHandler Select;

        event EventHandler SelectedChange;

        string FilterValue { get; set; }

        string PersonalName { get; set; }

        List<T> Personals { get; set; }

        T SelectPersonal { get; }

        void CloseToObjects();

        void CloseToInventory();
    }
}