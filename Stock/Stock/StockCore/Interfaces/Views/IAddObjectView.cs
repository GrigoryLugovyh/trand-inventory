using System;

namespace StockCore.Interfaces.Views
{
    public interface IAddObjectView : IView
    {
        string ObjectBarcode { get; set; }

        string ObjectCode { get; set; }

        string ObjectName { get; set; }

        string ObjectParent { get; set; }

        event EventHandler Add;

        event EventHandler Create;

        event EventHandler Delete;
    }
}