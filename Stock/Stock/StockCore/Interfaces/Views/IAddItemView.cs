using System;
using System.Collections.Generic;
using StockCore.Entity;

namespace StockCore.Interfaces.Views
{
    public interface IAddItemView : IView
    {
        string ItemBarcode { get; set; }

        string ItemName { get; set; }

        List<Statuses> Statuses { get; set; }

        Statuses SelectedStatus { get; }

        DateTime ItemBeginDate { get; set; }

        bool BeginDateEnable { get; set; }

        event EventHandler Add;
    }
}