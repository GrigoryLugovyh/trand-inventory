using System;

namespace StockCore.Interfaces.Views
{
    public interface ISerialNumberInputView : IView
    {
        event EventHandler Input;

        string SerialNumber { get; set; }

        string CurrentSerialNumber { get; set; }
    }
}