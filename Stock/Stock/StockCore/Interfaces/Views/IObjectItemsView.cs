using System;
using System.Collections.Generic;
using StockCore.Entity;

namespace StockCore.Interfaces.Views
{
    public interface IObjectItemsView : IView
    {
        string ObjectName { get; set; }

        bool SelectedCheck { get; set; }

        event EventHandler SelectedChecked;

        List<Items> Items { get; set; }

        event EventHandler Cancel;

        event EventHandler Print;

        void ItemsRefresh();
    }
}