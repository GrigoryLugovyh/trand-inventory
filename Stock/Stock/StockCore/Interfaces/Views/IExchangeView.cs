using System;

namespace StockCore.Interfaces.Views
{
    public interface IExchangeView : IView
    {
        bool ImportUpdate { get; set; }

        bool ExportFound { get; set; }

        bool ExportNew { get; set; }

        event EventHandler Import;

        event EventHandler Export;

        event EventHandler Exit;
    }
}