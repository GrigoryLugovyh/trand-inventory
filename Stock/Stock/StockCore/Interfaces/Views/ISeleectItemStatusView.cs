using System;
using System.Collections.Generic;
using StockCore.Entity;

namespace StockCore.Interfaces.Views
{
    public interface ISeleectItemStatusView : IView
    {
        event EventHandler Change;

        List<Statuses> Statuses { get; set; }

        Statuses SelectedStatus { get; }
    }
}