using System;
using System.Collections.Generic;
using StockCore.Entity;

namespace StockCore.Interfaces.Views
{
    public interface IFindObjectView : IView
    {
        event EventHandler Filter;

        event EventHandler Select;

        string FilterEdit { get; set; }

        Objects SelectedObject { get; set; }

        List<Objects> Objects { get; set; }
    }
}