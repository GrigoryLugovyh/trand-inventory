using System.Collections.Generic;
using StockCore.Entity;

namespace StockCore.Interfaces.Views
{
    public interface IInformationObjectsView : IView
    {
        List<Objects> FoundObjects { get; set; }

        List<Objects> NotFoundObjects { get; set; }
    }
}