using System;

namespace StockCore.Interfaces.Views
{
    public interface IObjectParameters : IView
    {
        event EventHandler FindMol;

        event EventHandler SelectMol;

        event EventHandler FindUser;

        event EventHandler SelectUser;

        event EventHandler Next;

        string ObjectName { get; set; }

        string MolName { get; set; }

        string MolFilter { get; set; }

        string UserName { get; set; }

        string UserFilter { get; set; }
    }
}