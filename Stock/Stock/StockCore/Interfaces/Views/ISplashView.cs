namespace StockCore.Interfaces.Views
{
    public interface ISplashView : IView
    {
        string Version { get; set; }
    }
}