using System;

namespace StockCore.Interfaces.Views
{
    public interface ISearchMobilePrinterView : IView
    {
        object Printers { get; set; }

        object SelectedPrinter { get; }

        event EventHandler Use;

        bool ViewEnable { set; }
    }
}