using System;
using System.Collections.Generic;
using StockCore.Handlers.Printing;

namespace StockCore.Interfaces.Views
{
    public interface IPrintingView : IView
    {
        event EventHandler Print;

        event EventHandler Search;

        event EventHandler Cancel;

        event EventHandler PrintedChanged;

        List<PrinterEntry> Printers { get; set; }

        PrinterEntry SelectedPrinter { get; }

        bool SearchSettingVisible { set; }

        bool ViewEnable { set; }

        string Information { set; }
    }
}