using System;

namespace StockCore.Interfaces.Views
{
    /// <summary>
    /// ��������� ������ ������� �� ������ � ������� ������
    /// </summary>
    public interface IExchangeProcessView : IView
    {
        string Header { get; set; }

        string Received { get; set; }

        string Prepared { get; set; }

        string Processed { get; set; }

        string NotProcessed { get; set; }

        string ExchangeTime { get; set; }

        string ProcessText { get; set; }

        int Progress { get; set; }

        event EventHandler Abort;
    }
}