using System;
using System.Collections.Generic;
using StockCore.Entity;

namespace StockCore.Interfaces.Views
{
    public interface IObjectsView : IView
    {
        event EventHandler Filter;

        event EventHandler Add;

        event EventHandler Select;

        event EventHandler SelectedChanging;

        string SelectedObjectName { get; set; }

        string FilterEdit { get; set; }

        Objects SelectedObject { get; }

        List<Objects> Objects { get; set; }
    }
}