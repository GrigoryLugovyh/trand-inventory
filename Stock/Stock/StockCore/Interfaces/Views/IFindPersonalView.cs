using System;
using System.Collections.Generic;
using StockCore.Entity;

namespace StockCore.Interfaces.Views
{
    public interface IFindPersonalView<T> : IView where T : Personal
    {
        event EventHandler Select;

        event EventHandler SelectedChange;

        List<T> Personals { get; set; }

        T SelectPersonal { get; }
    }
}