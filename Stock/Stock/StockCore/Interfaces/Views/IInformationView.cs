using System;
using System.Collections.Generic;
using StockCore.Entity;

namespace StockCore.Interfaces.Views
{
    public interface IInformationView : IView
    {
        string Status { get; set; }

        string Barcode { get; set; }

        string ItemName { get; set; }

        string Ibso { get; set; }

        string UserName { get; set; }

        string MolName { get; set; }

        string Serial { get; set; }

        event EventHandler BarcodeEnterHand;

        event EventHandler ToInformation;

        event EventHandler ToAll;

        List<Objects> Objects { get; set; }

        Objects SelectedObject { get; }

        void ObjectsRefresh();

        event EventHandler SelectAllChanged;

        bool SelectAll { get; }

        string Filter { get; set; }

        event EventHandler Find;

        event EventHandler Open;

        event EventHandler PrintItem;

        event EventHandler PrintObjects;
    }
}