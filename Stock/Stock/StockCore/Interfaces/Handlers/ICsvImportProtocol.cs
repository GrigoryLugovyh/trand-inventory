﻿
namespace StockCore.Interfaces.Handlers
{
    public interface ICsvImportProtocol : IExchangeProtocol
    {
        /// <summary>
        /// Пересоздавать базу перед импортом
        /// </summary>
        bool ReCreateDatabase { get; }

        /// <summary>
        /// Файл для  импорта
        /// </summary>
        string ImportFile { get; }

        /// <summary>
        /// Признак полного импорта
        /// </summary>
        bool IsFullImport { get; }

        /// <summary>
        /// Кол-во записей в транзакции
        /// </summary>
        int TransactionNumberRecord { get; } 
    }
}