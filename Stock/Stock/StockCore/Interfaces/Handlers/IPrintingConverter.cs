using StockCore.Entity;
using StockCore.Enums;

namespace StockCore.Interfaces.Handlers
{
    public interface IPrintingConverter
    {
        byte[] ToByte(Objects @object, string pattern, EncodingEnum code);

        byte[] ToByte(Items item, string pattern, EncodingEnum code);
    }
}