namespace StockCore.Interfaces.Handlers
{
    public interface ICsvExportProtocol : IExchangeProtocol
    {
        /// <summary>
        /// ���� ������ ��� ��������
        /// </summary>
        string DatabasePath { get; }

        /// <summary>
        /// ��������� ������ ���������
        /// </summary>
        bool ExportOnlyFound { get; }

        /// <summary>
        /// ��������� ������ �����
        /// </summary>
        bool ExportOnlyNew { get; }
    }
}