namespace StockCore.Interfaces.Handlers
{
    public interface IPrinter
    {
        void Send(byte[] label);
    }
}