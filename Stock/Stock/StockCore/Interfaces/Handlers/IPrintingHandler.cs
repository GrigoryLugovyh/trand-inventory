using StockCore.Entity;
using StockCore.Enums;

namespace StockCore.Interfaces.Handlers
{
    public interface IPrintingHandler
    {
        void Print(Objects[] objects, IPrinter printer, IPrintingConverter converter, string pattern, EncodingEnum encoding);

        void Print(Objects @object, IPrinter printer, IPrintingConverter converter, string pattern, EncodingEnum encoding);

        void Print(Items[] items, IPrinter printer, IPrintingConverter converter, string pattern, EncodingEnum encoding);

        void Print(Items item, IPrinter printer, IPrintingConverter converter, string pattern, EncodingEnum encoding);
    }
}