using StockCore.Enums;

namespace StockCore.Interfaces.Handlers
{
    public interface IStatusHandlerProtocol
    {
        /// <summary>
        /// ������� ������
        /// </summary>
        StatusesEnum CurrentStatus { get; }

        /// <summary>
        /// ���������� �������� � ������ �������� �����
        /// </summary>
        bool NeedIncludeDoubleStatus { get; }

        /// <summary>
        /// ���������� �������� �������� ��� ����� ����� ������� �� "������ ��"
        /// </summary>
        bool NeedPrintItemLable { get; }
    }
}