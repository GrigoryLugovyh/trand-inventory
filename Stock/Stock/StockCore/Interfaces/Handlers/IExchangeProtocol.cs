﻿namespace StockCore.Interfaces.Handlers
{
    /// <summary>
    /// Протокол обмена
    /// </summary>
    public interface IExchangeProtocol
    {
        /// <summary>
        ///  Останавливать процесс, если запить не была подготовлена
        /// </summary>
        bool StopWhenRecordNotPrepared { get; }

        /// <summary>
        /// Останавливать процесс, если запись не была импортирована
        /// </summary>
        bool StopWhenRecordNotProcessed { get; }
    }
}