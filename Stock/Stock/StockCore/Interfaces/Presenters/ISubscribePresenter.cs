namespace StockCore.Interfaces.Presenters
{
    public interface ISubscribePresenter
    {
        double Priority { get; }

        void Subscription();

        void Unsubscription();
    }
}