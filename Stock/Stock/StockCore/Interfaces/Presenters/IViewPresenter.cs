namespace StockCore.Interfaces.Presenters
{
    public interface IViewPresenter
    {
        void ShowView();

        void CloseView();
    }
}