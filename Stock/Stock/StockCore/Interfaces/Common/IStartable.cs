﻿using StockCore.Enums;

namespace StockCore.Interfaces.Common
{
    public interface IStartable
    {
        StartableEnum Status { get; }

        void Start();

        void Stop();

        void Restart();
    }
}