using StockCore.Enums;

namespace StockCore.Interfaces.Common
{
    /// <summary>
    /// ���������� �����������
    /// </summary>
    public interface IMessager
    {
        DialogEnum ShowExclamation(string message, string btn);

        DialogEnum ShowQuestion(string message, DialogEnum result, string btnYes, string bntNo);
    }
}