namespace StockCore.Interfaces.Common
{
    public interface IInicializable
    {
        void Init(params object[] args);
    }
}