﻿using System.Collections.Generic;

namespace StockCore.Handlers.Infrastructure
{
    public sealed class RandomHandler
    {
        private static readonly object Sync = new object();

        private static volatile RandomHandler _instance;

        public static RandomHandler Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (Sync)
                    {
                        if (_instance == null)
                        {
                            _instance = new RandomHandler();
                        }
                    }
                }

                return _instance;
            }
        }

        private int _increment;

        private readonly Dictionary<string, string> _buffer = new Dictionary<string, string>();

        public string Get(string prefix, string id)
        {
            string value;
            if (!_buffer.TryGetValue(id, out value))
            {
                _increment++;
                value = string.Format("{0}{1}", prefix, _increment);
                _buffer.Add(id, value);
            }

            return value;
        }

        public void Reset()
        {
            _buffer.Clear();
            _increment = 0;
        }
    }
}