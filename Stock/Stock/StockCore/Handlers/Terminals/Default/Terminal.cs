using StockCore.Interfaces.Terminal;

namespace StockCore.Handlers.Terminals.Default
{
    public class Terminal : ITerminal
    {
        public void Init()
        {
            Scaner = new Scaner();
        }

        public IScaner Scaner { get; protected set; }

        public void Dispose()
        {

        }
    }
}