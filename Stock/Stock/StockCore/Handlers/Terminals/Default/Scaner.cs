using StockCore.Interfaces.Terminal;

namespace StockCore.Handlers.Terminals.Default
{
    public class Scaner : IScaner
    {
        public bool IsActive { get; protected set; }

        public void Start()
        {
            IsActive = true;
        }

        public void Stop()
        {
            IsActive = false;
        }

        public event ScanDelegate Scanned;

        public void Dispose()
        {

        }
    }
}