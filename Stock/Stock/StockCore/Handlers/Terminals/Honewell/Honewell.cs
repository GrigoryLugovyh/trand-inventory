using StockCore.Interfaces.Terminal;

namespace StockCore.Handlers.Terminals.Honewell
{
    public class Honewell : ITerminal
    {
        public IScaner Scaner { get; protected set; }

        public void Init()
        {
            
        }

        public void Dispose()
        {
            
        }
    }
}