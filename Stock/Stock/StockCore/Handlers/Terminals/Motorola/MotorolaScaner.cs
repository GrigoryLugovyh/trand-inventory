using System;
using StockCore.Interfaces.Terminal;
using Symbol;
using Symbol.Barcode;

namespace StockCore.Handlers.Terminals.Motorola
{
    public class MotorolaScaner : IScaner
    {
		private readonly Reader _reader;

		private readonly ReaderData _readerData;

		private bool _started;

        public MotorolaScaner()
		{
			_reader = new Reader();
			_readerData = new ReaderData(ReaderDataTypes.Text, ReaderDataLengths.MaximumLabel);

			//_reader.Actions.Enable();
		}

        public bool IsActive { get; protected set; }

	    public void Start()
		{
			if (!_started)
			{
				_reader.ReadNotify += ReaderReadNotify;
                _started = IsActive = true;
                _reader.Actions.Enable();
			}
			if (!_readerData.IsPending)
			{
				_reader.Actions.Read(_readerData);
			}
		}

		public void Stop()
		{
			if (_started)
			{
				_reader.ReadNotify -= ReaderReadNotify;
                _started = IsActive = false;
                _reader.Actions.Disable();
			}
		}

		public event ScanDelegate Scanned;

		private void ReaderReadNotify(object sender, EventArgs e)
		{
			ReaderData data = _reader.GetNextReaderData();

			if (data.Result == Results.SUCCESS)
			{
				if (Scanned != null)
				{
					Scanned(data.Text);
				}
			}

			Start();
		}

		public void Dispose()
		{
			_reader.Actions.Dispose();
			_reader.Dispose();
			_readerData.Dispose();
		}
    }
}