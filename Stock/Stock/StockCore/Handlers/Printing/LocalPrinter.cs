﻿using System.Net;

namespace StockCore.Handlers.Printing
{
    public class LocalPrinter : DesktopPrinter
    {
        private IPHostEntry _host;
        protected override IPHostEntry Host
        {
            get
            {
                if (_host == null)
                {
                    _host = Dns.GetHostEntry(Constants.LocalIp);
                }

                return _host;
            }
        }
    }
}