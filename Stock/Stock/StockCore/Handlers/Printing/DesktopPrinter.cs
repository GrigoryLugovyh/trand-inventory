using System.Net;
using System.Net.Sockets;
using StockCore.Interfaces.Handlers;

namespace StockCore.Handlers.Printing
{
    public class DesktopPrinter : IPrinter
    {
        private IPHostEntry _host;
        protected virtual IPHostEntry Host
        {
            get
            {
                if(_host == null)
                {
                    _host = Dns.GetHostEntry(Model.Instance.Config.CurrentConfig.PrinterIp);
                }

                return _host;
            }
        }

        private IPEndPoint _point; 
        protected IPEndPoint EndPoint
        {
            get
            {
                if(_point == null)
                {
                    _point = new IPEndPoint(Host.AddressList[0], Model.Instance.Config.CurrentConfig.PrinterPort);
                }

                return _point;
            }
        }

        public void Send(byte[] label)
        {
            using (var socket = new Socket(EndPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp))
            {
                socket.Connect(EndPoint);
                socket.Send(label);
                socket.Close();
            }
        }
    }
}