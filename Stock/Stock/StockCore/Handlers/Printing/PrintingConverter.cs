using System;
using System.IO;
using System.Text;
using StockCore.Entity;
using StockCore.Enums;
using StockCore.Factories;
using StockCore.Interfaces.Handlers;

namespace StockCore.Handlers.Printing
{
    public class PrintingConverter : IPrintingConverter
    {
        protected string Clear(string lable)
        {
            return lable.Replace("\\", " ").Replace("\"", "\\\"");
        }

        protected string GetPattern(string pattern, Encoding encoding)
        {
            if (!File.Exists(pattern))
            {
                pattern = Path.Combine(Model.Instance.Common.CurrentDirectory, pattern);

                if (!File.Exists(pattern))
                {
                    throw new Exception("������ �������� ���� � ������� ��������.");
                }
            }

            string lable;

            using (TextReader reader = new StreamReader(pattern, encoding))
            {
                lable = reader.ReadToEnd();
            }

            return lable;
        }

        private string _objectLable;
        private Encoding _objectEncoding;

        public byte[] ToByte(Objects @object, string pattern, EncodingEnum code)
        {
            if (_objectEncoding == null)
            {
                _objectEncoding = EncodingFactory.Instance.GetInstance(code);
            }

            if(string.IsNullOrEmpty(_objectLable))
            {
                _objectLable = GetPattern(pattern, _objectEncoding);
            }

            var label = _objectLable;

            label = label.Replace("[SKOD]", @object.PobjectBarcode);

            label = label.Replace("[V_DATE]", string.Empty);

            string name = Clear(@object.PobjectName);

            if (name.Length <= 33)
            {
                label = label.Replace("[V_FIRST_NAME]", name);
                label = label.Replace("[V_LAST_NAME]", string.Empty);
            }
            else
            {
                label = label.Replace("[V_FIRST_NAME]", name.Substring(0, 33));

                label = label.Replace("[V_LAST_NAME]", name.Length > 66 ? name.Substring(33, 33) : name.Substring(33));
            }

            return _objectEncoding.GetBytes(label);
        }

        private string _itemLable;
        private Encoding _itemEncoding;

        public byte[] ToByte(Items item, string pattern, EncodingEnum code)
        {
            if (_itemEncoding == null)
            {
                _itemEncoding = EncodingFactory.Instance.GetInstance(code);
            }

            if(string.IsNullOrEmpty(_itemLable))
            {
                _itemLable = GetPattern(pattern, _itemEncoding);
            }

            var label = _itemLable;

            label = label.Replace("[SKOD]", item.Id);
            label = label.Replace("[V_DATE]", item.BeginDate.ToString("dd.MM.yyyy"));

            string name = Clear(item.Name);

            if (name.Length <= 33)
            {
                label = label.Replace("[V_FIRST_NAME]", name);
                label = label.Replace("[V_LAST_NAME]", string.Empty);
            }
            else
            {
                label = label.Replace("[V_FIRST_NAME]", name.Substring(0, 33));

                label = label.Replace("[V_LAST_NAME]", item.Name.Length > 66 ? name.Substring(33, 33) : name.Substring(33));
            }

            return _itemEncoding.GetBytes(label);
        }
    }
}