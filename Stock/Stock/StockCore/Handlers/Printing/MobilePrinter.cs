using System;
using StockCore.Interfaces.Handlers;
using ZSDK_API.Comm;

namespace StockCore.Handlers.Printing
{
    public class MobilePrinter : IPrinter
    {
        public void Send(byte[] label)
        {
            if (string.IsNullOrEmpty(Model.Instance.Config.CurrentConfig.PortableAddress))
            {
                throw new Exception("�� ������ ����� ���������� ��������");
            }

            var address = Model.Instance.Config.CurrentConfig.PortableAddress;

            var connect = new BluetoothPrinterConnection(address);

            connect.Open();

            if (connect.IsConnected())
            {
                connect.Write(label);

                connect.Close();
            }
            else
            {
                throw new Exception("�� ������� ������������ � ���������� ��������");
            }
        }
    }
}