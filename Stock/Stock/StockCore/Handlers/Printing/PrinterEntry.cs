using StockCore.Entity;
using StockCore.Enums;

namespace StockCore.Handlers.Printing
{
    public class PrinterEntry : StockEntity
    {
        public string Name { get; set; }

        public PrinterEnum Type { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}