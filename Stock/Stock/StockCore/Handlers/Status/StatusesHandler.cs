using System.Linq;
using System.Collections.Generic;
using StockCore.Entity;
using StockCore.Enums;
using StockCore.Interfaces.Handlers;

namespace StockCore.Handlers.Status
{
    /// <summary>
    /// ���������� ��������
    /// </summary>
    public class StatusesHandler
    {
        protected readonly IStatusHandlerProtocol Protocol;

        public StatusesHandler(IStatusHandlerProtocol protocol)
        {
            Protocol = protocol;
        }

        protected Items @Item
        {
            get { return Model.Instance.Session.CurrentItem; }
        }

        protected List<Statuses> Statuses;

        public List<Statuses> Load()
        {
            if (Statuses == null || Statuses.Count == 0)
            {
                Statuses = new List<Statuses>();
                Statuses.AddRange(Model.Instance.Data.Statuses);

                if (Protocol.CurrentStatus == StatusesEnum.Double)
                {
                    Statuses.Remove(Statuses.FirstOrDefault(s => s.Id == (int)StatusesEnum.Defective));
                    Statuses.Remove(Statuses.FirstOrDefault(s => s.Id == (int)StatusesEnum.Replacement));
                }
                else
                {
                    if (!Protocol.NeedIncludeDoubleStatus)
                    {
                        Statuses.Remove(Statuses.FirstOrDefault(s => s.Id == (int) StatusesEnum.Double));
                    }
                }
            }

            return Statuses;
        }

        /// <summary>
        /// ������ ����� ��������� ������� ���
        /// </summary>
        /// <param name="newStatus"></param>
        /// <returns></returns>
        public bool ChangeStatus(Statuses newStatus)
        {
            if (newStatus == null || newStatus.Id == int.Parse(@Item.Status))
                return true;

            // ������ ������ � ����� 
            if (Item.StatusEnum == StatusesEnum.Double)
            {
                if (Model.Instance.Messages.ShowQuestion("��� ��� �� �������� \"����� ��\" ��� ������� ��� ����� �������. �� ������������� ������ ������� ������?", DialogEnum.No, "��", "���") != DialogEnum.Yes)
                    return false;

                // ������� ��� �����
                if(!Model.Instance.Data.DeleteDoubles(@Item.Id))
                    return false;
            }

            @Item.Status = newStatus.Id.ToString();
            return Model.Instance.Data.UpdateItem(@Item);
        }
    }
}