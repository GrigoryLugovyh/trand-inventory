using StockCore.Enums;
using StockCore.Interfaces.Handlers;

namespace StockCore.Handlers.Status
{
    public class StatusesHandlerProtocol : IStatusHandlerProtocol
    {
        public StatusesHandlerProtocol(StatusesEnum status, bool needIncludeDoubleStatusm, bool needPrintItemLable)
        {
            NeedIncludeDoubleStatus = needIncludeDoubleStatusm;
            NeedPrintItemLable = needPrintItemLable;
            CurrentStatus = status;
        }

        public StatusesEnum CurrentStatus { get; protected set; }

        public bool NeedIncludeDoubleStatus { get; protected set; }

        public bool NeedPrintItemLable { get; protected set; }

    }
}