﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using StockCore.Entity;
using StockCore.Enums;
using StockCore.Handlers.Exchange.Configuration.CSV;
using StockCore.Handlers.Exchange.Imports.Commands;
using StockCore.Handlers.Infrastructure;
using StockCore.Interfaces.Handlers;

namespace StockCore.Handlers.Exchange.Imports
{
    /// <summary>
    ///     Импорт из фалов CVS, базовый механизм импорта (при необходимости отнаследовать)
    /// </summary>
    public class CsvImport : Exchange
    {
        public CsvImport(ICsvImportProtocol handler, CsvExchangeSettings settings)
            : base(handler, settings)
        {
            RecordCounter = 0;
        }

        /*
 * Обработчики импорта
 */

        /// <summary>
        ///     Импорт ТМЦ
        /// </summary>
        public ImportCommandProcessor<Items> ItemsProcessor { get; protected set; }

        /// <summary>
        ///     Импорт мест хранения
        /// </summary>
        public ImportCommandProcessor<Objects> ObjectsProcessor { get; protected set; }

        /// <summary>
        ///     Импорт людей
        /// </summary>
        public ImportCommandProcessor<Users> UsersProcessor { get; protected set; }

        /// <summary>
        ///     Импорт материально ответственных лиц
        /// </summary>
        public ImportCommandProcessor<Mols> MolsProcessor { get; protected set; }

        /// <summary>
        ///     Импорт кодов
        /// </summary>
        public ImportCommandProcessor<Codes> CodesProcessor { get; protected set; }

        /// <summary>
        ///     Импорт статусов
        /// </summary>
        public ImportCommandProcessor<Statuses> StatusesProcessor { get; protected set; }

        /// <summary>
        ///     Счетчик кол-ва записей в одной транзакции
        /// </summary>
        protected int RecordCounter { get; set; }

/*
 * *******************
 */

        /// <summary>
        ///     Обработчик импорта
        /// </summary>
        public CsvFullImportProcessor ImportProcessor { get; set; }

        /// <summary>
        ///     Признак полного импорта
        /// </summary>
        public bool IsFullImport { get; protected set; }

        /// <summary>
        ///     Кол-во записей в одной транзакции
        /// </summary>
        public int TransactionRecordCounter { get; protected set; }

        /// <summary>
        ///     Пересоздать базу данных
        /// </summary>
        public bool ReCreateDatabase { get; protected set; }

        /// <summary>
        ///     Путь до файла импорта
        /// </summary>
        public string FilePath { get; protected set; }

        /// <summary>
        ///     Разделитель полей
        /// </summary>
        public char[] Separator { get; protected set; }

        /// <summary>
        ///     Кодировка
        /// </summary>
        public Encoding Encoding { get; protected set; }

        /// <summary>
        ///     Настройки файла загрузки
        /// </summary>
        public CsvFile InputData { get; protected set; }

        /// <summary>
        ///     Статусы
        /// </summary>
        public List<Statuses> Statuses { get; protected set; }

        /// <summary>
        ///     Префиксы
        /// </summary>
        public List<CsvPrefix> Prefixes { get; protected set; }

        /// <summary>
        ///     Информаци о файле импорта
        /// </summary>
        public FileInfo Fileinfo { get; protected set; }

        /// <summary>
        ///     Поток чтения к файлу
        /// </summary>
        public StreamReader FileReader { get; protected set; }

        /// <summary>
        ///     Текущая считанная строка из файла в виде сырых данных
        /// </summary>
        public string RawLine { get; protected set; }

        public override string ExchangeFriandlyName
        {
            get { return "Загрузка данных"; }
        }

        public override string ExchangeProcessStatus
        {
            get { return "Загрузка ..."; }
        }

        public override string ExchangeProcessFail
        {
            get { return "Загрузка завершена с ошибками"; }
        }

        public override string ExchangeProcessSuccessfully
        {
            get { return "Загрузка завершена успешно"; }
        }

        /// <summary>
        ///     Доп. информация о каждой импортируемой позиции
        /// </summary>
        /// <returns></returns>
        protected override string CurrendRecordInformation()
        {
            if (!string.IsNullOrEmpty(RawLine))
            {
                return RawLine;
            }

            return null;
        }

        /// <summary>
        ///     Подготавливаем параметры для импорта
        /// </summary>
        protected override void InitExchange()
        {
            // получаем настройки

            if (!(Handler is ICsvImportProtocol))
                throw new Exception("Протокол обмена не соответствует реализации загрузки");

            if (!(Settings is CsvExchangeSettings))
                throw new Exception("Настройки не соответствуют реализации загрузки");

            var handler = Handler as ICsvImportProtocol;

            FilePath = handler.ImportFile;
            IsFullImport = handler.IsFullImport;
            TransactionRecordCounter = handler.TransactionNumberRecord > 1 ? handler.TransactionNumberRecord : 0;
            ReCreateDatabase = handler.ReCreateDatabase;

            var settings = Settings as CsvExchangeSettings;

            Separator = settings.ImportFile.Separator.ToCharArray();
            Encoding = settings.ImportFile.CsvEncoding;
            InputData = settings.ImportFile;
            Statuses = settings.Stauses;
            Prefixes = settings.Prefixes;

            if (InputData.Elements == null || InputData.Elements.Count < 1)
                throw new Exception("Нет полей для импорта");

            // ищем обработчик
            ImportProcessor = CsvImportFactory.Instance.GetProcessor(this);

            if (ImportProcessor == null)
                throw new Exception("Не удалось установить обработчик импорта данных");

            // подготавливаем импорт
            ImportProcessor.Prepare();

            // создаем обработчики для команд
            ItemsProcessor = (ImportCommandProcessor<Items>) ImportProcessor.CommandProcessor<Items>();
            ObjectsProcessor = (ImportCommandProcessor<Objects>) ImportProcessor.CommandProcessor<Objects>();
            UsersProcessor = (ImportCommandProcessor<Users>) ImportProcessor.CommandProcessor<Users>();
            MolsProcessor = (ImportCommandProcessor<Mols>) ImportProcessor.CommandProcessor<Mols>();
            CodesProcessor = (ImportCommandProcessor<Codes>) ImportProcessor.CommandProcessor<Codes>();
            StatusesProcessor = (ImportCommandProcessor<Statuses>) ImportProcessor.CommandProcessor<Statuses>();
        }

        /// <summary>
        ///     Завершаем импорт
        /// </summary>
        protected override void FinishExchange()
        {
            ImportProcessor.Finish();


            var csvImportProtocol = Handler as ICsvImportProtocol;
            if (csvImportProtocol != null && csvImportProtocol.ReCreateDatabase)
            {
                // обновляем кэш
                Model.Instance.Data.UpdateCash();
            }
        }

        protected override bool Connect()
        {
            if (!File.Exists(FilePath))
                throw new Exception(string.Format("Не найден файл \"{0}\"", FilePath));

            Fileinfo = new FileInfo(FilePath);

            // выставляем атрибут только на чтение
            Fileinfo.Attributes &= FileAttributes.ReadOnly;

            // создаем поток импорта
            FileReader = new StreamReader(FilePath, Encoding);

            return true;
        }

        /// <summary>
        ///     Кол-во импортированой инфы, от общего размера
        /// </summary>
        /// <returns></returns>
        protected override int PersentProcessed()
        {
            if (FileReader != null)
            {
                return (int) (((double) (FileReader.BaseStream.Position*100)/(FileReader.BaseStream.Length)));
            }

            return -1;
        }

        /// <summary>
        ///     Доступность записи для импорта
        /// </summary>
        /// <returns></returns>
        protected override bool ExistRecord()
        {
            if (FileReader != null)
            {
                return (RawLine = FileReader.ReadLine()) != null;
            }

            return false;
        }

        /// <summary>
        ///     Подготовка строки к импорту
        /// </summary>
        /// <returns></returns>
        protected override bool PrepareRecord()
        {
            if (string.IsNullOrEmpty(RawLine.Trim()))
                return false;

            return !string.IsNullOrEmpty(RawLine) && ImportProcessor.PrepareRecoed();
        }

        /// <summary>
        ///     Вставка строки, основное действие импорта данных
        /// </summary>
        /// <returns></returns>
        protected override bool ProcessRecord()
        {
            if (RecordCounter == 0 && ExchangeConnection != null && ExchangeConnection.State == ConnectionState.Open)
            {
                // начинаем таранзакцию
                ExchangeTransaction = ExchangeConnection.BeginTransaction();

                // готовим команды
                ImportProcessor.BindCommands();
            }

            bool imported = ImportProcessor.ProcessRecord();

            RecordCounter++;

            if (RecordCounter >= TransactionRecordCounter || FileReader.EndOfStream)
            {
                // фиксируем транзакцию, обнуляем счетчик
                ExchangeTransaction.Commit();

                RecordCounter = 0;
            }

            return imported;
        }

        protected override void PreExchange()
        {
        }

        protected override void PostExchange()
        {
            // импортируем статусы
            if (Statuses != null && Statuses.Count > 0)
            {
                ExchangeTransaction = ExchangeConnection.BeginTransaction();

                StatusesProcessor.BindParameters();

                foreach (Statuses status in Statuses)
                {
                    try
                    {
                        StatusesProcessor.Command.Parameters["@Id"].Value = status.Id;
                        StatusesProcessor.Command.Parameters["@Name"].Value = status.Name;
                        StatusesProcessor.Command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        Model.Instance.Log(LogsEnum.Exchange, false, "Ошибка вставки статуса: \"{0}\"\r\n Строка: {1}",
                            ex.Message, status.Name);
                    }
                }

                ExchangeTransaction.Commit();
            }

            RandomHandler.Instance.Reset();
        }

        protected override bool Disconnect()
        {
            // завершаем поток
            if (FileReader != null)
            {
                FileReader.Close();
                FileReader.Dispose();
            }

            // убираем атрибут на чтение
            if (Fileinfo != null)
            {
                if ((Fileinfo.Attributes & FileAttributes.ReadOnly) != FileAttributes.ReadOnly)
                {
                    Fileinfo.Attributes &= ~FileAttributes.ReadOnly;
                }
            }

            return true;
        }
    }
}