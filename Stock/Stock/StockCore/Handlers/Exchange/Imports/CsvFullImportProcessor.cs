﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SQLite;
using System.IO;
using StockCore.Entity;
using StockCore.Enums;
using StockCore.Handlers.Exchange.Configuration;
using StockCore.Handlers.Exchange.Imports.Commands;
using StockCore.Handlers.Infrastructure;

namespace StockCore.Handlers.Exchange.Imports
{
    /// <summary>
    /// Полный импорт данных
    /// </summary>
    public class CsvFullImportProcessor
    {
        public CsvFullImportProcessor(CsvImport handler)
        {
            Handler = handler;
        }

        /// <summary>
        /// Текущие данные для импорта
        /// </summary>
        public string[] CurrentRecord { get; protected set; }

        /// <summary>
        /// Основной обработчик импорта данных
        /// </summary>
        protected CsvImport Handler { get; set; }

        /// <summary>
        /// Команда на вставку ТМЦ
        /// </summary>
        public virtual ImportCommandsEnum Items
        {
            get { return ImportCommandsEnum.InsertItemses; }
        }

        /// <summary>
        /// Команда на вставку мест хранения
        /// </summary>
        public virtual ImportCommandsEnum Objects
        {
            get { return ImportCommandsEnum.InsertObjects; }
        }

        /// <summary>
        /// Команда на вставку людей
        /// </summary>
        public virtual ImportCommandsEnum Users
        {
            get { return ImportCommandsEnum.InsertUsers; }
        }

        /// <summary>
        /// Команда на вставку МОЛов
        /// </summary>
        public virtual ImportCommandsEnum Mols
        {
            get { return ImportCommandsEnum.InsertMols; }
        }

        /// <summary>
        /// Команда на вставку кодов
        /// </summary>
        public virtual ImportCommandsEnum Codes
        {
            get { return ImportCommandsEnum.InsertCodes; }
        }

        /// <summary>
        /// Подготовка
        /// </summary>
        public virtual void Prepare()
        {
            Model.Instance.Data.Stop();

            // нужно пересоздать базу данных
            if (Handler.ReCreateDatabase)
            {
                if (File.Exists(Model.Instance.Data.DatabasePath))
                {
                    File.Delete(Model.Instance.Data.DatabasePath);
                }

                // финт ушами (создание всех метаданных заново)
                Model.Instance.Data.Start();
                Model.Instance.Data.Stop();
            }
            else
            {
                Model.Instance.Data.Store = new SQLite.SQLiteConnection();
            }

            // настраиваем доп. параметры полного импорта
            ExistObjectBarcode = Handler.InputData.Elements.Any(e => e.ElementFiled == ExchangeField.PobjectBarcode);
            ExistUserCode = Handler.InputData.Elements.Any(e => e.ElementFiled == ExchangeField.UserNumber);
            ExistMolCode = Handler.InputData.Elements.Any(e => e.ElementFiled == ExchangeField.MolNumber);
            ExistBeginDate = Handler.InputData.Elements.Any(e => e.ElementFiled == ExchangeField.BeginDate);
            UseNativeFormat = Model.Instance.Config.CurrentConfig.UseNativeFormat;
            DefaultDate = new DateTime(1900, 1, 1);

            // создаем новое соединение, будем импортировать через интероп, так будет быстрее
            Handler.ExchangeConnection = new SQLiteConnection(string.Format(Constants.StockConnection, Model.Instance.Data.DatabasePath));
            Handler.ExchangeConnection.Open();
        }

        protected bool ExistObjectBarcode { get; set; }

        protected bool ExistUserCode { get; set; }

        protected bool ExistMolCode { get; set; }

        protected bool ExistBeginDate { get; set; }

        protected bool UseNativeFormat { get; set; }

        protected DateTime DefaultDate { get; set; }

        protected IDictionary<string, string> ObjectDictionary = new SortedList<string, string>();

        protected IDictionary<string, string> UserDictionary = new SortedList<string, string>();

        protected IDictionary<string, string> MolDictionary = new SortedList<string, string>();

        protected IDictionary<string, string> CodeDictionary = new SortedList<string, string>();

        protected Dictionary<string, string> BarcodesDictionary = new Dictionary<string, string>();

        /// <summary>
        /// Подготовка команд
        /// </summary>
        public virtual void BindCommands()
        {
            Handler.ItemsProcessor.BindParameters();
            Handler.ObjectsProcessor.BindParameters();
            Handler.UsersProcessor.BindParameters();
            Handler.MolsProcessor.BindParameters();
            Handler.CodesProcessor.BindParameters();
        }

        /// <summary>
        /// Завершение
        /// </summary>
        public virtual void Finish()
        {
            if(Handler.ExchangeConnection != null && Handler.ExchangeConnection.State != ConnectionState.Closed)
            {
                Handler.ExchangeConnection.Close();
            }

            if (Handler.ReCreateDatabase)
            {
                // запускаем окружение ОРМ
                Model.Instance.Data.Start();
            }
            else
            {
                Model.Instance.Data.Store = null;
            }
        }

        /// <summary>
        /// Обработчик команды
        /// </summary>
        public virtual object CommandProcessor<T>() where T : StockEntity
        {
            return ImportCommandsFactory.Instance.GetProcessor<T>(Handler);
        }

        /// <summary>
        /// Парсим строку в соотв. с настройками
        /// </summary>
        /// <returns></returns>
        public virtual bool PrepareRecoed()
        {
            // есть два варианта, когда строка позиционная или когда строка имеет разделитель
            if (Handler.Separator == null || Handler.Separator.Length == 0)
            {
                // без разделителя
                return PrepareRecordByPositions();
            }

            // через разделитель
            return PrepareRecordBySeparator();
        }

        /// <summary>
        /// Подготовка позиционной строки
        /// </summary>
        /// <returns></returns>
        protected virtual bool PrepareRecordByPositions()
        {
            int count = 0;
            int position = 0;

            CurrentRecord = new string[Handler.InputData.Elements.Count];

            foreach (var element in Handler.InputData.Elements)
            {
                if (position + element.CvsElementLength <= Handler.RawLine.Length)
                {
                    CurrentRecord[count] = Handler.RawLine.Substring(position, element.CvsElementLength);
                }
                else
                {
                    CurrentRecord[count] = null;
                }

                position += element.CvsElementLength;
                count++;
            }

            return CurrentRecord.Length > 0;
        }

        /// <summary>
        /// Подготовка строки с разделителями
        /// </summary>
        /// <returns></returns>
        protected virtual bool PrepareRecordBySeparator()
        {
            CurrentRecord = Handler.RawLine.Split(Handler.Separator);
            return CurrentRecord != null && CurrentRecord.Length > 0;
        }

        /// <summary>
        /// Импорт строки
        /// </summary>
        /// <returns></returns>
        public virtual bool ProcessRecord()
        {
            int errors = 0;

            string objectId, userId, molId, codeId = string.Empty;
            bool existObject, existUser, existMol, existCode = false;

            Handler.ItemsProcessor.FillParameters();
            Handler.ObjectsProcessor.FillParameters();
            Handler.UsersProcessor.FillParameters();
            Handler.MolsProcessor.FillParameters();
            Handler.CodesProcessor.FillParameters();

            if(!ExistBeginDate)
            {
                Handler.ItemsProcessor.Command.Parameters["@BeginDate"].Value = DefaultDate;
            }

            #region Обработка Items/Objects
            if (ExistObjectBarcode)
            {
                objectId = Handler.ItemsProcessor.Command.Parameters["@PobjectBarcode"].Value.ToString();
                if (string.IsNullOrEmpty(objectId))
                {
                    objectId = Handler.ItemsProcessor.Command.Parameters["@PobjectId"].Value.ToString();
                    Handler.ItemsProcessor.Command.Parameters["@PobjectBarcode"].Value = objectId;
                    Handler.ObjectsProcessor.Command.Parameters["@PobjectBarcode"].Value = objectId;
                    Handler.ObjectsProcessor.Command.Parameters["@UseBarcode"].Value = 0;
                }
                else
                {
                    Handler.ObjectsProcessor.Command.Parameters["@UseBarcode"].Value = 1;
                }

                if (!string.IsNullOrEmpty(objectId))
                {
                    existObject = !ObjectDictionary.ContainsKey(objectId);
                }
                else
                {
                    existObject = false;
                }
            }
            else
            {
                objectId = Handler.ItemsProcessor.Command.Parameters["@PobjectId"].Value.ToString();

                if (string.IsNullOrEmpty(objectId))
                {
                    existObject = false;
                    Handler.ItemsProcessor.Command.Parameters["@PobjectBarcode"].Value = string.Empty;
                }
                else
                {
                    existObject = !ObjectDictionary.ContainsKey(objectId);

                    if (existObject)
                    {
                        string objectBarcode = RandomHandler.Instance.Get("0", objectId);
                        Handler.ItemsProcessor.Command.Parameters["@PobjectBarcode"].Value = objectBarcode;
                        Handler.ObjectsProcessor.Command.Parameters["@PobjectBarcode"].Value = objectBarcode;
                    }
                    else
                    {
                        string existBarcode = ObjectDictionary[objectId];
                        Handler.ItemsProcessor.Command.Parameters["@PobjectBarcode"].Value = existBarcode;
                        Handler.ObjectsProcessor.Command.Parameters["@PobjectBarcode"].Value = existBarcode;
                    }

                    Handler.ObjectsProcessor.Command.Parameters["@UseBarcode"].Value = 0;
                }
            }
            #endregion

            #region Обработка Users
            if (ExistUserCode)
            {
                userId = Handler.UsersProcessor.Command.Parameters["@UserNumber"].Value.ToString();

                if (string.IsNullOrEmpty(userId))
                {
                    userId = Handler.UsersProcessor.Command.Parameters["@UserName"].Value.ToString().Trim();
                    if (string.IsNullOrEmpty(userId))
                    {
                        existUser = false;
                    }
                    else
                    {
                        existUser = !UserDictionary.ContainsKey(userId);

                        if (existUser)
                        {
                            string userNumber = RandomHandler.Instance.Get("u", userId);
                            Handler.ItemsProcessor.Command.Parameters["@UserNumber"].Value = userNumber;
                            Handler.UsersProcessor.Command.Parameters["@UserNumber"].Value = userNumber;
                        }
                        else
                        {
                            string existUserNumber = UserDictionary[userId];
                            Handler.ItemsProcessor.Command.Parameters["@UserNumber"].Value = existUserNumber;
                            Handler.UsersProcessor.Command.Parameters["@UserNumber"].Value = existUserNumber;
                        }
                    }

                    Handler.UsersProcessor.Command.Parameters["@UseNumber"].Value = 0;
                }
                else
                {
                    existUser = !UserDictionary.ContainsKey(userId);
                    Handler.UsersProcessor.Command.Parameters["@UseNumber"].Value = 1;
                }
            }
            else
            {
                if (Handler.UsersProcessor.Command.Parameters.Contains("@UserName"))
                {
                    userId = Handler.UsersProcessor.Command.Parameters["@UserName"].Value.ToString().Trim();

                    if (string.IsNullOrEmpty(userId))
                    {
                        existUser = false;
                        Handler.ItemsProcessor.Command.Parameters["@UserNumber"].Value = string.Empty;
                    }
                    else
                    {
                        existUser = !UserDictionary.ContainsKey(userId);

                        if (existUser)
                        {
                            string userNumber = RandomHandler.Instance.Get("u", userId);
                            Handler.ItemsProcessor.Command.Parameters["@UserNumber"].Value = userNumber;
                            Handler.UsersProcessor.Command.Parameters["@UserNumber"].Value = userNumber;
                        }
                        else
                        {
                            string existUserNumber = UserDictionary[userId];
                            Handler.ItemsProcessor.Command.Parameters["@UserNumber"].Value = existUserNumber;
                            Handler.UsersProcessor.Command.Parameters["@UserNumber"].Value = existUserNumber;
                        }

                        Handler.UsersProcessor.Command.Parameters["@UseNumber"].Value = 0;
                    }
                }
                else
                {
                    userId = string.Empty;
                    existUser = false;
                    Handler.ItemsProcessor.Command.Parameters["@UserNumber"].Value = string.Empty;
                }
            }
            #endregion

            #region Обработка Mols
            if (ExistMolCode)
            {
                molId = Handler.MolsProcessor.Command.Parameters["@MolNumber"].Value.ToString().Trim();

                if (string.IsNullOrEmpty(molId))
                {
                    molId = Handler.MolsProcessor.Command.Parameters["@MolName"].Value.ToString().Trim();

                    if (string.IsNullOrEmpty(molId))
                    {
                        existMol = false;
                    }
                    else
                    {
                        existMol = !MolDictionary.ContainsKey(molId);

                        if (existMol)
                        {
                            string molNumber = RandomHandler.Instance.Get("m", molId);
                            Handler.ItemsProcessor.Command.Parameters["@MolNumber"].Value = molNumber;
                            Handler.MolsProcessor.Command.Parameters["@MolNumber"].Value = molNumber;
                        }
                        else
                        {
                            string existMolNumber = MolDictionary[molId];
                            Handler.ItemsProcessor.Command.Parameters["@MolNumber"].Value = existMolNumber;
                            Handler.MolsProcessor.Command.Parameters["@MolNumber"].Value = existMolNumber;
                        }
                    }

                    Handler.MolsProcessor.Command.Parameters["@UseNumber"].Value = 0;
                }
                else
                {
                    existMol = !MolDictionary.ContainsKey(molId);
                    Handler.MolsProcessor.Command.Parameters["@UseNumber"].Value = 1;
                }
            }
            else
            {
                if (Handler.MolsProcessor.Command.Parameters.Contains("@MolName"))
                {
                    molId = Handler.MolsProcessor.Command.Parameters["@MolName"].Value.ToString().Trim();

                    if (string.IsNullOrEmpty(molId))
                    {
                        existMol = false;
                        Handler.ItemsProcessor.Command.Parameters["@MolNumber"].Value = string.Empty;
                    }
                    else
                    {

                        existMol = !MolDictionary.ContainsKey(molId);

                        if (existMol)
                        {
                            string molNumber = RandomHandler.Instance.Get("m", molId);
                            Handler.ItemsProcessor.Command.Parameters["@MolNumber"].Value = molNumber;
                            Handler.MolsProcessor.Command.Parameters["@MolNumber"].Value = molNumber;
                        }
                        else
                        {
                            string existMolNumber = MolDictionary[molId];
                            Handler.ItemsProcessor.Command.Parameters["@MolNumber"].Value = existMolNumber;
                            Handler.MolsProcessor.Command.Parameters["@MolNumber"].Value = existMolNumber;
                        }

                        Handler.MolsProcessor.Command.Parameters["@UseNumber"].Value = 0;
                    }
                }
                else
                {
                    molId = string.Empty;
                    existMol = false;
                    Handler.ItemsProcessor.Command.Parameters["@MolNumber"].Value = string.Empty;
                }
            }
            #endregion

            if (UseNativeFormat && Handler.InputData.Elements.Any(e => e.ElementFiled == ExchangeField.Id))
            {
                string itemBarcode = Handler.ItemsProcessor.Command.Parameters["@Id"].Value.ToString();

                string code = string.Empty;

                if (itemBarcode.IndexOf('.') > -1)
                {
                    code = itemBarcode.Substring(itemBarcode.LastIndexOf(".", StringComparison.Ordinal) + 1);
                }

                if (string.IsNullOrEmpty(code))
                {
                    codeId = string.Empty;
                }
                else
                {
                    if (!CodeDictionary.ContainsKey(code))
                    {
                        existCode = true;
                        Handler.CodesProcessor.Command.Parameters["@Code"].Value = code;
                        codeId = code;
                    }
                }
            }

            // ТМЦ
            try
            {
                Handler.ItemsProcessor.CheckNull("@Found", 0);
                Handler.ItemsProcessor.CheckNull("@Status", "0");

                Handler.ItemsProcessor.Command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Model.Instance.Log(LogsEnum.Exchange, false, "Ошибка вставки ТМЦ: \"{0}\"\r\n Строка: {1}", ex.Message, Handler.RawLine);
                errors++;
            }

            // Объекты
            try
            {
                Handler.ObjectsProcessor.CheckNull("@Found", 0);
                Handler.ObjectsProcessor.CheckNull("@UseBarcode", 0);

                if (existObject)
                {
                    Handler.ObjectsProcessor.Command.ExecuteNonQuery();
                    ObjectDictionary.Add(objectId, Handler.ObjectsProcessor.Command.Parameters["@PobjectBarcode"].Value.ToString());
                }
            }
            catch (Exception ex)
            {
                Model.Instance.Log(LogsEnum.Exchange, false, "Ошибка вставки объектов: \"{0}\"\r\n Строка: {1}", ex.Message, Handler.RawLine);
                errors++;
            }

            // Пользователи
            try
            {
                Handler.UsersProcessor.CheckNull("@UseNumber", 0);
                Handler.UsersProcessor.CheckNull("@UserMcCreated", 0);

                if (existUser)
                {
                    Handler.UsersProcessor.Command.ExecuteNonQuery();
                    UserDictionary.Add(userId, Handler.UsersProcessor.Command.Parameters["@UserNumber"].Value.ToString());
                }
            }
            catch (Exception ex)
            {
                Model.Instance.Log(LogsEnum.Exchange, false, "Ошибка вставки пользователей: \"{0}\"\r\n Строка: {1}", ex.Message, Handler.RawLine);
                errors++;
            }

            // Молы
            try
            {
                Handler.MolsProcessor.CheckNull("@UseNumber", 0);
                Handler.MolsProcessor.CheckNull("@MolMcCreated", 0);

                if (existMol)
                {
                    Handler.MolsProcessor.Command.ExecuteNonQuery();
                    MolDictionary.Add(molId, Handler.MolsProcessor.Command.Parameters["@MolNumber"].Value.ToString());
                }
            }
            catch (Exception ex)
            {
                Model.Instance.Log(LogsEnum.Exchange, false, "Ошибка вставки молов: \"{0}\"\r\n Строка: {1}", ex.Message, Handler.RawLine);
                errors++;
            }

            // Коды
            try
            {
                if (existCode && UseNativeFormat)
                {
                    Handler.CodesProcessor.Command.ExecuteNonQuery();
                    CodeDictionary.Add(codeId, codeId);
                }
            }
            catch (Exception ex)
            {
                Model.Instance.Log(LogsEnum.Exchange, false, "Ошибка вставки кода: \"{0}\"\r\n Строка: {1}", ex.Message, Handler.RawLine);
                errors++;
            }

            return errors == 0;
        }
    }
}