using StockCore.Entity;

namespace StockCore.Handlers.Exchange.Imports.Commands
{
    public class UpdateUsersCommandProcessor : UpdateCommandProcessor<Users>
    {
        public UpdateUsersCommandProcessor(CsvImport handler)
            : base(handler)
        {
        }

        protected override string ByUpdateFiled
        {
            get { return "UserNumber"; }
        }
    }
}