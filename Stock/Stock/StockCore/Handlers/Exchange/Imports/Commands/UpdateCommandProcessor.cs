namespace StockCore.Handlers.Exchange.Imports.Commands
{
    public abstract class UpdateCommandProcessor<T> : ImportCommandProcessor<T>
    {
        protected UpdateCommandProcessor(CsvImport handler)
            : base(handler)
        {
        }

        protected abstract string ByUpdateFiled { get; }

        protected override string CommandBuilder()
        {
            return Model.Instance.Data.Store.GetUpdateCommandExt(typeof (T), ByUpdateFiled);
        }
    }
}