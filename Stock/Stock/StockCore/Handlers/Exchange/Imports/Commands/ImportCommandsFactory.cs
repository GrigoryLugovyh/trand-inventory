﻿using System;
using StockCore.Entity;

namespace StockCore.Handlers.Exchange.Imports.Commands
{
    /// <summary>
    /// Фабрика команд импорта
    /// </summary>
    public sealed class ImportCommandsFactory
    {
        private static readonly object Sync = new object();

        private static volatile ImportCommandsFactory _instance;

        /// <summary>
        /// Инициализация
        /// </summary>
        public static ImportCommandsFactory Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (Sync)
                    {
                        if (_instance == null)
                        {
                            _instance = new ImportCommandsFactory();
                        }
                    }
                }

                return _instance;
            }
        }

        public object GetProcessor<T>(CsvImport handler)
        {
            object processor;

            if (typeof(T) == typeof(Items))
            {
                if (handler.IsFullImport)
                {
                    processor = new InsertItemsCommandProcessor(handler);
                }
                else
                {
                    processor = new UpdateItemsCommandProcessor(handler);
                }
            }
            else if (typeof(T) == typeof(Objects))
            {
                if (handler.IsFullImport)
                {
                    processor = new InsertObjectsCommandProcessor(handler);
                }
                else
                {
                    processor = new UpdateObjectsCommandProcessor(handler);
                }
            }
            else if (typeof(T) == typeof(Users))
            {
                if (handler.IsFullImport)
                {
                    processor = new InsertUsersCommandProcessor(handler);
                }
                else
                {
                    processor = new UpdateUsersCommandProcessor(handler);
                }
            }
            else if (typeof(T) == typeof(Mols))
            {
                if (handler.IsFullImport)
                {
                    processor = new InsertMolsCommandProcessor(handler);
                }
                else
                {
                    processor = new UpdateMolsCommandProcessor(handler);
                }
            }
            else if (typeof(T) == typeof(Codes))
            {
                if (handler.IsFullImport)
                {
                    processor = new InsertCodesCommandProcessor(handler);
                }
                else
                {
                    processor = new UpdateCodesCommandProcessor(handler);
                }
            }
            else if (typeof(T) == typeof(Statuses))
            {
                if (handler.IsFullImport)
                {
                    processor = new ImportStatusCommandProcessor(handler);
                }
                else
                {
                    processor = new UpdateStatusCommandProcessor(handler);
                }
            }
            else
            {
                throw new Exception("Не найден обработчик для команды указаного типа");
            }

            return processor;
        }
    }
}