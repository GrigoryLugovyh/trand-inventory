﻿using StockCore.Entity;

namespace StockCore.Handlers.Exchange.Imports.Commands
{
    public class InsertMolsCommandProcessor : ImportCommandProcessor<Mols>
    {
        public InsertMolsCommandProcessor(CsvImport handler)
            : base(handler)
        {
        }

        protected override ImportCommandsEnum ImportCommandType
        {
            get { return ImportCommandsEnum.InsertMols; }
        }
    }
}