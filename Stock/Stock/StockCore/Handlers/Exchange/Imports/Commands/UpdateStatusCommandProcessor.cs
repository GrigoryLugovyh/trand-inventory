using StockCore.Entity;

namespace StockCore.Handlers.Exchange.Imports.Commands
{
    public class UpdateStatusCommandProcessor : UpdateCommandProcessor<Statuses>
    {
        public UpdateStatusCommandProcessor(CsvImport handler)
            : base(handler)
        {
        }

        protected override string ByUpdateFiled
        {
            get { return "Id"; }
        }
    }
}