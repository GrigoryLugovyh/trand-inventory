﻿using StockCore.Entity;

namespace StockCore.Handlers.Exchange.Imports.Commands
{
    public class InsertObjectsCommandProcessor : ImportCommandProcessor<Objects>
    {
        public InsertObjectsCommandProcessor(CsvImport handler)
            : base(handler)
        {
        }

        protected override ImportCommandsEnum ImportCommandType
        {
            get { return ImportCommandsEnum.InsertObjects; }
        }
    }
}