using StockCore.Entity;

namespace StockCore.Handlers.Exchange.Imports.Commands
{
    public class UpdateCodesCommandProcessor : UpdateCommandProcessor<Codes>
    {
        public UpdateCodesCommandProcessor(CsvImport handler)
            : base(handler)
        {
        }

        protected override string ByUpdateFiled
        {
            get { return "Code"; }
        }
    }
}