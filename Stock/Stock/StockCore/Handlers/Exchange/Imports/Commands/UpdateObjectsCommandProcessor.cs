using StockCore.Entity;

namespace StockCore.Handlers.Exchange.Imports.Commands
{
    public class UpdateObjectsCommandProcessor : UpdateCommandProcessor<Objects>
    {
        public UpdateObjectsCommandProcessor(CsvImport handler)
            : base(handler)
        {
        }

        protected override string ByUpdateFiled
        {
            get { return "PobjectId"; }
        }
    }
}