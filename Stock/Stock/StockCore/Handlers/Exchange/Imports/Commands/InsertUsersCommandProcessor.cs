﻿using StockCore.Entity;

namespace StockCore.Handlers.Exchange.Imports.Commands
{
    public class InsertUsersCommandProcessor : ImportCommandProcessor<Users>
    {
        public InsertUsersCommandProcessor(CsvImport handler)
            : base(handler)
        {
        }

        protected override ImportCommandsEnum ImportCommandType
        {
            get { return ImportCommandsEnum.InsertUsers; }
        }
    }
}