using StockCore.Entity;

namespace StockCore.Handlers.Exchange.Imports.Commands
{
    public class InsertCodesCommandProcessor : ImportCommandProcessor<Codes>
    {
        public InsertCodesCommandProcessor(CsvImport handler)
            : base(handler)
        {
        }

        protected override ImportCommandsEnum ImportCommandType
        {
            get { return ImportCommandsEnum.InsertCodes; }
        }
    }
}