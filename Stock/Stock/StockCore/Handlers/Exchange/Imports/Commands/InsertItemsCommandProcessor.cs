﻿using StockCore.Entity;

namespace StockCore.Handlers.Exchange.Imports.Commands
{
    public class InsertItemsCommandProcessor : ImportCommandProcessor<Items>
    {
        public InsertItemsCommandProcessor(CsvImport handler)
            : base(handler)
        {
        }

        protected override ImportCommandsEnum ImportCommandType
        {
            get { return ImportCommandsEnum.InsertItemses; }
        }
    }
}