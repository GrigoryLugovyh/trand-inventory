using StockCore.Entity;

namespace StockCore.Handlers.Exchange.Imports.Commands
{
    public class ImportStatusCommandProcessor : ImportCommandProcessor<Statuses>
    {
        public ImportStatusCommandProcessor(CsvImport handler)
            : base(handler)
        {
        }

        protected override ImportCommandsEnum ImportCommandType
        {
            get { return ImportCommandsEnum.InsertStatus; }
        }
    }
}