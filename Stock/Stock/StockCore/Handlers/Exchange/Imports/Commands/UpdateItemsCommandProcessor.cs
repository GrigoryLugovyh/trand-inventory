using StockCore.Entity;

namespace StockCore.Handlers.Exchange.Imports.Commands
{
    public class UpdateItemsCommandProcessor : UpdateCommandProcessor<Items>
    {
        public UpdateItemsCommandProcessor(CsvImport handler)
            : base(handler)
        {
        }

        protected override string ByUpdateFiled
        {
            get { return "Id"; }
        }
    }
}