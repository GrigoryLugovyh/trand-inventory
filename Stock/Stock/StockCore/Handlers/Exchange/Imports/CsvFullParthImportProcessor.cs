﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using StockCore.Entity;
using StockCore.Enums;
using StockCore.Handlers.Exchange.Imports.Commands;

namespace StockCore.Handlers.Exchange.Imports
{
    /// <summary>
    /// Частичный импорт данных
    /// </summary>
    public class CsvFullParthImportProcessor : CsvFullImportProcessor
    {
        public CsvFullParthImportProcessor(CsvImport handler)
            : base(handler)
        {
            _isExistItemCommand = null;
        }

        public override ImportCommandsEnum Items
        {
            get { return ImportCommandsEnum.UpdateItems; }
        }

        public override ImportCommandsEnum Objects
        {
            get { return ImportCommandsEnum.UpdateObjects; }
        }

        public override ImportCommandsEnum Users
        {
            get { return ImportCommandsEnum.UpdateUsers; }
        }

        public override ImportCommandsEnum Mols
        {
            get { return ImportCommandsEnum.UpdateMols; }
        }

        /// <summary>
        /// Импортер ТМЦ
        /// </summary>
        protected ImportCommandProcessor<Items> InsertHandler;

        /// <summary>
        /// Текущие объекты
        /// </summary>
        protected List<Objects> ObjectsCollection;

        public override void Prepare()
        {
            InsertHandler = new InsertItemsCommandProcessor(Handler);

            // грузим все объекты
            ObjectsCollection = Model.Instance.Data.Store.Load<Objects>();

            Model.Instance.Data.Stop();

            Handler.ExchangeConnection = new SQLiteConnection(string.Format(Constants.StockConnection, Model.Instance.Data.Store.DatabasePath));
            Handler.ExchangeConnection.Open();
        }

        public override void BindCommands()
        {
            InsertHandler.BindParameters();
            Handler.ItemsProcessor.BindParameters();
        }

        private SQLiteCommand _isExistItemCommand;
        protected SQLiteCommand IsExistItemCommand
        {
            get
            {
                if (_isExistItemCommand == null)
                {
                    _isExistItemCommand = new SQLiteCommand(Constants.CheckItemExistsQuery, Handler.ExchangeConnection);
                    _isExistItemCommand.Parameters.Add("@Id", DbType.String);
                    _isExistItemCommand.Prepare();
                }

                return _isExistItemCommand;
            }
        }

        protected bool IsExistItem(object id)
        {
            IsExistItemCommand.Parameters["@Id"].Value = id;
            return int.Parse(IsExistItemCommand.ExecuteScalar().ToString()) > 0;
        }

        public override bool ProcessRecord()
        {
            int errors = 0;

            InsertHandler.FillParameters();
            Handler.ItemsProcessor.FillParameters();

            if (IsExistItem(Handler.ItemsProcessor.Command.Parameters["@Id"].Value))
            {
                // обновляем данные
                if (string.IsNullOrEmpty(Handler.ItemsProcessor.Command.Parameters["@PobjectBarcode"].Value.ToString()))
                {
                    var b = ObjectsCollection.FirstOrDefault(o => o.PobjectId == (string)Handler.ItemsProcessor.Command.Parameters["@PobjectId"].Value);
                    Handler.ItemsProcessor.Command.Parameters["@PobjectBarcode"].Value = b != null ? b.PobjectBarcode : string.Empty;
                }

                try
                {
                    Handler.ItemsProcessor.Command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    Model.Instance.Log(LogsEnum.Exchange, false, "Ошибка обновления ТМЦ: \"{0}\"\r\n Строка: {1}", ex.Message, Handler.RawLine);
                    errors++;
                }
            }
            else
            {
                // вставляем данные
                if (string.IsNullOrEmpty(InsertHandler.Command.Parameters["@PobjectBarcode"].Value.ToString()))
                {
                    var b = ObjectsCollection.FirstOrDefault(o => o.PobjectId == (string)Handler.ItemsProcessor.Command.Parameters["@PobjectId"].Value);

                    if (b != null)
                    {
                        InsertHandler.Command.Parameters["@PobjectBarcode"].Value = !string.IsNullOrEmpty(b.PobjectBarcode) ? b.PobjectBarcode : b.PobjectId;
                    }
                    else
                    {
                        InsertHandler.Command.Parameters["@PobjectBarcode"].Value = string.Empty;
                    }

                    try
                    {
                        InsertHandler.Command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        Model.Instance.Log(LogsEnum.Exchange, false, "Ошибка вставки ТМЦ: \"{0}\"\r\n Строка: {1}", ex.Message, Handler.RawLine);
                        errors++;
                    }
                }
            }

            return errors == 0;
        }
    }
}