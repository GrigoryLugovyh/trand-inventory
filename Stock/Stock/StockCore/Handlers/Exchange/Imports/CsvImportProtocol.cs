﻿using StockCore.Interfaces.Handlers;

namespace StockCore.Handlers.Exchange.Imports
{
    public class CsvImportProtocol : ICsvImportProtocol
    {
        public CsvImportProtocol(bool stopWhenRecordNotPrepared, bool stopWhenRecordNotProcessed, string file, bool isFullImport, int transactionNumberRecord, bool reCreateDatabase)
        {
            StopWhenRecordNotPrepared = stopWhenRecordNotPrepared;
            StopWhenRecordNotProcessed = stopWhenRecordNotProcessed;
            ImportFile = file;
            IsFullImport = isFullImport;
            TransactionNumberRecord = transactionNumberRecord;
            ReCreateDatabase = reCreateDatabase;
        }

        public bool StopWhenRecordNotPrepared { get; private set; }

        public bool StopWhenRecordNotProcessed { get; private set; }

        public bool ReCreateDatabase { get; private set; }

        public string ImportFile { get; private set; }

        public bool IsFullImport { get; private set; }

        public int TransactionNumberRecord { get; private set; }
    }
}