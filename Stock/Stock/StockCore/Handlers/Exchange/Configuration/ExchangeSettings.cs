﻿using System;
using System.Xml.Serialization;
using StockCore.Handlers.Config;
using StockCore.Handlers.Exchange.Configuration.CSV;

namespace StockCore.Handlers.Exchange.Configuration
{
    /// <summary>
    ///     Базовые настройки обмена
    /// </summary>
    [Serializable]
    [XmlInclude(typeof(CsvExchangeSettings))]
    public class ExchangeSettings
    {
        [XmlIgnore]
// ReSharper disable once InconsistentNaming
        protected static Configurator<CsvExchangeSettings> _configurator;

        [XmlIgnore]
        protected static Configurator<CsvExchangeSettings> Configurator
        {
            get { return _configurator ?? (_configurator = new Configurator<CsvExchangeSettings>()); }
        }
    }
}