﻿using System;
using System.Xml.Serialization;
using StockCore.Common.Attributes;

namespace StockCore.Handlers.Exchange.Configuration
{
    /// <summary>
    /// Список полей для обмена
    /// </summary>
    [Serializable]
    public enum ExchangeField
    {
        // Поля для загрузки

        [XmlEnum]
        [Description("Id")]
        Id,

        [XmlEnum]
        [Description("Name")]
        Name,

        [XmlEnum]
        [Description("PobjectName")]
        PobjectName,

        [XmlEnum]
        [Description("UserName")]
        UserName,

        [XmlEnum]
        [Description("PobjectId")]
        PobjectId,

        [XmlEnum]
        [Description("MolName")]
        MolName,

        [XmlEnum]
        [Description("SerialNumber")]
        SerialNumber,

        [XmlEnum]
        [Description("PobjectPid")]
        PobjectPid,

        [XmlEnum]
        [Description("PobjectPname")]
        PobjectPname,

        [XmlEnum]
        [Description("PobjectBarcode")]
        PobjectBarcode,

        [XmlEnum]
        [Description("MolNumber")]
        MolNumber,

        [XmlEnum]
        [Description("UserNumber")]
        UserNumber,

        [XmlEnum]
        [Description("BeginDate")]
        BeginDate,

        [XmlEnum]
        [Description("ExtItemId")]
        ExtItemId,

        [XmlEnum]
        [Description("ExtObjectId")]
        ExtObjectId,

        [XmlEnum]
        [Description("ExtUserId")]
        ExtUserId,

        [XmlEnum]
        [Description("ExtMolId")]
        ExtMolId,

        [XmlEnum]
        [Description("ExtParentObjectId")]
        ExtParentObjectId,

        // Поля только для выгрузки 

        [XmlEnum]
        [Description("FobjectId")]
        FobjectId,

        [XmlEnum]
        [Description("Found")]
        Found,

        [XmlEnum]
        [Description("Status")]
        Status,

        [XmlEnum]
        [Description("FobjectName")]
        FobjectName,

        [XmlEnum]
        [Description("MolMcCreated")]
        MolMcCreated,

        [XmlEnum]
        [Description("UserMcCreated")]
        UserMcCreated
    }
}