﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using StockCore.Enums;
using StockCore.Factories;

namespace StockCore.Handlers.Exchange.Configuration.CSV
{
    /// <summary>
    ///     Настройка файла обмена
    /// </summary>
    [Serializable]
    public class CsvFile
    {
        public CsvFile()
        {
        }

        public CsvFile(string file, string separator, EncodingEnum encoding)
        {
            FileName = file;
            Separator = separator;
            Encoding = encoding;
        }

        /// <summary>
        ///     Имя файла загрузкаи / выгрузки
        /// </summary>
        [XmlElement]
        public string FileName { get; set; }

        /// <summary>
        ///     Разделитель полей
        /// </summary>
        [XmlElement]
        public string Separator { get; set; }

        /// <summary>
        ///     Кодировка файла загрузки / выгрузки
        /// </summary>
        [XmlElement]
        public EncodingEnum Encoding { get; set; }

        /// <summary>
        ///     Поля для обмена
        /// </summary>
        [XmlArrayItem(typeof (CvsElement))]
        public List<CvsElement> Elements { get; set; }

        /// <summary>
        ///     Кодировка в формате .net
        /// </summary>
        [XmlIgnore]
        public Encoding CsvEncoding
        {
            get { return EncodingFactory.Instance.GetInstance(Encoding); }
        }
    }
}