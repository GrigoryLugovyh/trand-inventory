﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using StockCore.Entity;
using StockCore.Enums;

namespace StockCore.Handlers.Exchange.Configuration.CSV
{
    /// <summary>
    ///     Настройка обмена через CSV файлы
    /// </summary>
    [Serializable]
    public class CsvExchangeSettings : ExchangeSettings
    {
        [XmlIgnore] private static CsvExchangeSettings _instance;

        [XmlElement]
        public string Terminal { get; set; }

        [XmlElement]
        public string ImportDirectory { get; set; }

        [XmlElement]
        public string ExportDirectory { get; set; }

        [XmlElement]
        public CsvFile ImportFile { get; set; }

        [XmlElement]
        public CsvFile ExportFile { get; set; }

        [XmlArrayItem(typeof (Statuses))]
        public List<Statuses> Stauses { get; set; }

        [XmlArrayItem(typeof (CsvPrefix))]
        public List<CsvPrefix> Prefixes { get; set; }

        [XmlIgnore]
        public static CsvExchangeSettings Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance =
                        (Configurator.Load(Path.Combine(Model.Instance.Common.CurrentDirectory,
                            Constants.ExchangeSettingsName)) ?? new CsvExchangeSettings());

                    if (string.IsNullOrEmpty(_instance.Terminal))
                    {
                        _instance.Terminal = "1";
                    }

                    if (string.IsNullOrEmpty(_instance.ImportDirectory))
                    {
                        _instance.ImportDirectory = @"\Storage card";
                    }

                    if (string.IsNullOrEmpty(_instance.ExportDirectory))
                    {
                        _instance.ExportDirectory = @"\Storage card";
                    }

                    if (_instance.ImportFile == null)
                    {
                        _instance.ImportFile = new CsvFile("invout.txt", string.Empty, EncodingEnum.Dos)
                        {
                            Elements = new List<CvsElement>
                            {
                                new CvsElement(ExchangeField.Id, "string", 14),
                                new CvsElement(ExchangeField.Name, "string", 50),
                                new CvsElement(ExchangeField.PobjectName, "string", 50),
                                new CvsElement(ExchangeField.UserName, "string", 50),
                                new CvsElement(ExchangeField.PobjectId, "string", 20),
                                new CvsElement(ExchangeField.MolName, "string", 50),
                                new CvsElement(ExchangeField.SerialNumber, "string", 50),
                                new CvsElement(ExchangeField.PobjectPid, "string", 20),
                                new CvsElement(ExchangeField.PobjectPname, "string", 50),
                                new CvsElement(ExchangeField.PobjectBarcode, "string", 30),
                                new CvsElement(ExchangeField.MolNumber, "string", 10),
                                new CvsElement(ExchangeField.UserNumber, "string", 10),
                                new CvsElement(ExchangeField.BeginDate, "datetime", 10, "dd.MM.yyyy"),
                                new CvsElement(ExchangeField.ExtItemId, "string", 30),
                                new CvsElement(ExchangeField.ExtObjectId, "string", 30),
                                new CvsElement(ExchangeField.ExtUserId, "string", 30),
                                new CvsElement(ExchangeField.ExtMolId, "string", 30),
                                new CvsElement(ExchangeField.ExtParentObjectId, "string", 30)
                            }
                        };
                    }

                    if (_instance.ExportFile == null)
                    {
                        _instance.ExportFile = new CsvFile("SSSSS_NNNNNNNNNNNN_YYYYMMDD.txt", string.Empty,
                            EncodingEnum.Win)
                        {
                            Elements = new List<CvsElement>
                            {
                                new CvsElement(ExchangeField.Id, "string", 14),
                                new CvsElement(ExchangeField.Name, "string", 50),
                                new CvsElement(ExchangeField.PobjectId, "string", 20),
                                new CvsElement(ExchangeField.FobjectId, "string", 20),
                                new CvsElement(ExchangeField.UserName, "string", 50),
                                new CvsElement(ExchangeField.Found, "string", 1),
                                new CvsElement(ExchangeField.Status, "string", 1),
                                new CvsElement(ExchangeField.MolName, "string", 50),
                                new CvsElement(ExchangeField.SerialNumber, "string", 50),
                                new CvsElement(ExchangeField.BeginDate, "datetime", 10, "dd.MM.yyyy"),
                                new CvsElement(ExchangeField.FobjectName, "string", 50),
                                new CvsElement(ExchangeField.MolNumber, "string", 10),
                                new CvsElement(ExchangeField.UserNumber, "string", 10),
                                new CvsElement(ExchangeField.PobjectPid, "string", 20),
                                new CvsElement(ExchangeField.PobjectPname, "string", 50),
                                new CvsElement(ExchangeField.ExtItemId, "string", 30),
                                new CvsElement(ExchangeField.ExtObjectId, "string", 30),
                                new CvsElement(ExchangeField.ExtUserId, "string", 30),
                                new CvsElement(ExchangeField.ExtMolId, "string", 30),
                                new CvsElement(ExchangeField.ExtParentObjectId, "string", 30),
                                new CvsElement(ExchangeField.MolMcCreated, "int", 1),
                                new CvsElement(ExchangeField.UserMcCreated, "int", 1)
                            }
                        };
                    }

                    if (_instance.Stauses == null)
                    {
                        _instance.Stauses = new List<Statuses>
                        {
                            new Statuses {Id = 0, Name = "Исправен"},
                            new Statuses {Id = 1, Name = "Не исправен"},
                            new Statuses {Id = 2, Name = "Замена ШК"},
                            new Statuses {Id = 3, Name = "Дубль ШК"},
                        };
                    }

                    if (_instance.Prefixes == null)
                    {
                        _instance.Prefixes = new List<CsvPrefix>
                        {
                            new CsvPrefix("OS"),
                            new CsvPrefix("MB"),
                            new CsvPrefix("NA"),
                            new CsvPrefix("MA")
                        };
                    }

                    Configurator.Object = _instance;
                    Configurator.Save();
                }

                return _instance;
            }
        }
    }
}