﻿using System;
using System.Xml.Serialization;

namespace StockCore.Handlers.Exchange.Configuration.CSV
{
    /// <summary>
    ///     Префиксы Csv обмена
    /// </summary>
    [Serializable]
    public class CsvPrefix
    {
        public CsvPrefix()
        {
        }

        public CsvPrefix(string prefix)
        {
            Prefix = prefix;
        }

        [XmlElement]
        public string Prefix { get; set; }

        public override string ToString()
        {
            return Prefix;
        }
    }
}