using StockCore.Interfaces.Handlers;

namespace StockCore.Handlers.Exchange.Exports
{
    public class CsvExportProtocol : ICsvExportProtocol
    {
        public CsvExportProtocol(bool stopWhenRecordNotPrepared, bool stopWhenRecordNotProcessed, string databasePath, bool exportOnlyFound, bool exportOnlyNew)
        {
            StopWhenRecordNotPrepared = stopWhenRecordNotPrepared;
            StopWhenRecordNotProcessed = stopWhenRecordNotProcessed;
            DatabasePath = databasePath;
            ExportOnlyFound = exportOnlyFound;
            ExportOnlyNew = exportOnlyNew;
        }

        public bool StopWhenRecordNotPrepared { get; private set; }

        public bool StopWhenRecordNotProcessed { get; private set; }

        public string DatabasePath { get; private set; }

        public bool ExportOnlyFound { get; private set; }

        public bool ExportOnlyNew { get; private set; }
    }
}