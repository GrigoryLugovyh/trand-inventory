﻿using System;
using System.Data;
using System.Data.SQLite;
using System.Text;
using StockCore.Handlers.Exchange.Configuration.CSV;
using StockCore.Interfaces.Handlers;

namespace StockCore.Handlers.Exchange.Exports
{
    public class CsvExport : Exchange
    {
        public CsvExport(IExchangeProtocol handler, CsvExchangeSettings settings)
            : base(handler, settings)
        {
        }

        /// <summary>
        /// Выгружать только найденные ТМЦ
        /// </summary>
        public bool OnlyFound { get; protected set; }

        /// <summary>
        /// Выгружать только новые ТМЦ
        /// </summary>
        public bool OnlyNew { get; protected set; }

        /// <summary>
        /// Путь для выгрузки базыданных
        /// </summary>
        public string DatabasePath { get; protected set; }

        /// <summary>
        /// Разделитель полей
        /// </summary>
        public char[] Separator { get; protected set; }

        /// <summary>
        /// Кодировка
        /// </summary>
        public Encoding Encoding { get; protected set; }

        /// <summary>
        /// Директория для выгрузки
        /// </summary>
        public string OutputDirectory { get; protected set; }

        /// <summary>
        /// Настройки файла загрузки
        /// </summary>
        public CsvFile OutputData { get; protected set; }

        /// <summary>
        /// Обработчик экспорта
        /// </summary>
        public CsvExportProcessor ExportProcessor { get; set; }

        /// <summary>
        /// Поток чтения к файлу
        /// </summary>
        public SQLiteDataReader ExportReader { get; protected set; }

        /// <summary>
        /// Команда на выгрузку
        /// </summary>
        public SQLiteCommand ExportCommand { get; protected set; }
        
        /// <summary>
        /// Текущая считанная строка из ридера в виде сырых данных
        /// </summary>
        public IDataRecord RawLine { get; protected set; }

        protected int RecordCounter { get; set; }

        public override string ExchangeFriandlyName
        {
            get { return "Выгрузка данных"; }
        }

        public override string ExchangeProcessStatus
        {
            get { return "Выгрузка ..."; }
        }

        public override string ExchangeProcessFail
        {
            get { return "Выгрузка завершена с ошибками"; }
        }

        public override string ExchangeProcessSuccessfully
        {
            get { return "Выгрузка завершена успешно"; }
        }

        protected override string CurrendRecordInformation()
        {
            return null;
        }

        protected override void InitExchange()
        {
            if (Handler == null || !(Handler is ICsvExportProtocol))
                throw new Exception("Протокол обмена не соответствует реализации выгрузки");

            if (Settings == null || !(Settings is CsvExchangeSettings))
                throw new Exception("Настройки не соответствуют реализации выгрузки");

            OnlyFound = (Handler as ICsvExportProtocol).ExportOnlyFound;

            OnlyNew = (Handler as ICsvExportProtocol).ExportOnlyNew;

            DatabasePath = (Handler as ICsvExportProtocol).DatabasePath;

            var settings = Settings as CsvExchangeSettings;

            Separator = settings.ExportFile.Separator.ToCharArray();
            Encoding = settings.ExportFile.CsvEncoding;
            OutputData = settings.ExportFile;
            OutputDirectory = settings.ExportDirectory;
 
            if (OutputData.Elements == null || OutputData.Elements.Count < 1)
                throw new Exception("Нет полей для выгрузки");

            ExportProcessor = new CsvExportProcessor(this);

            if (ExportProcessor == null)
                throw new Exception("Не удалось установить обработчик выгрузки данных");

            ExportProcessor.Prepare();
        }

        protected override void FinishExchange()
        {
            ExportProcessor.Finish();
        }

        protected override bool Connect()
        {
            ExportCommand = new SQLiteCommand(ExportProcessor.Query, ExchangeConnection);
            ExportReader = ExportCommand.ExecuteReader(CommandBehavior.Default); 

            return true;
        }

        protected override int PersentProcessed()
        {
            return (int) (((double) (RecordCounter*100)/(ExportProcessor.TotalCount)));
        }

        protected override bool ExistRecord()
        {
            return ExportReader != null && ExportReader.Read();
        }

        protected override bool PrepareRecord()
        {
            RawLine = ExportReader;

            if (RawLine == null)
                return false;

            return ExportProcessor.PrepareRecord();
        }

        protected override bool ProcessRecord()
        {
            RecordCounter++;
            return ExportProcessor.ProcessRecord();
        }

        protected override void PreExchange()
        {
        }

        protected override void PostExchange()
        {
        }

        protected override bool Disconnect()
        {
            if(ExportReader != null)
            {
                ExportReader.Close();
                ExportReader.Dispose();
            }
 
            return true;
        }
    }
}