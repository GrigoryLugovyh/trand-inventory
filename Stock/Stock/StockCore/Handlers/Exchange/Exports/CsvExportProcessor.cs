using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Globalization;
using System.IO;
using System.Text;
using StockCore.Enums;

namespace StockCore.Handlers.Exchange.Exports
{
    public class CsvExportProcessor
    {
        public CsvExportProcessor(CsvExport handler)
        {
            Handler = handler;
        }

        protected CsvExport Handler { get; set; }

        protected StreamWriter Writer { get; set; }

        public int TotalCount { get; protected set; }
 
        /// <summary>
        /// ������ �� ��������
        /// </summary>
        public virtual string Query { get; protected set; }

        /// <summary>
        /// ������� ������ ��� ��������
        /// </summary>
        protected string CurrentLine { get; set; }

        /// <summary>
        /// ��� ����� ��� ��������
        /// </summary>
        protected string FileName { get; set; }
        
        /// <summary>
        /// ������ �� ��������
        /// </summary>
        protected virtual string ExportQuery
        {
            get
            {
                if (!Handler.OnlyNew)
                {
                    return Constants.ExportQuery;
                }

                return Constants.ExportNewQuery;
            }
        }

        /// <summary>
        /// ������ �� ����� ���-�� ����� ��� �������� 
        /// </summary>
        protected virtual string TotalCountQuery
        {
            get
            {
                if (!Handler.OnlyNew)
                {
                    return Constants.ExportTotalCountQuery;
                }

                return Constants.ExportNewCountQuery;
            }
        }

        /// <summary>
        /// �������� ���-�� ������� ��� ��������
        /// </summary>
        /// <returns></returns>
        protected virtual int GetTotalCount()
        {
            var query = TotalCountQuery;

            if(Handler.OnlyFound)
            {
                query += Constants.ExportQueryFilter;
            }

            var command = new SQLiteCommand(query, Handler.ExchangeConnection); 

            return int.Parse(command.ExecuteScalar().ToString());
        }

        protected virtual string ExportFilename()
        {
            const string separae = "_";
            string filename = string.Empty;

            string mask = Handler.OutputData.FileName; string ext = "txt";

            if (mask.IndexOf('.') != -1)
            {
                ext = mask.Substring(mask.IndexOf('.') + 1);
                mask = mask.Substring(0, mask.IndexOf('.'));
            }

            string[] specifiers = mask.Split(new[] { '_' });
            if (specifiers.Length > 0)
            {
                filename = "stock";
            }

            filename += separae;

            if (specifiers.Length > 1)
            {
                filename += Model.Instance.Config.CurrentConfig.TerminalId + separae;
            }

            if (specifiers.Length > 2)
            {
                string format = specifiers[2];
                format = format.Replace('m', 'M');
                format = format.Replace('Y', 'y');
                format = format.Replace('D', 'd');
                string date;
                try
                {
                    date = DateTime.Now.ToString(format);
                }
                catch
                {
                    date = DateTime.Now.ToString("DDMMYYYY");
                }

                filename += date;
            }

            var directory = string.IsNullOrEmpty(Path.GetDirectoryName(Handler.DatabasePath)) ? Handler.OutputDirectory : Path.GetDirectoryName(Handler.DatabasePath);

            if (string.IsNullOrEmpty(directory))
            {
                directory = Handler.OutputDirectory;
            }

            int increment = 1;

            while (true)
            {
                if (!File.Exists(Path.Combine(directory, string.Format("{0}_{1}.{2}", filename, increment, ext))))
                {
                    break;
                }

                increment++;
            }

            if (!Directory.Exists(directory))
            {
                directory = Model.Instance.Common.StorageCardDirectory;
            }

            return Path.Combine(directory, string.Format("{0}_{1}.{2}", filename, increment, ext));
        }

        /// <summary>
        /// ����������
        /// </summary>
        public virtual void Prepare()
        {
            Query = ExportQuery;

            FileName = ExportFilename();

            if(File.Exists(FileName))
            {
                if(Model.Instance.Messages.ShowQuestion(string.Format("���� � ������ \"{0}\" ��� ����������. ������ ������������ ���� ����?", FileName), DialogEnum.No, "��", "���") == DialogEnum.Yes)
                {
                    File.Delete(FileName);
                }
            }

            if(Handler.OnlyFound)
            {
                Query += Constants.ExportQueryFilter;
            }

            Model.Instance.Data.Stop();

            Handler.ExchangeConnection = new SQLiteConnection(string.Format(Constants.StockConnection, string.IsNullOrEmpty(Handler.DatabasePath) ? Model.Instance.Data.DatabasePath : Handler.DatabasePath));
            Handler.ExchangeConnection.Open();

            TotalCount = GetTotalCount();

            Writer = new StreamWriter(FileName, true, Handler.Encoding);
        }

        /// <summary>
        /// ��������� ��������
        /// </summary>
        public virtual void Finish()
        {
            if (Handler.ExchangeConnection != null && Handler.ExchangeConnection.State != ConnectionState.Closed)
            {
                Handler.ExchangeConnection.Close();
            }

            if (Writer != null)
            {
                Writer.Close();
                Writer.Dispose();
            }

            // ��������� ��������� ���
            Model.Instance.Data.Start();
        }

        /// <summary>
        /// ���������� ������
        /// </summary>
        /// <returns></returns>
        public virtual bool PrepareRecord()
        {
            // ���� ��� ��������, ����� ������ ����������� ��� ����� ������ ����� �����������
            if (Handler.Separator == null || Handler.Separator.Length == 0)
            {
                // ��� �����������
                return PrepareRecordByPositions();
            }

            // ����� �����������
            return PrepareRecordBySeparator();
        }

        protected virtual bool PrepareRecordByPositions()
        {
            var builder = new StringBuilder();

            foreach (var field in Handler.OutputData.Elements)
            {
                if (Handler.RawLine[field.CvsFieldName] != null)
                {
                    Type elementType = field.CvsType;
                    if (elementType == typeof(string))
                    {
                        builder.Append(Handler.RawLine[field.CvsFieldName].ToString().PadRight(field.CvsElementLength));
                    }
                    else if (elementType == typeof(DateTime))
                    {
                        builder.Append(Convert.ToDateTime(Handler.RawLine[field.CvsFieldName]).ToString(field.CvsElementFormat).PadRight(field.CvsElementLength));
                    }
                    else if (elementType == typeof(bool))
                    {
                        builder.Append(Convert.ToInt32((bool)Handler.RawLine[field.CvsFieldName]).ToString(CultureInfo.InvariantCulture));
                    }
                    else
                    {
                        builder.Append(Handler.RawLine[field.CvsFieldName].ToString().PadRight(field.CvsElementLength));
                    }
                }
            }

            return !string.IsNullOrEmpty(CurrentLine = builder.ToString());
        }

        protected virtual bool PrepareRecordBySeparator()
        {
            var stringBuilder = new List<string>();

            foreach (var field in Handler.OutputData.Elements)
            {
                if (Handler.RawLine[field.CvsFieldName] != null)
                {
                    Type elementType = field.CvsType;
                    string value = Handler.RawLine[field.CvsFieldName].ToString();
                    if (elementType == typeof(string))
                    {
                        stringBuilder.Add(value);
                    }
                    else if (elementType == typeof(DateTime))
                    {
                        stringBuilder.Add(Convert.ToDateTime(Handler.RawLine[field.CvsFieldName]).ToString(field.CvsElementFormat));
                    }
                    else if (elementType == typeof(bool))
                    {
                        stringBuilder.Add(Convert.ToInt32((bool)Handler.RawLine[field.CvsFieldName]).ToString(CultureInfo.InvariantCulture));
                    }
                    else
                    {
                        stringBuilder.Add(value);
                    }
                }
            }

            return !string.IsNullOrEmpty(CurrentLine = string.Join(Handler.Separator.ToString(), stringBuilder.ToArray()));
        }

        /// <summary>
        /// ��������� ������
        /// </summary>
        /// <returns></returns>
        public virtual bool ProcessRecord()
        {
            Writer.WriteLine(CurrentLine);
            return true;
        }
    }
}