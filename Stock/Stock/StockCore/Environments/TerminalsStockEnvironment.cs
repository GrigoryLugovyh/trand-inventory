﻿using System;
using StockCore.Events;
// ReSharper disable once RedundantUsingDirective
using StockCore.Factories;
using StockCore.Interfaces.Terminal;

namespace StockCore.Environments
{
    /// <summary>
    /// Окружение рабты с териналами
    /// </summary>
    public class TerminalsStockEnvironment : StockEnvironment
    {
        public override string FriendlyName
        {
            get { return "Терминалы"; }
        }

        /// <summary>
        /// Событие чтения ШК
        /// </summary>
        public event EventHandler<StockEventArgs> BarcodeRead;

        /// <summary>
        /// Текущий терминал
        /// </summary>
        public ITerminal Terminal { get; protected set; }

        /// <summary>
        /// Состояние сканера
        /// </summary>
        public bool Enabled { get; protected set; }

        /// <summary>
        /// Чтение ШК
        /// </summary>
        /// <param name="barcode"></param>
        protected void ScanerScanned(string barcode)
        {
            if(BarcodeRead != null)
            {
                BarcodeRead(Terminal, new StockEventArgs(barcode));
            }
        }

        /// <summary>
        /// Включить сканер
        /// </summary>
        public void Enable()
        {
            if (IsActive && !Enabled)
            {
                Enabled = true;
                Terminal.Scaner.Start();
                Terminal.Scaner.Scanned += ScanerScanned;
            }
        }

        /// <summary>
        /// Выключить сканер
        /// </summary>
        public void Disable()
        {
            if (IsActive && Enabled)
            {
                Terminal.Scaner.Scanned -= ScanerScanned;
                Terminal.Scaner.Stop();
                Enabled = false;
            }
        }

        protected override Action StartAction
        {
            get
            {
                return () =>
                           {
#if(WindowsCE)
                               Terminal = TerminalsFactory.Instance.GetTerminal();
#endif
                           };
            }
        }

        protected override Action StopAction
        {
            get
            {
                return () =>
                {
#if(WindowsCE)
// ReSharper disable once ConvertToLambdaExpression
                    Terminal.Dispose();
#endif
                };
            }
        }
    }
}