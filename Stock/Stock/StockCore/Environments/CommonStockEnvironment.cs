﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace StockCore.Environments
{
    /// <summary>
    /// Общее окружение
    /// </summary>
    public class CommonStockEnvironment : StockEnvironment
    {
        public override string FriendlyName
        {
            get { return "Общее"; }
        }

        protected string CurrentDir;
        /// <summary>
        /// Директория с программой
        /// </summary>
        public string CurrentDirectory
        {
            get
            {
                if (string.IsNullOrEmpty(CurrentDir))
                {
                    CurrentDir =
                        Path.GetDirectoryName(
                            Path.GetFullPath(Assembly.GetCallingAssembly().GetName().CodeBase.Replace("file:///", "")));
                }

                return CurrentDir;
            }
        }

        protected string StorageCardDir;
        /// <summary>
        /// Директория с картой памяти
        /// </summary>
        public string StorageCardDirectory
        {
            get
            {
                if (string.IsNullOrEmpty(StorageCardDir))
                {
                    StorageCardDir = GetStorageCardDirectory();
                    if (string.IsNullOrEmpty(StorageCardDir))
                    {
                        StorageCardDir = CurrentDirectory;
                    }
                }

                return StorageCardDir;
            }
        }

        public string GetStorageCardDirectory()
        {
            string result = "Storage Card";
            foreach (DirectoryInfo d in
                new DirectoryInfo("\\").GetDirectories().Where(
                     d => d.Attributes == (FileAttributes.Directory | FileAttributes.Temporary)))
            {
                if (d.ToString().IndexOf("card", StringComparison.OrdinalIgnoreCase) > -1 ||
                    d.ToString().IndexOf("sd", StringComparison.OrdinalIgnoreCase) > -1)
                {
                    result = d.FullName;
                    break;
                }
            }

            result = Path.GetFullPath(result);

            if (!Directory.Exists(result))
                result = string.Empty;

            return result;
        }

        protected override Action StartAction
        {
            get
            {
                return () =>
                    {

                    };
            }
        }

        protected override Action StopAction
        {
            get
            {
                return () =>
                    {

                    };
            }
        }
    }
}