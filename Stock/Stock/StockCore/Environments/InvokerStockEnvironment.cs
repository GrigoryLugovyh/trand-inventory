using System;
using System.Collections.Generic;
using System.Threading;
using StockCore.Enums;

namespace StockCore.Environments
{
    /// <summary>
    /// ���������� ���� �������� ����������
    /// </summary>
    public class InvokerStockEnvironment : StockEnvironment
    {
        public override string FriendlyName
        {
            get { return "��������� ��������"; }
        }

        /// <summary>
        /// ������ ������� ��������
        /// </summary>
        protected List<string> Operations = new List<string>();

        /// <summary>
        /// ������ �������������
        /// </summary>
        protected object Sync = new object();

        /// <summary>
        /// ������ �������� � ������������ ������
        /// </summary>
        /// <param name="operation"></param>
        /// <param name="action"></param>
        public void Do(string operation, Action action)
        {
            Do(operation, action, true, true);
        }

        /// <summary>
        /// ������ �������� ��� ����������� ������
        /// </summary>
        /// <param name="operation"></param>
        /// <param name="action"></param>
        public void DoSilent(string operation, Action action)
        {
            Do(operation, action, false, false);
        }

        /// <summary>
        /// ��������� ��������
        /// </summary>
        /// <param name="action"></param>
        /// <param name="?"></param>
        /// <param name="operation"></param>
        /// <param name="showWait"></param>
        /// <param name="showError"></param>
        public void Do(string operation, Action action, bool showWait, bool showError)
        {
            lock (Sync)
            {
                if (action == null)
                    return;

                if (Operations.Contains(operation))
                    return;

                try
                {
                    if (showWait)
                    {
                        Model.Instance.Wait.ShowWait();
                    }

                    Operations.Add(operation);

                    action.Invoke();
                }
                catch (Exception ex)
                {
                    if (showWait)
                    {
                        Model.Instance.Wait.CloseWait();
                    }

                    if(showError)
                    {
                        var message = string.Format("�� ����� ���������� �������� \"{0}\" �������� ������: \"{1}\"", operation, ex.Message);
                        Model.Instance.Messages.ShowExclamation(message, "��");
                    }
                }
                finally
                {
                    if (Operations.Contains(operation))
                    {
                        Operations.Remove(operation);
                    }

                    if (showWait)
                    {
                        Model.Instance.Wait.CloseWait();
                    }
                }
            }
        }

        public void DoIndependentThread(string operation, Action action, Action finish, bool showError)
        {
            lock (Sync)
            {
                if (action == null)
                    return;

                if (Operations.Contains(operation))
                    return;

                var thread = new Thread(() =>
                                            {
                                                try
                                                {
                                                    Operations.Add(operation);

                                                    Model.Instance.Wait.ShowWait();

                                                    action.Invoke();
                                                }
                                                catch (Exception ex)
                                                {
                                                    Model.Instance.Wait.CloseWait();

                                                    if (finish != null)
                                                    {
                                                        finish.Invoke();
                                                        finish = null;
                                                    }

                                                    var message = string.Format("�� ����� ���������� �������� \"{0}\" �������� ������: \"{1}\"", operation, ex.Message);
                                                    Model.Instance.Log(LogsEnum.System, false, message);
                                                    if (showError)
                                                    {
                                                        Model.Instance.Messages.ShowExclamation(message, "��");
                                                    }
                                                }
                                                finally
                                                {
                                                    Model.Instance.Wait.CloseWait();

                                                    if (finish != null)
                                                    {
                                                        finish.Invoke();
                                                    }

                                                    if (Operations.Contains(operation))
                                                    {
                                                        Operations.Remove(operation);
                                                    }
                                                }

                                            }) {IsBackground = true, Name = operation};

                thread.Start();
            }
        }

        protected override Action StartAction
        {
            get
            {
                return () =>
                {

                };
            }
        }

        protected override Action StopAction
        {
            get
            {
                return () =>
                {

                };
            }
        }
    }
}