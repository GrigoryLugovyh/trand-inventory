using System;
using System.Threading;
using StockCore.Entity;
using StockCore.Enums;
using StockCore.Handlers.Printing;
using StockCore.Interfaces.Handlers;

namespace StockCore.Environments
{
    public class PrintingStockEnvironment : StockEnvironment, IPrintingHandler
    {
        public override string FriendlyName
        {
            get { return "������ ����������"; }
        }

        public IPrinter LocalPrinter { get; protected set; }

        public IPrinter MobilePrinter { get; protected set; }

        public IPrinter DesktopPrinter { get; protected set; }

        public IPrintingConverter Converter { get; protected set; }

        private void Sleep()
        {
            Thread.Sleep(150);
        }

        public void Print(Objects[] objects, IPrinter printer, IPrintingConverter converter, string pattern, EncodingEnum encoding)
        {
            if(objects != null && objects.Length > 0)
            {
                foreach (var @object in objects)
                {
                    Print(@object, printer, converter, pattern, encoding);
                    Sleep();
                }
            }
        }

        public void Print(Items[] items, IPrinter printer, IPrintingConverter converter, string pattern, EncodingEnum encoding)
        {
            if (items != null && items.Length > 0)
            {
                foreach (var item in items)
                {
                    Print(item, printer, converter, pattern, encoding);
                    Sleep();
                }
            }
        }

        public void Print(Objects @object, IPrinter printer, IPrintingConverter converter, string pattern, EncodingEnum encoding)
        {
            printer.Send(converter.ToByte(@object, pattern, encoding));
            var items = Model.Instance.Data.Store.Table<Items>().Where(i => i.PobjectId == @object.PobjectId).ToList();
            if (items.Count > 0)
            {
                Print(items.ToArray(), printer, converter, pattern, encoding);
            }
        }

        public void Print(Items item, IPrinter printer, IPrintingConverter converter, string pattern, EncodingEnum encoding)
        {
            printer.Send(converter.ToByte(item, pattern, encoding));
        }

        public void Print(Objects[] objects, PrinterEnum printer)
        {
            if (objects != null && objects.Length > 0)
            {
                foreach (var @object in objects)
                {
                    Print(@object, printer);
                }
            }
        }

        public void Print(Items[] items, PrinterEnum printer)
        {
            if(items != null && items.Length > 0)
            {
                foreach (var item in items)
                {
                    Print(item, printer);
                }
            }
        }

        public void Print(Objects @object, PrinterEnum printer)
        {
            switch (printer)
            {
                case PrinterEnum.Local:
                {
                    {
                        Print(@object, LocalPrinter, Converter, Model.Instance.Config.CurrentConfig.ObjectBarcodeLabel,
                            Model.Instance.Config.CurrentConfig.PrinterCodePage);
                        break;
                    }
                }
                case PrinterEnum.Desctop:
                {
                    Print(@object, DesktopPrinter, Converter, Model.Instance.Config.CurrentConfig.ObjectBarcodeLabel,
                        Model.Instance.Config.CurrentConfig.PrinterCodePage);
                    break;
                }
                case PrinterEnum.Mobile:
                {
                    Print(@object, MobilePrinter, Converter,
                        Model.Instance.Config.CurrentConfig.PortableObjectBarcodeLabel,
                        Model.Instance.Config.CurrentConfig.PortablePrinterCodePage);
                    break;
                }
            }
        }

        public void Print(Items item, PrinterEnum printer)
        {
            switch (printer)
            {
                case PrinterEnum.Local:
                {
                    Print(item, LocalPrinter, Converter, Model.Instance.Config.CurrentConfig.ItemBarcodeLabel,
                        Model.Instance.Config.CurrentConfig.PrinterCodePage);
                    break;
                }
                case PrinterEnum.Desctop:
                {
                    Print(item, DesktopPrinter, Converter, Model.Instance.Config.CurrentConfig.ItemBarcodeLabel,
                        Model.Instance.Config.CurrentConfig.PrinterCodePage);
                    break;
                }
                case PrinterEnum.Mobile:
                {
                    Print(item, MobilePrinter, Converter, Model.Instance.Config.CurrentConfig.PortableItemBarcodeLabel,
                        Model.Instance.Config.CurrentConfig.PortablePrinterCodePage);
                    break;
                }
            }
        }

        protected override Action StartAction
        {
            get
            {
                return () =>
                {
                    LocalPrinter = new LocalPrinter();
                    MobilePrinter = new MobilePrinter();
                    DesktopPrinter = new DesktopPrinter();
                    Converter = new PrintingConverter();
                };
            }
        }

        protected override Action StopAction
        {
            get
            {
                return () =>
                {
                    LocalPrinter = null;
                    MobilePrinter = null;
                    DesktopPrinter = null;
                    Converter = null;
                };
            }
        }
    }
}