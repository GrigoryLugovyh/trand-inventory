using System;

namespace StockCore.Environments
{
    public class LicenseStockEnvironment : StockEnvironment
    {
        public override string FriendlyName
        {
            get { return "��������������"; }
        }

        public bool IsLicensed { get; protected set; }

        protected override Action StartAction
        {
            get
            {
                return () =>
                           {
                               IsLicensed = true;
                           };
            }
        }

        protected override Action StopAction
        {
            get
            {
                return () =>
                           {
                               IsLicensed = false;
                           };
            }
        }
    }
}