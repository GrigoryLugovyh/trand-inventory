﻿using System;

namespace StockCore.Environments
{
    /// <summary>
    /// Взаимодействие с базой данных, репозиторий доступа
    /// </summary>
    public class RepositoryStockEnvironment : StockEnvironment
    {
        public override string FriendlyName
        {
            get { return "Доступ к базе данных"; }
        }

        protected override Action StartAction
        {
            get
            {
                return () =>
                    {

                    };
            }
        }

        protected override Action StopAction
        {
            get
            {
                return () =>
                    {

                    };
            }
        }
    }
}