using System;
using System.IO;
using StockCore.Common;
using StockCore.Handlers.Config;

namespace StockCore.Environments
{
    public class ConfigStockEnvironment : StockEnvironment
    {
        public override string FriendlyName
        {
            get { return "Конфигурация"; }
        }

        public StockConfig CurrentConfig { get; protected set; }

        public Configurator<StockConfig> Configurator { get; protected set; }

        public string ConfigPath
        {
            get { return Path.Combine(Model.Instance.Common.CurrentDirectory, Constants.StockSettingsName); }
        }

        public void Save()
        {
            Configurator.Object = CurrentConfig;
            Configurator.Save();
        }

        protected override Action StartAction
        {
            get
            {
                return () =>
                           {
                               Action setDefault = ()=>
                               {
                                   CurrentConfig = Configurator.Object = StockConfig.Default;
                                   Configurator.Save();
                               };

                               if (Configurator == null)
                               {
                                   Configurator = new Configurator<StockConfig> {ObjectPath = ConfigPath};
                               }

                               if (!File.Exists(Configurator.ObjectPath))
                               {
                                   setDefault();
                               }
                               else
                               {
                                   Configurator.Load();
                                   CurrentConfig = Configurator.Object;

                                   if (Configurator.LastException != null)
                                   {
                                       setDefault();
                                   }
                               }
                           };
            }
        }

        protected override Action StopAction
        {
            get
            {
                return () =>
                           {
                               Configurator = null;
                           };
            }
        }
    }
}