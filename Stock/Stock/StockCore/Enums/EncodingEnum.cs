﻿using System;
using System.Xml.Serialization;

namespace StockCore.Enums
{
    /// <summary>
    /// Поддерживаемые кодировки файлов
    /// </summary>
    [Serializable]
    public enum EncodingEnum
    {
        [XmlEnum]
        Win,

        [XmlEnum]
        Dos,

        [XmlEnum]
        Unicode,

        [XmlEnum]
        Utf
    }
}