namespace StockCore.Enums
{
    public enum ViewsEnum
    {
        Unknown,

        Main,

        Inventory,

        Information,

        Onjects,

        Exchange
    }
}