﻿namespace StockCore.Enums
{
    /// <summary>
    /// Варианты лога
    /// </summary>
    public enum LogsEnum
    {
        /// <summary>
        /// Системный
        /// </summary>
        System,
 
        /// <summary>
        /// Лог обмена
        /// </summary>
        Exchange
    }
}