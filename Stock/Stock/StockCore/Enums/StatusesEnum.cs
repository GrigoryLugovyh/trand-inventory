namespace StockCore.Enums
{
    public enum StatusesEnum
    {
        Correct = 0,

        Defective = 1,

        Replacement = 2,

        Double = 3
    }
}