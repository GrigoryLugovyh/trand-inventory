using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;

namespace StockCore.Factories
{
    public sealed class DbTypeFactory
    {
        private static readonly object Sync = new object();

        private static volatile DbTypeFactory _instance;

        /// <summary>
        /// �������������
        /// </summary>
        public static DbTypeFactory Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (Sync)
                    {
                        if (_instance == null)
                        {
                            _instance = new DbTypeFactory();
                        }
                    }
                }

                return _instance;
            }
        }

        private Dictionary<Type, DbType> _map;
        private Dictionary<Type, DbType> TypeMap()
        {
            if (_map == null)
            {
                _map = new Dictionary<Type, DbType>();
                _map[typeof(byte)] = DbType.Byte;
                _map[typeof(sbyte)] = DbType.SByte;
                _map[typeof(short)] = DbType.Int16;
                _map[typeof(ushort)] = DbType.UInt16;
                _map[typeof(int)] = DbType.Int32;
                _map[typeof(uint)] = DbType.UInt32;
                _map[typeof(long)] = DbType.Int64;
                _map[typeof(ulong)] = DbType.UInt64;
                _map[typeof(float)] = DbType.Single;
                _map[typeof(double)] = DbType.Double;
                _map[typeof(decimal)] = DbType.Decimal;
                _map[typeof(bool)] = DbType.Boolean;
                _map[typeof(string)] = DbType.String;
                _map[typeof(char)] = DbType.StringFixedLength;
                _map[typeof(Guid)] = DbType.Guid;
                _map[typeof(DateTime)] = DbType.DateTime;
                _map[typeof(byte[])] = DbType.Binary;
                _map[typeof(byte?)] = DbType.Byte;
                _map[typeof(sbyte?)] = DbType.SByte;
                _map[typeof(short?)] = DbType.Int16;
                _map[typeof(ushort?)] = DbType.UInt16;
                _map[typeof(int?)] = DbType.Int32;
                _map[typeof(uint?)] = DbType.UInt32;
                _map[typeof(long?)] = DbType.Int64;
                _map[typeof(ulong?)] = DbType.UInt64;
                _map[typeof(float?)] = DbType.Single;
                _map[typeof(double?)] = DbType.Double;
                _map[typeof(decimal?)] = DbType.Decimal;
                _map[typeof(bool?)] = DbType.Boolean;
                _map[typeof(char?)] = DbType.StringFixedLength;
                _map[typeof(Guid?)] = DbType.Guid;
                _map[typeof(DateTime?)] = DbType.DateTime;
            }

            return _map;
        }

        /// <summary>
        /// ���������� ���
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public DbType GetType(Type t)
        {
            DbType result;
            TypeMap().TryGetValue(t, out result);
            return result;
        }

        /// <summary>
        /// ���������� ���
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public DbType GetType(string type)
        {
            switch (type.ToLower(CultureInfo.InvariantCulture))
            {
                case "long":
                case "int":
                case "int16":
                case "int32":
                case "integer":
                    {
                        return DbType.Int32;
                    }

                case "str":
                case "string":
                    {
                        return DbType.String;
                    }

                case "float":
                case "double":
                case "decimal":
                    {
                        return DbType.Double;
                    }

                case "date":
                case "time":
                case "datetime":
                    {
                        return DbType.DateTime;
                    }

                case "bit":
                case "bool":
                case "boolean":
                    {
                        return DbType.Boolean;
                    }
            }

            return DbType.String;
        }
    }
}