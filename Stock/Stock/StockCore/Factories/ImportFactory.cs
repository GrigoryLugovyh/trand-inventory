﻿using System;
using StockCore.Enums;
using StockCore.Handlers.Exchange;
using StockCore.Handlers.Exchange.Configuration;
using StockCore.Handlers.Exchange.Configuration.CSV;
using StockCore.Handlers.Exchange.Imports;
using StockCore.Interfaces.Common;
using StockCore.Interfaces.Handlers;
using StockCore.Interfaces.Views;

namespace StockCore.Factories
{
    /// <summary>
    /// Фабрика импорта
    /// </summary>
    public sealed class ImportFactory
    {
        private static readonly object Sync = new object();

        private static volatile ImportFactory _instance;

        /// <summary>
        /// Инициализация
        /// </summary>
        public static ImportFactory Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (Sync)
                    {
                        if (_instance == null)
                        {
                            _instance = new ImportFactory();
                        }
                    }
                }

                return _instance;
            }
        }

        public Exchange GetInstance(ImportEnum import, IExchangeProtocol protocol, ExchangeSettings settings)
        {
            switch (import)
            {
                case ImportEnum.Cvs:
                    {
                        return new CsvImport(protocol as ICsvImportProtocol, settings as CsvExchangeSettings);
                    }
            }

            throw new Exception("Не найдена реализация обмена");
        }
    }
}