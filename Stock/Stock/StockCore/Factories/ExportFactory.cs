﻿using System;
using StockCore.Enums;
using StockCore.Handlers.Exchange;
using StockCore.Handlers.Exchange.Configuration;
using StockCore.Handlers.Exchange.Configuration.CSV;
using StockCore.Handlers.Exchange.Exports;
using StockCore.Interfaces.Handlers;

namespace StockCore.Factories
{
    /// <summary>
    /// Фбрика экспорта
    /// </summary>
    public sealed class ExportFactory
    {
        private static readonly object Sync = new object();

        private static volatile ExportFactory _instance;

        /// <summary>
        /// Инициализация
        /// </summary>
        public static ExportFactory Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (Sync)
                    {
                        if (_instance == null)
                        {
                            _instance = new ExportFactory();
                        }
                    }
                }

                return _instance;
            }
        }

        public Exchange GetInstance(ExportEnum export, IExchangeProtocol protocol, ExchangeSettings settings)
        {
            switch (export)
            {
                    case ExportEnum.Cvs:
                    {
                        return new CsvExport(protocol, settings as CsvExchangeSettings);
                    }
            }

            throw new Exception("Не найдена реализация обмена");
        }
    }
}