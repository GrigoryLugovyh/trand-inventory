using System;
using System.Runtime.InteropServices;
using System.Text;
using StockCore.Common;
using StockCore.Handlers.Terminals.Default;
using StockCore.Handlers.Terminals.Honewell;
using StockCore.Handlers.Terminals.Motorola;
using StockCore.Interfaces.Terminal;

namespace StockCore.Factories
{
    public sealed class TerminalsFactory
    {
        private static readonly object Sync = new object();

        private static volatile TerminalsFactory _instance;

        /// <summary>
        /// �������������
        /// </summary>
        public static TerminalsFactory Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (Sync)
                    {
                        if (_instance == null)
                        {
                            _instance = new TerminalsFactory();
                        }
                    }
                }

                return _instance;
            }
        }

        public ITerminal GetTerminal()
        {
            ITerminal terminal = null;

            var strbuild = new StringBuilder(200);
            SystemParametersInfo(SPI_GETPLATFORMTYPE, 200, strbuild, 0);

            string deviceName = strbuild.ToString().ToLower();
            if (deviceName.IndexOf(Terminals.Motorola, StringComparison.OrdinalIgnoreCase) > -1 ||
                deviceName.IndexOf(Terminals.Symbol, StringComparison.OrdinalIgnoreCase) > -1)
            {
                terminal = new Motorola();
            }
            else if (deviceName.IndexOf(Terminals.Honewell, StringComparison.OrdinalIgnoreCase) > -1)
            {
                terminal = new Honewell();
            }
            else
            {
                SystemParametersInfo(SPI_GETOEMINFO, 200, strbuild, 0);
                deviceName = strbuild.ToString().ToLower();

                if (deviceName.IndexOf(Terminals.Motorola, StringComparison.OrdinalIgnoreCase) > -1 ||
                    deviceName.IndexOf(Terminals.Symbol, StringComparison.OrdinalIgnoreCase) > -1)
                {
                    terminal = new Motorola();
                }
                else if (deviceName.IndexOf(Terminals.Honewell, StringComparison.OrdinalIgnoreCase) > -1)
                {
                    terminal = new Honewell();
                }
            }

            if (terminal == null)
            {
                terminal = new Terminal();
            }

            terminal.Init();

            return terminal;
        }

        [DllImport("coredll.dll")]
        private static extern bool SystemParametersInfo(uint uiAction, uint uiParam, StringBuilder pvParam, uint fWinIni);

        private const uint SPI_GETPLATFORMTYPE = 257;
        private const uint SPI_GETOEMINFO = 258;
    }
}