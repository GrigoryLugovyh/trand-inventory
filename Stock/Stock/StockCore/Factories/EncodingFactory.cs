﻿using System.Text;
using StockCore.Common.Encoding;
using StockCore.Enums;

namespace StockCore.Factories
{
    /// <summary>
    /// Фабрика кодировок
    /// </summary>
    public sealed class EncodingFactory
    {
        private static readonly object Sync = new object();

        private static volatile EncodingFactory _instance;

        /// <summary>
        /// Инициализация
        /// </summary>
        public static EncodingFactory Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (Sync)
                    {
                        if (_instance == null)
                        {
                            _instance = new EncodingFactory();
                        }
                    }
                }

                return _instance;
            }
        }

        /// <summary>
        /// Релизация
        /// </summary>
        /// <param name="encoding"></param>
        /// <returns></returns>
        public Encoding GetInstance(EncodingEnum encoding)
        {
            Encoding code;

            switch (encoding)
            {
                case EncodingEnum.Dos:
                    {
                        code = new DosEncoding();
                        break;
                    }
                case EncodingEnum.Unicode:
                    {
                        code = Encoding.Unicode;
                        break;
                    }
                case EncodingEnum.Utf:
                    {
                        code = Encoding.UTF8;
                        break;
                    }
                default:
                    {
                        code = new WinEncoding();
                        break;
                    }
            }

            return code;
        }
    }
}