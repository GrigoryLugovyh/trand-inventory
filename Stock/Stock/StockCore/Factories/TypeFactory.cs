﻿using System;
using System.Globalization;

namespace StockCore.Factories
{
    /// <summary>
    /// Фабрика типов
    /// </summary>
    public sealed class TypeFactory
    {
        private static readonly object Sync = new object();

        private static volatile TypeFactory _instance;

        /// <summary>
        /// Инициализация
        /// </summary>
        public static TypeFactory Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (Sync)
                    {
                        if (_instance == null)
                        {
                            _instance = new TypeFactory();
                        }
                    }
                }

                return _instance;
            }
        }

        /// <summary>
        /// Возвращаем тип
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public Type GetType(string type)
        {
            switch (type.ToLower(CultureInfo.InvariantCulture))
            {
                case "long":
                case "int":
                case "int16":
                case "int32":
                case "integer":
                    {
                        return typeof (int);
                    }

                case "str":
                case "string":
                    {
                        return typeof (string);
                    }

                case "float":
                case "double":
                case "decimal":
                    {
                        return typeof (double);
                    }

                case "date":
                case "time":
                case "datetime":
                    {
                        return typeof (DateTime);
                    }

                case "bit":
                case "bool":
                case "boolean":
                    {
                        return typeof (bool);
                    }
            }

            return typeof (string);
        }
    }
}