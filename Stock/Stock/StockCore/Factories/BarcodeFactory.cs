using System;

namespace StockCore.Factories
{
    public sealed class BarcodeFactory
    {
        private static readonly object Sync = new object();

        private static volatile BarcodeFactory _instance;

        private string _template;

        public static BarcodeFactory Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (Sync)
                    {
                        if (_instance == null)
                        {
                            _instance = new BarcodeFactory();
                            _instance.Init();
                        }
                    }
                }

                return _instance;
            }
        }

        public string Prefix { get; set; }

        public string Barcode { get; set; }

        public string Postfix { get; set; }

        readonly Func<int, string[], string> _getValue = (i, g) =>
        {
            var value = string.Empty;

            if (g.Length > i)
            {
                value = g[i];
            }

            return value;
        };

        private void Init()
        {
            try
            {
                _template = Model.Instance.Config.CurrentConfig.ItemBcRegex;

                var g = _template.Split('.');

                Prefix = _getValue(0, g);
                Barcode = _getValue(1, g);
                Postfix = _getValue(2, g);
            }
            catch (Exception)
            {
                _template = Prefix = Barcode = Postfix = string.Empty;
            }
        }

        public bool Check(string barcode)
        {
            if (string.IsNullOrEmpty(_template))
            {
                return true;
            }

            var g = barcode.Split('.');

            return
                Prefix.Length >= _getValue(0, g).Length &&
                Barcode.Length >= _getValue(1, g).Length &&
                Postfix.Length >= _getValue(2, g).Length;
        }
    }
}