﻿using System;

namespace StockCore.Events
{
    public class StockEventArgs : EventArgs
    {
        public string Message { get; private set; }

        public object[] Arguments { get; private set; }

        public StockEventArgs(string msg, params object[] args)
        {
            Message = msg;
            Arguments = args;
        }
    }
}