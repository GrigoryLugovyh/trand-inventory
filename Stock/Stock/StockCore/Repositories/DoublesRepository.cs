using System.Collections.Generic;
using StockCore.Entity;
using StockCore.Interfaces.Repositories;
using StockCore.Repositories.Loaders;
using StockCore.Repositories.Savers;

namespace StockCore.Repositories
{
    public class DoublesRepository : IDoublesRepository
    {
        public List<Doubles> Load()
        {
            return Loader<Doubles>.Instance.Load();
        }

        public List<Doubles> Load(string where)
        {
            return Loader<Doubles>.Instance.Load(where);
        }

        public Doubles LoadSingle(string where)
        {
            return Loader<Doubles>.Instance.LoadSingle(where);
        }

        public bool Save(Doubles entity)
        {
            return Saver<Doubles>.Instance.Save(entity);
        }

        public bool Save(List<Doubles> entities)
        {
            return Saver<Doubles>.Instance.Save(entities);
        }

        public bool Delete(Doubles entity)
        {
            return Saver<Doubles>.Instance.Delete(entity);
        }

        public bool Delete(List<Doubles> entities)
        {
            return Saver<Doubles>.Instance.Delete(entities);
        }
    }
}