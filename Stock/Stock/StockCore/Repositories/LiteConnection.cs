using System;
using System.Data;
using System.Data.Common;
using System.Data.SQLite;
using IDbConnection = StockCore.Interfaces.Repositories.IDbConnection;

namespace StockCore.Repositories
{
    public class LiteConnection : IDbConnection
    {
        public LiteConnection(string connection, int timeout)
        {
            Id = Guid.NewGuid();

            Connection = new SQLiteConnection(connection);

            CmdTimeout = timeout;
            NameConnection = string.Format("{0}: {1}", Connection.DataSource, Connection.Database);
        }

        public Guid Id { get; protected set; }

        public string NameConnection { get; set; }

        public bool IsActive { get; set; }

        public int CmdTimeout
        {
            get;
            protected set;
        }

        public DbTransaction Transaction
        {
            get;
            protected set;
        }

        public DbConnection Connection
        {
            get;
            protected set;
        }

        public Exception LastException
        {
            get;
            protected set;
        }

        public void CheckConnection()
        {
            try
            {
                if (Connection == null)
                    throw new Exception("���������� � ���� ������ �� ����������������!");

                if (Connection.State != ConnectionState.Open)
                    Connection.Open();
            }
            catch (Exception ex)
            {
                LastException = ex;
            }
        }

        public void BeginTransaction(IsolationLevel isolation)
        {
            if (Connection.State != ConnectionState.Open)
                Connection.Open();

            Transaction = Connection.BeginTransaction(isolation);
        }

        public void CommitTransaction()
        {
            if (Transaction == null) return;

            Transaction.Commit();
            Transaction.Dispose();
            Transaction = null;
        }

        public void RollbackTransaction()
        {
            if (Transaction == null) return;

            Transaction.Rollback();
            Transaction.Dispose();
            Transaction = null;
        }

        public void Close()
        {
            if (Connection == null)
                return;

            Connection.Close();
        }

        public virtual IDbDataParameter CreateParameter()
        {
            return new SQLiteParameter();
        }

        public virtual IDbCommand CreateCommand(string query)
        {
            return new SQLiteCommand(query);
        }

        public virtual int GetLastInsertedRowId()
        {
            return Convert.ToInt32(ExecuteScalar("SELECT last_insert_rowid();") ?? -1);
        }

        public virtual int Fill(string query, ref DataTable table)
        {
            try
            {
                CheckConnection();

                using (var command = new SQLiteCommand(query, (SQLiteConnection)Connection, (SQLiteTransaction)Transaction))
                {
                    command.CommandTimeout = CmdTimeout;

                    using (var adapter = new SQLiteDataAdapter(command))
                    {
                        return adapter.Fill(table);
                    }
                }
            }
            catch (Exception ex)
            {
                LastException = ex;
                table = null;
                return -1;
            }
        }

        public virtual int Fill(IDbCommand cmd, ref DataTable table)
        {
            try
            {
                CheckConnection();

                using (var adapter = new SQLiteDataAdapter((SQLiteCommand)cmd))
                {
                    adapter.SelectCommand.Connection = (SQLiteConnection)Connection;
                    adapter.SelectCommand.Transaction = (SQLiteTransaction)Transaction;
                    adapter.SelectCommand.CommandTimeout = CmdTimeout;
                    return adapter.Fill(table);
                }
            }
            catch (Exception ex)
            {
                LastException = ex;
                table = null;
                return -1;
            }
        }

        public virtual int ExecuteNonQuery(string query)
        {
            var parameters = new SQLiteParameter[] { };
            return ExecuteNonQuery(query, parameters);
        }

        public virtual int ExecuteNonQuery(DbCommand cmd)
        {
            try
            {
                CheckConnection();

                cmd.Connection = Connection;
                cmd.Transaction = Transaction;
                cmd.CommandTimeout = CmdTimeout;
                return cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                LastException = ex;
                return -1;
            }
        }

        public virtual object ExecuteScalar(string query)
        {
            var parameters = new string[] { };
            return ExecuteScalar(query, parameters, parameters);
        }

        public virtual int ExecuteNonQueryOrError(string query, string[] paramNames, object[] paramValues)
        {
            var res = ExecuteNonQuery(query, paramNames, paramValues);
            if (res == -1)
            {
                throw LastException;
            }

            return res;
        }

        public virtual int ExecuteNonQuery(string query, string[] paramNames, object[] paramValues)
        {
            try
            {
                CheckConnection();

                using (var command = new SQLiteCommand(query, (SQLiteConnection)Connection, (SQLiteTransaction)Transaction))
                {
                    var length = paramNames.Length;
                    if (length != paramValues.Length) throw new Exception("param_names.Length != param_values.Length");
                    for (int i = 0; i < length; i++)
                    {
                        var param = new SQLiteParameter(paramNames[i], paramValues[i]);
                        command.Parameters.Add(param);
                    }

                    command.CommandTimeout = CmdTimeout;
                    return command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                LastException = ex;
                return -1;
            }
        }

        public virtual int ExecuteNonQuery(string query, DbParameter[] parameters)
        {
            try
            {
                CheckConnection();

                using (var command = new SQLiteCommand(query, (SQLiteConnection)Connection, (SQLiteTransaction)Transaction))
                {
                    foreach (var par in parameters)
                    {
                        var paramNew = new SQLiteParameter(par.ParameterName, par.Value);
                        command.Parameters.Add(paramNew);
                    }

                    command.CommandTimeout = CmdTimeout;
                    return command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                LastException = ex;
                return -1;
            }
        }

        public virtual int ExecuteNonQueryOrError(DbCommand command, string[] paramNames, object[] paramValues)
        {
            var res = ExecuteNonQuery(command, paramNames, paramValues);
            if (res == -1)
            {
                throw LastException;
            }
            return res;
        }

        public virtual int ExecuteNonQuery(DbCommand command, string[] paramNames, object[] paramValues)
        {
            try
            {
                CheckConnection();

                var length = paramNames.Length;
                if (length != paramValues.Length) throw new Exception("param_names.Length != param_values.Length");
                for (int i = 0; i < length; i++)
                {
                    command.Parameters[paramNames[i]].Value = paramValues[i];
                }

                command.Connection = Connection;
                command.Transaction = Transaction;
                command.CommandTimeout = CmdTimeout;
                return command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                LastException = ex;
                return -1;
            }
        }

        public virtual object ExecuteScalar(DbCommand cmd)
        {
            try
            {
                CheckConnection();

                cmd.Connection = Connection;
                cmd.Transaction = Transaction;
                cmd.CommandTimeout = CmdTimeout;
                return cmd.ExecuteScalar();
            }
            catch (Exception ex)
            {
                LastException = ex;
                return -1;
            }
        }

        public virtual object ExecuteScalar(string query, string[] paramNames, object[] paramValues)
        {
            var length = paramNames.Length;
            if (length != paramValues.Length) throw new Exception("param_names.Length != param_values.Length");
            var sqlParameters = new SQLiteParameter[length];

            for (int i = 0; i < length; i++)
            {
                sqlParameters[i] = new SQLiteParameter(paramNames[i], paramValues[i]);
            }

            return ExecuteScalar(query, sqlParameters);
        }

        public virtual object ExecuteScalar(string query, DbParameter[] parameters)
        {
            try
            {
                CheckConnection();

                using (var command = new SQLiteCommand(query, (SQLiteConnection)Connection, (SQLiteTransaction)Transaction))
                {
                    foreach (var par in parameters)
                    {
                        var paramNew = new SQLiteParameter(par.ParameterName, par.Value);
                        command.Parameters.Add(paramNew);
                    }
                    command.CommandTimeout = CmdTimeout;
                    var res = command.ExecuteScalar();
                    return res;
                }
            }
            catch (Exception ex)
            {
                LastException = ex;
                return -1;
            }
        }

        public virtual object ExecuteScalar(DbCommand command, string[] paramNames, object[] paramValues)
        {
            try
            {
                CheckConnection();

                var length = paramNames.Length;

                if (length != paramValues.Length) 
                    throw new Exception("param_names.Length != param_values.Length");

                for (int i = 0; i < length; i++)
                {
                    command.Parameters[paramNames[i]].Value = paramValues[i];
                }

                command.CommandTimeout = CmdTimeout;
                command.Connection = Connection;
                command.Transaction = Transaction;
                var res = command.ExecuteScalar();
                return res;
            }
            catch (Exception ex)
            {
                LastException = ex;
                return -1;
            }
        }
    }
}