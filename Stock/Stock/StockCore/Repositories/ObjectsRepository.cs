using System.Collections.Generic;
using StockCore.Entity;
using StockCore.Interfaces.Repositories;
using StockCore.Repositories.Loaders;
using StockCore.Repositories.Savers;

namespace StockCore.Repositories
{
    public class ObjectsRepository : IObjectsRepository
    {
        public List<Objects> Load()
        {
            return Loader<Objects>.Instance.Load();
        }

        public List<Objects> Load(string where)
        {
            return Loader<Objects>.Instance.Load(where);
        }

        public Objects LoadSingle(string where)
        {
            return Loader<Objects>.Instance.LoadSingle(where);
        }

        public bool Save(Objects entity)
        {
            return Saver<Objects>.Instance.Save(entity);
        }

        public bool Save(List<Objects> entities)
        {
            return Saver<Objects>.Instance.Save(entities);
        }

        public bool Delete(Objects entity)
        {
            return Saver<Objects>.Instance.Delete(entity);
        }

        public bool Delete(List<Objects> entities)
        {
            return Saver<Objects>.Instance.Delete(entities);
        }
    }
}