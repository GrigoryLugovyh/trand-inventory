using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using StockCore.Entity;
using StockCore.Interfaces.Repositories;

namespace StockCore.Repositories.Loaders
{
    public sealed class Loader<T> : ILoadable<T> where T : StockEntity, new()
    {
        private static readonly object Sync = new object();

        private static volatile Loader<T> _instance;

        public static Loader<T> Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (Sync)
                    {
                        if (_instance == null)
                        {
                            _instance = new Loader<T>();
                        }
                    }
                }

                return _instance;
            }
        }


        public List<T> Loading(int top, string where)
        {
            var entity = new T();

            var query = string.Format(entity.LoadQuery, top > 0 ? string.Format("TOP {0}", top) : string.Empty);

            if(string.IsNullOrEmpty(query))
            {
                throw new Exception("������ ������� � ������: �� ������� ������������ ������");
            }

            if(!string.IsNullOrEmpty(where))
            {
                query += string.Format(" WHERE {0}", where);
            }

            var data = new DataTable();

            var entities = new List<T>();

            if (Model.Instance.Repository.Connection.Fill(query, ref data) > 0)
            {
                if (data != null && data.Rows.Count > 0)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        entities.Add(entity.Prepare<T>(row));
                    }
                }

                return entities;
            }
            if(Model.Instance.Repository.Connection.LastException != null)
            {
                throw Model.Instance.Repository.Connection.LastException;
            }

            return entities;
        }

        public List<T> Load()
        {
            lock (Sync)
            {
                return Loading(0, string.Empty);
            }
        }

        public List<T> Load(string where)
        {
            lock (Sync)
            {
                return Loading(0, where);
            }
        }

        public T LoadSingle(string where)
        {
            lock (Sync)
            {
                return Loading(1, where).FirstOrDefault();
            }
        }
    }
}