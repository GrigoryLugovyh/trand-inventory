using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Text;
using StockCore.Common.Attributes;
using StockCore.Entity;
using StockCore.Factories;
using StockCore.Interfaces.Repositories;

namespace StockCore.Repositories.Savers
{
    public sealed class Saver<T> : ISaveable<T>, IDeleteable<T> where T : StockEntity, new()
    {
        private static readonly object Sync = new object();

        private static volatile Saver<T> _instance;

        public static Saver<T> Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (Sync)
                    {
                        if (_instance == null)
                        {
                            _instance = new Saver<T>();
                        }
                    }
                }

                return _instance;
            }
        }

        private readonly Dictionary<T, DbCommand> _insertCommands = new Dictionary<T, DbCommand>();
        private readonly Dictionary<T, DbCommand> _updateCommands = new Dictionary<T, DbCommand>();
        private readonly Dictionary<T, DbCommand> _deleteCommands = new Dictionary<T, DbCommand>();

        private static string GetColumnName(ICustomAttributeProvider property)
        {
            var columnNameInfo = property.GetCustomAttributes(typeof(ColumnAttribute), true).FirstOrDefault();
            if (columnNameInfo != null)
            {
                return ((ColumnAttribute)columnNameInfo).Column;
            }

            return null;
        }

        private static DbParameter[] CreateParameters(T entity)
        {
            var pars = new List<DbParameter>();

            object[] tableProperty = entity.GetType().GetCustomAttributes(typeof(TableAttribute), true);
            if (tableProperty.Length == 0)
                throw new ArgumentNullException(string.Format("C������� \"{0}\" ��� � ��", entity));

            PropertyInfo[] fieldProperties = entity.GetType().GetProperties();
            foreach (PropertyInfo property in
                     fieldProperties.Where( attr => attr.GetCustomAttributes(true).Where(info => info.GetType() == typeof(ColumnAttribute)).Count() > 0))
            {
                string columnName = GetColumnName(property);
                if (columnName != null)
                {
                    var par = (DbParameter)Model.Instance.Repository.Connection.CreateParameter();

                    object value = property.GetValue(entity, null);

                    Type propType = property.PropertyType;

                    par.ParameterName = String.Format("@{0}", columnName);
                    par.DbType = DbTypeFactory.Instance.GetType(propType);
                    par.IsNullable = value == null;
                    par.Value = value ?? DBNull.Value;
                    pars.Add(par);
                }
            }

            return pars.ToArray();
        }

        public DbCommand InsertBuilder(T entity)
        {
            const string queryPattern = "INSERT INTO [{0}] ({1}) VALUES ({2})";
            DbCommand result = null;

            DbParameter[] parameters = CreateParameters(entity);

            if (parameters.Count() > 0)
            {
                string table = string.Empty;
                object[] tableProperty = entity.GetType().GetCustomAttributes(typeof(TableAttribute), true);
                if (tableProperty.Length == 0)
                    throw new ArgumentNullException(string.Format("C������� \"{0}\" ��� � ��", entity));
                var tableAttribute = tableProperty[0] as TableAttribute;

                if (tableAttribute != null)
                    table = tableAttribute.Table;

                var columnsBuider = new StringBuilder();
                var valuesBuider = new StringBuilder();

                foreach (DbParameter param in parameters)
                {
                    string columnName = param.ParameterName.Substring(1);
                    if (columnName == "EntityId") 
                        continue;

                    columnsBuider.Append(String.Format("[{0}], ", columnName));
                    valuesBuider.Append(String.Format("{0}, ", param.ParameterName));
                }

                Func<string, string> format = x =>
                {
                    string y = x.Trim();
                    y = y.Substring(0, y.Length - 1);
                    return y;
                };

                string columns = format(columnsBuider.ToString());
                string values = format(valuesBuider.ToString());

                string insertQuery = string.Format(queryPattern, table, columns, values);

                result = (DbCommand) Model.Instance.Repository.Connection.CreateCommand(insertQuery);
                result.Parameters.AddRange(parameters);
                result.CommandType = CommandType.Text;
                result.Prepare();
            }

            return result;
        }

        public DbCommand UpdateBuilder(T entity)
        {
            const string queryPattern = "UPDATE [{0}] SET {1} WHERE {2}";
            DbCommand result = null;

            DbParameter[] parameters = CreateParameters(entity);

            if (parameters.Count() > 0)
            {
                string table = string.Empty;
                object[] tableProperty = entity.GetType().GetCustomAttributes(typeof(TableAttribute), true);
                if (tableProperty.Length == 0)
                    throw new ArgumentNullException(string.Format("C������� \"{0}\" ��� � ��", entity));

                var tableAttribute = tableProperty[0] as TableAttribute;

                if (tableAttribute != null)
                    table = tableAttribute.Table;

                var setPartBuider = new StringBuilder();
                foreach (DbParameter param in parameters)
                {
                    string columnName = param.ParameterName.Substring(1);
                    if (columnName == "EntityId") 
                        continue;

                    setPartBuider.Append(String.Format("[{0}] = {1}, ", columnName, param.ParameterName));
                }

                Func<string, string> format = x =>
                {
                    string y = x.Trim();
                    y = y.Substring(0, y.Length - 1);
                    return y;
                };

                string setPart = format(setPartBuider.ToString());
                const string wherePart = "[EntityId] = @EntityId";

                string updateQuery = string.Format(queryPattern, table, setPart, wherePart);

                result = (DbCommand)Model.Instance.Repository.Connection.CreateCommand(updateQuery);
                result.Parameters.AddRange(parameters);
                result.CommandType = CommandType.Text;
                result.Prepare();
            }

            return result;
        }

        public DbCommand DeleteBuilder(T entity)
        {
            const string queryPattern = "DELETE FROM [{0}]";

            string table = string.Empty;
            object[] tableProperty = typeof(T).GetCustomAttributes(typeof(TableAttribute), true);
            if (tableProperty.Length == 0)
                throw new ArgumentNullException(string.Format("C������� \"{0}\" ��� � ��", typeof(T)));

            var tableAttribute = tableProperty[0] as TableAttribute;

            if (tableAttribute != null)
                table = tableAttribute.Table;

            string deleteQuery = string.Format(queryPattern, table);

            var result = (DbCommand)Model.Instance.Repository.Connection.CreateCommand(deleteQuery);
            result.CommandType = CommandType.Text;

            DbParameter[] parameters = CreateParameters(entity);

            if (parameters.Count() == 0)
                return null;

            const string where = "[EntityId] = @EntityId";
            result.CommandText = string.Format("{0} WHERE {1}", result.CommandText, where);
            result.Parameters.AddRange(parameters);
            result.Prepare();

            return result;
        }

        public bool Saving(T entity)
        {
            DbCommand command;

            if (entity.EntityId == 0)
            {
                if (!_insertCommands.TryGetValue(entity, out command))
                {
                    command = InsertBuilder(entity);
                    _insertCommands.Add(entity, command);
                }
            }
            else
            {
                if (!_updateCommands.TryGetValue(entity, out command))
                {
                    command = UpdateBuilder(entity);
                    _updateCommands.Add(entity, command);
                }
            }

            return Model.Instance.Repository.Connection.ExecuteNonQuery(command) > 0;
        }

        public bool Deleting(T entity)
        {
            DbCommand command;

            if (!_deleteCommands.TryGetValue(entity, out command))
            {
                command = DeleteBuilder(entity);
                _deleteCommands.Add(entity, command);
            }

            return Model.Instance.Repository.Connection.ExecuteNonQuery(command) > 0;
        }

        public bool Save(T entity)
        {
            lock (Sync)
            {
                return Saving(entity);
            }
        }

        public bool Save(List<T> entities)
        {
            lock (Sync)
            {
                return entities.Aggregate(true, (current, entity) => current & Saving(entity));
            }
        }

        public bool Delete(T entity)
        {
            lock (Sync)
            {
                return Deleting(entity);
            }
        }

        public bool Delete(List<T> entities)
        {
            lock (Sync)
            {
                return entities.Aggregate(true, (current, entity) => current & Deleting(entity));
            }
        }
    }
}