using StockCore.Interfaces.Repositories;

namespace StockCore.Repositories
{
    public class Repositories
    {
        public Repositories(
            ICodesRepository codes,
            IDoublesRepository doubles,
            IItemsRepository items,
            IMolsRepository mols,
            IObjectsRepository objects,
            IStatusesRepository status,
            IUsersRepository users)
        {
            Codes = codes;
            Doubles = doubles;
            Items = items;
            Mols = mols;
            Objects = objects;
            Status = status;
            Users = users;
        }

        public ICodesRepository Codes { get; protected set; }

        public IDoublesRepository Doubles { get; protected set; }

        public IItemsRepository Items { get; protected set; }

        public IMolsRepository Mols { get; protected set; }

        public IObjectsRepository Objects { get; protected set; }

        public IStatusesRepository Status { get; protected set; }

        public IUsersRepository Users { get; protected set; }
    }
}