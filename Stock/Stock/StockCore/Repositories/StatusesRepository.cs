using System.Collections.Generic;
using StockCore.Entity;
using StockCore.Interfaces.Repositories;
using StockCore.Repositories.Loaders;
using StockCore.Repositories.Savers;

namespace StockCore.Repositories
{
    public class StatusesRepository : IStatusesRepository
    {
        public List<Statuses> Load()
        {
            return Loader<Statuses>.Instance.Load();
        }

        public List<Statuses> Load(string where)
        {
            return Loader<Statuses>.Instance.Load(where);
        }

        public Statuses LoadSingle(string where)
        {
            return Loader<Statuses>.Instance.LoadSingle(where);
        }

        public bool Save(Statuses entity)
        {
            return Saver<Statuses>.Instance.Save(entity);
        }

        public bool Save(List<Statuses> entities)
        {
            return Saver<Statuses>.Instance.Save(entities);
        }

        public bool Delete(Statuses entity)
        {
            return Saver<Statuses>.Instance.Delete(entity);
        }

        public bool Delete(List<Statuses> entities)
        {
            return Saver<Statuses>.Instance.Delete(entities);
        }
    }
}