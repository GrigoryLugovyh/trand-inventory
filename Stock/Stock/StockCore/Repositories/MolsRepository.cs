using System.Collections.Generic;
using StockCore.Entity;
using StockCore.Interfaces.Repositories;
using StockCore.Repositories.Loaders;
using StockCore.Repositories.Savers;

namespace StockCore.Repositories
{
    public class MolsRepository : IMolsRepository
    {
        public List<Mols> Load()
        {
            return Loader<Mols>.Instance.Load();
        }

        public List<Mols> Load(string where)
        {
            return Loader<Mols>.Instance.Load(where);
        }

        public Mols LoadSingle(string where)
        {
            return Loader<Mols>.Instance.LoadSingle(where);
        }

        public bool Save(Mols entity)
        {
            return Saver<Mols>.Instance.Save(entity);
        }

        public bool Save(List<Mols> entities)
        {
            return Saver<Mols>.Instance.Save(entities);
        }

        public bool Delete(Mols entity)
        {
            return Saver<Mols>.Instance.Delete(entity);
        }

        public bool Delete(List<Mols> entities)
        {
            return Saver<Mols>.Instance.Delete(entities);
        }
    }
}