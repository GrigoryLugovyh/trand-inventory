using System.Collections.Generic;
using StockCore.Entity;
using StockCore.Interfaces.Repositories;
using StockCore.Repositories.Loaders;
using StockCore.Repositories.Savers;

namespace StockCore.Repositories
{
    public class UsersRepository : IUsersRepository
    {
        public List<Users> Load()
        {
            return Loader<Users>.Instance.Load();
        }

        public List<Users> Load(string where)
        {
            return Loader<Users>.Instance.Load(where);
        }

        public Users LoadSingle(string where)
        {
            return Loader<Users>.Instance.LoadSingle(where);
        }

        public bool Save(Users entity)
        {
            return Saver<Users>.Instance.Save(entity);
        }

        public bool Save(List<Users> entities)
        {
            return Saver<Users>.Instance.Save(entities);
        }

        public bool Delete(Users entity)
        {
            return Saver<Users>.Instance.Delete(entity);
        }

        public bool Delete(List<Users> entities)
        {
            return Saver<Users>.Instance.Delete(entities);
        }
    }
}