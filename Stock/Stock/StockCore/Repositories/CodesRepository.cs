using System.Collections.Generic;
using StockCore.Entity;
using StockCore.Interfaces.Repositories;
using StockCore.Repositories.Loaders;
using StockCore.Repositories.Savers;

namespace StockCore.Repositories
{
    public class CodesRepository : ICodesRepository
    {
        public List<Codes> Load()
        {
            return Loader<Codes>.Instance.Load();
        }

        public List<Codes> Load(string where)
        {
            return Loader<Codes>.Instance.Load(where);
        }

        public Codes LoadSingle(string where)
        {
            return Loader<Codes>.Instance.LoadSingle(where);
        }

        public bool Save(Codes entity)
        {
            return Saver<Codes>.Instance.Save(entity);
        }

        public bool Save(List<Codes> entities)
        {
            return Saver<Codes>.Instance.Save(entities);
        }

        public bool Delete(Codes entity)
        {
            return Saver<Codes>.Instance.Delete(entity);
        }

        public bool Delete(List<Codes> entities)
        {
            return Saver<Codes>.Instance.Delete(entities);
        }
    }
}