using System.Collections.Generic;
using StockCore.Entity;
using StockCore.Interfaces.Repositories;
using StockCore.Repositories.Loaders;
using StockCore.Repositories.Savers;

namespace StockCore.Repositories
{
    public class ItemsRepository : IItemsRepository
    {
        public List<Items> Load()
        {
            return Loader<Items>.Instance.Load();
        }

        public List<Items> Load(string where)
        {
            return Loader<Items>.Instance.Load(where);
        }

        public Items LoadSingle(string where)
        {
            return Loader<Items>.Instance.LoadSingle(where);
        }

        public bool Save(Items entity)
        {
            return Saver<Items>.Instance.Save(entity);
        }

        public bool Save(List<Items> entities)
        {
            return Saver<Items>.Instance.Save(entities);
        }

        public bool Delete(Items entity)
        {
            return Saver<Items>.Instance.Delete(entity);
        }

        public bool Delete(List<Items> entities)
        {
            return Saver<Items>.Instance.Delete(entities);
        }
    }
}