﻿
namespace StockCore.Entity
{
    /// <summary>
    /// Статусы
    /// </summary>
    public class Statuses : StockEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}