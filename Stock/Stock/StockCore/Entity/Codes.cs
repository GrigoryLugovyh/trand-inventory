﻿namespace StockCore.Entity
{
    /// <summary>
    /// ОСБ
    /// </summary>
    public class Codes : StockEntity
    {
        public string Code { get; set; }

        public override string ToString()
        {
            return Code;
        }
    }
}