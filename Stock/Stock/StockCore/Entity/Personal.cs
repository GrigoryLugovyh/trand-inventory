using SQLite;

namespace StockCore.Entity
{
    public abstract class Personal : StockEntity
    {
        [Ignore]
        public abstract string Number { get; }

        [Ignore]
        public abstract string Name { get; }
    }
}