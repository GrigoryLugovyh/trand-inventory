﻿using SQLite;

namespace StockCore.Entity
{
    public class Users : Personal
    {
        public string UserNumber { get; set; }

        public string UserName { get; set; }

        [Ignore]
        private int _useNumber;
        public int UseNumber
        {
            get { return _useNumber; }
            set { _useNumber = value; }
        } 

        public string ExtUserId { get; set; }

        [Ignore]
        private int _userMcCreated;
        public int UserMcCreated
        {
            get { return _userMcCreated; }
            set { _userMcCreated = value; }
        }

        [Ignore]
        public override string Number
        {
            get { return UserNumber; }
        }

        [Ignore]
        public override string Name
        {
            get { return UserName; }
        }
    }
}