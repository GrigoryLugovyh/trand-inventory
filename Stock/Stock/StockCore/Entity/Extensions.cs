using System.Collections.Generic;
using System.Linq;

namespace StockCore.Entity
{
    public static class Extensions
    {
        public static List<Items> ToItems(this List<Doubles> doubles)
        {
            var items = new List<Items>();

            if (doubles != null && doubles.Count > 0)
            {
                items.AddRange(doubles.Select(@double => @double.ToItem()));
            }

            return items;
        }

        public static Items ToItem(this Doubles @double)
        {
            return new Items
                       {
                           BeginDate = @double.BeginDate,
                           ExtItemId = @double.ExtItemId,
                           FobjectBarcode = @double.FobjectBarcode,
                           FobjectId = @double.FobjectId,
                           FobjectName = @double.FobjectName,
                           Found = @double.Found,
                           Id = @double.Id,
                           IsNew = @double.IsNew,
                           MolName = @double.MolName,
                           MolNumber = @double.MolNumber,
                           Name = @double.Name,
                           PobjectBarcode = @double.PobjectBarcode,
                           PobjectId = @double.PobjectId,
                           PobjectName = @double.PobjectName,
                           SerialNumber = @double.SerialNumber,
                           Status = @double.Status,
                           UserName = @double.UserName,
                           UserNumber = @double.UserNumber
                       };
        }
    }
}