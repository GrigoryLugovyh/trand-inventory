﻿using System;
using SQLite;

namespace StockCore.Entity
{
    /// <summary>
    /// Дубли
    /// </summary>
    public class Doubles : StockEntity
    {
        [Indexed]
        public string Id { get; set; }

        public string Name { get; set; }

        public string PobjectName { get; set; }

        public string PobjectId { get; set; }

        public string PobjectBarcode { get; set; }

        public string FobjectName { get; set; }

        public string FobjectId { get; set; }

        public string FobjectBarcode { get; set; }

        public string UserNumber { get; set; }

        public string UserName { get; set; }

        [Ignore]
        private int _found;

        public int Found
        {
            get { return _found; }
            set { _found = value; }
        }

        [Ignore]
        private string _status = "0";

        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public string MolNumber { get; set; }

        public string MolName { get; set; }

        public string SerialNumber { get; set; }

        public string IsNew { get; set; }

        public DateTime BeginDate { get; set; }

        public string ExtItemId { get; set; }
    }
}