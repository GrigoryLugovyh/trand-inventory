﻿using SQLite;

namespace StockCore.Entity
{
    /// <summary>
    /// Материально-ответственные лица
    /// </summary>
    public class Mols : Personal
    {
        public string MolNumber { get; set; }

        public string MolName { get; set; }

        [Ignore]
        private int _useNumber;
        public int UseNumber
        {
            get { return _useNumber; }
            set { _useNumber = value; }
        } 

        public string ExtMolId { get; set; }

        [Ignore]
        private int _molMcCreated;
        public int MolMcCreated
        {
            get { return _molMcCreated; }
            set { _molMcCreated = value; }
        }

        [Ignore]
        public override string Number
        {
            get { return MolNumber; }
        }

        [Ignore]
        public override string Name
        {
            get { return MolName; }
        }
    }
}