﻿using System.Collections.Generic;
using SQLite;

namespace StockCore.Entity
{
    /// <summary>
    /// Места хранения
    /// </summary>
    public class Objects : StockEntity
    {
        [Indexed]
        public string PobjectId { get; set; }

        public string PobjectBarcode { get; set; }

        [Indexed]
        public string PobjectName { get; set; }

        [Ignore]
        private int _found;
        [Indexed]
        public int Found
        {
            get { return _found; }
            set { _found = value; }
        }

        [Indexed]
        public string PobjectPid { get; set; }

        [Indexed]
        public string PobjectPname { get; set; }

        [Ignore]
        private int _useBarcode;
        public int UseBarcode
        {
            get { return _useBarcode; }
            set { _useBarcode = value; }
        }

        public string ExtObjectId { get; set; }

        public string ExtParentObjectId { get; set; }

        [Ignore]
        public List<Objects> Childs { get; set; }

        public override string ToString()
        {
            return PobjectName;
        }
    }
}