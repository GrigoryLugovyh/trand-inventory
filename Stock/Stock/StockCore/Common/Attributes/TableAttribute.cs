using System;

namespace StockCore.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
    public class TableAttribute : Attribute
    {
        public string Table { get; private set; }

        public TableAttribute(string table)
        {
            Table = table;
        }
    }
}