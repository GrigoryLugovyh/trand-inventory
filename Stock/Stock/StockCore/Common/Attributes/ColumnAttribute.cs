using System;

namespace StockCore.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class ColumnAttribute : Attribute
    {
        public string Column { get; set; }
        public bool IsPrimaryKey { get; set; }

        public ColumnAttribute()
        {
        }

        public ColumnAttribute(string column)
            : this()
        {
            Column = column;
            IsPrimaryKey = false;
        }
    }
}