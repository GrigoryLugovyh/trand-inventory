﻿using System;

namespace StockCore.Common.Attributes
{
    public class DescriptionAttribute : Attribute
    {
        public string Description { get; private set; }

        public object[] Arguments { get; private set; }

        public DescriptionAttribute(string description, params object[] args)
        {
            Description = description;
            Arguments = args;
        }
    }
}