﻿namespace StockCore.Common.Attributes
{
    /// <summary>
    /// Обработка атрибура DescriptionAttribute
    /// </summary>
    public static class DescriptionHandler
    {
        public static void Description<T>(this T field, out string desc, out object[] args) where T : new()
        {
            field.Description(out desc);
            field.Description(out args);
        }

        public static void Description<T>(this T field, out string desc) where T : new()
        {
            object[] attrs = field.GetType().GetField(field.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), true);
            desc = attrs.Length > 0 ? ((DescriptionAttribute)attrs[0]).Description : string.Empty;
        }

        public static void Description<T>(this T field, out object[] args) where T : new()
        {
            object[] attrs = field.GetType().GetField(field.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), true);
            args = attrs.Length > 0 ? ((DescriptionAttribute)attrs[0]).Arguments : new object[0];
        }
    }
}