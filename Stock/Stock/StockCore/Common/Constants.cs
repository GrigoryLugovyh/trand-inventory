namespace StockCore
{
    public class Constants
    {
        public const string LocalIp = "192.168.55.100";

        public const string CheckItemExistsQuery = "SELECT COUNT([Id]) FROM [Items] WHERE [Id] = @Id";

        public const string CurrentSerialText = "������� �������� �����: \"{0}\"";

        public const string DatabaseName = "stock.db";

        public const string DefaultEntityName = "�� ������";

        public const string ExchangeSettingsName = "ExchangeSettings.xml";

        public const string ExportNewCountQuery = @"SELECT SUM(Count) AS [Count]
FROM (
SELECT COUNT(i.Id) AS [Count] FROM [Items] AS i WHERE PobjectId IS NULL
UNION
SELECT COUNT(d.Id) AS [Count] FROM [Doubles] AS d)";

        public const string ExportQuery = @"SELECT
i.Id, i.Name, i.PobjectName, i.PobjectId, i.PobjectBarcode, i.FobjectName, i.FobjectId, i.FobjectBarcode,
COALESCE(m.MolNumber, '') AS MolNumber,
i.MolName,
COALESCE(m.ExtMolId, '') AS ExtMolId,
COALESCE(u.UserNumber, '') AS UserNumber,
i.UserName,
COALESCE(u.ExtUserId, '') AS ExtUserId,
i.Found, i.Status, i.SerialNumber, i.BeginDate, COALESCE(i.ExtItemId, '') AS ExtItemId,
o.PobjectPid AS PobjectPid,
o.PobjectPname AS PobjectPname,
COALESCE(o.ExtObjectId, '') AS ExtObjectId,
COALESCE(o.ExtParentObjectId, '') AS ExtParentObjectId,
m.MolMcCreated, u.UserMcCreated
FROM [Items] AS i
LEFT OUTER JOIN [Objects] AS o ON o.PobjectBarcode = i.FobjectBarcode
LEFT OUTER JOIN [Mols] AS m ON m.MolNumber = i.MolNumber
LEFT OUTER JOIN [Users] AS u ON u.UserNumber = i.UserNumber
WHERE i.PobjectId IS NOT NULL";

        public const string ExportNewQuery = @"SELECT
i.Id AS Id,
i.Name as Name,
i.FobjectName AS PobjectName,
i.FobjectId AS PobjectId,
i.FobjectBarcode AS PobjectBarcode,
i.FobjectName AS FobjectName,
i.FobjectId AS FobjectId,
i.FobjectBarcode AS FobjectBarcode,
COALESCE(m.MolNumber, '') AS MolNumber,
i.MolName AS MolName,
COALESCE(m.ExtMolId, '') AS ExtMolId,
COALESCE(u.UserNumber, '') AS UserNumber,
i.UserName AS UserName,
COALESCE(u.ExtUserId, '') AS ExtUserId,
i.Found AS Found,
i.Status AS Status,
i.SerialNumber AS SerialNumber,
i.BeginDate AS BeginDate,
COALESCE(i.ExtItemId, '') AS ExtItemId,
o.PobjectPid AS PobjectPid,
o.PobjectPname AS PobjectPname,
COALESCE(o.ExtObjectId, '') AS ExtObjectId,
COALESCE(o.ExtParentObjectId, '') AS ExtParentObjectId,
m.MolMcCreated AS MolMcCreated,
u.UserMcCreated AS UserMcCreated
FROM [Items] AS i
LEFT OUTER JOIN [Objects] AS o ON o.PobjectBarcode = i.FobjectBarcode
LEFT OUTER JOIN [Mols] AS m ON m.MolNumber = i.MolNumber
LEFT OUTER JOIN [Users] AS u ON u.UserNumber = i.UserNumber
WHERE i.PobjectId IS NULL
UNION
SELECT DISTINCT
i.Id AS Id,
i.Name as Name,
i.FobjectName AS PobjectName,
i.FobjectId AS PobjectId,
i.FobjectBarcode AS PobjectBarcode,
i.FobjectName AS FobjectName,
i.FobjectId AS FobjectId,
i.FobjectBarcode AS FobjectBarcode,
COALESCE(m.MolNumber, '') AS MolNumber,
i.MolName AS MolName,
COALESCE(m.ExtMolId, '') AS ExtMolId,
COALESCE(u.UserNumber, '') AS UserNumber,
i.UserName AS UserName,
COALESCE(u.ExtUserId, '') AS ExtUserId,
i.Found AS Found,
i.Status AS Status,
i.SerialNumber AS SerialNumber,
i.BeginDate AS BeginDate,
COALESCE(i.ExtItemId, '') AS ExtItemId,
o.PobjectPid AS PobjectPid,
o.PobjectPname AS PobjectPname,
COALESCE(o.ExtObjectId, '') AS ExtObjectId,
COALESCE(o.ExtParentObjectId, '') AS ExtParentObjectId,
m.MolMcCreated AS MolMcCreated,
u.UserMcCreated AS UserMcCreated
FROM [Doubles] AS i
LEFT OUTER JOIN [Objects] AS o ON o.PobjectBarcode = i.FobjectBarcode
LEFT OUTER JOIN [Mols] AS m ON m.MolNumber = i.MolNumber
LEFT OUTER JOIN [Users] AS u ON u.UserNumber = i.UserNumber";

        public const string ExportQueryFilter = " AND i.Found <> 0";

        public const string ExportTotalCountQuery = "SELECT COUNT(i.Id) FROM [Items] i WHERE PobjectId IS NOT NULL ";

        public const string InventoryStatus = "����� - {0} / ������� - {1}";

        public const string MobilePrintingServiceGuid = "{7A51FDC2-FDDF-4c9b-AFFC-98BCD91BF93B}";

        public const string StockConnection = "Data Source={0}; PRAGMA synchronous=OFF; PRAGMA count_changes=OFF; PRAGMA journal_mode=MEMORY; PRAGMA temp_store=MEMORY;";

        public const string StockSettingsName = "StockSettings.xml";
    }
}