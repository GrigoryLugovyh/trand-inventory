using System;
using System.Xml.Serialization;
using StockCore.Enums;

namespace StockCore.Common
{
    [Serializable]
    public class StockConfig
    {
        [XmlElement]
        public string TerminalId { get; set; }

        [XmlElement]
        public string ItemBcRegex { get; set; }

        [XmlElement]
        public string ObjectBcRegex { get; set; }

        [XmlElement]
        public bool SearchInSerialNumbers { get; set; }

        [XmlElement]
        public string PrinterIp { get; set; }

        [XmlElement]
        public int PrinterPort { get; set; }

        [XmlElement]
        public EncodingEnum PrinterCodePage { get; set; }

        [XmlElement]
        public string ItemBarcodeLabel { get; set; }

        [XmlElement]
        public string ObjectBarcodeLabel { get; set; }

        [XmlElement]
        public bool UseNativeFormat { get; set; }

        [XmlElement]
        public string PortableAddress { get; set; }

        [XmlElement]
        public string PortablePin { get; set; }

        [XmlElement]
        public EncodingEnum PortablePrinterCodePage { get; set; }

        [XmlElement]
        public string PortableItemBarcodeLabel { get; set; }

        [XmlElement]
        public string PortableObjectBarcodeLabel { get; set; }

        [XmlIgnore]
        public static StockConfig Default
        {
            get
            {
                return new StockConfig
                           {
                               TerminalId = "1",
                               ItemBcRegex = "xx.xxxxxxxxx.xxxx",
                               ObjectBcRegex = @".*",
                               SearchInSerialNumbers = true,
                               PrinterIp = "10.1.12.133",
                               PrinterPort = 8989,
                               PrinterCodePage = EncodingEnum.Dos,
                               ItemBarcodeLabel = @"Labels\Item.txt",
                               ObjectBarcodeLabel = @"Labels\Object.txt",
                               UseNativeFormat = true,
                               PortableAddress = "",
                               PortablePin = "",
                               PortablePrinterCodePage = EncodingEnum.Dos,
                               PortableItemBarcodeLabel = @"Labels\pItem.txt",
                               PortableObjectBarcodeLabel = @"Labels\pObject.txt"
                           };
            }
        }
    }
}