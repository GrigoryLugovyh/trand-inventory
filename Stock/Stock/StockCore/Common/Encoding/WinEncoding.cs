
namespace StockCore.Common.Encoding
{
    public class WinEncoding : System.Text.Encoding
    {
        public override int GetByteCount(char[] chs)
        {
            return chs.Length;
        }

        public override int GetByteCount(string str)
        {
            return str.Length;
        }

        public override int GetByteCount(char[] chs, int index, int count)
        {
            return count;
        }

        public override byte[] GetBytes(char[] chs, int index, int count)
        {
            byte[] bytes = new byte[count];
            int i = 0, code;
            for (; i < count; i++)
            {
                code = (int)chs[index + i];
                if (code < 0x80)
                    bytes[i] = (byte)code;
                else if (code >= 0x0410 && code <= 0x044F) // ����� �� � �� �
                    bytes[i] = (byte)(0xC0 + code - 0x0410);
                else if (code == 0x0401)		// �
                    bytes[i] = (byte)0xA8;
                else if (code == 0x0451)		// �
                    bytes[i] = (byte)0xB8;
                else if (code == 0x00A9)
                    bytes[i] = (byte)0xA9;  // CopyRight
                else if (code == 0x2116) // �
                    bytes[i] = 0xB9;
                else
                    bytes[i] = (byte)code;
            }
            return bytes;
        }

        public override byte[] GetBytes(char[] chs)
        {
            return GetBytes(chs, 0, chs.Length);
        }

        public override byte[] GetBytes(string str)
        {
            return GetBytes(str.ToCharArray(), 0, str.ToCharArray().Length);
        }

        public override int GetBytes(char[] chs, int cIndex, int cCount, byte[] bytes, int bIndex)
        {
            int i = 0, code;
            for (; i < cCount; i++)
            {
                code = (int)chs[cIndex + i];
                if (code < 0x80)
                    bytes[bIndex + i] = (byte)code;
                else if (code >= 0x0410 && code <= 0x044F) // ����� �� � �� �
                    bytes[bIndex + i] = (byte)(0xC0 + code - 0x0410);
                else if (code == 0x0401)		// �
                    bytes[bIndex + i] = (byte)0xA8;
                else if (code == 0x0451)		// �
                    bytes[bIndex + i] = (byte)0xB8;
                else if (code == 0x00A9)
                    bytes[bIndex + i] = (byte)0xA9;  // CopyRight
                else if (code == 0x2116) // �
                    bytes[bIndex + i] = 0xB9;
                else
                    bytes[bIndex + i] = (byte)code;
            }
            return cCount;
        }

        public override int GetBytes(string s, int charIndex, int charCount, byte[] bytes, int byteIndex)
        {
            return GetBytes(s.ToCharArray(), charIndex, charCount, bytes, byteIndex);
        }

        public override int GetCharCount(byte[] bytes, int index, int count)
        {
            return count;
        }

        public override int GetCharCount(byte[] bytes)
        {
            return bytes.Length;
        }

        public override int GetChars(byte[] bytes, int byteIndex, int byteCount, char[] chars, int charIndex)
        {
            int i = 0, code;
            for (; i < byteCount; i++)
            {
                code = (int)bytes[byteIndex + i];
                if (code < 0x80)
                    chars[charIndex + i] = (char)code;
                else if (code >= 0xC0 && code <= 0xFF)
                    chars[charIndex + i] = (char)(0x0410 + code - 0xC0);
                else if (code == 0xA8)
                    chars[charIndex + i] = (char)0x0401;
                else if (code == 0xB8)
                    chars[charIndex + i] = (char)0x0451;
                else if (code == 0xB9)
                    chars[charIndex + i] = (char)0x2116;
                else
                    chars[charIndex + i] = (char)code;
            }
            return i;
        }

        public override char[] GetChars(byte[] bytes, int byteIndex, int byteCount)
        {
            char[] chars = new char[byteCount];
            int i = 0, code;
            for (; i < byteCount; i++)
            {
                code = (int)bytes[byteIndex + i];
                if (code < 0x80)
                    chars[i] = (char)code;
                else if (code >= 0xC0 && code <= 0xFF)
                    chars[i] = (char)(0x0410 + code - 0xC0);
                else if (code == 0xA8)
                    chars[i] = (char)0x0401;
                else if (code == 0xB8)
                    chars[i] = (char)0x0451;
                else if (code == 0xB9)
                    chars[i] = (char)0x2116;
                else
                    chars[i] = (char)code;
            }
            return chars;
        }

        public override char[] GetChars(byte[] bytes)
        {
            return GetChars(bytes, 0, bytes.Length);
        }

        public override int GetMaxByteCount(int charCount)
        {
            return charCount;
        }

        public override int GetMaxCharCount(int byteCount)
        {
            return byteCount;
        }
    }

}