using System;
using StockCore.Entity;
using StockCore.Enums;

namespace StockCore.Common
{
    public class Session
    {
        public ViewsEnum CurrentView { get; protected set; }

        public Objects CurrentObject { get; protected set; }

        public Users CurrentUser { get; protected set; }

        public Mols CurrentMol { get; protected set; }

        public Items CurrentItem { get; protected set; }

        public string CurrentItemBarcode { get; protected set; }

        public event EventHandler ViewChanged;

        public event EventHandler ObjectChanged;

        public event EventHandler UserChanged;

        public event EventHandler MolChanged;

        public event EventHandler ItemChanged;

        public event EventHandler ItemBarcodeChanged;

        public event EventHandler NewItemWasAdded;

        public event EventHandler ItemStatusChanged;

        public void SetView(ViewsEnum v)
        {
            if(v == CurrentView) return;

            CurrentView = v;

            if(ViewChanged != null)
            {
                ViewChanged(CurrentView, EventArgs.Empty);
            }
        }

        public void SetObject(Objects o)
        {
            CurrentObject = o;

            if(ObjectChanged != null)
            {
                ObjectChanged(CurrentObject, EventArgs.Empty);
            }
        }

        public void SetUser(Users u)
        {
            CurrentUser = u;

            if(UserChanged != null)
            {
                UserChanged(CurrentUser, EventArgs.Empty);
            }
        }

        public void SetMol(Mols m)
        {
            CurrentMol = m;

            if(MolChanged != null)
            {
                MolChanged(CurrentMol, EventArgs.Empty);
            }
        }

        public void SetItem(Items i)
        {
            CurrentItem = i;

            if(ItemChanged != null)
            {
                ItemChanged(CurrentMol, EventArgs.Empty);
            }
        }

        public void SetItemBarcode(string barcode)
        {
            CurrentItemBarcode = barcode;

            if(ItemBarcodeChanged != null)
            {
                ItemBarcodeChanged(CurrentItemBarcode, EventArgs.Empty);
            }
        }

        /// <summary>
        /// ��������� ������� � ���������� ����� ���
        /// </summary>
        public void RiseNewItemAdded()
        {
            if(NewItemWasAdded != null)
            {
                NewItemWasAdded(CurrentItemBarcode, EventArgs.Empty);
            }
        }

        /// <summary>
        /// ��������� �������� �������
        /// </summary>
        public void RiseItemStatusChanged()
        {
            if(ItemStatusChanged != null)
            {
                ItemStatusChanged(CurrentItem, EventArgs.Empty);
            }
        }

        public void Clear()
        {
            CurrentObject = null;
            CurrentUser = null;
            CurrentMol = null;
            CurrentItem = null;
            CurrentItemBarcode = null;
        }
    }
}