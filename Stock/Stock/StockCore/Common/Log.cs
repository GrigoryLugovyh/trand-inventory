﻿using System;
using System.IO;
using StockCore.Common.Encoding;
using StockCore.Enums;

namespace StockCore.Common
{
    public static class Log
    {
        public static Exception LastException { get; private set; }

        public static void Write(LogsEnum @type, string message, params object[] args)
        {
            try
            {
                switch (@type)
                {
                    case LogsEnum.System:
                        {
                            Write(string.Format(message, args), SystemLogPath);
                            break;
                        }
                    case LogsEnum.Exchange:
                        {
                            Write(string.Format(message, args), ExchangeLogPath);
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                LastException = ex;
            }
        }

        private const string SystemLog = "Stock.log";

        private const string ExchangeLog = "StockExchange.log";

        private static readonly string SystemLogPath = Path.Combine(Model.Instance.Common.CurrentDirectory, SystemLog);

        private static readonly string ExchangeLogPath = Path.Combine(Model.Instance.Common.CurrentDirectory, ExchangeLog);

        private static System.Text.Encoding _code;
        private static System.Text.Encoding CodePage
        {
            get { return _code ?? (_code = new WinEncoding()); }
        }

        private static void Write(string message, string path)
        {
            // удалим, если лог стал слишком большим
            if (File.Exists(path))
            {
                var fi = new FileInfo(path);
                if (fi.Length / 1024 / 1024 > 1)
                {
                    fi.Delete();
                }
            }

            using (var stream = File.Open(path, FileMode.Append, FileAccess.Write, FileShare.Read))
            {
                message = string.Format("{0} {1}" + Environment.NewLine, DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss.f"), message);
                var buffer = CodePage.GetBytes(message);
                stream.Write(buffer, 0, buffer.Length);
                stream.Close();
            }
        }
    }
}