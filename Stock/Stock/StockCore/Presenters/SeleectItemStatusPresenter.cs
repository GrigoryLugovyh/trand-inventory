using System;
using StockCore.Handlers.Status;
using StockCore.Interfaces.Views;

namespace StockCore.Presenters
{
    public class SeleectItemStatusPresenter : StockPresenter<ISeleectItemStatusView>
    {
        public SeleectItemStatusPresenter(ISeleectItemStatusView view)
            : base(view)
        {
        }

        protected StatusesHandler StatusHandler { get; set; }

        protected void ViewShow(object sender, EventArgs e)
        {
            StatusHandler = new StatusesHandler(new StatusesHandlerProtocol(Model.Instance.Session.CurrentItem.StatusEnum, true, true));
            View.Statuses = StatusHandler.Load();
        }

        protected void ViewChange(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("��������� ������� ���",()=>
             {
                 if (StatusHandler.ChangeStatus(View.SelectedStatus))
                 {
                     CloseView();

                     Model.Instance.Session.RiseItemStatusChanged();
                 }
             });
        }

        public override void Subscription()
        {
            View.Show += ViewShow;
            View.Change += ViewChange;
        }

        public override void Unsubscription()
        {
            View.Show -= ViewShow;
            View.Change -= ViewChange;
        }
    }
}