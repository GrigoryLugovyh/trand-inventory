using StockCore.Interfaces.Views;

namespace StockCore.Presenters
{
    public class SplashPresenter : StockPresenter<ISplashView>
    {
        public SplashPresenter(ISplashView view)
            : base(view)
        {
        }

        public override void Subscription()
        {

        }

        public override void Unsubscription()
        {

        }
    }
}