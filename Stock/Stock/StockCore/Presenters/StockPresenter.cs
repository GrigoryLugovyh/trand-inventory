using StockCore.Interfaces.Presenters;
using StockCore.Interfaces.Views;

namespace StockCore.Presenters
{
    public abstract class StockPresenter<T> : ISubscribePresenter, IViewPresenter where T : IView
    {
        protected StockPresenter(T view)
        {
            View = view;
        }

        public T View { get; protected set; }

        public virtual double Priority
        {
            get { return 1; }
        }

        public virtual void ShowView()
        {
            View.ShowView();
        }

        public virtual void CloseView()
        {
            View.CloseView();
        }

        public abstract void Subscription();

        public abstract void Unsubscription();

        /// <summary>
        /// ������������ �� ����������
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void TerminalBarcodeRead(object sender, Events.StockEventArgs e)
        {
        }

        /// <summary>
        /// �������� ������ ��
        /// </summary>
        protected virtual void StartTerminal()
        {
            if (!Model.Instance.Terminal.Enabled)
            {
                Model.Instance.Terminal.Enable();
                Model.Instance.Terminal.BarcodeRead += TerminalBarcodeRead;
            }
        }

        /// <summary>
        /// ��������� ������ ��
        /// </summary>
        protected virtual void StopTerminal()
        {
            if (Model.Instance.Terminal.Enabled)
            {
                Model.Instance.Terminal.BarcodeRead -= TerminalBarcodeRead;
                Model.Instance.Terminal.Disable();
            }
        }
    }
}