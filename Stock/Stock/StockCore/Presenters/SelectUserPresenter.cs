using System;
using System.Collections.Generic;
using System.Linq;
using StockCore.Entity;
using StockCore.Interfaces.Views;

namespace StockCore.Presenters
{
    public class SelectUserPresenter : StockPresenter<ISelectPersonalView<Users>>
    {
        public SelectUserPresenter(ISelectPersonalView<Users> view)
            : base(view)
        {
        }

        /// <summary>
        /// ���� ���������
        /// </summary>
        public List<Users> Personals { get; set; }

        protected void ViewShow(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("�������� ��������", () =>
            {
                if (Personals != null)
                {
                    View.Personals = Personals;
                }
                else
                {
                    CloseView();
                }
            });
        }

        protected void ViewSelect(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("����� ���������", () =>
            {
                if (View.SelectPersonal != null)
                {
                    if (Model.Instance.Session.CurrentItem == null)
                    {
                        Model.Instance.Session.SetUser(View.SelectPersonal);
                    }
                    else
                    {
                        var item = Model.Instance.Session.CurrentItem;

                        item.UserName = View.SelectPersonal.UserName;
                        item.UserNumber = View.SelectPersonal.UserNumber;

                        Model.Instance.Data.UpdateItem(item);
                    }

                    CloseView();
                }
                else
                {
                    Model.Instance.Messages.ShowExclamation("���������� ������� ������������", "��");
                }

            });
        }

        protected void ViewFilter(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("������ �������������", () =>
                        {
                            if (!string.IsNullOrEmpty(View.FilterValue))
                            {
                                var persons = new List<Users>();
                                persons.AddRange(Personals);

                                View.Personals = persons.Where(
                                                        u => u.UserName.IndexOf(View.FilterValue, StringComparison.OrdinalIgnoreCase) > -1 ||
                                                        u.UserNumber.IndexOf(View.FilterValue, StringComparison.OrdinalIgnoreCase) > -1).OrderBy(u => u.UserName).ToList();
                            }
                            else
                            {
                                View.Personals = Personals;
                            }
                        });
        }

        protected void ViewClose(object sender, EventArgs e)
        {
            if(Model.Instance.Session.CurrentItem == null)
            {
                View.CloseToObjects();
            }
            else
            {
                View.CloseToInventory();
            }
        }

        public override void Subscription()
        {
            View.Show += ViewShow;
            View.Close += ViewClose;
            View.Select += ViewSelect;
            View.Filter += ViewFilter;
        }

        public override void Unsubscription()
        {
            View.Show -= ViewShow;
            View.Close -= ViewClose;
            View.Select -= ViewSelect;
            View.Filter -= ViewFilter;
        }
    }
}