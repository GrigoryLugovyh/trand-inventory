using System;
using System.Linq;
using StockCore.Handlers.Entry;
using StockCore.Interfaces.Views;
using ZSDK_API.Comm;
using ZSDK_API.Discovery;

namespace StockCore.Presenters
{
    public class SearchMobilePrinterPresenter : StockPresenter<ISearchMobilePrinterView>
    {
        public SearchMobilePrinterPresenter(ISearchMobilePrinterView view)
            : base(view)
        {
        }

        protected void ViewShow(object sender, EventArgs e)
        {
            Model.Instance.Invoker.DoIndependentThread("����� ��������� ���������",
                       () =>
                           {
                               View.StatusText = "����� ������������ ...";
                               View.ViewEnable = false;

                               var printers = BluetoothDiscoverer.FindPrinters();

                               if(printers.Length > 0)
                               {
                                   var devices = printers.Select(printer => new Device
                                                    {
                                                        Address = printer.Address,
                                                        Name = ((DiscoveredPrinterBluetooth)
                                                                 printer).FriendlyName
                                                    }).ToList();

                                   View.Printers = devices;
                               }
                               else
                               {
                                   Model.Instance.Messages.ShowExclamation("���������� �� �������", "Ok");
                               }
                           },
                       () =>
                           {
                               View.StatusText = string.Empty;
                               View.ViewEnable = true;

                           }, true);
        }

        protected void ViewUse(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("���������� ���������� � ��������", () =>
                          {
                              if (View.SelectedPrinter == null || !(View.SelectedPrinter is Device))
                              {
                                  throw new Exception("���������� ������� ����������");
                              }

                              var device = View.SelectedPrinter as Device;

                              var connect = new BluetoothPrinterConnection(device.Address);

                              connect.Open();

                              if (connect.IsConnected())
                              {
                                  Model.Instance.Config.CurrentConfig.PortableAddress = device.Address;
                                  Model.Instance.Config.Save();

                                  connect.Close();

                                  Model.Instance.Messages.ShowExclamation(string.Format("������� ������������ � ���������� \"{0}\"", device.Name), "Ok");

                                  View.CloseView();
                              }
                              else
                              {
                                  throw new Exception("�� ������� ������������ � ���������� ����������");
                              }

                          });
        }

        protected void ViewClose(object sender, EventArgs e)
        {
            View.Printers = null;
        }

        public override void Subscription()
        {
            View.Show += ViewShow;
            View.Close += ViewClose;
            View.Use += ViewUse;
        }

        public override void Unsubscription()
        {
            View.Show -= ViewShow;
            View.Close -= ViewClose;
            View.Use -= ViewUse;
        }
    }
}