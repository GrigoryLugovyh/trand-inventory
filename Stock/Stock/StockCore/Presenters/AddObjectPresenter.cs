using System;
using StockCore.Entity;
using StockCore.Interfaces.Views;

namespace StockCore.Presenters
{
    public class AddObjectPresenter : StockPresenter<IAddObjectView>
    {
        public AddObjectPresenter(IAddObjectView view)
            : base(view)
        {
            ParentObject = null;
        }

        public Objects ParentObject { get; set; }

        public Objects AddedObject { get; set; }

        protected void ViewShow(object sender, EventArgs e)
        {
            AddedObject = null;
            View.ObjectBarcode = View.ObjectCode = View.ObjectName = View.ObjectParent = string.Empty;

            if(ParentObject != null)
            {
                View.ObjectParent = ParentObject.PobjectName;
            }
        }

        protected void ViewCreate(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("�������� ������ �������", () =>
                 {
                     if (string.IsNullOrEmpty(View.ObjectBarcode))
                     {
                         Model.Instance.Messages.ShowExclamation("���������� ������ �����-��� �������", "Ok");
                         return;
                     }

                     if (string.IsNullOrEmpty(View.ObjectCode))
                     {
                         Model.Instance.Messages.ShowExclamation("���������� ������ ��� �������", "Ok");
                         return;
                     }

                     if (string.IsNullOrEmpty(View.ObjectName))
                     {
                         Model.Instance.Messages.ShowExclamation("���������� ������ �������� �������", "Ok");
                         return;
                     }

                     if (Model.Instance.Data.ObjectExist(View.ObjectBarcode))
                     {
                         Model.Instance.Messages.ShowExclamation("������ � ����� �����-����� ��� ���� � ���� ������", "Ok");
                         return;
                     }

                     AddedObject = new Objects
                     {
                         PobjectBarcode = View.ObjectBarcode,
                         PobjectId = View.ObjectCode,
                         PobjectName = View.ObjectName
                     };

                     if (ParentObject != null)
                     {
                         AddedObject.PobjectPid = ParentObject.PobjectId;
                         AddedObject.PobjectPname = ParentObject.PobjectName;
                         AddedObject.ExtParentObjectId = ParentObject.ExtObjectId;
                     }
                     else
                     {
                         AddedObject.PobjectPid = AddedObject.PobjectPname = string.Empty;
                     }

                     AddedObject.Found = ParentObject != null ? ParentObject.Found : 0;
                     AddedObject.UseBarcode = ParentObject != null ? ParentObject.UseBarcode : 0;

                     if (Model.Instance.Data.InsertObject(AddedObject))
                     {
                         View.CloseView();
                     }
                 });
        }

        protected void ViewAdd(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("���������� ������������ �������", () =>
              {
                  Model.Instance.Presenters.FindObjects.View.ShowView();
              });
        }

        protected void UpdateParentObject(object sender, EventArgs e)
        {
            if(Model.Instance.Presenters.FindObjects.SelectedObject != null)
            {
                ParentObject = Model.Instance.Presenters.FindObjects.SelectedObject;
                View.ObjectParent = ParentObject.PobjectName;
            }
            else
            {
                ParentObject = null;
                View.ObjectParent = string.Empty;
            }
        }

        protected void ViewDelete(object sender, EventArgs e)
        {
            ParentObject = null;
            View.ObjectParent = string.Empty;
        }

        public override void Subscription()
        {
            View.Show += ViewShow;
            View.Add += ViewAdd;
            View.Delete += ViewDelete;
            View.Create += ViewCreate;

            Model.Instance.Presenters.FindObjects.View.Close += UpdateParentObject;
        }

        public override void Unsubscription()
        {
            View.Show -= ViewShow;
            View.Add -= ViewAdd;
            View.Delete -= ViewDelete;
            View.Create -= ViewCreate;

            Model.Instance.Presenters.FindObjects.View.Close -= UpdateParentObject;
        }
    }
}