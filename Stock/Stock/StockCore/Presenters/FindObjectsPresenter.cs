using System;
using System.Linq;
using StockCore.Entity;
using StockCore.Interfaces.Views;

namespace StockCore.Presenters
{
    public class FindObjectsPresenter : StockPresenter<IFindObjectView>
    {
        public FindObjectsPresenter(IFindObjectView view)
            : base(view)
        {
        }

        public Objects SelectedObject
        {
            get { return View.SelectedObject; }
        }

        protected void ViewShow(object sender, EventArgs e)
        {
            Model.Instance.Invoker.DoIndependentThread("�������� �������� ��� ������ ��������", () =>
            {
                View.StatusText = "�������� �������� ... ";

                View.SelectedObject = null;

                var objects = Model.Instance.Data.Store.Table<Objects>().OrderBy(o => o.PobjectName).ToList();
                if (objects.Count > 0)
                {
                    View.Objects = objects;
                }
                },
                () =>
                {
                    View.StatusText = "�������� ������������ ������";
                },
                true);
        }

        protected void ViewFilter(object sender, EventArgs e)
        {
            Model.Instance.Invoker.DoIndependentThread("������ �������� ��� ������ ��������", () =>
            {
                if (string.IsNullOrEmpty(View.FilterEdit))
                {
                    ViewShow(sender, e);
                    return;
                }

                View.StatusText = "������ �������� ... ";

                var objects = Model.Instance.Data.Objects(false);

                View.Objects = objects.Where(
                    o => o.PobjectBarcode.IndexOf(View.FilterEdit, StringComparison.OrdinalIgnoreCase) > -1 ||
                         o.PobjectId.IndexOf(View.FilterEdit, StringComparison.OrdinalIgnoreCase) > -1 ||
                         o.PobjectName.IndexOf(View.FilterEdit, StringComparison.OrdinalIgnoreCase) > -1 ||
                         o.PobjectPid.IndexOf(View.FilterEdit, StringComparison.OrdinalIgnoreCase) > -1 ||
                         o.PobjectPname.IndexOf(View.FilterEdit, StringComparison.OrdinalIgnoreCase) > -1).OrderBy(
                         o => o.PobjectName).ToList();
            },
            () =>
            {
                View.StatusText = "�������� ������������ ������";
            },
            true);
        }

        protected void ViewSelect(object sender, EventArgs e)
        {
            View.CloseView();
        }

        protected void ViewClose(object sender, EventArgs e)
        {
            View.SelectedObject = null;
        }

        public override void Subscription()
        {
            View.Show += ViewShow;
            View.Close += ViewClose;
            View.Filter += ViewFilter;
            View.Select += ViewSelect;
        }

        public override void Unsubscription()
        {
            View.Show -= ViewShow;
            View.Close -= ViewClose;
            View.Filter -= ViewFilter;
            View.Select -= ViewSelect;
        }
    }
}