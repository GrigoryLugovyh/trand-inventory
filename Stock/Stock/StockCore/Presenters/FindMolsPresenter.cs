using System;
using System.Collections.Generic;
using StockCore.Entity;
using StockCore.Interfaces.Views;

namespace StockCore.Presenters
{
    public class FindMolsPresenter : StockPresenter<IFindPersonalView<Mols>>
    {
        public FindMolsPresenter(IFindPersonalView<Mols> view) : base(view)
        {
        }

        /// <summary>
        /// ���� ���������
        /// </summary>
        public List<Mols> Personals { get; set; }

        protected void ViewShow(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("�������� ��������", () =>
            {
                if (Personals != null)
                {
                    View.Personals = Personals;
                }
                else
                {
                    CloseView();
                }
            });
        }

        protected void ViewSelect(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("����� ���������", () =>
            {
                if (View.SelectPersonal != null)
                {
                    Model.Instance.Session.SetMol(View.SelectPersonal);
                    CloseView();
                }
                else
                {
                    Model.Instance.Messages.ShowExclamation("���������� ������� ����", "��");
                }

            });
        }

        public override void Subscription()
        {
            View.Show += ViewShow;
            View.Select += ViewSelect;
        }

        public override void Unsubscription()
        {
            View.Show -= ViewShow;
            View.Select -= ViewSelect;
        }
    }
}