using System;
using System.Collections.Generic;
using System.Linq;
using StockCore.Entity;
using StockCore.Enums;
using StockCore.Factories;
using StockCore.Handlers.Status;
using StockCore.Interfaces.Views;

namespace StockCore.Presenters
{
    public class InventoryPresenter : StockPresenter<IInventoryView>
    {
        public InventoryPresenter(IInventoryView view)
            : base(view)
        {
        }

        public override double Priority
        {
            get { return 2; }
        }

        protected Objects @Object
        {
            get { return Model.Instance.Session.CurrentObject; }
        }

        protected Mols @Mol
        {
            get { return Model.Instance.Session.CurrentMol; }
        }

        protected Users @User
        {
            get { return Model.Instance.Session.CurrentUser; }
        }

        protected Items @Item
        {
            get { return Model.Instance.Session.CurrentItem; }
        }

        protected StatusesHandler StatusHandler { get; set; }

        protected void ViewShow(object sender, EventArgs e)
        {
            Model.Instance.Session.SetView(ViewsEnum.Inventory);

            ClearView();

            SessionItemChanged(sender, e);

            StartTerminal();

            View.StatusText = Model.Instance.Session.CurrentObject.PobjectName;

            if (Model.Instance.Session != null)
            {
                Model.Instance.Session.ItemBarcodeChanged += SessionItemBarcodeChanged;
                Model.Instance.Session.ItemStatusChanged += SessionItemStatusChanged;
                Model.Instance.Session.NewItemWasAdded += SessionItemBarcodeChanged;
                Model.Instance.Session.ItemChanged += SessionItemChanged;
            }
        }

        protected void ViewClose(object sender, EventArgs e)
        {
            StopTerminal();

            Model.Instance.Session.Clear();

            if (Model.Instance.Session != null)
            {
                Model.Instance.Session.ItemBarcodeChanged -= SessionItemBarcodeChanged;
                Model.Instance.Session.ItemStatusChanged -= SessionItemStatusChanged;
                Model.Instance.Session.NewItemWasAdded -= SessionItemBarcodeChanged;
                Model.Instance.Session.ItemChanged -= SessionItemChanged;
            }
        }

        protected void ViewToInventory(object sender, EventArgs e)
        {
            StartTerminal();
        }

        protected void ViewToFind(object sender, EventArgs e)
        {
            StopTerminal();

            Model.Instance.Invoker.DoIndependentThread("�������� �������� ���", () =>
            {
                View.StatusText = "���������� ��������� ��� ... ";
                var items = Model.Instance.Data.Store.Table<Items>().Where(i => i.FobjectBarcode == @Object.PobjectBarcode && i.Found == 1).ToList() ?? new List<Items>();
                items.AddRange(Model.Instance.Data.Store.Table<Doubles>().Where(d => d.FobjectBarcode == @Object.PobjectBarcode && d.Found == 1).ToList().ToItems());
                View.FindItems = null;
                View.FindItems = items;
            },
            () =>
            {
                if (@Object != null)
                {
                    View.StatusText = @Object.PobjectName;
                }
            },
            true);
        }

        protected void ViewToNotFind(object sender, EventArgs e)
        {
            StopTerminal();

            Model.Instance.Invoker.DoIndependentThread("�������� ���������� ���", () =>
            {
                View.StatusText = "���������� ����������� ��� ... ";
                var items = Model.Instance.Data.Store.Table<Items>().Where(i => i.PobjectBarcode == @Object.PobjectBarcode).ToList();
                View.NotFindItems = null;
                View.NotFindItems = items.Where(i => i.Found == 0).ToList();
            },
            () =>
            {
                if (@Object != null)
                {
                    View.StatusText = @Object.PobjectName;
                }
            },
            true);
        }

        /// <summary>
        /// ������������ �� ����������
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void TerminalBarcodeRead(object sender, Events.StockEventArgs e)
        {
            View.Barcode = e.Message;

            EnterBarcode();
        }

        /// <summary>
        /// ������ ���� ��
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SessionItemBarcodeChanged(object sender, EventArgs e)
        {
            View.Barcode = Model.Instance.Session.CurrentItemBarcode;

            EnterBarcode();
        }

        /// <summary>
        /// �������� ����� �������������, ���� �����-����
        /// </summary>
        public virtual void EnterBarcode()
        {
            Model.Instance.Invoker.Do("��������� ���������� ��", () =>
            {

                    if (string.IsNullOrEmpty(View.Barcode))
                    {
                        return;
                    }

                    SearchBarcodeEnum mode;

                    if (!BarcodeFactory.Instance.Check(View.Barcode))
                    {
                        if (!Model.Instance.Config.CurrentConfig.SearchInSerialNumbers)
                        {
                            Model.Instance.Messages.ShowExclamation("�����-��� �� ������������� ������� �����", "Ok");
                            return;
                        }

                        mode = SearchBarcodeEnum.Code;
                        Model.Instance.Session.SetItem(Model.Instance.Data.Store.Table<Items>().Where(i => i.SerialNumber == View.Barcode).Take(1).FirstOrDefault());
                    }
                    else
                    {
                        mode = SearchBarcodeEnum.Barcode;
                        Model.Instance.Session.SetItem(Model.Instance.Data.Store.Table<Items>().Where(i => i.Id == View.Barcode).Take(1).FirstOrDefault());
                    }

                    if (@Item != null)
                    {
                        View.MolEnterEnable = true;
                        View.UserEnterEnable = true;
                        View.SerialEnterEnable = true;

                        // ���� ����� ���
                        if (@Item.FobjectBarcode != null && !string.IsNullOrEmpty(@Item.FobjectBarcode))
                        {
                            if (@Item.FobjectBarcode != @Object.PobjectBarcode)
                            {
                                if (Model.Instance.Messages.ShowQuestion(
                                    string.Format("{0}\r\n\"{1}\"\r\n��� ���� �\r\n\"{2}\"\r\n�������� �����?", @Item.Id,
                                                  @Item.Name, @Item.FobjectName),
                                    DialogEnum.No, "��", "���") == DialogEnum.Yes)
                                {
                                    // �����
                                    Model.Instance.Data.MakeDouble(@Item, @Object);
                                    Model.Instance.Data.MakeObjectFound(@Object);
                                    View.StatusChanged = null;
                                }
                                else
                                {
                                    ClearView();
                                    return;
                                }
                            }
                            else
                            {
                                // ��� ���� � ���� �������, �������� ������
                                Item.Status = ((int) StatusesEnum.Correct).ToString();
                                Model.Instance.Presenters.SelectItemStatus.ShowView();
                                View.StatusChanged = null;
                            }

                            UpdateItemPersons();
                        }
                        else
                        {
                            // �����
                            Model.Instance.Data.MakeItemFound(@Item, @Object);
                            Model.Instance.Data.MakeObjectFound(@Object);
                            UpdateItemPersons();
                        }

                        // ������� ���� �� ���
                        FillView();
                    }
                    else
                    {
                        // ��� ���
                        ClearView();

                        if (mode == SearchBarcodeEnum.Barcode)
                        {
                            // ����� �������� �����
                            if (
                                Model.Instance.Messages.ShowQuestion("��� �� ������.\r\n�������� � ����?", DialogEnum.No, "��", "���") == DialogEnum.Yes)
                            {
                                Model.Instance.Invoker.DoIndependentThread("���������� ����� ���", () => Model.Instance.Presenters.AddItem.ShowView(), null, true);
                            }
                        }
                        else
                        {
                            Model.Instance.Messages.ShowExclamation("�� ������� ����� ���", "Ok");
                        }
                    }

            }, false, true);
        }

        /// <summary>
        /// ������� �� ��������� �������
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SessionItemStatusChanged(object sender, EventArgs e)
        {
            FillView();
        }

        /// <summary>
        /// ���������� ���� � ������������
        /// </summary>
        protected void UpdateItemPersons()
        {
            Model.Instance.Invoker.Do("���������� ���� � ������������", 
                ()=> Model.Instance.Data.UpdatePersonals(@Item, @Mol, @User));
        }

        protected void FillView()
        {
            View.StatusChanged = null;

            if (@Item != null)
            {
                StatusHandler = new StatusesHandler(new StatusesHandlerProtocol(@Item.StatusEnum, false, true));
                View.Statuses = StatusHandler.Load();
                View.SelectedStatus = Model.Instance.Data.Statuses.FirstOrDefault(s => s.Id == int.Parse(@Item.Status));

                View.ItemName = @Item.Name;
                View.Ibso = @Item.PobjectName;
                View.MolName = @Item.MolName;
                View.UserName = @Item.UserName;
                View.Serial = @Item.SerialNumber;

                if (@Item.Status == ((int)StatusesEnum.Replacement).ToString())
                {
                    // �������� ������, ����� ���������� ������� ��� ������� ���
                    Model.Instance.Presenters.Printing.PrintObjects = Item;
                    Model.Instance.Presenters.Printing.PrintInfo = "���� ����������� ������ �� � ���. ���������� ���������� ����� ��������.";
                    Model.Instance.Presenters.Printing.ShowView();
                }
            }

            if (View.StatusChanged == null)
            {
                View.StatusChanged += ViewStatusChanged;
            }
        }

        protected void ClearView()
        {
            View.Statuses = null;
            View.SelectedStatus = null;
            View.Barcode = View.Ibso = View.ItemName = View.MolName = View.UserName = View.Serial = string.Empty;
            View.UserEnterEnable = View.MolEnterEnable = View.SerialEnterEnable = false;
        }

        protected void ViewBarcodeEnterHand(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("���� ��", () => Model.Instance.Presenters.EnterBarcode.ShowView(), true, true);
        }

        /// <summary>
        /// ��������� ������� ����� �����
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ViewStatusChanged(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("��������� ������� ��� ����� ����� ��������������",
                ()=>
                    {
                        if(StatusHandler != null)
                        {
                            StatusHandler.ChangeStatus(View.SelectedStatus);
                            FillView();
                        }
                    });
        }

        protected void ViewDeleteFind(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("�������� �������� ���",
                  () =>
                      {
                          if (View.FindItemSelected == null)
                          {
                              Model.Instance.Messages.ShowExclamation("���������� ������� ��� ��� ��������", "Ok");
                              return;
                          }

                          if (Model.Instance.Messages.ShowQuestion("������� ��������� ��� �� ������ ���������?", DialogEnum.No, "��", "���") != DialogEnum.Yes)
                          {
                              return;
                          }

                          var item = Model.Instance.Data.Store.Table<Items>().Where(i => i.Id == View.FindItemSelected.Id && i.FobjectId == @Object.PobjectId).Take(1).FirstOrDefault();

                          if(item == null)
                          {
                              Model.Instance.Messages.ShowExclamation("�� ������� ����� ��������� ��� � ���� ������", "Ok");
                              return;
                          }

                          item.ToNotFound();

                          Model.Instance.Data.Store.Update(item);

                          Model.Instance.Data.DeleteDoubles(item.Id, @Object.PobjectId);

                          ViewToFind(sender, e);

                          SessionItemChanged(sender, e);
                      });
        }

        protected void SessionItemChanged(object sender, EventArgs e)
        {
            Model.Instance.Invoker.DoIndependentThread("���������� ���������� ����� ��������",
               () =>
                   {
                       View.StatusText = "���������� ���������� ... ";

                       int all;
                       int found;

                       Model.Instance.Data.ObjectStatistics(@Object, out all, out found);

                       View.Header = string.Format(Constants.InventoryStatus, all, found);
                   },
               () =>
                   {
                       if (@Object != null)
                       {
                           View.StatusText = @Object.PobjectName;
                       }

                   }, false);
        }

        protected void ViewAddUser(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("����� ������������", () =>
            {
                StopTerminal();
                Model.Instance.Presenters.SelectUsers.Personals = Model.Instance.Data.Users(false);
                Model.Instance.Presenters.SelectUsers.ShowView();
            });
        }

        protected void ViewAddMol(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("����� ����", () =>
            {
                StopTerminal();
                Model.Instance.Presenters.SelectMols.Personals = Model.Instance.Data.Mols(false);
                Model.Instance.Presenters.SelectMols.ShowView();
            });
        }

        protected void ViewAddSerial(object sender, EventArgs e)
        {
            StopTerminal();
            Model.Instance.Presenters.SerialNumberInput.ShowView();
        }

        /// <summary>
        /// ���� ��
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ItemParametersChanged(object sender, EventArgs e)
        {
            if (@Item != null)
            {
                View.Serial = @Item.SerialNumber;
                View.UserName = @Item.UserName;
                View.MolName = @Item.MolName;
            }

            if (Model.Instance.Session.CurrentView == ViewsEnum.Inventory)
            {
                StartTerminal();
            }
        }

        protected void ViewPrintNotFind(object sender, EventArgs e)
        {
            var items = View.NotFindItems.Where(i => i.Selected == 1).ToList();
            if(items.Count > 0)
            {
                Model.Instance.Presenters.Printing.PrintObjects = items.ToArray();
                Model.Instance.Presenters.Printing.ShowView();
            }
            else
            {
                Model.Instance.Messages.ShowExclamation("��� ������ ��� ������", "Ok");
            }
        }

        public override void Subscription()
        {
            View.Show += ViewShow;
            View.ToInventory += ViewToInventory;
            View.ToFind += ViewToFind;
            View.ToNotFind += ViewToNotFind;
            View.BarcodeEnter += EnterBarcode;
            View.BarcodeEnterHand += ViewBarcodeEnterHand;
            View.StatusChanged += ViewStatusChanged;
            View.DeleteFind += ViewDeleteFind;
            View.Close += ViewClose;
            View.AddUser += ViewAddUser;
            View.AddMol += ViewAddMol;
            View.AddSerial += ViewAddSerial;
            View.PrintNotFind += ViewPrintNotFind;

            Model.Instance.Presenters.SerialNumberInput.View.Close += ItemParametersChanged;
            Model.Instance.Presenters.SelectUsers.View.Close += ItemParametersChanged;
            Model.Instance.Presenters.SelectMols.View.Close += ItemParametersChanged;
        }

        public override void Unsubscription()
        {
            View.Show -= ViewShow;
            View.ToInventory -= ViewToInventory;
            View.ToFind -= ViewToFind;
            View.ToNotFind -= ViewToNotFind;
            View.BarcodeEnter -= EnterBarcode;
            View.BarcodeEnterHand -= ViewBarcodeEnterHand; 
            View.StatusChanged -= ViewStatusChanged;
            View.DeleteFind -= ViewDeleteFind;
            View.Close -= ViewClose;
            View.AddUser -= ViewAddUser;
            View.AddMol -= ViewAddMol;
            View.AddSerial -= ViewAddSerial;
            View.PrintNotFind -= ViewPrintNotFind;

            Model.Instance.Presenters.SerialNumberInput.View.Close -= ItemParametersChanged;
            Model.Instance.Presenters.SelectUsers.View.Close -= ItemParametersChanged;
            Model.Instance.Presenters.SelectMols.View.Close -= ItemParametersChanged;
        }
    }
}