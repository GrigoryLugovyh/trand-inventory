using System;
using System.Collections.Generic;
using System.Linq;
using StockCore.Entity;
using StockCore.Interfaces.Views;

namespace StockCore.Presenters
{
    public class SelectMolPresenter : StockPresenter<ISelectPersonalView<Mols>>
    {
        public SelectMolPresenter(ISelectPersonalView<Mols> view)
            : base(view)
        {
        }

        /// <summary>
        /// ���� ���������
        /// </summary>
        public List<Mols> Personals { get; set; }

        protected void ViewShow(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("�������� ��������", () =>
            {
                if (Personals != null)
                {
                    View.Personals = Personals;
                }
                else
                {
                    CloseView();
                }
            });
        }

        protected void ViewSelect(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("����� ����", () =>
            {
                if (View.SelectPersonal != null)
                {
                    if (Model.Instance.Session.CurrentItem == null)
                    {
                        Model.Instance.Session.SetMol(View.SelectPersonal);
                    }
                    else
                    {
                        var item = Model.Instance.Session.CurrentItem;

                        item.MolName = View.SelectPersonal.MolName;
                        item.MolNumber = View.SelectPersonal.MolNumber;

                        Model.Instance.Data.UpdateItem(item);
                    }

                    CloseView();
                }
                else
                {
                    Model.Instance.Messages.ShowExclamation("���������� ������� ����", "��");
                }

            });
        }

        protected void ViewFilter(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("������ �����", () =>
                         {
                             if (!string.IsNullOrEmpty(View.FilterValue))
                             {
                                 var persons = new List<Mols>();
                                 persons.AddRange(Personals);

                                 View.Personals = persons.Where(
                                                         m => m.MolName.IndexOf(View.FilterValue, StringComparison.OrdinalIgnoreCase) > -1 ||
                                                         m.MolNumber.IndexOf(View.FilterValue, StringComparison.OrdinalIgnoreCase) > -1).OrderBy(m => m.MolName).ToList();
                             }
                             else
                             {
                                 View.Personals = Personals;
                             }
                         });
        }

        protected void ViewClose(object sender, EventArgs e)
        {
            if (Model.Instance.Session.CurrentItem == null)
            {
                View.CloseToObjects();
            }
            else
            {
                View.CloseToInventory();
            }
        }

        public override void Subscription()
        {
            View.Show += ViewShow;
            View.Select += ViewSelect;
            View.Filter += ViewFilter;
            View.Close += ViewClose;
        }

        public override void Unsubscription()
        {
            View.Show -= ViewShow;
            View.Select -= ViewSelect;
            View.Filter -= ViewFilter;
            View.Close -= ViewClose;
        }
    }
}