using System;
using System.Linq;
using StockCore.Entity;
using StockCore.Interfaces.Views;

namespace StockCore.Presenters
{
    public class ObjectItemsPresenter : StockPresenter<IObjectItemsView>
    {
        public ObjectItemsPresenter(IObjectItemsView view)
            : base(view)
        {
        }

        public Objects CurrentObject { get; set; }


        protected void ViewClose(object sender, EventArgs e)
        {
            CurrentObject = null;
            View.CloseView();
        }

        protected void ViewShow(object sender, EventArgs e)
        {
            Model.Instance.Invoker.DoIndependentThread("�������� ������ ���� ��� �������", () =>
                      {
                          if (CurrentObject == null)
                          {
                              CloseView();
                              return;
                          }

                          View.StatusText = "�������� ��� ...";

                          View.SelectedCheck = false;

                          View.ObjectName = CurrentObject.PobjectName;

                          View.Items = null;

                          var items = Model.Instance.Data.Store.Table<Items>().Where(i => i.PobjectBarcode == CurrentObject.PobjectBarcode).ToList();

                          View.Items = items;
                      },
                      () =>
                          {
                              View.StatusText = "��� ��� � �������";

                          }, true);
        }

        protected void ViewSelectedChecked(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("���������� ��������� ��� ��� ������", () =>
            {
                if (View.Items != null && View.Items.Count > 0)
                {
                    if (View.SelectedCheck)
                    {
                        foreach (var o in View.Items)
                        {
                            o.Selected = 1;
                        }
                    }
                    else
                    {
                        foreach (var o in View.Items)
                        {
                            o.Selected = 0;
                        }
                    }

                    View.ItemsRefresh();
                }

            });
        }

        protected void ViewPrint(object sender, EventArgs e)
        {
            var items = View.Items.Where(i => i.Selected == 1).ToList();
            if(items.Count > 0)
            {
                Model.Instance.Presenters.Printing.PrintObjects = items.ToArray();
                Model.Instance.Presenters.Printing.ShowView();
            }
            else
            {
                Model.Instance.Messages.ShowExclamation("��� ������ ��� ������", "Ok");
            }
        }

        public override void Subscription()
        {
            View.Show += ViewShow;
            View.Close += ViewClose;
            View.SelectedChecked += ViewSelectedChecked;
            View.Print += ViewPrint;
        }

        public override void Unsubscription()
        {
            View.Show -= ViewShow;
            View.Close -= ViewClose;
            View.SelectedChecked -= ViewSelectedChecked;
            View.Print -= ViewPrint;
        }
    }
}