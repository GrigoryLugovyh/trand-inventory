using System;
using System.Text;
using StockCore.Enums;
using StockCore.Factories;
using StockCore.Handlers.Exchange.Configuration.CSV;
using StockCore.Interfaces.Views;

namespace StockCore.Presenters
{
    public class EnterBarcodePresenter : StockPresenter<IEnterBarcodeView>
    {
        public EnterBarcodePresenter(IEnterBarcodeView view)
            : base(view)
        {
        }

        protected void ViewShow(object sender, EventArgs e)
        {
            View.Barcode = string.Empty;

            var prefixes = CsvExchangeSettings.Instance.Prefixes;

            if (prefixes != null && prefixes.Count > 0 && BarcodeFactory.Instance.Prefix.Length > 0)
            {
                View.Prefixes = prefixes;
            }
            else
            {
                View.PrefixesVisible = false;
            }

            var postfixes = Model.Instance.Data.Codes;

            if (postfixes != null && postfixes.Count > 0 && BarcodeFactory.Instance.Postfix.Length > 0)
            {
                View.Postfixes = postfixes;
            }
            else
            {
                View.PostfixesVisible = false;
            }
        }

        protected void ViewEnter(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("������ ���� ��", () =>
             {
                 //if(View.SelectedPrefix == null)
                 //{
                 //    Model.Instance.Messages.ShowExclamation("���������� ������� ������� �����-����", "Ok");
                 //    return;
                 //}

                 //if(View.SelectedPostfix == null)
                 //{
                 //    Model.Instance.Messages.ShowExclamation("���������� ������� �������� �����-����", "Ok");
                 //    return;
                 //}

                 if(string.IsNullOrEmpty(View.Barcode))
                 {
                     Model.Instance.Messages.ShowExclamation("���������� ������ �����-���", "Ok");
                     return;
                 }

                 const string separator = ".";

                 var barcodeBuilder = new StringBuilder();

                 if (View.SelectedPrefix != null && !string.IsNullOrEmpty(View.SelectedPrefix.Prefix))
                 {
                     barcodeBuilder.Append(View.SelectedPrefix.Prefix);
                     barcodeBuilder.Append(separator);
                 }

                 barcodeBuilder.Append(View.Barcode);

                 if (View.SelectedPostfix != null && !string.IsNullOrEmpty(View.SelectedPostfix.Code))
                 {
                     barcodeBuilder.Append(separator);
                     barcodeBuilder.Append(View.SelectedPostfix.Code);
                 }

                 var barcode = barcodeBuilder.ToString();

                 if(!BarcodeFactory.Instance.Check(barcode))
                 {
                     Model.Instance.Messages.ShowExclamation("��������� �����-��� �� ������������� ������� ����� �����-����", "Ok");
                     return;
                 }

                 Close();

                 Model.Instance.Session.SetItemBarcode(barcode);

             });
        }

        protected void Close()
        {
            if (Model.Instance.Session.CurrentView == ViewsEnum.Inventory)
            {
                View.CloseToInventory();
            }
            else if (Model.Instance.Session.CurrentView == ViewsEnum.Information)
            {
                View.CloseToInformation();
            }
            else
            {
                Model.Instance.Presenters.Main.ShowView();
            }
        }

        protected void ViewClose(object sender, EventArgs e)
        {
            Close();
        }

        public override void Subscription()
        {
            View.Show += ViewShow;
            View.Close += ViewClose;
            View.Enter += ViewEnter;
        }

        public override void Unsubscription()
        {
            View.Show -= ViewShow;
            View.Close -= ViewClose;
            View.Enter -= ViewEnter;
        }
    }
}