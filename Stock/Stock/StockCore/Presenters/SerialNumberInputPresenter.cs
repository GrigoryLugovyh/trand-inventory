using System;
using StockCore.Interfaces.Views;

namespace StockCore.Presenters
{
    public class SerialNumberInputPresenter : StockPresenter<ISerialNumberInputView>
    {
        public SerialNumberInputPresenter(ISerialNumberInputView view)
            : base(view)
        {
        }

        public override double Priority
        {
            get { return 2; }
        }

        protected void ViewShow(object sender, EventArgs e)
        {
            View.SerialNumber = string.Empty;
            var currentSerial = Model.Instance.Session.CurrentItem.SerialNumber;
            View.CurrentSerialNumber = string.Format(Constants.CurrentSerialText, !string.IsNullOrEmpty(currentSerial) ? currentSerial : "-");

            StartTerminal();
        }

        protected void ViewClose(object sender, EventArgs e)
        {
            StopTerminal();
        }

        protected override void TerminalBarcodeRead(object sender, Events.StockEventArgs e)
        {
            View.SerialNumber = e.Message;

            ViewInput(sender, e);
        }

        protected void ViewInput(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("���� ��������� ������ ���", () =>
                  {
                      if(Model.Instance.Session.CurrentItem != null)
                      {
                          var item = Model.Instance.Session.CurrentItem;

                          item.SerialNumber = View.SerialNumber;

                          Model.Instance.Data.UpdateItem(item);

                          View.CloseView();
                      }

                  });
        }

        public override void Subscription()
        {
            View.Show += ViewShow;
            View.Close += ViewClose;
            View.Input += ViewInput;
        }

        public override void Unsubscription()
        {
            View.Show -= ViewShow;
            View.Close -= ViewClose;
            View.Input -= ViewInput;
        }
    }
}