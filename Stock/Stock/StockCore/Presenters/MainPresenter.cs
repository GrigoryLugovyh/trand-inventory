using System;
using StockCore.Enums;
using StockCore.Interfaces.Views;

namespace StockCore.Presenters
{
    public class MainPresenter : StockPresenter<IMainView>
    {
        public MainPresenter(IMainView view)
            : base(view)
        {
        }

        /// <summary>
        /// ��������������
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ViewInventoryClick(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("������ ��������������", () =>
                                       {
                                           Model.Instance.Presenters.Objects.ShowView();
                                       });
        }

        /// <summary>
        /// ����������
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ViewInformationClick(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("���������� �� ���", ()=>
                                      {
                                         Model.Instance.Presenters.Information.ShowView(); 
                                      });
        }

        /// <summary>
        /// �������
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ViewObjectsClick(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("���������� �� ��������", () =>
                                          {
                                              Model.Instance.Presenters.InformationObjects.ShowView();
                                          });
        }

        /// <summary>
        /// �����
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ViewExchangeClick(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("������ ������", () =>
                                          {
                                              Model.Instance.Presenters.Exchange.ShowView();
                                          });

        }

        /// <summary>
        /// �����
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ViewExitClick(object sender, EventArgs e)
        {
            StopTerminal();
        }

        /// <summary>
        /// ������� ����������
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ViewShow(object sender, EventArgs e)
        {
            Model.Instance.Invoker.DoIndependentThread("���������� ���������� ������", () =>
            {
                Model.Instance.Session.SetView(ViewsEnum.Main);

                View.StatusText = "���������� ���������� ...";

                int itemsAll;
                int objectsAll;
                int itemsProcessed;
                int objectsProcessed;

                Model.Instance.Data.Statistics(out itemsAll, out objectsAll, out itemsProcessed, out objectsProcessed);

                View.ItemsAll = itemsAll.ToString();
                View.ObjectsAll = objectsAll.ToString();
                View.ItemsProcessed = itemsProcessed.ToString();
                View.ObjectsProcessed = objectsProcessed.ToString();

            },
            ()=>
                {
                    View.StatusText = "�������� ��������";
                },
            false);
        }

        public override void Subscription()
        {
            View.Show += ViewShow;
            View.InventoryClick += ViewInventoryClick;
            View.InformationClick += ViewInformationClick;
            View.ObjectsClick += ViewObjectsClick;
            View.ExchangeClick += ViewExchangeClick;
            View.ExitClick += ViewExitClick;

            Model.Instance.Presenters.Exchange.View.Close += ViewShow;
            Model.Instance.Presenters.Objects.View.Close += ViewShow;
        }

        public override void Unsubscription()
        {
            View.Show -= ViewShow;
            View.InventoryClick -= ViewInventoryClick;
            View.InformationClick -= ViewInformationClick;
            View.ObjectsClick -= ViewObjectsClick;
            View.ExchangeClick -= ViewExchangeClick;
            View.ExitClick -= ViewExitClick;

            Model.Instance.Presenters.Exchange.View.Close -= ViewShow;
            Model.Instance.Presenters.Objects.View.Close -= ViewShow;
        }
    }
}