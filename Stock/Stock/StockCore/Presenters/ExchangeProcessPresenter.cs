using System;
using System.Threading;
using StockCore.Enums;
using StockCore.Handlers.Exchange;
using StockCore.Interfaces.Views;

namespace StockCore.Presenters
{
    public class ExchangeProcessPresenter : StockPresenter<IExchangeProcessView>
    {
        public ExchangeProcessPresenter(IExchangeProcessView view)
            : base(view)
        {
        }

        /// <summary>
        /// ��������� ������
        /// </summary>
        public Exchange Exchanger { get; set; }

        /// <summary>
        /// ����� ������
        /// </summary>
        protected Thread ExchangThread { get; set; }

        public override void ShowView()
        {
            if (Exchanger != null)
            {
                View.Header = Exchanger.ExchangeFriandlyName;
                View.ProcessText = "���������� ...";
                View.ExchangeTime = "0:00:00";
                View.Progress = 0;
                View.NotProcessed = View.Prepared = View.Processed = View.Received = "0";

                Exchanger.MoveNextRecord += ExchangerMoveNextRecord;
                Exchanger.ExchangeFinishedException += ExchangerExchangeFinishedException;
                Exchanger.ExchangeFinishedSuccessfully += ExchangerExchangeFinishedSuccessfully;

                ExchangThread = new Thread(Exchanger.Start) {IsBackground = true, Name = "ExchangThread"};
                ExchangThread.Start();

                base.ShowView();
            }
        }

        protected void FinishExchange()
        {
            if(Exchanger != null)
            {
                Exchanger.MoveNextRecord -= ExchangerMoveNextRecord;
                Exchanger.ExchangeFinishedException -= ExchangerExchangeFinishedException;
                Exchanger.ExchangeFinishedSuccessfully -= ExchangerExchangeFinishedSuccessfully;
            }
        }

        protected void ExchangerExchangeFinishedSuccessfully(object sender, EventArgs e)
        {
            FinishExchange();
            View.ProcessText = Exchanger.ExchangeProcessSuccessfully;
            Model.Instance.Messages.ShowExclamation("����� ������� �������� �������!", "��");
        }

        protected void ExchangerExchangeFinishedException(object sender, Events.StockEventArgs e)
        {
            FinishExchange();
            View.ProcessText = Exchanger.ExchangeProcessFail;
            Model.Instance.Messages.ShowExclamation(e.Message, "��");
        }

        protected void ExchangerMoveNextRecord(object sender, EventArgs e)
        {
            View.Header = Exchanger.ExchangeFriandlyName;
            View.ProcessText = Exchanger.ExchangeProcessStatus;
            View.ExchangeTime = string.Format("{0}:{1}:{2}", Exchanger.Watcher.Elapsed.Hours, Exchanger.Watcher.Elapsed.Minutes, Exchanger.Watcher.Elapsed.Seconds);
            View.Progress = Exchanger.Persent;
            View.Received = Exchanger.Received.ToString();
            View.Prepared = Exchanger.Prepared.ToString();
            View.Processed = Exchanger.Processed.ToString();
            View.NotProcessed = Exchanger.NotProcessed.ToString();
        }

        protected void ViewAbort(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("���������� ������ �������", () =>
            {
                if (Exchanger != null && Exchanger.Status != StartableEnum.Stopped)
                {
                    if(Model.Instance.Messages.ShowQuestion("�� ������������� ������ ���������� ����� �������?",DialogEnum.No, "��", "���") == DialogEnum.Yes)
                    {
                        Exchanger.Stop();
                    }
                }
                else
                {
                    CloseView();
                }

            }, false, true);
        }

        public override void Subscription()
        {
            View.Abort += ViewAbort;
        }

        public override void Unsubscription()
        {
            View.Abort -= ViewAbort;
        }
    }
}