using StockCore.Entity;
using StockCore.Enums;
using StockCore.Interfaces.Views;

namespace StockCore.Presenters
{
    public class InformationObjectsPresenter : StockPresenter<IInformationObjectsView>
    {
        public InformationObjectsPresenter(IInformationObjectsView view)
            : base(view)
        {
        }

        protected void ViewShow(object sender, System.EventArgs e)
        {
            Model.Instance.Session.SetView(ViewsEnum.Onjects);

            StopTerminal();

            Model.Instance.Invoker.Do("�������� ��������� ��������", ()=>
                      {
                          View.StatusText = "�������� ��������� �������� ...";

                          View.FoundObjects = Model.Instance.Data.Store.Table<Objects>().Where(o => o.Found > 0).OrderBy(o => o.PobjectName).ToList();

                      });

            Model.Instance.Invoker.DoIndependentThread("�������� ����������� ��������",
                       () =>
                           {
                               View.StatusText = "�������� ����������� �������� ...";

                               View.NotFoundObjects = Model.Instance.Data.Store.Table<Objects>().Where(o => o.Found == 0).OrderBy(o => o.PobjectName).ToList();

                           },
                       () =>
                           {
                               View.StatusText = "�������";

                           }, true);
        }

        protected void ViewClose(object sender, System.EventArgs e)
        {
            View.CloseView();
        }

        public override void Subscription()
        {
            View.Show += ViewShow;
            View.Close += ViewClose;
        }

        public override void Unsubscription()
        {
            View.Show -= ViewShow;
            View.Close -= ViewClose;
        }
    }
}