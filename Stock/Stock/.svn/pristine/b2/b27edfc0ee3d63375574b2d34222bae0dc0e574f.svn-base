﻿using System;
using System.Data.SQLite;
using System.Diagnostics;
using System.Threading;
using StockCore.Enums;
using StockCore.Events;
using StockCore.Handlers.Exchange.Configuration;
using StockCore.Interfaces.Common;
using StockCore.Interfaces.Handlers;

namespace StockCore.Handlers.Exchange
{
    /// <summary>
    /// Механизм обмена
    /// </summary>
    public abstract class Exchange : IStartable
    {
        protected Exchange(IExchange view, IExchangeProtocol handler, ExchangeSettings settings)
        {
            View = view;
            Handler = handler;
            Settings = settings;

            Status = StartableEnum.Stopped;
        }

        /// <summary>
        /// Соединение с базой
        /// </summary>
        public SQLiteConnection ExchangeConnection { get; set; }

        /// <summary>
        /// Транзакция sql
        /// </summary>
        public SQLiteTransaction ExchangeTransaction { get; set; }

        /// <summary>
        /// Визуально представление обмена данными
        /// </summary>
        public IExchange View { get; protected set; }

        /// <summary>
        /// Обработчик обмена
        /// </summary>
        public IExchangeProtocol Handler { get; protected set; }

        /// <summary>
        /// Настройки обмена
        /// </summary>
        public ExchangeSettings Settings { get; protected set; }

        /// <summary>
        /// Сведение о подключениии к источнику данных
        /// </summary>
        protected bool IsConnected { get; set; }

        /// <summary>
        /// Успешно оединились с источником данных
        /// </summary>
        public event EventHandler Connected;

        /// <summary>
        /// Запись не была подготовлена
        /// </summary>
        public event EventHandler RacordNotPrepared;

        /// <summary>
        /// Запись не была обработана
        /// </summary>
        public event EventHandler RecordNotProcessed;

        /// <summary>
        /// Переход к обработке следующей записи
        /// </summary>
        public event EventHandler MoveNextRecord;
        
        /// <summary>
        /// Обмен завершен успешно
        /// </summary>
        public event EventHandler ExchangeFinishedSuccessfully;

        /// <summary>
        /// Ошибка в процессе обработки строки
        /// </summary>
        public event EventHandler<StockEventArgs> ExchangeProcessedException;
        
        /// <summary>
        /// Ошибка в процессе обмена
        /// </summary>
        public event EventHandler<StockEventArgs> ExchangeFinishedException;

        /// <summary>
        /// Отсоединились от источника данных
        /// </summary>
        public event EventHandler Disconnected;

        /// <summary>
        /// Ошибка в момент отсоединения
        /// </summary>
        public event EventHandler<StockEventArgs> DisconnectedException;

        /// <summary>
        /// Обмен был прерван пользователем
        /// </summary>
        public event EventHandler ExchangeAbortedByUser;

        /// <summary>
        /// Название обмена
        /// </summary>
        protected abstract string ExchangeFriandlyName { get; }

        /// <summary>
        /// Статус обмена
        /// </summary>
        protected abstract string ExchangeProcessStatus { get; }

        /// <summary>
        /// Инициализация обмена
        /// </summary>
        protected abstract void InitExchange();

        /// <summary>
        /// Завершение обмена
        /// </summary>
        protected abstract void FinishExchange();

        /// <summary>
        /// Подлючились к источнику данных
        /// </summary>
        /// <returns></returns>
        protected abstract bool Connect();

        /// <summary>
        /// Процент обработанных записей
        /// </summary>
        /// <returns></returns>
        protected abstract int PersentProcessed();

        /// <summary>
        /// Есть запись для обработки
        /// </summary>
        /// <returns></returns>
        protected abstract bool ExistRecord();
        
        /// <summary>
        /// Подгатовили запись
        /// </summary>
        /// <returns></returns>
        protected abstract bool PrepareRecord();

        /// <summary>
        /// Обработали запись
        /// </summary>
        /// <returns></returns>
        protected abstract bool ProcessRecord();

        /// <summary>
        /// Обключились от источника данных
        /// </summary>
        /// <returns></returns>
        protected abstract bool Disconnect();

        /// <summary>
        /// Информация о текущей обрабатываемой позиции
        /// </summary>
        /// <returns></returns>
        protected abstract string CurrendRecordInformation();

        /// <summary>
        /// Записей получено
        /// </summary>
        public long Received { get; protected set; }

        /// <summary>
        /// Записей подготовлено
        /// </summary>
        public long Prepared { get; protected set; }

        /// <summary>
        /// Записей обработано
        /// </summary>
        public long Processed { get; protected set; }

        /// <summary>
        /// Кол-во необработанных записей
        /// </summary>
        public long NotProcessed { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public int Persent { get; protected set; }

        /// <summary>
        /// Время выполнения операции обмена
        /// </summary>
        public Stopwatch Watcher { get; protected set; }

        /// <summary>
        /// Сброс счетчиков
        /// </summary>
        protected virtual void ResetCounters()
        {
            Received = Prepared = Processed = NotProcessed = 0;
            Persent = 0;
        }

        /// <summary>
        /// Инициализация визульного интерфейса для обмена
        /// </summary>
        protected virtual void InitView()
        {
            if (View != null)
            {
                var thread = new Thread(() =>
                                            {
                                                View.ShowView();

                                                View.Header = ExchangeFriandlyName;

                                                View.Progress = 0;
                                                View.ExchangeTime = "00:00:00";
                                                View.ProcessText = "Подготовка ...";
                                                View.NotProcessed = View.Prepared = View.Processed = View.Received = "0";

                                                View.Abort += ExchangeAbort;
                                            })
                                 {
                                     IsBackground = true,
                                     Name = "Exchange view thread"
                                 };
                thread.Start();
            }
        }

        /// <summary>
        /// Закрываем представление
        /// </summary>
        protected virtual void CloseView()
        {
            if (View != null)
            {
                View.CloseView();
                View.Abort -= ExchangeAbort;
            }
        }

        /// <summary>
        /// Обновляем данные об импорте
        /// </summary>
        protected virtual void RefreshView()
        {
            if (View != null)
            {
                View.Progress = Persent;
                View.ExchangeTime = string.Format("{0}:{1}:{2}", Watcher.Elapsed.Hours, Watcher.Elapsed.Minutes, Watcher.Elapsed.Seconds);
                View.ProcessText = ExchangeProcessStatus;
                View.NotProcessed = NotProcessed.ToString();
                View.Prepared = Prepared.ToString();
                View.Processed = Processed.ToString();
                View.Received = Received.ToString();
            }
        }

        /// <summary>
        /// Реакция на окончания импорта данных
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void ExchangeAbort(object sender, EventArgs e)
        {
            if (View != null)
            {
                if (Model.Instance.Messages.ShowQuestion("Вы уверены, что хотите основить обмен данными?", DialogEnum.No, "Да", "Нет") == DialogEnum.Yes)
                {
                    Stop();
                }
            }
        }

        public StartableEnum Status { get; protected set; }

        /// <summary>
        /// Запуск обработки
        /// </summary>
        public void Start()
        {
            try
            {
                if (!Model.Instance.StockDatabase.IsActive || Model.Instance.StockDatabase.Store == null)
                    throw new Exception(string.Format("Нет подключения к базе данных, невозможно выполнить операцию \"{0}\"", ExchangeFriandlyName));

                // запускаем таймер
                if (Watcher == null)
                {
                    Watcher = new Stopwatch();
                }

                Watcher.Reset();
                Watcher.Start();

                Status = StartableEnum.Started;

                // Инициализируем внешнее представление
                InitView();

                // Подготовительные работы перед обменом
                InitExchange();

                if (Connect())
                {
                    IsConnected = true;

                    if (Connected != null)
                    {
                        Connected(this, EventArgs.Empty);
                    }

                    // начинаем обмен
                    while (ExistRecord())
                    {
                        if (Status == StartableEnum.Stopped)
                            break;

                        try
                        {
                            Received++;

                            // подготавливаем запись
                            if (PrepareRecord())
                            {
                                Prepared++;

                                // обрабатываем запись
                                if (!ProcessRecord())
                                {
                                    if (RecordNotProcessed != null)
                                    {
                                        RecordNotProcessed(this, EventArgs.Empty);
                                    }

                                    if (Handler.StopWhenRecordNotProcessed)
                                    {
                                        Status = StartableEnum.Stopped;
                                    }

                                    NotProcessed++;
                                }
                                else
                                {
                                    Processed++;
                                }
                            }
                            else
                            {
                                if (RacordNotPrepared != null)
                                {
                                    RacordNotPrepared(this, EventArgs.Empty);
                                }

                                if (Handler.StopWhenRecordNotPrepared)
                                {
                                    Status = StartableEnum.Stopped;
                                }
                            }

                            // получем процент обработанных записей
                            Persent = PersentProcessed();

                            // обновляем представление
                            RefreshView();

                            if (MoveNextRecord != null)
                            {
                                MoveNextRecord(this, EventArgs.Empty);
                            }
                        }
                        catch (Exception ex)
                        {
                            NotProcessed++;

                            if (ExchangeProcessedException != null)
                            {
                                var info = CurrendRecordInformation();
                                ExchangeProcessedException(this, new StockEventArgs("Ошибка в процессе обработки: {0}\r\n Дополнительная информация: {1}", ex.Message, !string.IsNullOrEmpty(info) ? info : "отсутствует"));
                            }

                            if (Handler.StopWhenRecordNotProcessed || Handler.StopWhenRecordNotPrepared)
                            {
                                Status = StartableEnum.Stopped;
                            }
                        }
                    }

                    // обмен успешно завершился
                    if (ExchangeFinishedSuccessfully != null)
                    {
                        ExchangeFinishedSuccessfully(this, EventArgs.Empty);
                    }
                }
                else
                {
                    throw new Exception("Не удалось соедениться с источником данных");
                }
            }
            catch (Exception ex)
            {
                // закрываем внешнее представление
                CloseView();

                // событие аварийного завершения процесса обмена, необработанной исключение
                if (ExchangeFinishedException != null)
                {
                    ExchangeFinishedException(this, new StockEventArgs(string.Format("В процессе обмена данными возникла ошибка:\r\n{0}", ex.Message)));
                }
            }
            finally
            {
                try
                {
                    Status = StartableEnum.Stopped;

                    if (IsConnected)
                    {
                        Disconnect();

                        if (Disconnected != null)
                        {
                            Disconnected(this, EventArgs.Empty);
                        }
                    }

                    FinishExchange();
                }
                catch (Exception ex)
                {
                    if (DisconnectedException != null)
                    {
                        DisconnectedException(this, new StockEventArgs("В момент завершения процесса обмена возникла ошибка: {0}", ex.Message));
                    }
                }
                finally
                {
                    IsConnected = false;

                    // закрываем внешнее представление
                    CloseView();

                    // чистим память после обмена
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }

                // останавливам таймер
                if (Watcher != null)
                {
                    Watcher.Stop();
                }
            }
        }

        /// <summary>
        /// Остановка обработки
        /// </summary>
        public void Stop()
        {
            Status = StartableEnum.Stopped;

            if (ExchangeAbortedByUser != null)
            {
                ExchangeAbortedByUser(this, EventArgs.Empty);
            }
        }

        public void Restart()
        {
            // нет реализации
        }
    }
}