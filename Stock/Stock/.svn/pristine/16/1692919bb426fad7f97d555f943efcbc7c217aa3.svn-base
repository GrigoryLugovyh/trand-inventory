﻿using System;
using System.Collections.Generic;
using Resco.UIElements;
using StockCore.Entity;
using StockCore.Interfaces.Views;

namespace Stock.Views
{
    public partial class AddItemControl : StockControl, IAddItemView
    {
        public AddItemControl()
        {
            InitializeComponent();
        }

        public event EventHandler Show;

        public event EventHandler Close;

        public event EventHandler Add;

        public string ItemBarcode
        {
            get { return BarcodeText.Text; }
            set { Invoker(() => BarcodeText.Text = value); }
        }

        public string ItemName
        {
            get { return ItemNameText.Text; }
            set { Invoker(() => ItemNameText.Text = value); }
        }

        public List<Statuses> Statuses
        {
            get { return ItemStatusesBox.DataSource as List<Statuses>; }
            set { Invoker(() => ItemStatusesBox.DataSource = value); }
        }

        public Statuses SelectedStatus
        {
            get { return ItemStatusesBox.SelectedItem as Statuses; }
        }

        public DateTime ItemBeginDate
        {
            get { return ItemBegindatePiker.Value; }
            set { Invoker(() => ItemBegindatePiker.Value = value); }
        }

        public bool BeginDateEnable
        {
            get { return ItemBegindatePiker.Enabled; }
            set { Invoker(() => ItemBegindatePiker.Enabled = value); }
        }

        public void ShowView()
        {
            if(Show != null)
            {
                Show(this, EventArgs.Empty);
            }

            ShowView<AddItemControl>(AnimationEffect.MoveLeft);
            Invoker(() => ItemNameText.Focus());
        }

        public void CloseView()
        {
            if(Close != null)
            {
                Close(this, EventArgs.Empty);
            }

            ShowView<InventoryControl>(AnimationEffect.MoveRight);
        }

        private void AddBtnClick(object sender, UIMouseEventArgs e)
        {
            if(Add != null)
            {
                Add(sender, e);
            }
        }

        private void CancelBtnClick(object sender, UIMouseEventArgs e)
        {
            CloseView();
        }
    }
}
