using System;
using System.Collections.Generic;
using System.Drawing;
using Resco.UIElements;
using Resco.UIElements.Controls;

namespace Stock.Handlers
{
    /// <summary>
    /// ��������� ������������ ������� 
    /// </summary>
    public sealed class ControlHandler
    {
        private static readonly object Sync = new object();

        private static volatile ControlHandler _instance;

        /// <summary>
        /// �������������
        /// </summary>
        public static ControlHandler Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (Sync)
                    {
                        if (_instance == null)
                        {
                            _instance = new ControlHandler();
                        }
                    }
                }

                return _instance;
            }
        }

        public Dictionary<Type, UIUserPanel> Views = new Dictionary<Type, UIUserPanel>();

        public UIElement CurrentView { get; private set; }

        public SizeF ScaleFactor
        {
            get { return new SizeF(1f, 1f); }
        }

        public void ShowView(UIElement view)
        {
            ShowView(view, AnimationEffect.None);
        }

        public void ShowView(UIElement view, AnimationEffect animationEffect)
        {
            if (CurrentView == null)
            {
                UIAnimation.Show(view, AnimationEffect.None);
            }
            else
            {
                UIAnimation.Exchange(CurrentView, view, animationEffect);
            }

            view.BringToFront();

            view.Focus();

            CurrentView = view;
        }

        public T GetView<T>(UIElementPanelControl panel) where T : UIUserPanel
        {
            UIUserPanel view;

            if (!Views.TryGetValue(typeof(T), out view))
            {
                view = CreateView<T>(panel);
                Views.Add(typeof(T), view);
            }

            return (T)view;
        }

        public T CreateView<T>(UIElementPanelControl panel) where T : UIUserPanel
        {
            DeleteView<T>();

            var view = Activator.CreateInstance<T>();

            view.Visible = false;

            view.Layout = new ElementLayout(0, 0);

            panel.Children.Add(view);

            view.Scale(ScaleFactor);

            Views.Add(typeof(T), view);

            return view;
        }

        public void DeleteView<T>() where T : UIUserPanel
        {
            UIUserPanel view;
            if (Views.TryGetValue(typeof(T), out view))
            {
                Views.Remove(typeof(T));
            }
        }

        public void ClearViews()
        {
            if (Views.Count != 0)
            {
                Views.Clear();
            }
        }
    }
}