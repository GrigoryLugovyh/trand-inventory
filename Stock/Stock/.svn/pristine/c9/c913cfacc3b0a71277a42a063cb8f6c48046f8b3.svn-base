﻿using System;
using System.Collections.Generic;
using Resco.UIElements;
using Stock.Views.Items;
using StockCore.Interfaces.Views;

namespace Stock.Views
{
    public partial class InventoryControl : StockControl, IInventoryView
    {
        public InventoryControl()
        {
            InitializeComponent();
        }

        public event EventHandler Show;

        public event EventHandler Close;

        public void ShowView()
        {
            FindItemsListView.DataTemplate = new ItemNormalControl { SelectedVisible = false };
            FindItemsListView.SelectedDataTemplate = new ItemSelectedControl { SelectedVisible = false };

            NotFindItemsListView.DataTemplate = new ItemNormalControl();
            NotFindItemsListView.SelectedDataTemplate = new ItemSelectedControl();

            if (Show != null)
            {
                Show(this, EventArgs.Empty);
            }

            ShowView<InventoryControl>(AnimationEffect.MoveLeft);
        }

        public void CloseView()
        {
            if (Close != null)
            {
                Close(this, EventArgs.Empty);
            }

            ShowView<ObjectsControl>(AnimationEffect.MoveRight);
        }

        private void FinishBtnClick(object sender, UIMouseEventArgs e)
        {
            CloseView();
        }

        public event EventHandler BarcodeEnter;

        public event EventHandler UserEnter;

        public event EventHandler MolEnter;

        public event EventHandler SerialEnter;

        public event EventHandler Finish;

        public string Statistics
        {
            get { return HeaderLabel.Text; }
            set { Invoker(() => HeaderLabel.Text = value); }
        }

        public string Place
        {
            get { return StatusLabel.Text; }
            set { Invoker(() => StatusLabel.Text = value); }
        }

        public string Barcode
        {
            get { return BarcodeText.Text; }
            set { Invoker(() => BarcodeText.Text = value); }
        }

        public string ItemName
        {
            get { return ItemNameText.Text; }
            set { Invoker(() => ItemNameText.Text = value); }
        }

        public string Ibso
        {
            get { return IbsoText.Text; }
            set { Invoker(() => IbsoText.Text = value); }
        }

        public string UserName
        {
            get { return UserNameText.Text; }
            set { Invoker(() => UserNameText.Text = value); }
        }

        public string MolName
        {
            get { return MolNameText.Text; }
            set { Invoker(() => MolNameText.Text = value); }
        }

        public string Serial
        {
            get { return StatusText.Text; }
            set { Invoker(() => StatusText.Text = value); }
        }

        public event EventHandler ToInventory;

        public event EventHandler ToFind;

        public event EventHandler ToNotFind;

        public event EventHandler DeleteFind;

        public event EventHandler PrintNotFind;

        public List<StockCore.Entity.Items> FindItems
        {
            get { return FindItemsListView.DataSource as List<StockCore.Entity.Items>; }
            set { Invoker(() => FindItemsListView.DataSource = value); }
        }

        public List<StockCore.Entity.Items> NotFindItems
        {
            get { return NotFindItemsListView.DataSource as List<StockCore.Entity.Items>; }
            set { Invoker(() => NotFindItemsListView.DataSource = value); }
        }

        public StockCore.Entity.Items FindItemSelected { get; protected set; }

        public StockCore.Entity.Items NotFindItemSelected { get; protected set; }

        private void InventoryPanelSelectedIndexChanging(object sender, SelectedIndexChangingEventArgs e)
        {
            switch (e.NewIndex)
            {
                case 0:
                    {
                        if (ToInventory != null)
                        {
                            ToInventory(sender, e);
                        }
                        break;
                    }
                case 1:
                    {
                        if (ToFind != null)
                        {
                            ToFind(sender, e);
                        }
                        break;
                    }
                case 2:
                    {
                        if (ToNotFind != null)
                        {
                            ToNotFind(sender, e);
                        }
                        break;
                    }
            }
        }

        private void DeleteBtnClick(object sender, UIMouseEventArgs e)
        {
            if(DeleteFind != null)
            {
                DeleteFind(sender, e);
            }
        }

        private void PrintBtnClick(object sender, UIMouseEventArgs e)
        {
            if(PrintNotFind != null)
            {
                PrintNotFind(sender, e);
            }
        }

        private void FindItemsListViewSelectedIndexChanging(object sender, SelectedIndexChangingEventArgs e)
        {
            if(FindItems != null && FindItems.Count > 0)
            {
                FindItemSelected = FindItems[e.NewIndex];
            }
        }

        private void NotFindItemsListViewSelectedIndexChanging(object sender, SelectedIndexChangingEventArgs e)
        {
            if(NotFindItems != null && NotFindItems.Count > 0)
            {
                NotFindItemSelected = NotFindItems[e.NewIndex];
            }
        }
    }
}