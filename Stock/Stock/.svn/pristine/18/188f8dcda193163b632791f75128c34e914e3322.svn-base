﻿using System;
using System.Collections.Generic;
using System.Linq;
using StockCore.Common;
using StockCore.Enums;
using StockCore.Environments;
using StockCore.Events;
using StockCore.Interfaces.Common;

namespace StockCore
{
    /// <summary>
    ///  Модель приложения (потокобезопасный синглтон)
    /// </summary>
    public sealed class Model : IStartable
    {
        public Model()
        {
            Status = StartableEnum.Stopped;
        }

/*
 * Статические элементы, общедоступные свойства
 */

        private static readonly object Sync = new object();

        private static volatile Model _instance;

        /// <summary>
        /// Инициализация
        /// </summary>
        public static Model Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (Sync)
                    {
                        if (_instance == null)
                        {
                            _instance = new Model();
                        }
                    }
                }

                return _instance;
            }
        }

/*
 * НЕстатические элементы, реализация интерфейсов и управление моделью
 */

        /// <summary>
        /// Сообщение об ошибке
        /// </summary>
        /// <param name="message"></param>
        /// <param name="args"></param>
        public void ErrorLog(string message, params object[] args)
        {
            Log(LogsEnum.System, true, message, args);
        }

        /// <summary>
        /// Запись в системный лог
        /// </summary>
        /// <param name="message"></param>
        /// <param name="args"></param>
        public void SystemLog(string message, params object[] args)
        {
            Log(LogsEnum.System, false, message, args);
        }

        /// <summary>
        /// ЗАпись в лог обмена
        /// </summary>
        /// <param name="message"></param>
        /// <param name="args"></param>
        public void ExchangeLog(string message, params object[] args)
        {
            Log(LogsEnum.Exchange, false, message, args);
        }

        /// <summary>
        /// Запись в лог
        /// </summary>
        /// <param name="message"></param>
        /// <param name="type"></param>
        /// <param name="rise"></param>
        /// <param name="args"></param>
        public void Log(LogsEnum @type, bool rise, string message, params object[] args)
        {
            lock (Sync)
            {
                Common.Log.Write(@type, message, args);

                if (ErrorModelEvent != null)
                {
                    ErrorModelEvent(null, new StockEventArgs(string.Format(message, args)));
                }
            }
        }

        /// <summary>
        /// Событие возникновения ошибки в модели
        /// </summary>
        public event EventHandler<StockEventArgs> ErrorModelEvent;

        /// <summary>
        /// Обьекты окружения
        /// </summary>
        public List<StockEnvironment> Environments { get; private set; }

        /// <summary>
        /// Презентеры приложения
        /// </summary>
        public Presenters.Presenters Presenters { get; set; }

        /// <summary>
        /// Управление сообщениями
        /// </summary>
        public IMessager Messages { get; set; }

        /// <summary>
        /// Управдение диалогами
        /// </summary>
        public IDialoger Dialogs { get; set; }

        /// <summary>
        /// Управление ожиданием
        /// </summary>
        public IWait Wait { get; set; }

        /// <summary>
        /// Текущая сессия
        /// </summary>
        public Session Session { get; set; }

        public CommonStockEnvironment StockCommon
        {
            get { return Environments.FirstOrDefault(e => e is CommonStockEnvironment) as CommonStockEnvironment; }
        }

        public DatabaseStockEnvironment StockDatabase
        {
            get { return Environments.FirstOrDefault(e => e is DatabaseStockEnvironment) as DatabaseStockEnvironment; }
        }

        public RepositoryStockEnvironment StockRepository
        {
            get { return Environments.FirstOrDefault(e => e is RepositoryStockEnvironment) as RepositoryStockEnvironment; }
        }

        public TerminalsStockEnvironment StockTerminals
        {
            get { return Environments.FirstOrDefault(e => e is TerminalsStockEnvironment) as TerminalsStockEnvironment; }
        }

        public InvokerStockEnvironment Invoker
        {
            get { return Environments.FirstOrDefault(e => e is InvokerStockEnvironment) as InvokerStockEnvironment; }
        }

        public StartableEnum Status { get; private set; }

        public void Start()
        {
            if (Status == StartableEnum.Stopped)
            {
                if (Presenters == null)
                    throw  new Exception("Необходимо инициализировать коллекцию презентеров перед запуском приложения");

                if (Environments == null)
                {
                    Environments = new List<StockEnvironment>();
                }

                // добавление элементов окружения (важен порядок запуска)

                Environments.Add(new CommonStockEnvironment());
                Environments.Add(new InvokerStockEnvironment());
                Environments.Add(new TerminalsStockEnvironment());
                Environments.Add(new DatabaseStockEnvironment());
                Environments.Add(new RepositoryStockEnvironment());

                Session = new Session();

                if (Environments.Count > 0)
                {
                    foreach (var environment in Environments)
                    {
                        try
                        {
                            environment.Start();
                        }
                        catch (Exception ex)
                        {
                            ErrorLog("Ошибка запуска элемента окружения \"{0}\": {1}", environment.FriendlyName, ex.Message);
                        }
                    }
                }

                if (Presenters.Count > 0)
                {
                    foreach (var presenter in Presenters)
                    {
                        presenter.Subscription();
                    }
                }

                if (Environments.Count > 0 && !Environments.Any(e => !e.IsActive))
                {
                    Status = StartableEnum.Started;
                }
                else
                {
                    ErrorLog("Приложение не может продолжить свою дальнейшую работу т.к. один или несколько элементов окружения не быди запущены. Обратитель к специалистам.");
                }
            }
        }

        public void Stop()
        {
            if (Status == StartableEnum.Started)
            {
                Session.Clear();

                if (Presenters.Count > 0)
                {
                    foreach (var presenter in Presenters)
                    {
                        presenter.Unsubscription();
                    }
                }

                if (Environments != null && Environments.Count > 0)
                {
                    foreach (var environment in Environments)
                    {
                        try
                        {
                            if (environment.IsActive)
                                environment.Stop();
                        }
                        catch (Exception ex)
                        {
                            ErrorLog("Ошибка остановки элемента окружения \"{0}\": {1}", environment.FriendlyName,
                                     ex.Message);
                        }
                    }

                    Environments.Clear();
                }

                Environments = null;

                Status = StartableEnum.Stopped;
            }
        }

        public void Restart()
        {
            Stop();
            Start();
        }
    }
}