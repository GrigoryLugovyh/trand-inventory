﻿using System;
using System.Collections.Generic;
using Resco.UIElements;
using Stock.Views.Personals;
using StockCore.Entity;
using StockCore.Interfaces.Views;

namespace Stock.Views
{
    public partial class FindMolsControl : StockControl, IFindPersonalView<Mols>
    {
        public FindMolsControl()
        {
            InitializeComponent();
        }

        public event EventHandler Show;

        public event EventHandler Close;

        public event EventHandler Select;

        public event EventHandler SelectedChange;

        public List<Mols> Personals
        {
            get { return PersonalListView.DataSource as List<Mols>; }
            set { Invoker(() => PersonalListView.DataSource = value); }
        }

        public Mols SelectPersonal { get; protected set; }

        public void ShowView()
        {
            PersonalListView.DataTemplate = new NormalPersonalControl();
            PersonalListView.SelectedDataTemplate = new SelectPersonalControl();

            if (Show != null)
            {
                Show(this, EventArgs.Empty);
            }

            ShowView<FindMolsControl>(AnimationEffect.MoveLeft);
        }

        public void CloseView()
        {
            if (Close != null)
            {
                Close(this, EventArgs.Empty);
            }

            ShowView<ObjectParametersControl>(AnimationEffect.MoveRight);
        }

        private void SelectBtnClick(object sender, UIMouseEventArgs e)
        {
            if(Select != null)
            {
                Select(sender, e);
            }
        }

        private void CancelBtnClick(object sender, UIMouseEventArgs e)
        {
            CloseView();
        }

        private void PersonalListViewSelectedIndexChanging(object sender, SelectedIndexChangingEventArgs e)
        {
            if (Personals != null && Personals.Count > 0 && e.NewIndex > -1)
            {
                SelectPersonal = Personals[e.NewIndex];
            }
            else
            {
                SelectPersonal = null;
            }

            if (SelectedChange != null)
            {
                SelectedChange(sender, e);
            }
        }
    }
}