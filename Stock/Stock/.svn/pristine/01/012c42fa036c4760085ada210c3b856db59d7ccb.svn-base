using System;
using System.IO;
using StockCore.Common;
using StockCore.Handlers.Config;
using StockCore.Properties;

namespace StockCore.Environments
{
    public class ConfigStockEnvironment : StockEnvironment
    {
        public override string FriendlyName
        {
            get { return "Конфигурация"; }
        }

        public StockConfig CurrentConfig { get; protected set; }

        public Configurator<StockConfig> Configurator { get; protected set; }

        public string ConfigPath
        {
            get { return Path.Combine(Model.Instance.Common.CurrentDirectory, Resources.StockSettingsName); }
        }

        public void Save()
        {
            Configurator.Object = CurrentConfig;
            Configurator.Save();
        }

        protected override Action StartAction
        {
            get
            {
                return () =>
                           {
                               if (Configurator == null)
                               {
                                   Configurator = new Configurator<StockConfig> {ObjectPath = ConfigPath};
                               }

                               if (!File.Exists(Configurator.ObjectPath))
                               {
                                   Configurator.Object = StockConfig.Default;
                                   Configurator.Save();
                               }
                               else
                               {
                                   Configurator.Load();
                                   CurrentConfig = Configurator.Object;
                               }
                           };
            }
        }

        protected override Action StopAction
        {
            get
            {
                return () =>
                           {
                               Configurator = null;
                           };
            }
        }
    }
}