using System;
using System.Linq;
using StockCore.Entity;
using StockCore.Interfaces.Views;
using StockCore.Properties;

namespace StockCore.Presenters
{
    public class ObjectParametersPresenter : StockPresenter<IObjectParameters>
    {
        public ObjectParametersPresenter(IObjectParameters view)
            : base(view)
        {
        }

        protected void ViewShow(object sender, EventArgs e)
        {
            View.ObjectName = View.UserName = View.MolName = Resources.DefaultEntityName;
            View.MolFilter = View.UserFilter = string.Empty;

            if (Model.Instance.Session != null && Model.Instance.Session.CurrentObject != null)
            {
                View.ObjectName = Model.Instance.Session.CurrentObject.PobjectName;
            }
        }

        protected void ViewFindMol(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("����� ����", () =>
                {
                    if (!string.IsNullOrEmpty(View.MolFilter))
                    {
                        var filter = View.MolFilter;
                        var personals = Model.Instance.Data.Store.Load<Mols>();

                        if (personals != null && personals.Count > 0)
                        {
                            Model.Instance.Presenters.FindMols.Personals = personals.Where(
                                                        m => m.MolName.IndexOf(filter, StringComparison.OrdinalIgnoreCase) > -1 ||
                                                        m.MolNumber.IndexOf(filter, StringComparison.OrdinalIgnoreCase) > -1).OrderBy(m => m.MolName).ToList();

                            Model.Instance.Presenters.FindMols.ShowView();
                        }
                        else
                        {
                            Model.Instance.Messages.ShowExclamation("��� ����� ��������������� ������", "��");
                        }
                    }
                    else
                    {
                        Model.Instance.Messages.ShowExclamation("���������� ������� �������� ������ ����", "��");
                    }
                });
        }

        protected void ViewFindUser(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("����� ������������", () =>
            {
                if (!string.IsNullOrEmpty(View.UserFilter))
                {
                    var filter = View.UserFilter;
                    var personals = Model.Instance.Data.Store.Load<Users>();

                    if (personals != null && personals.Count > 0)
                    {
                        Model.Instance.Presenters.FindUsers.Personals = personals.Where(
                                                    u => u.UserName.IndexOf(filter, StringComparison.OrdinalIgnoreCase) > -1 ||
                                                    u.UserNumber.IndexOf(filter, StringComparison.OrdinalIgnoreCase) > -1).OrderBy(m => m.UserName).ToList();

                        Model.Instance.Presenters.FindUsers.ShowView();
                    }
                    else
                    {
                        Model.Instance.Messages.ShowExclamation("��� ������������� ��������������� ������", "��");
                    }
                }
                else
                {
                    Model.Instance.Messages.ShowExclamation("���������� ������� �������� ������ ������������", "��");
                }
            });
        }

        protected void ViewSelectUser(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("����� ������������", () =>
            {
                Model.Instance.Presenters.SelectUsers.Personals = Model.Instance.Data.Store.Load<Users>().OrderBy(u => u.UserName).ToList();
                Model.Instance.Presenters.SelectUsers.ShowView();
            });
        }

        protected void ViewSelectMol(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("����� ����", () =>
            {
                Model.Instance.Presenters.SelectMols.Personals = Model.Instance.Data.Store.Load<Mols>().OrderBy(m => m.MolName).ToList();
                Model.Instance.Presenters.SelectMols.ShowView();
            });
        }

        protected void ObjectParametersChanged(object sender, EventArgs e)
        {
            if (Model.Instance.Session != null)
            {
                if (Model.Instance.Session.CurrentMol != null)
                {
                    View.MolName = Model.Instance.Session.CurrentMol.MolName;
                    View.MolFilter = string.Empty;
                }

                if (Model.Instance.Session.CurrentUser != null)
                {
                    View.UserName = Model.Instance.Session.CurrentUser.UserName;
                    View.UserFilter = string.Empty;
                }
            }
        }

        public override void Subscription()
        {
            View.Show += ViewShow;
            View.FindMol += ViewFindMol;
            View.FindUser += ViewFindUser;
            View.SelectMol += ViewSelectMol;
            View.SelectUser += ViewSelectUser;

            if (Model.Instance.Session != null)
            {
                Model.Instance.Session.UserChanged += ObjectParametersChanged;
                Model.Instance.Session.MolChanged += ObjectParametersChanged;
            }
        }

        public override void Unsubscription()
        {
            View.Show -= ViewShow;
            View.FindMol -= ViewFindMol;
            View.FindUser -= ViewFindUser;
            View.SelectMol -= ViewSelectMol;
            View.SelectUser -= ViewSelectUser;

            if (Model.Instance.Session != null)
            {
                Model.Instance.Session.UserChanged -= ObjectParametersChanged;
                Model.Instance.Session.MolChanged -= ObjectParametersChanged;
            }
        }
    }
}