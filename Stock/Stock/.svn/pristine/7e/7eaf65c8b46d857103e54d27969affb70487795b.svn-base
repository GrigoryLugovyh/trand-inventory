﻿using System;
using Resco.UIElements;
using StockCore.Interfaces.Views;

namespace Stock.Views
{
    public partial class InformationControl : StockControl, IInformationView
    {
        public InformationControl()
        {
            InitializeComponent();
        }

        public event EventHandler Show;

        public event EventHandler Close;

        public event EventHandler BarcodeEnterHand;

        public event EventHandler ToInformation;

        public event EventHandler ToAll;

        public void ShowView()
        {
            if (Show != null)
            {
                Show(this, EventArgs.Empty);
            }

            ShowView<InformationControl>(AnimationEffect.MoveLeft);
        }

        public void CloseView()
        {
            if (Close != null)
            {
                Close(this, EventArgs.Empty);
            }

            ShowView<MainControl>(AnimationEffect.MoveRight);
        }

        private void CloseInfoBtnClick(object sender, UIMouseEventArgs e)
        {
            CloseView();
        }

        private void CloseAllBtnClick(object sender, UIMouseEventArgs e)
        {
            CloseView();
        }

        public string Status
        {
            get { return StatusBox.Text; }
            set { Invoker(() => StatusBox.Text = value); }
        }

        public string Barcode
        {
            get { return BarcodeText.Text; }
            set { Invoker(() => BarcodeText.Text = value); }
        }

        public string ItemName
        {
            get { return ItemNameText.Text; }
            set { Invoker(() => ItemNameText.Text = value); }
        }

        public string Ibso
        {
            get { return IbsoText.Text; }
            set { Invoker(() => IbsoText.Text = value); }
        }

        public string UserName
        {
            get { return UserNameText.Text; }
            set { Invoker(() => UserNameText.Text = value); }
        }

        public string MolName
        {
            get { return MolNameText.Text; }
            set { Invoker(() => MolNameText.Text = value); }
        }

        public string Serial
        {
            get { return StatusText.Text; }
            set { Invoker(() => StatusText.Text = value); }
        }

        private void InventoryPanelSelectedIndexChanging(object sender, SelectedIndexChangingEventArgs e)
        {
            switch (e.NewIndex)
            {
                case 0:
                    {
                        if (ToInformation != null)
                        {
                            ToInformation(sender, e);
                        }
                        break;
                    }
                case 1:
                    {
                        if (ToAll != null)
                        {
                            ToAll(sender, e);
                        }
                        break;
                    }
            }
        }

        private void EnterBarcodeBtnClick(object sender, UIMouseEventArgs e)
        {
            if(BarcodeEnterHand != null)
            {
                BarcodeEnterHand(sender, e);
            }
        }
    }
}