using System;
using System.Linq;
using StockCore.Entity;
using StockCore.Interfaces.Views;

namespace StockCore.Presenters
{
    public class ObjectsPresenter : StockPresenter<IObjectsView>
    {
        public ObjectsPresenter(IObjectsView view)
            : base(view)
        {
            NeedLoadObjects = true;
        }

        public void ViewBack(object sender, EventArgs e)
        {
            Model.Instance.Presenters.Main.ShowView();
        }

        /// <summary>
        /// ����, �� �������� ������� �������� ������ �� ����
        /// </summary>
        public bool NeedLoadObjects { get; set; }

        protected void ViewShow(object sender, EventArgs e)
        {
            Model.Instance.Invoker.DoIndependentThread("�������� ��������", () =>
            {
                //if ((View.Objects == null || View.Objects.Count == 0) || NeedLoadObjects)
                {
                    View.StatusText = "�������� �������� ... ";

                    var objects = Model.Instance.Data.Objects(NeedLoadObjects);
                    if (objects.Count > 0)
                    {
                        View.Objects = objects;
                        NeedLoadObjects = false;
                    }
                }
            },
            ()=>
                {
                    View.StatusText = "������������� �������� ��� ������� ��� �������";
                },
            true);

            StartTerminal();
        }

        public void ViewClose(object sender, EventArgs e)
        {
            StopTerminal();
        }

        protected override void TerminalBarcodeRead(object sender, Events.StockEventArgs e)
        {
            View.FilterEdit = e.Message;

            ViewFilter(sender, e);
        }

        protected void ViewSelectedChanging(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("�������� �������� ��������", () =>
                  {
                      var childs = Model.Instance.Data.Store.Table<Objects>().Where(o => o.PobjectPid == View.SelectedObject.PobjectId).ToList();
                      View.SelectedObject.Childs = childs;
                  });
        }

        protected void ViewSelect(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("����� �������", () =>
            {
                if(View.SelectedObject == null)
                {
                    Model.Instance.Messages.ShowExclamation("���������� ������� ������ ��������������", "��");
                    return;
                }

                StopTerminal();

                Model.Instance.Session.SetObject(View.SelectedObject);
                Model.Instance.Presenters.ObjectParameters.ShowView();
            });
        }

        /// <summary>
        /// ������
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ViewFilter(object sender, EventArgs e)
        {
            Model.Instance.Invoker.DoIndependentThread("������ ��������", () =>
            {
                if (string.IsNullOrEmpty(View.FilterEdit))
                {
                    ViewShow(sender, e);
                    return;
                }

                View.StatusText = "������ �������� ... ";

                var objects = Model.Instance.Data.Objects(false);

                View.Objects = objects.Where(
                    o => o.PobjectBarcode.IndexOf(View.FilterEdit, StringComparison.OrdinalIgnoreCase) > -1 ||
                         o.PobjectId.IndexOf(View.FilterEdit, StringComparison.OrdinalIgnoreCase) > -1 ||
                         o.PobjectName.IndexOf(View.FilterEdit, StringComparison.OrdinalIgnoreCase) > -1 ||
                         o.PobjectPid.IndexOf(View.FilterEdit, StringComparison.OrdinalIgnoreCase) > -1 ||
                         o.PobjectPname.IndexOf(View.FilterEdit, StringComparison.OrdinalIgnoreCase) > -1).OrderBy(
                             o => o.PobjectName).ToList();
            },
            () =>
            {
                View.StatusText = "������������� �������� ��� ������� ��� �������";
            },
            true);
        }

        /// <summary>
        /// ���������� �������
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ViewAdd(object sender, EventArgs e)
        {
            StopTerminal();
            Model.Instance.Presenters.AddObject.ParentObject = View.SelectedObject;
            Model.Instance.Presenters.AddObject.ShowView();
        }

        protected void StartTerminalView(object sender, EventArgs e)
        {
            if (Model.Instance.Presenters.AddObject.AddedObject != null)
            {
                View.FilterEdit = Model.Instance.Presenters.AddObject.AddedObject.PobjectBarcode;
                Model.Instance.Presenters.AddObject.AddedObject = null;
                NeedLoadObjects = true;
                ViewFilter(sender, e);
            }

            StartTerminal();
        }

        public override void Subscription()
        {
            View.Select += ViewSelect;
            View.Show += ViewShow;
            View.Close += ViewClose;
            View.SelectedChanging += ViewSelectedChanging;
            View.Filter += ViewFilter;
            View.Add += ViewAdd;

            Model.Instance.Presenters.AddObject.View.Close += StartTerminalView;
            Model.Instance.Presenters.Inventory.View.Close += StartTerminalView;
            Model.Instance.Presenters.ObjectParameters.View.Close += StartTerminalView;
        }

        public override void Unsubscription()
        {
            View.Select -= ViewSelect;
            View.Show -= ViewShow;
            View.Close -= ViewClose;
            View.SelectedChanging -= ViewSelectedChanging;
            View.Filter -= ViewFilter;
            View.Add -= ViewAdd;

            Model.Instance.Presenters.AddObject.View.Close -= StartTerminalView;
            Model.Instance.Presenters.Inventory.View.Close -= StartTerminalView;
            Model.Instance.Presenters.ObjectParameters.View.Close -= StartTerminalView;
        }
    }
}