﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Resco.UIElements.Controls;
using Stock.Common;
using Stock.Handlers;
using Stock.Views;
using StockCore;
using StockCore.Presenters;

namespace Stock
{
    /// <summary>
    /// Функция передачи панели опций
    /// </summary>
    /// <param name="options"></param>
    public delegate void SendStockViewEvent(ref UIElementPanelControl options);

    /// <summary>
    /// Выход из приложения 
    /// </summary>
    public delegate void CloseApplicationEvent();

    /// <summary>
    /// Показать клавиатуру
    /// </summary>
    public delegate void ShowKeyboard(object sender);

    /// <summary>
    /// Скрыть клавиатуру
    /// </summary>
    public delegate void CloseKeyboard();

    /// <summary>
    /// Обновление представлеия
    /// </summary>
    public delegate void ApplicationDoEvents();

    public partial class StockView : Form
    {
        public const int KeyboardHeight = 120;

        public static SendStockViewEvent SendStockViewHandler;

        public static CloseApplicationEvent CloseApplicationHandler;

        public static ApplicationDoEvents ApplicationDoEventsHandler;

        public static ShowKeyboard ShowKeyboardHandler;

        public static CloseKeyboard CloseKeyboardHandler;

        protected readonly FullScreen FullScreenHandler;

        public StockView()
        {
            InitializeComponent();

            FullScreenHandler = new FullScreen(this);

            Height = Screen.PrimaryScreen.Bounds.Height;

            Width = Screen.PrimaryScreen.Bounds.Width;

            SendStockViewHandler = SendTemplateView;

            CloseApplicationHandler = ApplicationClose;

            ApplicationDoEventsHandler = ApplicationDoEvent;

            ShowKeyboardHandler = ShowKeyboard;

            CloseKeyboardHandler = CloseKeyboard;

            Keyboard.Bounds = new Rectangle(0, Screen.PrimaryScreen.Bounds.Height - KeyboardHeight, Screen.PrimaryScreen.Bounds.Width, KeyboardHeight);
        }

        public void ShowKeyboard(object sender)
        {
            if (!Keyboard.Visible)
            {
                Keyboard.Visible = true;
            }
        }

        public void CloseKeyboard()
        {
            if (Keyboard.Visible)
            {
                Keyboard.Visible = false;
            }
        }

        public void SendTemplateView(ref UIElementPanelControl template)
        {
            template = Template;
        }

        public void ApplicationClose()
        {
            Invoke(new Action(Close));
        }

        public void ApplicationDoEvent()
        {
            Invoke(new Action(() =>
                                  {
                                      Application.DoEvents();
                                      Refresh();

                                  }));
        }

        protected override void OnLoad(EventArgs e)
        {
            if (FullScreenHandler != null)
            {
                FullScreenHandler.Start();
            }

            base.OnLoad(e);
        }

        protected override void OnClosed(EventArgs e)
        {
            if (FullScreenHandler != null)
            {
                FullScreenHandler.Stop();
            }

            base.OnClosed(e);
        }

        protected override void OnActivated(EventArgs e)
        {
            if (FullScreenHandler != null)
            {
                FullScreenHandler.Start();
            }

            base.OnActivated(e);
        }

        /// <summary>
        /// Запуск приложения 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StockViewLoad(object sender, EventArgs e)
        {
            // Добавляем сплэш
            ControlHandler.Instance.Views.Add(typeof(SplashControl), new SplashControl());

            // Инициализируем внешние обработчики
            Model.Instance.Messages = new MessageDistributor();
            Model.Instance.Dialogs = new DialogDistributor();
            Model.Instance.Wait = new WaitDistributor();

            Model.Instance.Presenters = new Presenters(
                new SplashPresenter(ControlHandler.Instance.CreateView<SplashControl>(Template)));

            // Показываем экран загрузки
            Model.Instance.Presenters.Splash.ShowView();

            // Инициализируем остальные контролы
            ControlHandler.Instance.Views.Add(typeof (MainControl), new MainControl());
            ControlHandler.Instance.Views.Add(typeof (ObjectsControl), new ObjectsControl());
            ControlHandler.Instance.Views.Add(typeof (ExchangeControl), new ExchangeControl());
            ControlHandler.Instance.Views.Add(typeof (ExchangeProcessControl), new ExchangeProcessControl());
            ControlHandler.Instance.Views.Add(typeof (ObjectParametersControl), new ObjectParametersControl());
            ControlHandler.Instance.Views.Add(typeof (FindUsersControl), new FindUsersControl());
            ControlHandler.Instance.Views.Add(typeof (FindMolsControl), new FindUsersControl());
            ControlHandler.Instance.Views.Add(typeof(UsersControl), new UsersControl());
            ControlHandler.Instance.Views.Add(typeof(MolsControl), new MolsControl());
            ControlHandler.Instance.Views.Add(typeof (InventoryControl), new InventoryControl());
            ControlHandler.Instance.Views.Add(typeof(EnterBarcodePresenter), new EnterBarcodeControl());
            ControlHandler.Instance.Views.Add(typeof(SeleectItemStatusPresenter), new SeleectItemStatusControl());
            ControlHandler.Instance.Views.Add(typeof(AddItemPresenter), new AddItemControl());
            ControlHandler.Instance.Views.Add(typeof(SerialNumberInputPresenter), new SerialNumberInputControl());
            ControlHandler.Instance.Views.Add(typeof(AddObjectPresenter), new AddObjectControl());

            // Инициализируем презентеры данных
            Model.Instance.Presenters.InitPresenters(
                new MainPresenter(ControlHandler.Instance.CreateView<MainControl>(Template)),
                new ObjectsPresenter(ControlHandler.Instance.CreateView<ObjectsControl>(Template)),
                new ExchangePresenter(ControlHandler.Instance.CreateView<ExchangeControl>(Template)),
                new ExchangeProcessPresenter(ControlHandler.Instance.CreateView<ExchangeProcessControl>(Template)),
                new ObjectParametersPresenter(ControlHandler.Instance.CreateView<ObjectParametersControl>(Template)),
                new FindUsersPresenter(ControlHandler.Instance.CreateView<FindUsersControl>(Template)),
                new FindMolsPresenter(ControlHandler.Instance.CreateView<FindMolsControl>(Template)),
                new SelectUserPresenter(ControlHandler.Instance.CreateView<UsersControl>(Template)),
                new SelectMolPresenter(ControlHandler.Instance.CreateView<MolsControl>(Template)),
                new InventoryPresenter(ControlHandler.Instance.CreateView<InventoryControl>(Template)),
                new EnterBarcodePresenter(ControlHandler.Instance.CreateView<EnterBarcodeControl>(Template)),
                new SeleectItemStatusPresenter(ControlHandler.Instance.CreateView<SeleectItemStatusControl>(Template)),
                new AddItemPresenter(ControlHandler.Instance.CreateView<AddItemControl>(Template)),
                new SerialNumberInputPresenter(ControlHandler.Instance.CreateView<SerialNumberInputControl>(Template)),
                new AddObjectPresenter(ControlHandler.Instance.CreateView<AddObjectControl>(Template)));

            // Запускаем модель
            Model.Instance.Start();

            // Начинаем работу
            Model.Instance.Presenters.Main.ShowView();
        }
    }
}