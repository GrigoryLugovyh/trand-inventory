using System.Collections.Generic;
using StockCore.Interfaces.Presenters;

namespace StockCore.Presenters
{
    public class Presenters : List<ISubscribePresenter>
    {
        public Presenters(
            SplashPresenter splashPresenter,
            MainPresenter mainPresenter, 
            ObjectsPresenter objectsPresenter,
            ExchangePresenter exchangePresenter,
            ExchangeProcessPresenter exchangeProcessPresenter,
            ObjectParametersPresenter objectParametersPresenter,
            FindUsersPresenter findUsersPresenter,
            FindMolsPresenter findMolsPresenter,
            SelectUserPresenter selectUserPresenter,
            SelectMolPresenter selectMolPresenter,
            InventoryPresenter inventoryPresenter,
            EnterBarcodePresenter enterBarcodePresenter)
        {
            Splash = splashPresenter;
            Main = mainPresenter;
            Objects = objectsPresenter;
            Exchange = exchangePresenter;
            ExchangeProcess = exchangeProcessPresenter;
            ObjectParameters = objectParametersPresenter;
            FindUsers = findUsersPresenter;
            FindMols = findMolsPresenter;
            SelectUsers = selectUserPresenter;
            SelectMols = selectMolPresenter;
            Inventory = inventoryPresenter;
            EnterBarcode = enterBarcodePresenter;

            Add(Splash);
            Add(Main);
            Add(Objects);
            Add(Exchange);
            Add(ExchangeProcess);
            Add(ObjectParameters);
            Add(FindUsers);
            Add(FindMols);
            Add(SelectUsers);
            Add(SelectMols);
            Add(Inventory);
            Add(EnterBarcode);
        }

        public SplashPresenter Splash { get; protected set; }

        public MainPresenter Main { get; protected set; }

        public ObjectsPresenter Objects { get; protected set; }

        public ExchangePresenter Exchange { get; protected set; }

        public ExchangeProcessPresenter ExchangeProcess { get; protected set; }

        public ObjectParametersPresenter ObjectParameters { get; protected set; }

        public FindUsersPresenter FindUsers { get; protected set; }

        public FindMolsPresenter FindMols { get; protected set; }

        public SelectUserPresenter SelectUsers { get; protected set; }

        public SelectMolPresenter SelectMols { get; protected set; }

        public InventoryPresenter Inventory { get; protected set; }

        public EnterBarcodePresenter EnterBarcode { get; protected set; }
    }
}