﻿
namespace StockCore.Handlers.Exchange.Imports
{
    /// <summary>
    /// Фабрика импорта Csv
    /// </summary>
    public sealed class CsvImportFactory
    {
        private static readonly object Sync = new object();

        private static volatile CsvImportFactory _instance;

        /// <summary>
        /// Инициализация
        /// </summary>
        public static CsvImportFactory Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (Sync)
                    {
                        if (_instance == null)
                        {
                            _instance = new CsvImportFactory();
                        }
                    }
                }

                return _instance;
            }
        }

        public CsvFullImportProcessor GetProcessor(CsvImport handler)
        {
            CsvFullImportProcessor processor;

            if (handler.IsFullImport)
            {
                processor = new CsvFullImportProcessor(handler);
            }
            else
            {
                processor = new CsvFullParthImportProcessor(handler);
            }

            return processor;
        }
    }
}