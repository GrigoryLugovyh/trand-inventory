using System;
using System.Collections.Generic;
using System.Linq;
using StockCore.Entity;
using StockCore.Enums;
using StockCore.Factories;
using StockCore.Interfaces.Views;
using StockCore.Properties;

namespace StockCore.Presenters
{
    public class InventoryPresenter : StockPresenter<IInventoryView>
    {
        public InventoryPresenter(IInventoryView view)
            : base(view)
        {
        }

        protected Objects @Object
        {
            get { return Model.Instance.Session.CurrentObject; }
        }

        protected Mols @Mol
        {
            get { return Model.Instance.Session.CurrentMol; }
        }

        protected Users @User
        {
            get { return Model.Instance.Session.CurrentUser; }
        }

        protected Items @Item
        {
            get { return Model.Instance.Session.CurrentItem; }
        }

        protected void ViewShow(object sender, EventArgs e)
        {
            ClearView();
            SessionItemChanged(sender, e);
            View.StatusText = Model.Instance.Session.CurrentObject.PobjectName;
        }

        protected void ViewToInventory(object sender, EventArgs e)
        {

        }

        protected void ViewToFind(object sender, EventArgs e)
        {
            Model.Instance.Invoker.DoIndependentThread("�������� �������� ���", () =>
            {
                View.StatusText = "���������� ��������� ��� ... ";
                var items = Model.Instance.Data.Store.Table<Items>().Where(i => i.FobjectBarcode == @Object.PobjectBarcode && i.Found == 1).ToList() ?? new List<Items>();
                items.AddRange(Model.Instance.Data.Store.Table<Doubles>().Where(d => d.FobjectBarcode == @Object.PobjectBarcode && d.Found == 1).ToList().ToItems());
                View.FindItems = null;
                View.FindItems = items;
            },
            () =>
            {
                View.StatusText = @Object.PobjectName;
            },
            true);
        }

        protected void ViewToNotFind(object sender, EventArgs e)
        {
            Model.Instance.Invoker.DoIndependentThread("�������� ���������� ���", () =>
            {
                View.StatusText = "���������� ����������� ��� ... ";
                var items = Model.Instance.Data.Store.Table<Items>().Where(i => i.PobjectBarcode == @Object.PobjectBarcode && i.Found == 0).ToList();
                View.NotFindItems = null;
                View.NotFindItems = items;
            },
            () =>
            {
                View.StatusText = @Object.PobjectName;
            },
            true);
        }

        /// <summary>
        /// ��������� ������ ��������� ��
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SessionItemBarcodeChanged(object sender, EventArgs e)
        {
            Model.Instance.Invoker.DoIndependentThread("������ ��������� ��",
                          () =>
                              {
                                  View.Barcode = Model.Instance.Session.CurrentItemBarcode;
                                  EnterBarcode();

                              }, null, false);
        }

        /// <summary>
        /// �������� ����� �������������, ���� �����-����
        /// </summary>
        public virtual void EnterBarcode()
        {
            Model.Instance.Invoker.Do("��������� ���������� ��", () =>
            {
                if(string.IsNullOrEmpty(View.Barcode))
                {
                    return;
                }

                SearchBarcodeEnum mode;

                if(!BarcodeFactory.Instance.Check(View.Barcode))
                {
                    if(!Model.Instance.Config.CurrentConfig.SearchInSerialNumbers)
                    {
                        Model.Instance.Messages.ShowExclamation("�����-��� �� ������������� ������� �����", "Ok");
                        return;
                    }

                    mode = SearchBarcodeEnum.Code;
                    Model.Instance.Session.SetItem(Model.Instance.Data.Store.Table<Items>().Where(i=>i.SerialNumber == View.Barcode).Take(1).FirstOrDefault());
                }
                else
                {
                    mode = SearchBarcodeEnum.Barcode;
                    Model.Instance.Session.SetItem(Model.Instance.Data.Store.Table<Items>().Where(i => i.Id == View.Barcode).Take(1).FirstOrDefault());
                }

                if(@Item != null)
                {
                    View.MolEnterEnable = true;
                    View.UserEnterEnable = true;
                    View.SerialEnterEnable = true;

                    // ���� ����� ���
                    if (@Item.FobjectBarcode != null && !string.IsNullOrEmpty(@Item.FobjectBarcode))
                    {
                        if (@Item.FobjectBarcode != @Item.PobjectBarcode)
                        {
                            if (Model.Instance.Messages.ShowQuestion(
                                string.Format("{0}\r\n\"{1}\"\r\n��� ���� �\r\n\"{2}\"\r\n�������� �����?", @Item.Id, @Item.Name, @Item.FobjectName),
                                DialogEnum.No, "��", "���") == DialogEnum.Yes)
                            {
                                // �����
                                Model.Instance.Data.MakeDouble(@Item, @Object);
                                Model.Instance.Data.MakeObjectFound(@Object);

                                // ��������� ���� � ������������
                                UpdateItemPersons(@Item);
                            }
                            else
                            {
                                ClearView();
                                return;
                            }
                        }
                        else
                        {
                            // ��� ���� � ���� �������, ������ ������
                            Model.Instance.Presenters.SelectItemStatus.ShowView();
                        }

                        UpdateItemPersons(@Item);
                    }
                    else
                    {
                        // �����
                        Model.Instance.Data.MakeItemFound(@Item, @Object);
                        Model.Instance.Data.MakeObjectFound(@Object);
                        UpdateItemPersons(@Item);
                    }

                    // ������� ���� �� ���
                    FillView();
                }
                else
                {
                    // ��� ���
                    ClearView();

                    if(mode == SearchBarcodeEnum.Barcode)
                    {
                        // ����� �������� �����
                        if (Model.Instance.Messages.ShowQuestion("��� �� ������.\r\n�������� � ����?", DialogEnum.No, "��", "���") == DialogEnum.Yes)
                        {

                        }
                    }
                    else
                    {
                        Model.Instance.Messages.ShowExclamation("�� ������� ����� ���", "Ok");
                    }
                }

            }, false, true);
        }

        /// <summary>
        /// ���������� ���� � ������������
        /// </summary>
        protected void UpdateItemPersons(Items item)
        {
            Model.Instance.Invoker.Do("���������� ���� � ������������", 
                ()=>
                    {
                        if(@Mol != null)
                        {
                            Model.Instance.Data.UpdateMol(item, @Mol);
                        }

                        if(@User != null)
                        {
                            Model.Instance.Data.UpdateUser(item, @User);
                        }

                    });
        }

        protected void FillView()
        {
            if(@Item != null)
            {
                View.Statuses = Model.Instance.Data.Statuses;
                View.SelectedStatus = Model.Instance.Data.Statuses.FirstOrDefault(s => s.Id == int.Parse(@Item.Status));

                View.ItemName = @Item.Name;
                View.Ibso = @Item.PobjectName;
                View.MolName = @Item.MolName;
                View.UserName = @Item.UserName;
                View.Serial = @Item.SerialNumber;
            }
        }

        protected void ClearView()
        {
            View.Statuses = null;
            View.SelectedStatus = null;
            View.Barcode = View.Ibso = View.ItemName = View.MolName = View.UserName = View.Serial = string.Empty;
            View.UserEnterEnable = View.MolEnterEnable = View.SerialEnterEnable = false;
        }

        protected void ViewBarcodeEnterHand(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("���� ��", () => Model.Instance.Presenters.EnterBarcode.ShowView(), true, true);
        }

        protected void SessionItemChanged(object sender, EventArgs e)
        {
            Model.Instance.Invoker.DoIndependentThread("���������� ���������� ����� ��������",
               () =>
                   {
                       View.StatusText = "���������� ���������� ... ";

                       int all;
                       int found;

                       Model.Instance.Data.ObjectStatistics(@Object, out all, out found);

                       View.Header = string.Format(Resources.InventoryStatus, all, found);
                   },
               () =>
                   {
                       View.StatusText = @Object.PobjectName;

                   }, false);
        }

        public override void Subscription()
        {
            View.Show += ViewShow;
            View.ToInventory += ViewToInventory;
            View.ToFind += ViewToFind;
            View.ToNotFind += ViewToNotFind;
            View.BarcodeEnter += EnterBarcode;
            View.BarcodeEnterHand += ViewBarcodeEnterHand;

            if(Model.Instance.Session != null)
            {
                Model.Instance.Session.ItemBarcodeChanged += SessionItemBarcodeChanged;
                Model.Instance.Session.ItemChanged += SessionItemChanged;
            }
        }

        public override void Unsubscription()
        {
            View.Show -= ViewShow;
            View.ToInventory -= ViewToInventory;
            View.ToFind -= ViewToFind;
            View.ToNotFind -= ViewToNotFind;
            View.BarcodeEnter -= EnterBarcode;
            View.BarcodeEnterHand -= ViewBarcodeEnterHand;

            if (Model.Instance.Session != null)
            {
                Model.Instance.Session.ItemBarcodeChanged -= SessionItemBarcodeChanged;
                Model.Instance.Session.ItemChanged -= SessionItemChanged;
            }
        }
    }
}