using System;
using System.Drawing;
using System.Windows.Forms;
using StockCore.Interfaces.Common;

namespace Stock.Common
{
    public class ExchangeDistributor : IExchange
    {
        protected ExchangeView View;

        protected void DoEvents()
        {
            if (StockView.ApplicationDoEventsHandler != null)
            {
                StockView.ApplicationDoEventsHandler();
            }

            Application.DoEvents();
        }

        public void ShowView()
        {
            if (View == null)
            {
                View = new ExchangeView();
                View.Abort += AbortBtnClick;
            }

            Invoker(() =>
            {
                var screen = Screen.PrimaryScreen.Bounds;
                View.Location = new Point((screen.Width / 2) - (View.Width / 2), (screen.Height / 2) - (View.Height / 2));
                View.ShowDialog();
                DoEvents();
            });
        }

        public void CloseView()
        {
            Invoker(() =>
            {
                View.Abort -= AbortBtnClick;
                View.Close();
                View = null;
                DoEvents();
            });
        }

        public string Header
        {
            get { return View.ExchangeHeaderText.Text; }
            set { Invoker(() => View.ExchangeHeaderText.Text = value); }
        }

        public string Received
        {
            get { return View.ExchangeReceivedText.Text; }
            set { Invoker(() => View.ExchangeReceivedText.Text = value); }
        }

        public string Prepared
        {
            get { return View.ExchangePreparedText.Text; }
            set { Invoker(() => View.ExchangePreparedText.Text = value); }
        }

        public string Processed
        {
            get { return View.ExchangeProcessedText.Text; }
            set { Invoker(() => View.ExchangeProcessedText.Text = value); }
        }

        public string NotProcessed
        {
            get { return View.ExchangeNotProcessedText.Text; }
            set { Invoker(() => View.ExchangeNotProcessedText.Text = value); }
        }

        public string ExchangeTime
        {
            get { return View.ExchangeProcessTimeText.Text; }
            set { Invoker(() => View.ExchangeProcessTimeText.Text = value); }
        }

        public string ProcessText
        {
            get { return View.ExchangeProcessText.Text; }
            set { Invoker(() => View.ExchangeProcessText.Text = value); }
        }

        public int Progress
        {
            get { return View.ExchangeProgress.Value; }
            set { Invoker(() => View.ExchangeProgress.Value = value); }
        }

        public event EventHandler Abort;

        private void AbortBtnClick(object sender, EventArgs e)
        {
            if (Abort != null)
            {
                Abort(sender, e);
            }
        }

        protected void Invoker(Action action)
        {
            if (View != null)
            {
                View.Invoke(new Action(action.Invoke));
            }
        }
    }
}