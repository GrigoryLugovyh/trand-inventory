﻿using System;
using Resco.UIElements;
using StockCore.Interfaces.Views;

namespace Stock.Views
{
    public partial class ObjectParametersControl : StockControl, IObjectParameters
    {
        public ObjectParametersControl()
        {
            InitializeComponent();
        }

        public event EventHandler Show;

        public event EventHandler Close;

        public event EventHandler FindMol;

        public event EventHandler SelectMol;

        public event EventHandler FindUser;

        public event EventHandler SelectUser;

        public event EventHandler Next;

        public string ObjectName
        {
            get { return ObjectNameText.Text; }
            set { Invoker(() => ObjectNameText.Text = value); }
        }

        public string MolName
        {
            get { return MolNameText.Text; }
            set { Invoker(() => MolNameText.Text = value); }
        }

        public string MolFilter
        {
            get { return FilterMolText.Text; }
            set { Invoker(() => FilterMolText.Text = value); }
        }

        public string UserName
        {
            get { return UserNameText.Text; }
            set { Invoker(() => UserNameText.Text = value); }
        }

        public string UserFilter
        {
            get { return FilterUserText.Text; }
            set { Invoker(() => FilterUserText.Text = value); }
        }

        public void ShowView()
        {
            if (Show != null)
            {
                Show(this, EventArgs.Empty);
            }

            ShowView<ObjectParametersControl>(AnimationEffect.MoveLeft);
        }

        public void CloseView()
        {
            if (Close != null)
            {
                Close(this, EventArgs.Empty);
            }

            ShowView<ObjectsControl>(AnimationEffect.MoveRight);
        }

        private void FindMolBtnClick(object sender, UIMouseEventArgs e)
        {
            if(FindMol != null)
            {
                FindMol(sender, e);
            }
        }

        private void SelectMolBtnClick(object sender, UIMouseEventArgs e)
        {
            if(SelectMol != null)
            {
                SelectMol(sender, e);
            }
        }

        private void FindUserBtnClick(object sender, UIMouseEventArgs e)
        {
            if(FindUser != null)
            {
                FindUser(sender, e);
            }
        }

        private void SelectUserBtnClick(object sender, UIMouseEventArgs e)
        {
            if(SelectUser != null)
            {
                SelectUser(sender, e);
            }
        }

        private void OkBtnClick(object sender, UIMouseEventArgs e)
        {
            if(Next != null)
            {
                Next(sender, e);
            }
        }

        private void CancelBtnClick(object sender, UIMouseEventArgs e)
        {
            CloseView();
        }
    }
}