﻿using System;
using System.Globalization;
using SQLite;
using StockCore.Factories;
using SQLiteCommand = System.Data.SQLite.SQLiteCommand;

namespace StockCore.Handlers.Exchange.Imports.Commands
{
    /// <summary>
    /// Подготовщик команд для импорта
    /// </summary>
    public abstract class ImportCommandProcessor<T>
    {
        protected ImportCommandProcessor(CsvImport handler)
        {
            Handler = handler;
        }

        /// <summary>
        /// Основной обработчик импорта данных
        /// </summary>
        protected CsvImport Handler { get; set; }

        /// <summary>
        /// Тип команды ипорта
        /// </summary>
        protected abstract ImportCommandsEnum ImportCommandType { get; }

        /// <summary>
        /// Команда на вставку
        /// </summary>
        /// <returns></returns>
        protected virtual string InsertCommandBuilder()
        {
            return Model.Instance.Data.Store.GetInsertCommandExt(typeof (T));
        }

        /// <summary>
        /// Параметры таблицы
        /// </summary>
        /// <returns></returns>
        protected virtual TableMapping.Column[] CommandParameters()
        {
            return Model.Instance.Data.Store.Paramenters(typeof (T));
        }

        /// <summary>
        /// Экземпляр команды
        /// </summary>
        protected SQLiteCommand CommandInstance;

        /// <summary>
        /// Проверяет параметр на наличие значения
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="default"></param>
        public virtual void CheckValue(string parameter, object @default)
        {
            if (Command.Parameters[parameter].Value == null || string.IsNullOrEmpty(Command.Parameters[parameter].Value.ToString()))
            {
                Command.Parameters[parameter].Value = @default;
            }
        }

        /// <summary>
        /// Команда для импорта
        /// </summary>
        /// <returns></returns>
        public virtual SQLiteCommand Command
        {
            get
            {
                if (CommandInstance == null)
                {
                    CommandInstance = new SQLiteCommand(Handler.ExchangeConnection)
                                          {
                                              CommandText = InsertCommandBuilder(),
                                              Transaction = Handler.ExchangeTransaction
                                          };
                }

                return CommandInstance;
            }
        }

        /// <summary>
        /// Присвоение параметрам команды значений
        /// </summary>
        public virtual void BindParameters()
        {
            if (CommandInstance != null)
            {
                CommandInstance = null;
            }

            var columns = CommandParameters();

            foreach (var column in columns)
            {
                if (!column.IsAutoInc)
                {
                    Command.Parameters.Add(column.Parameter, DbTypeFactory.Instance.GetType(column.ColumnType));
                }
            }

            Command.Prepare();
        }

        /// <summary>
        /// Заполяем параметры команды
        /// </summary>
        public virtual void FillParameters()
        {
            int count = 0;
            foreach (var element in Handler.InputData.Elements)
            {
                if (Command.Parameters.Contains(element.CvsParameter) && Handler.ImportProcessor.CurrentRecord[count] != null)
                {
                    object value;
                    string @object = Handler.ImportProcessor.CurrentRecord[count].TrimStart().TrimEnd();

                    if (element.CvsType == typeof(DateTime))
                    {
                        value = DateTime.ParseExact(@object, element.CvsElementFormat, CultureInfo.CurrentCulture);
                    }
                    else
                    {
                        value = Convert.ChangeType(@object, element.CvsType, CultureInfo.CurrentCulture);
                    }

                    Command.Parameters[element.CvsParameter].Value = value;
                }

                count++;
            }
        }
    }
}