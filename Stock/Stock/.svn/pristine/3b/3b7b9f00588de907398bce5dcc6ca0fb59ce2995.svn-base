﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using SQLite;
using StockCore.Entity;
using StockCore.Enums;
using StockCore.Properties;

namespace StockCore.Environments
{
    /// <summary>
    /// Окружение работы с базой данных
    /// </summary>
    public class DatabaseStockEnvironment : StockEnvironment
    {
        public override string FriendlyName
        {
            get { return "Обработчик базы данных"; }
        }

        public string DatabasePath
        {
            get { return Path.Combine(Model.Instance.Common.StorageCardDirectory, Resources.DatabaseName); }
        }

        /// <summary>
        /// Хранилище данных
        /// </summary>
        public SQLiteConnection Store { get; protected set; }

        private List<Statuses> _statuses;
        /// <summary>
        /// Текущие статусы
        /// </summary>
        public List<Statuses> Statuses
        {
            get
            {
                if (_statuses == null)
                {
                    _statuses = Store.Load<Statuses>();
                }

                return _statuses;
            }
        }

        private List<Codes> _codes;
        /// <summary>
        /// Текущие коды
        /// </summary>
        public List<Codes> Codes
        {
            get
            {
                if (_codes == null)
                {
                    _codes = Store.Load<Codes>().OrderBy(c => c.Code).ToList();
                }

                return _codes;
            }
        }

        /// <summary>
        /// Получение статистики по проекту
        /// </summary>
        public void Statistics(out int itemsAll, out int objectsAll, out int itemsProcessed, out int objectsProcessed)
        {
            itemsAll = 0;
            objectsAll = 0;
            itemsProcessed = 0;
            objectsProcessed = 0;

            if(IsActive)
            {
                var command = new SQLiteCommand(Store);

                command.CommandText = "SELECT COUNT(*) FROM [Items]";
                itemsAll = command.ExecuteScalar<int>();

                command.CommandText = "SELECT COUNT(*) FROM [Objects]";
                objectsAll = command.ExecuteScalar<int>();

                command.CommandText = "SELECT COUNT([Id]) FROM [Items] WHERE [Found] > 0";
                itemsProcessed = command.ExecuteScalar<int>();

                command.CommandText = "SELECT COUNT([Found]) FROM [Objects] WHERE [Found] = 1";
                objectsProcessed = command.ExecuteScalar<int>();
            }
        }

        public void ObjectStatistics(Objects @object, out int all, out int found)
        {
            all = 0;
            found = 0;

            if(IsActive)
            {
                var command = new SQLiteCommand(Store);

                command.CommandText = string.Format("SELECT COUNT(*) FROM [Items] WHERE [PobjectBarcode] = '{0}'", @object.PobjectBarcode);
                all = command.ExecuteScalar<int>();

                command.CommandText = string.Format("SELECT COUNT(*) FROM [Items] WHERE FobjectBarcode = '{0}'", @object.PobjectBarcode);
                found = command.ExecuteScalar<int>();

                command.CommandText = string.Format("SELECT COUNT(Id) FROM [Doubles] WHERE FobjectBarcode = '{0}'", @object.PobjectBarcode);
                found += command.ExecuteScalar<int>();
            }
        }

        /// <summary>
        /// Создание дубля
        /// </summary>
        /// <param name="item"></param>
        /// <param name="object"></param>
        public bool MakeDouble(Items item, Objects @object)
        {
            item.Status = ((int) StatusesEnum.Double).ToString();

            if (!InsertDouble(item))
            {
                throw new Exception("Не удалось создать дубль в текущем объекте");
            }

            item.FobjectId = @object.PobjectId;
            item.FobjectBarcode = @object.PobjectBarcode;
            item.FobjectName = @object.PobjectName;

            if (!InsertDouble(item))
            {
                throw new Exception("Не удалось создать дубль в новом объекте");
            }

            item.Found = 1;
            var r = Store.Update(item) > 0;

            if (r)
                Model.Instance.Session.SetItem(item);

            return r;
        }

        /// <summary>
        /// Добавление новой ТМЦ
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool InsertItem(Items item)
        {
            var r = Store.Insert(item) > 0;

            if(r)
                Model.Instance.Session.SetItem(item);

            return r;
        }

        /// <summary>
        /// Обновление ТМЦ
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool UpdateItem(Items item)
        {
            var r = Store.Update(item) > 0;

            if(r)
                Model.Instance.Session.SetItem(item);

            return r;
        }

        /// <summary>
        /// Вставляем дубль ТМЦ
        /// </summary>
        /// <param name="item"></param>
        public bool InsertDouble(Items item)
        {
            var count = Store.Table<Doubles>().Where(d => d.Id == item.Id && d.FobjectBarcode == item.FobjectBarcode).Count();

            if (count != 0)
                return true;

            return Store.Insert(new Doubles
                                    {
                                        Id = item.Id,
                                        Name = item.Name,
                                        PobjectId = item.PobjectId,
                                        PobjectBarcode = item.PobjectBarcode,
                                        PobjectName = item.PobjectName,
                                        FobjectId = item.FobjectId,
                                        FobjectBarcode = item.FobjectBarcode,
                                        FobjectName = item.FobjectName,
                                        UserName = item.UserName,
                                        UserNumber = item.UserNumber,
                                        MolName = item.MolName,
                                        MolNumber = item.MolNumber,
                                        Status = item.Status,
                                        Found = item.Found,
                                        BeginDate = item.BeginDate,
                                        ExtItemId = item.ExtItemId
                                    }) > 0;
        }

        /// <summary>
        /// Помечаем объект найденым
        /// </summary>
        /// <param name="object"></param>
        /// <returns></returns>
        public bool MakeObjectFound(Objects @object)
        {
            if (@object.Found == 1)
                return true;

            @object.Found = 1;
            return Store.Update(@object) > 0;
        }

        /// <summary>
        /// Обновление мола у ТМЦ
        /// </summary>
        /// <param name="item"></param>
        /// <param name="mol"></param>
        /// <returns></returns>
        public bool UpdateMol(Items item, Mols mol)
        {
            if (item.MolNumber != mol.MolNumber)
            {
                item.MolName = mol.MolName;
                item.MolNumber = mol.MolNumber;

                var ri = Store.Update(item);

                bool rd = true;
                var doubles = Store.Table<Doubles>().Where(d => d.Id == item.Id).ToList();
                if (doubles != null && doubles.Count > 0)
                {
                    foreach (var @double in doubles)
                    {
                        @double.MolName = mol.MolName;
                        @double.MolNumber = mol.MolNumber;

                        rd &= Store.Update(@double) > 0;
                    }
                }

                Model.Instance.Session.SetItem(item);
                return ri > 0 && rd;
            }

            return true;
        }

        /// <summary>
        /// Обновление пользователя у ТМЦ
        /// </summary>
        /// <param name="item"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool UpdateUser(Items item, Users user)
        {
            if (item.MolNumber != user.UserNumber)
            {
                item.UserName = user.UserName;
                item.UserNumber = user.UserNumber;

                var ri = Store.Update(item);

                bool rd = true;
                var doubles = Store.Table<Doubles>().Where(d => d.Id == item.Id).ToList();
                if (doubles != null && doubles.Count > 0)
                {
                    foreach (var @double in doubles)
                    {
                        @double.UserName = user.UserName;
                        @double.UserNumber = user.UserNumber;

                        rd &= Store.Update(@double) > 0;
                    }
                }

                Model.Instance.Session.SetItem(item);
                return ri > 0 && rd;
            }

            return true;
        }

        /// <summary>
        /// Обновление персонала ТМЦ
        /// </summary>
        /// <param name="item"></param>
        /// <param name="mol"></param>
        /// <param name="user"></param>
        public bool UpdatePersonals(Items item, Mols mol, Users user)
        {
            bool needMolUpdate = false;
            bool needUserUpdate = false;

            bool md = true;
            if (mol != null && item.MolNumber != mol.MolNumber)
            {
                needMolUpdate = true;

                item.MolName = mol.MolName;
                item.MolNumber = mol.MolNumber;

                var doubles = Store.Table<Doubles>().Where(d => d.Id == item.Id).ToList();
                if (doubles != null && doubles.Count > 0)
                {
                    foreach (var @double in doubles)
                    {
                        @double.MolName = mol.MolName;
                        @double.MolNumber = mol.MolNumber;

                        md &= Store.Update(@double) > 0;
                    }
                }
            }

            bool ud = true;
            if (user != null && item.UserName != user.UserNumber)
            {
                needUserUpdate = true;

                item.UserName = user.UserName;
                item.UserNumber = user.UserNumber;

                var doubles = Store.Table<Doubles>().Where(d => d.Id == item.Id).ToList();
                if (doubles != null && doubles.Count > 0)
                {
                    foreach (var @double in doubles)
                    {
                        @double.UserName = user.UserName;
                        @double.UserNumber = user.UserNumber;

                        ud &= Store.Update(@double) > 0;
                    }
                }
            }

            if (needMolUpdate || needUserUpdate)
            {
                var r = Store.Update(item) > 0;

                if (r)
                    Model.Instance.Session.SetItem(item);

                return r && md && ud;
            }

            return true;
        }

        /// <summary>
        /// Нашли ТМЦ в объекте
        /// </summary>
        /// <param name="item"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        public bool MakeItemFound(Items item, Objects @object)
        {
            if (item.Found == 1)
                return true;

            item.FobjectId = @object.PobjectId;
            item.FobjectName = @object.PobjectName;
            item.FobjectBarcode = @object.PobjectBarcode;
            item.Found = 1;

            var r = Store.Update(item) > 0;

            if(r)
                Model.Instance.Session.SetItem(item);

            return r;
        }

        /// <summary>
        /// Удаляем все дубли ТМЦ
        /// </summary>
        /// <param name="itemId"></param>
        /// <returns></returns>
        public bool DeleteDoubles(string itemId)
        {
            var r = true;
            var doubles = Store.Table<Doubles>().Where(d => d.Id == itemId).ToList();
            if (doubles != null && doubles.Count > 0)
            {
                r = doubles.Aggregate(true, (current, @double) => current & Store.Delete(@double) > 0);
            }

            return r;
        }

        /// <summary>
        /// Удаляем все дубли ТМЦ в указаном месте
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="objectId"></param>
        /// <returns></returns>
        public bool DeleteDoubles(string itemId, string objectId)
        {
            var r = true;
            var doubles = Store.Table<Doubles>().Where(d => d.Id == itemId && d.FobjectBarcode == objectId).ToList();
            if (doubles != null && doubles.Count > 0)
            {
                r = doubles.Aggregate(true, (current, @double) => current & Store.Delete(@double) > 0);
            }

            return r;
        }

        /// <summary>
        /// Пределяет есть объект с кодам в базе или нет
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public bool ObjectExist(string code)
        {
            var objects = Store.Table<Objects>().Where(o => o.PobjectId == code || o.PobjectId == code).ToList();
            return objects.Count > 0;
        }

        /// <summary>
        /// Сохранение нового объекта
        /// </summary>
        /// <param name="object"></param>
        /// <returns></returns>
        public bool InsertObject(Objects @object)
        {
            return Store.Insert(@object) > 0;
        }

        protected override Action StartAction
        {
            get
            {
                return () =>
                    {
                        // ищем базу данных, в начале на карте потом в директории с программой, если не нашли, 
                        // то создаем или на карте, или в директории с программой

                        Store = new SQLiteConnection(DatabasePath);

                        /* создаем таблицы
                         * 
                         * Items
                         * Doubles
                         * Objects
                         * Statuses
                         * Mols
                         * Users
                         * Codes
                         */

                        Store.CreateTable<Items>();
                        Store.CreateTable<Doubles>();
                        Store.CreateTable<Objects>();
                        Store.CreateTable<Statuses>();
                        Store.CreateTable<Mols>();
                        Store.CreateTable<Users>();
                        Store.CreateTable<Codes>();

                        // обнуляем значения на случай если в импорте придут новые
                        _codes = null;
                        _statuses = null;
                    };
            }
        }

        protected override Action StopAction
        {
            get
            {
                return () =>
                    {
                        if (Store != null)
                        {
                            Store.Close();
                        }
                    };
            }
        }
    }
}