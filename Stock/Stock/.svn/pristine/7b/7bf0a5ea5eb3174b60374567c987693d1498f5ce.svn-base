using System;
using System.Linq;
using StockCore.Entity;
using StockCore.Enums;
using StockCore.Events;
using StockCore.Factories;
using StockCore.Interfaces.Views;

namespace StockCore.Presenters
{
    public class InformationPresenter : StockPresenter<IInformationView>
    {
        public InformationPresenter(IInformationView view)
            : base(view)
        {
        }

        protected override void TerminalBarcodeRead(object sender, StockEventArgs e)
        {
            View.Barcode = e.Message;

            EnterBarcode();
        }

        protected Items Item { get; set; }

        protected void ViewBarcodeEnterHand(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("���� ��", () =>
                                         {
                                             StopTerminal();
                                             Model.Instance.Presenters.EnterBarcode.ShowView();

                                         }, true, true);
        }

        protected void EnterBarcode()
        {
            Model.Instance.Invoker.Do("��������� ���������� ��", () =>
            {
                Item = null;

                ClearView();

                if (string.IsNullOrEmpty(View.Barcode))
                {
                    return;
                }

                if (!BarcodeFactory.Instance.Check(View.Barcode))
                {
                    if (!Model.Instance.Config.CurrentConfig.SearchInSerialNumbers)
                    {
                        Model.Instance.Messages.ShowExclamation("�����-��� �� ������������� ������� �����", "Ok");
                        return;
                    }

                    Item = Model.Instance.Data.Store.Table<Items>().Where(i => i.SerialNumber == View.Barcode).Take(1).FirstOrDefault();
                }
                else
                {
                    Item = Model.Instance.Data.Store.Table<Items>().Where(i => i.Id == View.Barcode).Take(1).FirstOrDefault();
                }

                if(Item == null)
                {
                    Model.Instance.Messages.ShowExclamation("�� ������� ����� ���", "Ok");
                }
                else
                {
                    FillView();
                }

            }, false, true);
        }

        protected void ViewShow(object sender, EventArgs e)
        {
            Model.Instance.Session.SetView(ViewsEnum.Information);

            View.Barcode = string.Empty;

            ClearView();

            StartTerminal();

            if (Model.Instance.Session != null)
            {
                Model.Instance.Session.ItemBarcodeChanged += SessionItemBarcodeChanged;
            }
        }

        protected void ViewClose(object sender, EventArgs e)
        {
            StopTerminal();

            if (Model.Instance.Session != null)
            {
                Model.Instance.Session.ItemBarcodeChanged -= SessionItemBarcodeChanged;
            }
        }

        protected void ViewToInformation(object sender, EventArgs e)
        {
            StartTerminal();
        }

        protected void ViewToAll(object sender, EventArgs e)
        {
            StopTerminal();

            Model.Instance.Invoker.DoIndependentThread("�������� ��������", () =>
            {
                View.StatusText = "�������� �������� ... ";

                View.Filter = string.Empty;

                var objects = Model.Instance.Data.Objects(false);
                if (objects.Count > 0)
                {
                    View.Objects = objects;
                }
                },
                () =>
                {
                    View.StatusText = "������������� �����-���";
                },
                true);
        }

        protected void ViewSelectAllChanged(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("���������� ��������� �������� ��� ������", () =>
                   {
                       if(View.Objects != null && View.Objects.Count > 0)
                       {
                           if (View.SelectAll)
                           {
                               foreach (var o in View.Objects)
                               {
                                   o.Selected = 1;
                               }
                           }
                           else
                           {
                               foreach (var o in View.Objects)
                               {
                                   o.Selected = 0;
                               }
                           }

                           View.ObjectsRefresh();
                       }

                   });
        }

        protected void ViewFind(object sender, EventArgs e)
        {
            Model.Instance.Invoker.DoIndependentThread("����� ����� ���� ��������", 
                ()=>
                    {
                        if (string.IsNullOrEmpty(View.Filter))
                        {
                            ViewToAll(sender, e);
                            return;
                        }

                        View.StatusText = "������ �������� ... ";

                        var objects = Model.Instance.Data.Store.Table<Objects>().ToList();

                        View.Objects = objects.Where(
                            o => o.PobjectBarcode.IndexOf(View.Filter, StringComparison.OrdinalIgnoreCase) > -1 ||
                                 o.PobjectId.IndexOf(View.Filter, StringComparison.OrdinalIgnoreCase) > -1 ||
                                 o.PobjectName.IndexOf(View.Filter, StringComparison.OrdinalIgnoreCase) > -1 ||
                                 o.PobjectPid.IndexOf(View.Filter, StringComparison.OrdinalIgnoreCase) > -1 ||
                                 o.PobjectPname.IndexOf(View.Filter, StringComparison.OrdinalIgnoreCase) > -1).OrderBy(
                                     o => o.PobjectName).ToList();
                    },
            () =>
            {
                View.StatusText = "������������� �����-���";
            },
            true);
        }

        protected void ViewOpen(object sender, EventArgs e)
        {
            Model.Instance.Invoker.Do("�������� ���������� �������", () =>
                      {

                          if(View.SelectedObject == null)
                          {
                              Model.Instance.Messages.ShowExclamation("���������� ������� ������", "Ok");
                              return;
                          }

                          Model.Instance.Presenters.ObjectItems.CurrentObject = View.SelectedObject;
                          Model.Instance.Presenters.ObjectItems.ShowView();

                      });
        }

        protected void SessionItemBarcodeChanged(object sender, EventArgs e)
        {
            StartTerminal();

            View.Barcode = Model.Instance.Session.CurrentItemBarcode;

            EnterBarcode();
        }

        protected void ViewPrintItem(object sender, EventArgs e)
        {
            if(Item != null)
            {
                Model.Instance.Presenters.Printing.PrintObjects = Item;
                Model.Instance.Presenters.Printing.ShowView();
            }
            else
            {
                Model.Instance.Messages.ShowExclamation("��� ������ ��� ������", "Ok");
            }
        }

        protected void ViewPrintObjects(object sender, EventArgs e)
        {
            var objects = View.Objects.Where(o => o.Selected == 1).ToList();
            if(objects.Count > 0)
            {
                Model.Instance.Presenters.Printing.PrintObjects = objects.ToArray();
                Model.Instance.Presenters.Printing.ShowView();
            }
            else
            {
                Model.Instance.Messages.ShowExclamation("��� ������ ��� ������", "Ok");
            }
        }

        protected void ClearView()
        {
            View.Status = 
                View.Ibso = 
                View.ItemName = 
                View.MolName = 
                View.UserName = 
                View.Serial = string.Empty;
        }

        protected void FillView()
        {
            View.Barcode = Item.Id;
            View.Status = Item.StatusFriendly;
            View.ItemName = Item.Name;
            View.Ibso = Item.PobjectName;
            View.MolName = Item.MolName;
            View.UserName = Item.UserName;
            View.Serial = Item.SerialNumber;
        }

        public override void Subscription()
        {
            View.Show += ViewShow;
            View.Close += ViewClose;
            View.ToInformation += ViewToInformation;
            View.ToAll += ViewToAll;
            View.BarcodeEnterHand += ViewBarcodeEnterHand;
            View.SelectAllChanged += ViewSelectAllChanged;
            View.Find += ViewFind;
            View.Open += ViewOpen;
            View.PrintItem += ViewPrintItem;
            View.PrintObjects += ViewPrintObjects;
        }

        public override void Unsubscription()
        {
            View.Show -= ViewShow;
            View.Close -= ViewClose;
            View.ToInformation -= ViewToInformation;
            View.ToAll -= ViewToAll;
            View.BarcodeEnterHand -= ViewBarcodeEnterHand;
            View.SelectAllChanged -= ViewSelectAllChanged;
            View.Find -= ViewFind;
            View.Open -= ViewOpen;
            View.PrintItem -= ViewPrintItem;
            View.PrintObjects -= ViewPrintObjects;
        }
    }
}