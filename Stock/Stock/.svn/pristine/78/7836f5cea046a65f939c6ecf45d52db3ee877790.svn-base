﻿using System;
using Resco.UIElements;
using Stock.Views.Printers;
using StockCore.Interfaces.Views;

namespace Stock.Views
{
    public partial class SearchMobilePrinterControl : StockControl, ISearchMobilePrinterView
    {
        public SearchMobilePrinterControl()
        {
            InitializeComponent();
        }

        public override bool Rememberable
        {
            get { return false; }
        }

        public event EventHandler Show;

        public event EventHandler Close;

        public event EventHandler Use;

        public bool ViewEnable
        {
            set
            {
                Invoker(() =>
                            {
                                CancelBtn.Enabled = UseBtn.Enabled = value;
                            });
            }
        }

        public void ShowView()
        {
            PrintersListView.DataTemplate = new NormalPrinterControl {HorizontalAlignment = HAlignment.Stretch};
            PrintersListView.SelectedDataTemplate = new SelectedPrinterControl { HorizontalAlignment = HAlignment.Stretch };

            if(Show != null)
            {
                Show(this, EventArgs.Empty);
            }

            ShowView<SearchMobilePrinterControl>(AnimationEffect.MoveLeft);

            Invoker(() => CancelBtn.Focus());
        }

        public void CloseView()
        {
            if(Close != null)
            {
                Close(this, EventArgs.Empty);
            }

            BackView();
        }

        private void CancelClick(object sender, UIMouseEventArgs e)
        {
            CloseView();
        }

        private void UseClick(object sender, UIMouseEventArgs e)
        {
            if(Use != null)
            {
                Use(sender, e);
            }
        }

        public object Printers
        {
            get { return PrintersListView.DataSource; }
            set { Invoker(() => PrintersListView.DataSource = value); }
        }

        public object SelectedPrinter
        {
            get { return PrintersListView.SelectedItem; }
        }
    }
}