﻿using System;
using System.Xml.Serialization;
using StockCore.Factories;
using StockCore.Common.Attributes;

namespace StockCore.Handlers.Exchange.Configuration.CSV
{
    /// <summary>
    /// Элемент поля обмена
    /// </summary>
    [Serializable]
    public class CvsElement
    {
        public CvsElement(ExchangeField filed, string type, int length, string format)
        {
            ElementFiled = filed;
            CvsElementType = type;
            CvsElementLength = length;
            CvsElementFormat = format;
        }

        public CvsElement(ExchangeField filed, string type, int length)
            : this(filed, type, length, string.Empty)
        {
        }

        /// <summary>
        /// Имя 
        /// </summary>
        [XmlElement]
        public ExchangeField ElementFiled { get; set; }

        /// <summary>
        /// Тип
        /// </summary>
        [XmlElement]
        public string CvsElementType { get; set; }

        /// <summary>
        /// Длина
        /// </summary>
        [XmlElement]
        public int CvsElementLength { get; set; }

        /// <summary>
        /// Формат
        /// </summary>
        [XmlElement]
        public string CvsElementFormat { get; set; }

        [XmlIgnore]
        private string _cvsName;
        [XmlIgnore]
        public string CvsFieldName
        {
            get
            {
                if (string.IsNullOrEmpty(_cvsName))
                {
                    ElementFiled.Description(out _cvsName);
                }

                return _cvsName;
            }
        }

        [XmlIgnore]
        public string CvsParameter
        {
            get { return string.Format("\"{0}\"", CvsFieldName); }
        }

        /// <summary>
        /// Тип поля в формате .net
        /// </summary>
        [XmlIgnore]
        public Type CvsType
        {
            get { return TypeFactory.Instance.GetType(CvsElementType); }
        }

        public override int GetHashCode()
        {
            return (int) ElementFiled;
        }

        public override bool Equals(object obj)
        {
            return (obj as CvsElement) != null && ((CvsElement) obj).ElementFiled == ElementFiled;
        }
    }
}