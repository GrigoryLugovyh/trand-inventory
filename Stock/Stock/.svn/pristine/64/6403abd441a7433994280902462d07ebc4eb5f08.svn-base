using System.Windows.Forms;

namespace Stock.Handlers
{
    public class WaitHandler
    {
        private static readonly object Sync = new object();

        private static volatile WaitHandler _instance;

        public static WaitHandler Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (Sync)
                    {
                        if (_instance == null)
                        {
                            _instance = new WaitHandler();
                        }
                    }
                }

                return _instance;
            }
        }

        public bool IsShow { get; protected set; }

        public void Show()
        {
            Cursor.Current = Cursors.WaitCursor;
            IsShow = true;
        }

        public void Close()
        {
            Cursor.Current = Cursors.Default;
            IsShow = false;
        }
    }
}