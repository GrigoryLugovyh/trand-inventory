﻿using System;
using System.Windows.Forms;
using Resco.UIElements.Controls;
using Stock.Common;
using Stock.Handlers;
using Stock.Views;
using StockCore;
using StockCore.Presenters;

namespace Stock
{
    /// <summary>
    /// Функция передачи панели опций
    /// </summary>
    /// <param name="options"></param>
    public delegate void SendStockViewEvent(ref UIElementPanelControl options);

    /// <summary>
    /// Выход из приложения 
    /// </summary>
    public delegate void CloseApplicationEvent();

    /// <summary>
    /// Обновление представлеия
    /// </summary>
    public delegate void ApplicationDoEvents();

    public partial class StockView : Form
    {
        public static SendStockViewEvent SendStockViewHandler;

        public static CloseApplicationEvent CloseApplicationHandler;

        public static ApplicationDoEvents ApplicationDoEventsHandler;

        protected readonly FullScreen FullScreenHandler;

        public StockView()
        {
            InitializeComponent();

            FullScreenHandler = new FullScreen(this);

            SendStockViewHandler = SendTemplateView;

            CloseApplicationHandler = ApplicationClose;

            ApplicationDoEventsHandler = ApplicationDoEvent;
        }

        public void SendTemplateView(ref UIElementPanelControl template)
        {
            template = Template;
        }

        public void ApplicationClose()
        {
            Invoke(new Action(Close));
        }

        public void ApplicationDoEvent()
        {
            Invoke(new Action(() =>
                                  {
                                      Application.DoEvents();
                                      Refresh();
                                  }));
        }

        protected override void OnLoad(EventArgs e)
        {
            if (FullScreenHandler != null)
            {
                FullScreenHandler.Start();
            }

            base.OnLoad(e);
        }

        protected override void OnClosed(EventArgs e)
        {
            if (FullScreenHandler != null)
            {
                FullScreenHandler.Stop();
            }

            base.OnClosed(e);
        }

        protected override void OnActivated(EventArgs e)
        {
            if (FullScreenHandler != null)
            {
                FullScreenHandler.Start();
            }

            base.OnActivated(e);
        }

        /// <summary>
        /// Запуск приложения 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StockViewLoad(object sender, EventArgs e)
        {
            // Добавляем все контролы
            ControlaHandler.Instance.Views.Add(typeof(SplashControl), new SplashControl());
            ControlaHandler.Instance.Views.Add(typeof (MainControl), new MainControl());
            ControlaHandler.Instance.Views.Add(typeof (InventoryControl), new InventoryControl());
            ControlaHandler.Instance.Views.Add(typeof (ExchangeControl), new ExchangeControl());
            ControlaHandler.Instance.Views.Add(typeof (ExchangeProcessControl), new ExchangeProcessControl());
            ControlaHandler.Instance.Views.Add(typeof (ObjectParametersControl), new ObjectParametersControl());

            // Инициализируем внешние обработчики
            Model.Instance.Messages = new MessageDistributor();
            Model.Instance.Dialogs = new DialogDistributor();
            Model.Instance.Wait = new WaitDistributor();

            // Инициализируем презентеры данных
            Model.Instance.Presenters = new Presenters(
                new SplashPresenter(ControlaHandler.Instance.CreateView<SplashControl>(Template)),
                new MainPresenter(ControlaHandler.Instance.CreateView<MainControl>(Template)),
                new InventoryPresenter(ControlaHandler.Instance.CreateView<InventoryControl>(Template)),
                new ExchangePresenter(ControlaHandler.Instance.CreateView<ExchangeControl>(Template)),
                new ExchangeProcessPresenter(ControlaHandler.Instance.CreateView<ExchangeProcessControl>(Template)),
                new ObjectParametersPresenter(ControlaHandler.Instance.CreateView<ObjectParametersControl>(Template)));

            // Показываем экран загрузки
            Model.Instance.Presenters.Splash.ShowView();

            // Запускаем модель
            Model.Instance.Start();

            // Начинаем работу
            Model.Instance.Presenters.Main.ShowView();
        }
    }
}