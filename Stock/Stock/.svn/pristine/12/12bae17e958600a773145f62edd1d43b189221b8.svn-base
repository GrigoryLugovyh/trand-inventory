﻿using System;
using System.Collections.Generic;
using Resco.UIElements;
using Stock.Views.Items;
using StockCore.Interfaces.Views;

namespace Stock.Views
{
    public partial class ObjectItemsControl : StockControl, IObjectItemsView
    {
        public ObjectItemsControl()
        {
            InitializeComponent();
        }

        public event EventHandler Show;

        public event EventHandler Close;

        public event EventHandler SelectedChecked;

        public event EventHandler Cancel;

        public event EventHandler Print;

        public void ItemsRefresh()
        {
            Invoker(() => ItemsListView.Refresh());
        }

        public void ShowView()
        {
            ItemsListView.DataTemplate = new ItemNormalControl();
            ItemsListView.SelectedDataTemplate = new ItemSelectedControl();

            if(Show != null)
            {
                Show(this, EventArgs.Empty);
            }

            ShowView<ObjectItemsControl>(AnimationEffect.MoveLeft);
        }

        public void CloseView()
        {
            ShowView<InformationControl>(AnimationEffect.MoveRight);
        }

        public string ObjectName
        {
            get { return ObjectNameText.Text; }
            set { Invoker(() => ObjectNameText.Text = value); }
        }

        public bool SelectedCheck
        {
            get { return SelectCheck.Selected; }
            set { Invoker(() => SelectCheck.Selected = value); }
        }

        public List<StockCore.Entity.Items> Items
        {
            get { return ItemsListView.DataSource as List<StockCore.Entity.Items>; }
            set { Invoker(() => ItemsListView.DataSource = value); }
        }

        private void CancelBtnClick(object sender, UIMouseEventArgs e)
        {
            if (Close != null)
            {
                Close(sender, e);
            }
        }

        private void SelectCheckCheckedChanged(object sender, EventArgs e)
        {
            if(SelectedChecked != null)
            {
                SelectedChecked(sender, e);
            }
        }
    }
}
