using System.Collections.Generic;
using StockCore.Interfaces.Presenters;

namespace StockCore.Presenters
{
    public class Presenters : List<ISubscribePresenter>
    {
        public Presenters(
            SplashPresenter splashPresenter)
        {
            Splash = splashPresenter;

            Add(Splash);
        }

        public void InitPresenters(
            MainPresenter mainPresenter,
            ObjectsPresenter objectsPresenter,
            ExchangePresenter exchangePresenter,
            ExchangeProcessPresenter exchangeProcessPresenter,
            ObjectParametersPresenter objectParametersPresenter,
            FindUsersPresenter findUsersPresenter,
            FindMolsPresenter findMolsPresenter,
            SelectUserPresenter selectUserPresenter,
            SelectMolPresenter selectMolPresenter,
            InventoryPresenter inventoryPresenter,
            InformationPresenter informationPresenter,
            EnterBarcodePresenter enterBarcodePresenter,
            SeleectItemStatusPresenter seleectItemStatusPresenter,
            AddItemPresenter addItemPresenter,
            SerialNumberInputPresenter serialNumberInputPresenter,
            AddObjectPresenter addObjectPresenter,
            FindObjectsPresenter findObjectsPresenter,
            ObjectItemsPresenter objectItemsPresenter)
        {
            Main = mainPresenter;
            Objects = objectsPresenter;
            Exchange = exchangePresenter;
            ExchangeProcess = exchangeProcessPresenter;
            ObjectParameters = objectParametersPresenter;
            FindUsers = findUsersPresenter;
            FindMols = findMolsPresenter;
            SelectUsers = selectUserPresenter;
            SelectMols = selectMolPresenter;
            Inventory = inventoryPresenter;
            Information = informationPresenter;
            EnterBarcode = enterBarcodePresenter;
            SelectItemStatus = seleectItemStatusPresenter;
            AddItem = addItemPresenter;
            SerialNumberInput = serialNumberInputPresenter;
            AddObject = addObjectPresenter;
            FindObjects = findObjectsPresenter;
            ObjectItems = objectItemsPresenter;

            Add(Main);
            Add(Objects);
            Add(Exchange);
            Add(ExchangeProcess);
            Add(ObjectParameters);
            Add(FindUsers);
            Add(FindMols);
            Add(SelectUsers);
            Add(SelectMols);
            Add(Inventory);
            Add(Information);
            Add(EnterBarcode);
            Add(SelectItemStatus);
            Add(AddItem);
            Add(SerialNumberInput);
            Add(AddObject);
            Add(FindObjects);
            Add(ObjectItems);
        }

        public SplashPresenter Splash { get; protected set; }

        public MainPresenter Main { get; protected set; }

        public ObjectsPresenter Objects { get; protected set; }

        public ExchangePresenter Exchange { get; protected set; }

        public ExchangeProcessPresenter ExchangeProcess { get; protected set; }

        public ObjectParametersPresenter ObjectParameters { get; protected set; }

        public FindUsersPresenter FindUsers { get; protected set; }

        public FindMolsPresenter FindMols { get; protected set; }

        public SelectUserPresenter SelectUsers { get; protected set; }

        public SelectMolPresenter SelectMols { get; protected set; }

        public InventoryPresenter Inventory { get; protected set; }

        public InformationPresenter Information { get; protected set; }

        public EnterBarcodePresenter EnterBarcode { get; protected set; }

        public SeleectItemStatusPresenter SelectItemStatus { get; protected set; }

        public AddItemPresenter AddItem { get; protected set; }

        public SerialNumberInputPresenter SerialNumberInput { get; protected set; }

        public AddObjectPresenter AddObject { get; protected set; }

        public FindObjectsPresenter FindObjects { get; protected set; }

        public ObjectItemsPresenter ObjectItems { get; protected set; }
    }
}