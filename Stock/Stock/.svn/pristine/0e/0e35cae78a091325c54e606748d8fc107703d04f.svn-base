﻿using System;
using StockCore.Entity;

namespace StockCore.Handlers.Exchange.Imports.Commands
{
    /// <summary>
    /// Фабрика команд импорта
    /// </summary>
    public sealed class ImportCommandsFactory
    {
        private static readonly object Sync = new object();

        private static volatile ImportCommandsFactory _instance;

        /// <summary>
        /// Инициализация
        /// </summary>
        public static ImportCommandsFactory Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (Sync)
                    {
                        if (_instance == null)
                        {
                            _instance = new ImportCommandsFactory();
                        }
                    }
                }

                return _instance;
            }
        }

        public object GetProcessor<T>(CsvImport handler)
        {
            object processor;

            if (typeof(T) == typeof(Items))
            {
                processor = new InsertItemsCommandProcessor(handler);
            }
            else if (typeof(T) == typeof(Objects))
            {
                processor = new InsertObjectsCommandProcessor(handler);
            }
            else if (typeof(T) == typeof(Users))
            {
                processor = new InsertUsersCommandProcessor(handler);
            }
            else if (typeof(T) == typeof(Mols))
            {
                processor = new InsertMolsCommandProcessor(handler);
            }
            else if(typeof(T) == typeof(Codes))
            {
                processor = new InsertCodesCommandProcessor(handler);
            }
            else
            {
                throw new Exception("Не найден обработчик для команды указаного типа");
            }

            return processor;
        }
    }
}