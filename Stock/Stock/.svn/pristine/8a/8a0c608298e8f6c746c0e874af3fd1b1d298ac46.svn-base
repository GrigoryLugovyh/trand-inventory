﻿using System;
using System.Data.SQLite;
using System.Diagnostics;
using StockCore.Enums;
using StockCore.Events;
using StockCore.Handlers.Exchange.Configuration;
using StockCore.Interfaces.Common;
using StockCore.Interfaces.Handlers;

namespace StockCore.Handlers.Exchange
{
    /// <summary>
    /// Механизм обмена
    /// </summary>
    public abstract class Exchange : IStartable
    {
        protected Exchange(IExchangeProtocol handler, ExchangeSettings settings)
        {
            Handler = handler;
            Settings = settings;

            Status = StartableEnum.Stopped;
        }

        /// <summary>
        /// Соединение с базой
        /// </summary>
        public SQLiteConnection ExchangeConnection { get; set; }

        /// <summary>
        /// Транзакция sql
        /// </summary>
        public SQLiteTransaction ExchangeTransaction { get; set; }

        /// <summary>
        /// Обработчик обмена
        /// </summary>
        public IExchangeProtocol Handler { get; protected set; }

        /// <summary>
        /// Настройки обмена
        /// </summary>
        public ExchangeSettings Settings { get; protected set; }

        /// <summary>
        /// Сведение о подключениии к источнику данных
        /// </summary>
        protected bool IsConnected { get; set; }

        /// <summary>
        /// Успешно оединились с источником данных
        /// </summary>
        public event EventHandler Connected;

        /// <summary>
        /// Запись не была подготовлена
        /// </summary>
        public event EventHandler RacordNotPrepared;

        /// <summary>
        /// Запись не была обработана
        /// </summary>
        public event EventHandler RecordNotProcessed;

        /// <summary>
        /// Переход к обработке следующей записи
        /// </summary>
        public event EventHandler MoveNextRecord;
        
        /// <summary>
        /// Обмен завершен успешно
        /// </summary>
        public event EventHandler ExchangeFinishedSuccessfully;

        /// <summary>
        /// Ошибка в процессе обработки строки
        /// </summary>
        public event EventHandler<StockEventArgs> ExchangeProcessedException;
        
        /// <summary>
        /// Ошибка в процессе обмена
        /// </summary>
        public event EventHandler<StockEventArgs> ExchangeFinishedException;

        /// <summary>
        /// Отсоединились от источника данных
        /// </summary>
        public event EventHandler Disconnected;

        /// <summary>
        /// Ошибка в момент отсоединения
        /// </summary>
        public event EventHandler<StockEventArgs> DisconnectedException;

        /// <summary>
        /// Обмен был прерван пользователем
        /// </summary>
        public event EventHandler ExchangeAbortedByUser;

        /// <summary>
        /// Название обмена
        /// </summary>
        public abstract string ExchangeFriandlyName { get; }

        /// <summary>
        /// Статус обмена
        /// </summary>
        public abstract string ExchangeProcessStatus { get; }

        /// <summary>
        /// Статус аварийного завершения
        /// </summary>
        public abstract string ExchangeProcessFail { get; }

        /// <summary>
        /// Статус успешного завершения
        /// </summary>
        public abstract string ExchangeProcessSuccessfully { get; }

        /// <summary>
        /// Инициализация обмена
        /// </summary>
        protected abstract void InitExchange();

        /// <summary>
        /// Завершение обмена
        /// </summary>
        protected abstract void FinishExchange();

        /// <summary>
        /// Подлючились к источнику данных
        /// </summary>
        /// <returns></returns>
        protected abstract bool Connect();

        /// <summary>
        /// Процент обработанных записей
        /// </summary>
        /// <returns></returns>
        protected abstract int PersentProcessed();

        /// <summary>
        /// Есть запись для обработки
        /// </summary>
        /// <returns></returns>
        protected abstract bool ExistRecord();
        
        /// <summary>
        /// Подгатовили запись
        /// </summary>
        /// <returns></returns>
        protected abstract bool PrepareRecord();

        /// <summary>
        /// Обработали запись
        /// </summary>
        /// <returns></returns>
        protected abstract bool ProcessRecord();

        /// <summary>
        /// Обключились от источника данных
        /// </summary>
        /// <returns></returns>
        protected abstract bool Disconnect();

        /// <summary>
        /// Информация о текущей обрабатываемой позиции
        /// </summary>
        /// <returns></returns>
        protected abstract string CurrendRecordInformation();

        /// <summary>
        /// Записей получено
        /// </summary>
        public long Received { get; protected set; }

        /// <summary>
        /// Записей подготовлено
        /// </summary>
        public long Prepared { get; protected set; }

        /// <summary>
        /// Записей обработано
        /// </summary>
        public long Processed { get; protected set; }

        /// <summary>
        /// Кол-во необработанных записей
        /// </summary>
        public long NotProcessed { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public int Persent { get; protected set; }

        /// <summary>
        /// Время выполнения операции обмена
        /// </summary>
        public Stopwatch Watcher { get; protected set; }

        /// <summary>
        /// Сброс счетчиков
        /// </summary>
        protected virtual void ResetCounters()
        {
            Received = Prepared = Processed = NotProcessed = 0;
            Persent = 0;
        }

        public StartableEnum Status { get; protected set; }

        /// <summary>
        /// Запуск обработки
        /// </summary>
        public void Start()
        {
            try
            {
                if (!Model.Instance.StockDatabase.IsActive || Model.Instance.StockDatabase.Store == null)
                    throw new Exception(string.Format("Нет подключения к базе данных, невозможно выполнить операцию \"{0}\"", ExchangeFriandlyName));

                // запускаем таймер
                if (Watcher == null)
                {
                    Watcher = new Stopwatch();
                }

                Watcher.Reset();
                Watcher.Start();

                Status = StartableEnum.Started;

                // Подготовительные работы перед обменом
                InitExchange();

                if (Connect())
                {
                    IsConnected = true;

                    if (Connected != null)
                    {
                        Connected(this, EventArgs.Empty);
                    }

                    // начинаем обмен
                    while (ExistRecord())
                    {
                        if (Status == StartableEnum.Stopped)
                            break;

                        try
                        {
                            Received++;

                            // подготавливаем запись
                            if (PrepareRecord())
                            {
                                Prepared++;

                                // обрабатываем запись
                                if (!ProcessRecord())
                                {
                                    if (RecordNotProcessed != null)
                                    {
                                        RecordNotProcessed(this, EventArgs.Empty);
                                    }

                                    if (Handler.StopWhenRecordNotProcessed)
                                    {
                                        Status = StartableEnum.Stopped;
                                    }

                                    NotProcessed++;
                                }
                                else
                                {
                                    Processed++;
                                }
                            }
                            else
                            {
                                if (RacordNotPrepared != null)
                                {
                                    RacordNotPrepared(this, EventArgs.Empty);
                                }

                                if (Handler.StopWhenRecordNotPrepared)
                                {
                                    Status = StartableEnum.Stopped;
                                }
                            }

                            // получем процент обработанных записей
                            Persent = PersentProcessed();

                            if (MoveNextRecord != null)
                            {
                                MoveNextRecord(this, EventArgs.Empty);
                            }
                        }
                        catch (Exception ex)
                        {
                            NotProcessed++;

                            if (ExchangeProcessedException != null)
                            {
                                var info = CurrendRecordInformation();
                                ExchangeProcessedException(this, new StockEventArgs("Ошибка в процессе обработки: {0}\r\n Дополнительная информация: {1}", ex.Message, !string.IsNullOrEmpty(info) ? info : "отсутствует"));
                            }

                            if (Handler.StopWhenRecordNotProcessed || Handler.StopWhenRecordNotPrepared)
                            {
                                Status = StartableEnum.Stopped;
                            }
                        }
                    }

                    // обмен успешно завершился
                    if (ExchangeFinishedSuccessfully != null)
                    {
                        ExchangeFinishedSuccessfully(this, EventArgs.Empty);
                    }
                }
                else
                {
                    throw new Exception("Не удалось соедениться с источником данных");
                }
            }
            catch (Exception ex)
            {
                // событие аварийного завершения процесса обмена, необработанной исключение
                if (ExchangeFinishedException != null)
                {
                    ExchangeFinishedException(this, new StockEventArgs(string.Format("В процессе обмена данными возникла ошибка:\r\n{0}", ex.Message)));
                }
            }
            finally
            {
                try
                {
                    Status = StartableEnum.Stopped;

                    if (IsConnected)
                    {
                        Disconnect();

                        if (Disconnected != null)
                        {
                            Disconnected(this, EventArgs.Empty);
                        }
                    }

                    FinishExchange();
                }
                catch (Exception ex)
                {
                    if (DisconnectedException != null)
                    {
                        DisconnectedException(this, new StockEventArgs("В момент завершения процесса обмена возникла ошибка: {0}", ex.Message));
                    }
                }
                finally
                {
                    IsConnected = false;

                    // чистим память после обмена
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }

                // останавливам таймер
                if (Watcher != null)
                {
                    Watcher.Stop();
                }
            }
        }

        /// <summary>
        /// Остановка обработки
        /// </summary>
        public void Stop()
        {
            Status = StartableEnum.Stopped;

            if (ExchangeAbortedByUser != null)
            {
                ExchangeAbortedByUser(this, EventArgs.Empty);
            }
        }

        public void Restart()
        {
            // нет реализации
        }
    }
}