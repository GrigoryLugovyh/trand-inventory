﻿using System;
using System.Collections.Generic;
using Resco.UIElements;
using Resco.UIElements.Controls;
using Stock.Handlers;
using Stock.Interfaces;

namespace Stock.Views
{
    public partial class StockControl : UIUserPanel, IControl
    {
        public StockControl()
        {
            InitializeComponent();
        }

        public string HeaderText
        {
            get { return HeaderLabel.Text; }
            set { Invoker(() => HeaderLabel.Text = value); }
        }

        public bool HeaderVisible
        {
            get { return HeaderLabel.Visible; }
            set { Invoker(() => HeaderLabel.Visible = value); }
        }

        public string StatusText
        {
            get { return StatusLabel.Text; }
            set { Invoker(() => StatusLabel.Text = value); }
        }

        public bool StatusVisible
        {
            get { return StatusLabel.Visible; }
            set { Invoker(() => StatusLabel.Visible = value); }
        }

        public UIUserPanel Control
        {
            get { return this; }
        }

        public UIElementPanelControl Template
        {
            get
            {
                UIElementPanelControl option = null;
                if (StockView.SendStockViewHandler != null)
                {
                    StockView.SendStockViewHandler(ref option);
                }

                return option;
            }
        }

        public void CloseApplication()
        {
            if(StockView.CloseApplicationHandler != null)
            {
                StockView.CloseApplicationHandler();
            }
        }

        protected virtual void ShowView<T>(AnimationEffect animation) where T : UIUserPanel
        {
            Invoker(() => ShowView<T>(animation, true));
        }

        protected virtual void ShowView<T>(AnimationEffect animation, bool update) where T : UIUserPanel
        {
            var detail = ControlHandler.Instance.GetView<T>(Template/*, update*/);
            ControlHandler.Instance.ShowView(detail, animation);
        }

        protected void Invoker(Action action)
        {
            if (!InvokeRequired)
            {
                action.Invoke();
            }
            else
            {
                Invoke(new Action(action.Invoke));
            }
        }
    }
}
