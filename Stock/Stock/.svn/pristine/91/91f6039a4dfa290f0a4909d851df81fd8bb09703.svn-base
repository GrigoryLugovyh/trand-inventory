﻿using System;
using System.Collections.Generic;
using Resco.UIElements;
using StockCore.Handlers.Printing;
using StockCore.Interfaces.Views;

namespace Stock.Views
{
    public partial class PrintingControl : StockControl, IPrintingView
    {
        public PrintingControl()
        {
            InitializeComponent();
        }

        public override bool Rememberable
        {
            get { return false; }
        }

        public event EventHandler Show;

        public event EventHandler Close;

        public void ShowView()
        {
            if (Show != null)
            {
                Show(this, EventArgs.Empty);
            }

            ShowView<PrintingControl>(AnimationEffect.MoveLeft);
        }

        public void CloseView()
        {
            if(Close != null)
            {
                Close(this, EventArgs.Empty);
            }
        }

        public override void BackView()
        {
            if (Close != null)
            {
                Close(this, EventArgs.Empty);
            }

            base.BackView();
        }

        private void PrintingBtnClick(object sender, UIMouseEventArgs e)
        {
            if(Print != null)
            {
                Print(sender, e);
            }
        }

        private void CancelBtnClick(object sender, UIMouseEventArgs e)
        {
            if (Cancel != null)
            {
                Cancel(sender, e);
            }
        }

        private void SearchBtnClick(object sender, UIMouseEventArgs e)
        {
            if(Search != null)
            {
                Search(sender, e);
            }
        }

        private void PrintersBoxSelectedIndexChanged(object sender, EventArgs e)
        {
            if(PrintedChanged != null)
            {
                PrintedChanged(sender, e);
            }
        }

        public event EventHandler Print;

        public event EventHandler Search;

        public event EventHandler Cancel;

        public event EventHandler PrintedChanged;

        public List<PrinterEntry> Printers
        {
            get { return PrintersBox.DataSource as List<PrinterEntry>; }
            set { Invoker(() => PrintersBox.DataSource = value); }
        }

        public PrinterEntry SelectedPrinter
        {
            get { return PrintersBox.SelectedItem as PrinterEntry; }
        }

        public bool SearchSettingVisible
        {
            set
            {
                Invoker(() =>
                            {
                                SearchBtn.Visible = value;
                            });
            }
        }

        public bool ViewEnable
        {
            set
            {
                Invoker(() =>
                {
                    PrintersBox.Enabled =
                          SearchBtn.Enabled =
                              CancelBtn.Enabled =
                                  PrintingBtn.Enabled = value;
                });
            }
        }

        public string Information
        {
            set
            {
                Invoker(() =>
                            {
                                InfoText.Text = value;
                            });
            }
        }
    }
}