﻿using System;
using System.Collections.Generic;
using Resco.UIElements;
using Stock.Views.InventoryObjects;
using StockCore.Entity;
using StockCore.Interfaces.Views;

namespace Stock.Views
{
    public partial class ObjectsControl : StockControl, IObjectsView
    {
        public ObjectsControl()
        {
            InitializeComponent();
        }

        public event EventHandler Show;

        public event EventHandler Close;

        public void ShowView()
        {
            ObjectsListView.DataTemplate = new NormalObjectControl { HorizontalAlignment = HAlignment.Stretch };
            ObjectsListView.SelectedDataTemplate = new SelectObjectControl
                                                       {
                                                           HorizontalAlignment = HAlignment.Stretch,
                                                           ChildsDataTemplate = new ChildNormalObjectControl {HorizontalAlignment = HAlignment.Stretch},
                                                           ChildsSelectedDataTemplate = new ChildSelectObjectControl {HorizontalAlignment = HAlignment.Stretch}
                                                       };

            if(Show != null)
            {
                Show(this, EventArgs.Empty);
            }

            ShowView<ObjectsControl>(AnimationEffect.MoveLeft);
        }

        public void CloseView()
        {
            if(Close != null)
            {
                Close(this, EventArgs.Empty);
            }

            ShowView<MainControl>(AnimationEffect.MoveRight);
        }

        private void CanselBtnClick(object sender, UIMouseEventArgs e)
        {
            CloseView();
        }

        public string SelectedObjectName
        {
            get { return ObjectNameText.Text; }
            set { Invoker(() => ObjectNameText.Text = value); }
        }

        public Objects SelectedObject { get; protected set; }

        public List<Objects> Objects
        {
            get { return ObjectsListView.DataSource as List<Objects>; }
            set { Invoker(() => ObjectsListView.DataSource = value); }
        }

        public event EventHandler Filter;

        public event EventHandler Add;

        public event EventHandler Select;

        public event EventHandler SelectedChanging;

        private void FilterBtnClick(object sender, UIMouseEventArgs e)
        {
            if (Filter != null)
            {
                Filter(sender, e);
            }
        }

        private void AddBtnClick(object sender, UIMouseEventArgs e)
        {
            if (Add != null)
            {
                Add(sender, e);
            }
        }

        private void SelectBtnClick(object sender, UIMouseEventArgs e)
        {
            if (Select != null)
            {
                Select(sender, e);
            }
        }

        private void ObjectsListViewSelectedIndexChanging(object sender, SelectedIndexChangingEventArgs e)
        {
            if (Objects.Count > 0 && e.NewIndex > -1)
            {
                SelectedObject = Objects[e.NewIndex];

                if (SelectedObject != null && SelectedChanging != null)
                {
                    SelectedChanging(sender, e);
                }

                if (SelectedObject != null)
                {
                    if (SelectedObject.Childs.Count == 0)
                    {
                        ObjectsListView.SelectedDataTemplate.Height = ObjectsListView.DataTemplate.Height;
                    }
                    else
                    {
                        ObjectsListView.SelectedDataTemplate.Height = 22 * SelectedObject.Childs.Count + ObjectsListView.DataTemplate.Height;
                    }
                }
            }
        }
    }
}