using System.Windows.Forms;
using Resco.Controls.MessageBox;

using MenuItem = Resco.Controls.MessageBox.MenuItem;

namespace Stock.Handlers
{
    public sealed class MessageHandler
    {
        private static readonly object Sync = new object();

        private static volatile MessageHandler _instance;

        public static MessageHandler Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (Sync)
                    {
                        if (_instance == null)
                        {
                            _instance = new MessageHandler();
                        }
                    }
                }

                return _instance;
            }
        }

        /// <summary>
        /// ���������� ���������� ���� � ����������
        /// </summary>
        public DialogResult ShowExclamation(MessageBoxIcon icon, string message, string btnText)
        {
            CustomizeMessageBox();
            return MessageBoxEx.Show(icon, true, message,
                                     DialogResult.Yes,
                                     new[]
                                         {
                                             new MenuItem(btnText, DialogResult.Yes)
                                         });
        }

        public void ShowExclamation(string message)
        {
            ShowExclamation(MessageBoxIcon.Exclamation, message, "��");
        }

        /// <summary>
        /// ���������� ��������� � ��������
        /// </summary>
        /// <param name="icon"></param>
        /// <param name="message"></param>
        /// <param name="result"></param>
        /// <param name="btnYes"></param>
        /// <param name="bntNo"></param>
        /// <returns></returns>
        public DialogResult ShowQuestion(MessageBoxIcon icon, string message, DialogResult result, string btnYes, string bntNo)
        {
            CustomizeMessageBox();
            return MessageBoxEx.Show(icon, true, message,
                                     result,
                                     new[]
                                         {
                                             new MenuItem(btnYes, DialogResult.Yes),
                                             new MenuItem(bntNo, DialogResult.No),
                                         });
        }

        /// <summary>
        /// ���������� ��������� � ��������
        /// </summary>
        /// <param name="message"></param>
        /// <param name="caption"></param>
        /// <param name="buttons"></param>
        /// <param name="icon"></param>
        /// <param name="defButton"></param>
        /// <returns></returns>
        public DialogResult ShowQuestion(string message, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defButton)
        {
            CustomizeMessageBox();
            return MessageBoxEx.Show(message, caption, buttons, icon, defButton);
        }

        /// <summary>
        /// ��������� MessageBoxEx
        /// </summary>
        public void CustomizeMessageBox()
        {
            MessageBoxEx.Settings.TopMost = true;
            MessageBoxEx.Settings.AutoDefaultButton = true;
            MessageBoxEx.Settings.IconStyle = MessageBoxIconStyle.Left;
        }
    }
}