using System;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Stock.Handlers
{
    /// <summary>
    /// ������� ������ ���� �� ����������� ��� ���������� Mobile
    /// </summary>
    public sealed class FullScreen
    {
        readonly Form _form;

        IntPtr _hwnd = IntPtr.Zero;
        private IntPtr Hwnd
        {
            get
            {
                if (_hwnd == IntPtr.Zero)
                {
                    _form.Capture = true;
                    _hwnd = GetCapture();
                    _form.Capture = false;
                }

                return _hwnd;
            }
        }

        public FullScreen(Form mainForm)
        {
            if (mainForm == null) throw new ArgumentNullException();
            _form = mainForm;
        }

        public void Start()
        {
            _form.WindowState = FormWindowState.Maximized;

            _form.FormBorderStyle = FormBorderStyle.None;
            _form.ControlBox = false;
            EnableToolbar(false);
            bool a = SetWindowPos(Hwnd, 0, 0, 0, Screen.PrimaryScreen.Bounds.Width,
                                  Screen.PrimaryScreen.Bounds.Height, SWP_SHOWWINDOW);

            SipEnabled = false;
        }

        public void Stop()
        {
            EnableToolbar(true);
        }

        public static void EnableToolbar(bool enabled)
        {
            try
            {
                IntPtr p = FindWindow("HHTaskBar", null);
                if (p == IntPtr.Zero)
                {
                    return;
                }

                IntPtr i = ShowWindow(p, enabled ? SW_SHOW : SW_HIDE);
                bool b = EnableWindow(p, enabled);
            }
            finally
            {
            }
        }

        public void ShowSip(bool value)
        {
            try
            {
                SipShowIM(value ? SIPF_ON : SIPF_OFF);
            }
            catch (Exception)
            {
            }
        }

        public bool SipEnabled
        {
            get { var s = new SIPINFO(); SipGetInfo(s); return ((s.fdwFlags & 1) == SIPF_ON); }
            set { ShowSip(value); }
        }

        private void SipClick(object sender, EventArgs e)
        {
            var s = new SIPINFO();
            SipGetInfo(s);
            ShowSip((s.fdwFlags & 1) != SIPF_ON);
        }

        #region P/Invoke declarations

        const uint SIPF_OFF = 0x0;
        const uint SIPF_ON = 0x1;

        const int HWND_TOPMOST = -1;
        const int SWP_SHOWWINDOW = 0x0040;

        const int SW_HIDE = 0;
        const int SW_SHOW = 1;

        [DllImport("coredll.dll")]
        private static extern bool SetWindowPos(IntPtr hwnd, int hwnd2, int x, int y, int cx, int cy, int uFlags);

        [DllImport("coredll.dll")]
        private static extern IntPtr FindWindow(
            string lpClassName,
            string lpWindowName);

        [DllImport("coredll.dll")]
        private static extern IntPtr GetCapture();

        [DllImport("coredll.dll")]
        private static extern IntPtr ShowWindow(IntPtr hWnd, int visible);

        [DllImport("coredll.dll")]
        private static extern bool EnableWindow(IntPtr hwnd, bool enabled);

        [DllImport("coredll.dll")]
        private extern static void SipShowIM(uint dwFlag);


        private class SIPINFO
        {
            public SIPINFO()
            {
                cbSize = Marshal.SizeOf(typeof(SIPINFO));
            }

            public int cbSize;
            public int fdwFlags;
            public RECT rcVisibleDesktop;
            public RECT rcSipRect;
            public int dwImDataSize;
            public int pvImData;
        }

        private struct RECT
        {
            public int left;
            public int top;
            public int right;
            public int bottom;
        }

        [DllImport("coredll.dll")]
        private extern static bool SipGetInfo(SIPINFO sipInfo);
        #endregion
    }
}
