
namespace Stock.Handlers
{
    public sealed class KeyboardHandler
    {
        private static readonly object Sync = new object();

        private static volatile KeyboardHandler _instance;

        public static KeyboardHandler Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (Sync)
                    {
                        if (_instance == null)
                        {
                            _instance = new KeyboardHandler();
                        }
                    }
                }

                return _instance;
            }
        }

        public bool Shown { get; protected set; }

        public void Show(object sender)
        {
            lock (Sync)
            {
                if (StockView.ShowKeyboardHandler != null && !Shown)
                {
                    StockView.ShowKeyboardHandler(sender);
                    Shown = true;
                }
            }
        }

        public void Hide()
        {
            lock (Sync)
            {
                if (StockView.CloseKeyboardHandler != null && Shown)
                {
                    StockView.CloseKeyboardHandler();
                    Shown = false;
                }
            }
        }
    }
}