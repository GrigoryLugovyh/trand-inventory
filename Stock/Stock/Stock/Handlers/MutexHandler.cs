﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

namespace Stock.Handlers
{
    public sealed class MutexHandler
    {
        private static readonly object Sync = new object();

        private static volatile MutexHandler _instance;

        public static MutexHandler Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (Sync)
                    {
                        if (_instance == null)
                        {
                            _instance = new MutexHandler();
                        }
                    }
                }

                return _instance;
            }
        }

        private IntPtr _handle;

        public bool IsSingle()
        {
            string appname = Assembly.GetExecutingAssembly().GetName().Name;
            _handle = CreateMutex(IntPtr.Zero, false, appname);

            var err = Marshal.GetLastWin32Error();
            if (err == ERROR_ALREADY_EXISTS)
            {
                return false;
            }
            return true;
        }

        public void CloseHandle()
        {
            CloseHandle(_handle);
        }

        private const int ERROR_ALREADY_EXISTS = 183;

        [DllImport("coredll.dll", SetLastError = true)]
        private static extern IntPtr CreateMutex(IntPtr lpMutexAttributes, bool InitialOwner, string MutexName);

        [DllImport("coredll.dll")]
        private static extern bool CloseHandle(IntPtr hMutex); 
    }
}