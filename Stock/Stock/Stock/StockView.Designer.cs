﻿namespace Stock
{
    partial class StockView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Keyboard = new Resco.Controls.KeyboardPro.KeyboardPro(this, this.components);
            this.Template = new Resco.UIElements.Controls.UIElementPanelControl();
            this.Template.SuspendElementLayout();
            this.SuspendLayout();
            // 
            // Keyboard
            // 
            this.Keyboard.Language = "Russian";
            // 
            // Template
            // 
            this.Template.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Template.Name = "Template";
            this.Template.Size = new System.Drawing.Size(320, 320);
            this.Template.TabIndex = 2;
            // 
            // StockView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(320, 320);
            this.ControlBox = false;
            this.Controls.Add(this.Template);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "StockView";
            this.Text = "Главная форма приложения";
            this.Load += new System.EventHandler(this.StockViewLoad);
            this.Template.ResumeElementLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.KeyboardPro.KeyboardPro Keyboard;
        private Resco.UIElements.Controls.UIElementPanelControl Template;

    }
}

