using Resco.UIElements;

namespace Stock.Views.InventoryObjects
{
    public class ChildNormalObjectControl : UIUserPanel
    {
        private UIGridPanel ChildGridPanel;
        private UILabel ObjectName;

        private void InitializeComponent()
        {
            this.ChildGridPanel = new Resco.UIElements.UIGridPanel();
            this.ObjectName = new Resco.UIElements.UILabel();
            this.SuspendLayout();
            // 
            // ChildGridPanel
            // 
            this.ChildGridPanel.Children.Add(this.ObjectName, 0, 0);
            this.ChildGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ChildGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 320, 22);
            this.ChildGridPanel.Name = "ChildGridPanel";
            this.ChildGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            // 
            // ObjectName
            // 
            this.ObjectName.AutoSize = false;
            this.ObjectName.DataBindings.Add(new Resco.UIElements.ElementBinding("Text", "PobjectName", System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, false));
            this.ObjectName.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 320, 22);
            this.ObjectName.Name = "ObjectName";
            this.ObjectName.Padding = new Resco.UIElements.Thickness(2);
            // 
            // ChildNormalObjectControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.Children.Add(this.ChildGridPanel);
            this.Size = new System.Drawing.Size(320, 22);
            this.ResumeLayout();

        }
    }
}