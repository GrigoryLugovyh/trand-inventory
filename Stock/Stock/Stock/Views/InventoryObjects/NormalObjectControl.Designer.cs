﻿using Stock.Controls;

namespace Stock.Views.InventoryObjects
{
    partial class NormalObjectControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ObjectGridPanel = new Resco.UIElements.UIGridPanel();
            this.ObjectName = new Resco.UIElements.UILabel();
            this.SelectedCheck = new Stock.Controls.StockCheckBox();
            this.SuspendLayout();
            // 
            // ObjectGridPanel
            // 
            this.ObjectGridPanel.Children.Add(this.ObjectName, 2, 0);
            this.ObjectGridPanel.Children.Add(this.SelectedCheck, 0, 0);
            this.ObjectGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(20, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ObjectGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(2, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ObjectGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ObjectGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 320, 22);
            this.ObjectGridPanel.Name = "ObjectGridPanel";
            this.ObjectGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            // 
            // ObjectName
            // 
            this.ObjectName.AutoSize = false;
            this.ObjectName.DataBindings.Add(new Resco.UIElements.ElementBinding("Text", "PobjectName", System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, false));
            this.ObjectName.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 298, 22);
            this.ObjectName.Name = "ObjectName";
            this.ObjectName.Padding = new Resco.UIElements.Thickness(2);
            // 
            // SelectedCheck
            // 
            this.SelectedCheck.DataBindings.Add(new Resco.UIElements.ElementBinding("Selected", "Selected", System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, false));
            this.SelectedCheck.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 29, 29);
            this.SelectedCheck.Name = "SelectedCheck";
            this.SelectedCheck.TabIndex = 1;
            // 
            // NormalObjectControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.Children.Add(this.ObjectGridPanel);
            this.Size = new System.Drawing.Size(320, 22);
            this.ResumeLayout();

        }

        #endregion

        private Resco.UIElements.UIGridPanel ObjectGridPanel;
        private Resco.UIElements.UILabel ObjectName;
        private StockCheckBox SelectedCheck;
    }
}
