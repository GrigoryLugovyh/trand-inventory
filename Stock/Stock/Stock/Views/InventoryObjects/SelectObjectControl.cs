﻿using Resco.UIElements;

namespace Stock.Views.InventoryObjects
{
    public partial class SelectObjectControl : UIUserPanel
    {
        public SelectObjectControl()
        {
            InitializeComponent();

            SelectedVisible = false;
        }

        public bool SelectedVisible
        {
            get { return ObjectSelectionGridPanel.Columns[0].Value > 0; }
            set { ObjectSelectionGridPanel.Columns[0].Value = value ? 20 : 0; }
        }
    }
}
