﻿using Resco.UIElements;

namespace Stock.Views.InventoryObjects
{
    public partial class NormalObjectControl : UIUserPanel
    {
        public NormalObjectControl()
        {
            InitializeComponent();

            SelectedVisible = false;
        }

        public bool SelectedVisible
        {
            get { return ObjectGridPanel.Columns[0].Value > 0; }
            set { ObjectGridPanel.Columns[0].Value = value ? 20 : 0; }
        }
    }
}
