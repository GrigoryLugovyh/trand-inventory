﻿using Stock.Controls;

namespace Stock.Views.InventoryObjects
{
    partial class SelectObjectControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ObjectGridPanel = new Resco.UIElements.UIGridPanel();
            this.SelectedObjectDetailsGridPanel = new Resco.UIElements.UIGridPanel();
            this.uiGridPanel1 = new Resco.UIElements.UIGridPanel();
            this.uiLabel1 = new Resco.UIElements.UILabel();
            this.uiLabel2 = new Resco.UIElements.UILabel();
            this.uiGridPanel2 = new Resco.UIElements.UIGridPanel();
            this.uiLabel3 = new Resco.UIElements.UILabel();
            this.uiGridPanel3 = new Resco.UIElements.UIGridPanel();
            this.uiLabel4 = new Resco.UIElements.UILabel();
            this.uiLabel5 = new Resco.UIElements.UILabel();
            this.uiLabel6 = new Resco.UIElements.UILabel();
            this.uiLabel7 = new Resco.UIElements.UILabel();
            this.ObjectSelectionGridPanel = new Resco.UIElements.UIGridPanel();
            this.uiLabel8 = new Resco.UIElements.UILabel();
            this.SelectedCheck = new Stock.Controls.StockCheckBox();
            this.SuspendLayout();
            // 
            // ObjectGridPanel
            // 
            this.ObjectGridPanel.Children.Add(this.SelectedObjectDetailsGridPanel, 2, 1);
            this.ObjectGridPanel.Children.Add(this.ObjectSelectionGridPanel, 2, 0);
            this.ObjectGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(0, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ObjectGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(2, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ObjectGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ObjectGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 320, 100);
            this.ObjectGridPanel.Name = "ObjectGridPanel";
            this.ObjectGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(25, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ObjectGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            // 
            // SelectedObjectDetailsGridPanel
            // 
            this.SelectedObjectDetailsGridPanel.BorderThickness = 1;
            this.SelectedObjectDetailsGridPanel.Children.Add(this.uiGridPanel1, 0, 0);
            this.SelectedObjectDetailsGridPanel.Children.Add(this.uiGridPanel2, 0, 1);
            this.SelectedObjectDetailsGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.SelectedObjectDetailsGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 318, 75);
            this.SelectedObjectDetailsGridPanel.Name = "SelectedObjectDetailsGridPanel";
            this.SelectedObjectDetailsGridPanel.Padding = new Resco.UIElements.Thickness(1);
            this.SelectedObjectDetailsGridPanel.RoundedCornerRadius = 3;
            this.SelectedObjectDetailsGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(30, Resco.UIElements.GridLineValueType.Percentage, true));
            this.SelectedObjectDetailsGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.SelectedObjectDetailsGridPanel.TabIndex = 2;
            // 
            // uiGridPanel1
            // 
            this.uiGridPanel1.Children.Add(this.uiLabel1, 0, 0);
            this.uiGridPanel1.Children.Add(this.uiLabel2, 1, 0);
            this.uiGridPanel1.Columns.Add(new Resco.UIElements.GridLinePosition(50, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel1.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.uiGridPanel1.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 314, 16);
            this.uiGridPanel1.Name = "uiGridPanel1";
            this.uiGridPanel1.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            // 
            // uiLabel1
            // 
            this.uiLabel1.AutoSize = false;
            this.uiLabel1.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 3, 0, 0, 0, 101, 16);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Text = "Код объекта:";
            this.uiLabel1.TextAlignment = Resco.Drawing.Alignment.MiddleLeft;
            // 
            // uiLabel2
            // 
            this.uiLabel2.AutoSize = false;
            this.uiLabel2.DataBindings.Add(new Resco.UIElements.ElementBinding("Text", "PobjectId", System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, false));
            this.uiLabel2.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 209, 16);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.TabIndex = 1;
            this.uiLabel2.TextAlignment = Resco.Drawing.Alignment.MiddleLeft;
            // 
            // uiGridPanel2
            // 
            this.uiGridPanel2.Children.Add(this.uiLabel3, 0, 0);
            this.uiGridPanel2.Children.Add(this.uiGridPanel3, 0, 1);
            this.uiGridPanel2.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel2.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 314, 54);
            this.uiGridPanel2.Name = "uiGridPanel2";
            this.uiGridPanel2.Padding = new Resco.UIElements.Thickness(1);
            this.uiGridPanel2.Rows.Add(new Resco.UIElements.GridLinePosition(35, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel2.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel2.TabIndex = 1;
            // 
            // uiLabel3
            // 
            this.uiLabel3.AutoSize = false;
            this.uiLabel3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.uiLabel3.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 312, 13);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Text = "Вышестоящий объект";
            this.uiLabel3.TextAlignment = Resco.Drawing.Alignment.MiddleCenter;
            // 
            // uiGridPanel3
            // 
            this.uiGridPanel3.BorderThickness = 1;
            this.uiGridPanel3.Children.Add(this.uiLabel4, 0, 0);
            this.uiGridPanel3.Children.Add(this.uiLabel5, 0, 1);
            this.uiGridPanel3.Children.Add(this.uiLabel6, 1, 0);
            this.uiGridPanel3.Children.Add(this.uiLabel7, 1, 1);
            this.uiGridPanel3.Columns.Add(new Resco.UIElements.GridLinePosition(50, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel3.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel3.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 312, 38);
            this.uiGridPanel3.Name = "uiGridPanel3";
            this.uiGridPanel3.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel3.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel3.TabIndex = 1;
            // 
            // uiLabel4
            // 
            this.uiLabel4.AutoSize = false;
            this.uiLabel4.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.uiLabel4.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 3, 0, 0, 0, 100, 18);
            this.uiLabel4.Name = "uiLabel4";
            this.uiLabel4.Text = "Код:";
            this.uiLabel4.TextAlignment = Resco.Drawing.Alignment.MiddleLeft;
            // 
            // uiLabel5
            // 
            this.uiLabel5.AutoSize = false;
            this.uiLabel5.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.uiLabel5.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 3, 0, 0, 0, 100, 18);
            this.uiLabel5.Name = "uiLabel5";
            this.uiLabel5.TabIndex = 1;
            this.uiLabel5.Text = "Наименование:";
            this.uiLabel5.TextAlignment = Resco.Drawing.Alignment.MiddleLeft;
            // 
            // uiLabel6
            // 
            this.uiLabel6.AutoSize = false;
            this.uiLabel6.DataBindings.Add(new Resco.UIElements.ElementBinding("Text", "PobjectPid", System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, false));
            this.uiLabel6.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 5, 0, 0, 0, 201, 18);
            this.uiLabel6.Name = "uiLabel6";
            this.uiLabel6.TabIndex = 2;
            this.uiLabel6.TextAlignment = Resco.Drawing.Alignment.MiddleLeft;
            // 
            // uiLabel7
            // 
            this.uiLabel7.AutoSize = false;
            this.uiLabel7.DataBindings.Add(new Resco.UIElements.ElementBinding("Text", "PobjectPname", System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, false));
            this.uiLabel7.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 5, 0, 0, 0, 201, 18);
            this.uiLabel7.Name = "uiLabel7";
            this.uiLabel7.TabIndex = 3;
            this.uiLabel7.TextAlignment = Resco.Drawing.Alignment.MiddleLeft;
            // 
            // ObjectSelectionGridPanel
            // 
            this.ObjectSelectionGridPanel.Children.Add(this.uiLabel8, 2, 0);
            this.ObjectSelectionGridPanel.Children.Add(this.SelectedCheck, 0, 0);
            this.ObjectSelectionGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(20, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ObjectSelectionGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(2, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ObjectSelectionGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ObjectSelectionGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 318, 25);
            this.ObjectSelectionGridPanel.Name = "ObjectSelectionGridPanel";
            this.ObjectSelectionGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ObjectSelectionGridPanel.TabIndex = 3;
            // 
            // uiLabel8
            // 
            this.uiLabel8.AutoSize = false;
            this.uiLabel8.DataBindings.Add(new Resco.UIElements.ElementBinding("Text", "PobjectName", System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, false));
            this.uiLabel8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.uiLabel8.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 296, 25);
            this.uiLabel8.Name = "uiLabel8";
            this.uiLabel8.TextAlignment = Resco.Drawing.Alignment.MiddleLeft;
            // 
            // SelectedCheck
            // 
            this.SelectedCheck.DataBindings.Add(new Resco.UIElements.ElementBinding("Selected", "Selected", System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, false));
            this.SelectedCheck.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 29, 29);
            this.SelectedCheck.Name = "SelectedCheck";
            this.SelectedCheck.TabIndex = 1;
            // 
            // SelectObjectControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.Children.Add(this.ObjectGridPanel);
            this.Size = new System.Drawing.Size(320, 100);
            this.ResumeLayout();

        }

        #endregion

        private Resco.UIElements.UIGridPanel ObjectGridPanel;
        private Resco.UIElements.UIGridPanel SelectedObjectDetailsGridPanel;
        private Resco.UIElements.UIGridPanel uiGridPanel1;
        private Resco.UIElements.UILabel uiLabel1;
        private Resco.UIElements.UILabel uiLabel2;
        private Resco.UIElements.UIGridPanel uiGridPanel2;
        private Resco.UIElements.UILabel uiLabel3;
        private Resco.UIElements.UIGridPanel uiGridPanel3;
        private Resco.UIElements.UILabel uiLabel4;
        private Resco.UIElements.UILabel uiLabel5;
        private Resco.UIElements.UILabel uiLabel6;
        private Resco.UIElements.UILabel uiLabel7;
        private Resco.UIElements.UIGridPanel ObjectSelectionGridPanel;
        private Resco.UIElements.UILabel uiLabel8;
        private StockCheckBox SelectedCheck;
    }
}
