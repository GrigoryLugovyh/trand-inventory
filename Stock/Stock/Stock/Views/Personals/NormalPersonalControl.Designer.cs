﻿namespace Stock.Views.Personals
{
    partial class NormalPersonalControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PersonalGridPanel = new Resco.UIElements.UIGridPanel();
            this.uiLabel1 = new Resco.UIElements.UILabel();
            this.uiLabel2 = new Resco.UIElements.UILabel();
            this.SuspendLayout();
            // 
            // PersonalGridPanel
            // 
            this.PersonalGridPanel.Children.Add(this.uiLabel1, 0, 0);
            this.PersonalGridPanel.Children.Add(this.uiLabel2, 1, 0);
            this.PersonalGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(60, Resco.UIElements.GridLineValueType.Absolute, true));
            this.PersonalGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.PersonalGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 320, 25);
            this.PersonalGridPanel.Name = "PersonalGridPanel";
            this.PersonalGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            // 
            // uiLabel1
            // 
            this.uiLabel1.AutoSize = false;
            this.uiLabel1.DataBindings.Add(new Resco.UIElements.ElementBinding("Text", "Number", System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, false));
            this.uiLabel1.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 2, 0, 0, 0, 58, 25);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.TextAlignment = Resco.Drawing.Alignment.MiddleLeft;
            // 
            // uiLabel2
            // 
            this.uiLabel2.AutoSize = false;
            this.uiLabel2.DataBindings.Add(new Resco.UIElements.ElementBinding("Text", "Name", System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, false));
            this.uiLabel2.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 2, 0, 0, 0, 258, 25);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.TabIndex = 1;
            this.uiLabel2.TextAlignment = Resco.Drawing.Alignment.MiddleLeft;
            // 
            // NormalPersonalControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.Children.Add(this.PersonalGridPanel);
            this.Size = new System.Drawing.Size(320, 25);
            this.ResumeLayout();

        }

        #endregion

        private Resco.UIElements.UIGridPanel PersonalGridPanel;
        private Resco.UIElements.UILabel uiLabel1;
        private Resco.UIElements.UILabel uiLabel2;
    }
}
