﻿namespace Stock.Views
{
    partial class MainControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainMenuPanel = new Resco.UIElements.UIGridPanel();
            this.InventoryBtn = new Stock.Controls.StockButton();
            this.InformationBtn = new Stock.Controls.StockButton();
            this.ObjectsBtn = new Stock.Controls.StockButton();
            this.ExchangeBtn = new Stock.Controls.StockButton();
            this.ExitBtn = new Stock.Controls.StockButton();
            this.InventoryInformationPanel = new Resco.UIElements.UIGridPanel();
            this.uiLabel1 = new Resco.UIElements.UILabel();
            this.uiLabel2 = new Resco.UIElements.UILabel();
            this.uiLabel3 = new Resco.UIElements.UILabel();
            this.uiLabel4 = new Resco.UIElements.UILabel();
            this.AllItems = new Resco.UIElements.UILabel();
            this.AllObjects = new Resco.UIElements.UILabel();
            this.ProcessedItems = new Resco.UIElements.UILabel();
            this.ProcessedObjects = new Resco.UIElements.UILabel();
            this.SuspendLayout();
            // 
            // StockGridPanel
            // 
            this.StockGridPanel.BorderColor = System.Drawing.Color.Transparent;
            this.StockGridPanel.Children.Add(this.MainMenuPanel, 0, 1);
            // 
            // StatusLabel
            // 
            this.StatusLabel.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.StatusLabel.Text = "Выберите действие";
            // 
            // MainMenuPanel
            // 
            this.MainMenuPanel.Children.Add(this.InventoryBtn, 0, 1);
            this.MainMenuPanel.Children.Add(this.InformationBtn, 0, 3);
            this.MainMenuPanel.Children.Add(this.ObjectsBtn, 0, 5);
            this.MainMenuPanel.Children.Add(this.ExchangeBtn, 0, 7);
            this.MainMenuPanel.Children.Add(this.ExitBtn, 0, 9);
            this.MainMenuPanel.Children.Add(this.InventoryInformationPanel, 0, 11);
            this.MainMenuPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 290);
            this.MainMenuPanel.Name = "MainMenuPanel";
            this.MainMenuPanel.Rows.Add(new Resco.UIElements.GridLinePosition(5, Resco.UIElements.GridLineValueType.Absolute, true));
            this.MainMenuPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.MainMenuPanel.Rows.Add(new Resco.UIElements.GridLinePosition(5, Resco.UIElements.GridLineValueType.Absolute, true));
            this.MainMenuPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.MainMenuPanel.Rows.Add(new Resco.UIElements.GridLinePosition(5, Resco.UIElements.GridLineValueType.Absolute, true));
            this.MainMenuPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.MainMenuPanel.Rows.Add(new Resco.UIElements.GridLinePosition(5, Resco.UIElements.GridLineValueType.Absolute, true));
            this.MainMenuPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.MainMenuPanel.Rows.Add(new Resco.UIElements.GridLinePosition(5, Resco.UIElements.GridLineValueType.Absolute, true));
            this.MainMenuPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.MainMenuPanel.Rows.Add(new Resco.UIElements.GridLinePosition(5, Resco.UIElements.GridLineValueType.Absolute, true));
            this.MainMenuPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.MainMenuPanel.TabIndex = 1;
            // 
            // InventoryBtn
            // 
            this.InventoryBtn.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.InventoryBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 20, 4, 20, 4, 200, 35);
            this.InventoryBtn.Name = "InventoryBtn";
            this.InventoryBtn.RoundedCornerRadius = 3;
            this.InventoryBtn.Text = "Инвентаризация";
            this.InventoryBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.InventoryBtnClick);
            // 
            // InformationBtn
            // 
            this.InformationBtn.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.InformationBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 20, 4, 20, 4, 200, 35);
            this.InformationBtn.Name = "InformationBtn";
            this.InformationBtn.RoundedCornerRadius = 3;
            this.InformationBtn.TabIndex = 1;
            this.InformationBtn.Text = "Просмотр информации";
            this.InformationBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.InformationBtnClick);
            // 
            // ObjectsBtn
            // 
            this.ObjectsBtn.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.ObjectsBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 20, 4, 20, 4, 200, 35);
            this.ObjectsBtn.Name = "ObjectsBtn";
            this.ObjectsBtn.RoundedCornerRadius = 3;
            this.ObjectsBtn.TabIndex = 2;
            this.ObjectsBtn.Text = "Просмотр объектов";
            this.ObjectsBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.ObjectsBtnClick);
            // 
            // ExchangeBtn
            // 
            this.ExchangeBtn.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.ExchangeBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 20, 4, 20, 4, 200, 35);
            this.ExchangeBtn.Name = "ExchangeBtn";
            this.ExchangeBtn.RoundedCornerRadius = 3;
            this.ExchangeBtn.TabIndex = 3;
            this.ExchangeBtn.Text = "Обмен данными";
            this.ExchangeBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.ExchangeBtnClick);
            // 
            // ExitBtn
            // 
            this.ExitBtn.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.ExitBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 20, 4, 20, 4, 200, 35);
            this.ExitBtn.Name = "ExitBtn";
            this.ExitBtn.RoundedCornerRadius = 3;
            this.ExitBtn.TabIndex = 4;
            this.ExitBtn.Text = "Выход";
            this.ExitBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.ExitBtnClick);
            // 
            // InventoryInformationPanel
            // 
            this.InventoryInformationPanel.Children.Add(this.uiLabel1, 0, 0);
            this.InventoryInformationPanel.Children.Add(this.uiLabel2, 0, 1);
            this.InventoryInformationPanel.Children.Add(this.uiLabel3, 0, 2);
            this.InventoryInformationPanel.Children.Add(this.uiLabel4, 0, 3);
            this.InventoryInformationPanel.Children.Add(this.AllItems, 1, 0);
            this.InventoryInformationPanel.Children.Add(this.AllObjects, 1, 1);
            this.InventoryInformationPanel.Children.Add(this.ProcessedItems, 1, 2);
            this.InventoryInformationPanel.Children.Add(this.ProcessedObjects, 1, 3);
            this.InventoryInformationPanel.Columns.Add(new Resco.UIElements.GridLinePosition(500, Resco.UIElements.GridLineValueType.Percentage, true));
            this.InventoryInformationPanel.Columns.Add(new Resco.UIElements.GridLinePosition(50, Resco.UIElements.GridLineValueType.Absolute, true));
            this.InventoryInformationPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Bottom, 0, 0, 0, 0, 240, 43);
            this.InventoryInformationPanel.Name = "InventoryInformationPanel";
            this.InventoryInformationPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.InventoryInformationPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.InventoryInformationPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.InventoryInformationPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.InventoryInformationPanel.TabIndex = 5;
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold);
            this.uiLabel1.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Right, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 55, 12);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Text = "Всего ТМЦ:";
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold);
            this.uiLabel2.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Right, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 79, 12);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.TabIndex = 1;
            this.uiLabel2.Text = "Всего объектов:";
            // 
            // uiLabel3
            // 
            this.uiLabel3.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold);
            this.uiLabel3.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Right, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 144, 12);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.TabIndex = 2;
            this.uiLabel3.Text = "Проинвентаризировано ТМЦ:";
            // 
            // uiLabel4
            // 
            this.uiLabel4.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold);
            this.uiLabel4.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Right, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 168, 12);
            this.uiLabel4.Name = "uiLabel4";
            this.uiLabel4.TabIndex = 3;
            this.uiLabel4.Text = "Проинвентаризировано объектов:";
            // 
            // AllItems
            // 
            this.AllItems.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.AllItems.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 7, 13);
            this.AllItems.Name = "AllItems";
            this.AllItems.TabIndex = 4;
            this.AllItems.Text = "0";
            // 
            // AllObjects
            // 
            this.AllObjects.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.AllObjects.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 7, 13);
            this.AllObjects.Name = "AllObjects";
            this.AllObjects.TabIndex = 5;
            this.AllObjects.Text = "0";
            // 
            // ProcessedItems
            // 
            this.ProcessedItems.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.ProcessedItems.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 7, 13);
            this.ProcessedItems.Name = "ProcessedItems";
            this.ProcessedItems.TabIndex = 6;
            this.ProcessedItems.Text = "0";
            // 
            // ProcessedObjects
            // 
            this.ProcessedObjects.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.ProcessedObjects.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 7, 13);
            this.ProcessedObjects.Name = "ProcessedObjects";
            this.ProcessedObjects.TabIndex = 7;
            this.ProcessedObjects.Text = "0";
            // 
            // MainControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.Size = new System.Drawing.Size(240, 320);
            this.StatusText = "Выберите действие";
            this.ResumeLayout();

        }

        #endregion

        private Resco.UIElements.UIGridPanel MainMenuPanel;
        private Stock.Controls.StockButton InventoryBtn;
        private Stock.Controls.StockButton InformationBtn;
        private Stock.Controls.StockButton ObjectsBtn;
        private Stock.Controls.StockButton ExchangeBtn;
        private Stock.Controls.StockButton ExitBtn;
        private Resco.UIElements.UIGridPanel InventoryInformationPanel;
        private Resco.UIElements.UILabel uiLabel1;
        private Resco.UIElements.UILabel uiLabel2;
        private Resco.UIElements.UILabel uiLabel3;
        private Resco.UIElements.UILabel uiLabel4;
        private Resco.UIElements.UILabel AllItems;
        private Resco.UIElements.UILabel AllObjects;
        private Resco.UIElements.UILabel ProcessedItems;
        private Resco.UIElements.UILabel ProcessedObjects;

    }
}
