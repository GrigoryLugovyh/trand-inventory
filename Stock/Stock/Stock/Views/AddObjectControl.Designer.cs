﻿namespace Stock.Views
{
    partial class AddObjectControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AddObjectGridPanel = new Resco.UIElements.UIGridPanel();
            this.uiGridPanel1 = new Resco.UIElements.UIGridPanel();
            this.uiLabel1 = new Resco.UIElements.UILabel();
            this.BarcodeText = new Stock.Controls.StockTextEdit();
            this.uiTextBoxButton1 = new Resco.UIElements.UITextBoxButton();
            this.uiGridPanel2 = new Resco.UIElements.UIGridPanel();
            this.uiLabel2 = new Resco.UIElements.UILabel();
            this.CodeText = new Stock.Controls.StockTextEdit();
            this.uiTextBoxButton2 = new Resco.UIElements.UITextBoxButton();
            this.uiGridPanel3 = new Resco.UIElements.UIGridPanel();
            this.uiLabel3 = new Resco.UIElements.UILabel();
            this.NameText = new Stock.Controls.StockTextEdit();
            this.uiTextBoxButton3 = new Resco.UIElements.UITextBoxButton();
            this.uiGridPanel4 = new Resco.UIElements.UIGridPanel();
            this.uiLabel4 = new Resco.UIElements.UILabel();
            this.ParentText = new Stock.Controls.StockTextEdit();
            this.AddParentBtn = new Resco.UIElements.UITextBoxButton();
            this.DeleteParentBtn = new Resco.UIElements.UITextBoxButton();
            this.uiGridPanel5 = new Resco.UIElements.UIGridPanel();
            this.CancelBtn = new Stock.Controls.StockButton();
            this.AddBtn = new Stock.Controls.StockButton();
            this.SuspendLayout();
            // 
            // StockGridPanel
            // 
            this.StockGridPanel.Children.Add(this.AddObjectGridPanel, 0, 1);
            // 
            // HeaderLabel
            // 
            this.HeaderLabel.Text = "Создание объекта инвентаризации";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Text = "Укажите параметры объекта";
            // 
            // AddObjectGridPanel
            // 
            this.AddObjectGridPanel.Children.Add(this.uiGridPanel1, 0, 0);
            this.AddObjectGridPanel.Children.Add(this.uiGridPanel2, 0, 1);
            this.AddObjectGridPanel.Children.Add(this.uiGridPanel3, 0, 2);
            this.AddObjectGridPanel.Children.Add(this.uiGridPanel4, 0, 3);
            this.AddObjectGridPanel.Children.Add(this.uiGridPanel5, 0, 5);
            this.AddObjectGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.AddObjectGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 290);
            this.AddObjectGridPanel.Name = "AddObjectGridPanel";
            this.AddObjectGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(50, Resco.UIElements.GridLineValueType.Absolute, true));
            this.AddObjectGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(50, Resco.UIElements.GridLineValueType.Absolute, true));
            this.AddObjectGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(50, Resco.UIElements.GridLineValueType.Absolute, true));
            this.AddObjectGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(50, Resco.UIElements.GridLineValueType.Absolute, true));
            this.AddObjectGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.AddObjectGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(30, Resco.UIElements.GridLineValueType.Absolute, true));
            this.AddObjectGridPanel.TabIndex = 1;
            // 
            // uiGridPanel1
            // 
            this.uiGridPanel1.Children.Add(this.uiLabel1, 0, 0);
            this.uiGridPanel1.Children.Add(this.BarcodeText, 0, 1);
            this.uiGridPanel1.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel1.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 50);
            this.uiGridPanel1.Name = "uiGridPanel1";
            this.uiGridPanel1.Rows.Add(new Resco.UIElements.GridLinePosition(20, Resco.UIElements.GridLineValueType.Absolute, true));
            this.uiGridPanel1.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.uiLabel1.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 138, 17);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Text = "Штрих-код объекта";
            // 
            // BarcodeText
            // 
            this.BarcodeText.Buttons.Add(this.uiTextBoxButton1);
            this.BarcodeText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 5, 2, 5, 2, 230, 26);
            this.BarcodeText.Name = "BarcodeText";
            this.BarcodeText.TabIndex = 1;
            // 
            // uiTextBoxButton1
            // 
            this.uiTextBoxButton1.Action = Resco.UIElements.TextBoxAction.Clear;
            this.uiTextBoxButton1.BackColor = System.Drawing.Color.Transparent;
            this.uiTextBoxButton1.BorderThickness = 0;
            this.uiTextBoxButton1.HorizontalAlignment = Resco.UIElements.HAlignment.Right;
            this.uiTextBoxButton1.Name = "uiTextBoxButton1";
            this.uiTextBoxButton1.PressedBackground.BackColor = System.Drawing.Color.Transparent;
            this.uiTextBoxButton1.Size = new System.Drawing.Size(18, 18);
            this.uiTextBoxButton1.StateIcon = Resco.UIElements.StateIcon.Delete;
            this.uiTextBoxButton1.VisibleMode = Resco.UIElements.UITextBoxButtonVisibleMode.WhileEditing;
            // 
            // uiGridPanel2
            // 
            this.uiGridPanel2.Children.Add(this.uiLabel2, 0, 0);
            this.uiGridPanel2.Children.Add(this.CodeText, 0, 1);
            this.uiGridPanel2.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel2.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 50);
            this.uiGridPanel2.Name = "uiGridPanel2";
            this.uiGridPanel2.Rows.Add(new Resco.UIElements.GridLinePosition(20, Resco.UIElements.GridLineValueType.Absolute, true));
            this.uiGridPanel2.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel2.TabIndex = 1;
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.uiLabel2.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 87, 17);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Text = "Код объекта";
            // 
            // CodeText
            // 
            this.CodeText.Buttons.Add(this.uiTextBoxButton2);
            this.CodeText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 5, 2, 5, 2, 230, 26);
            this.CodeText.Name = "CodeText";
            this.CodeText.TabIndex = 1;
            // 
            // uiTextBoxButton2
            // 
            this.uiTextBoxButton2.Action = Resco.UIElements.TextBoxAction.Clear;
            this.uiTextBoxButton2.BackColor = System.Drawing.Color.Transparent;
            this.uiTextBoxButton2.BorderThickness = 0;
            this.uiTextBoxButton2.HorizontalAlignment = Resco.UIElements.HAlignment.Right;
            this.uiTextBoxButton2.Name = "uiTextBoxButton2";
            this.uiTextBoxButton2.PressedBackground.BackColor = System.Drawing.Color.Transparent;
            this.uiTextBoxButton2.Size = new System.Drawing.Size(18, 18);
            this.uiTextBoxButton2.StateIcon = Resco.UIElements.StateIcon.Delete;
            this.uiTextBoxButton2.VisibleMode = Resco.UIElements.UITextBoxButtonVisibleMode.WhileEditing;
            // 
            // uiGridPanel3
            // 
            this.uiGridPanel3.Children.Add(this.uiLabel3, 0, 0);
            this.uiGridPanel3.Children.Add(this.NameText, 0, 1);
            this.uiGridPanel3.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel3.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 50);
            this.uiGridPanel3.Name = "uiGridPanel3";
            this.uiGridPanel3.Rows.Add(new Resco.UIElements.GridLinePosition(20, Resco.UIElements.GridLineValueType.Absolute, true));
            this.uiGridPanel3.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel3.TabIndex = 2;
            // 
            // uiLabel3
            // 
            this.uiLabel3.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.uiLabel3.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 127, 17);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Text = "Название объекта";
            // 
            // NameText
            // 
            this.NameText.Buttons.Add(this.uiTextBoxButton3);
            this.NameText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 5, 2, 5, 2, 230, 26);
            this.NameText.Name = "NameText";
            this.NameText.TabIndex = 1;
            // 
            // uiTextBoxButton3
            // 
            this.uiTextBoxButton3.Action = Resco.UIElements.TextBoxAction.Clear;
            this.uiTextBoxButton3.BackColor = System.Drawing.Color.Transparent;
            this.uiTextBoxButton3.BorderThickness = 0;
            this.uiTextBoxButton3.HorizontalAlignment = Resco.UIElements.HAlignment.Right;
            this.uiTextBoxButton3.Name = "uiTextBoxButton3";
            this.uiTextBoxButton3.PressedBackground.BackColor = System.Drawing.Color.Transparent;
            this.uiTextBoxButton3.Size = new System.Drawing.Size(18, 18);
            this.uiTextBoxButton3.StateIcon = Resco.UIElements.StateIcon.Delete;
            this.uiTextBoxButton3.VisibleMode = Resco.UIElements.UITextBoxButtonVisibleMode.WhileEditing;
            // 
            // uiGridPanel4
            // 
            this.uiGridPanel4.Children.Add(this.uiLabel4, 0, 0);
            this.uiGridPanel4.Children.Add(this.ParentText, 0, 1);
            this.uiGridPanel4.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel4.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 50);
            this.uiGridPanel4.Name = "uiGridPanel4";
            this.uiGridPanel4.Rows.Add(new Resco.UIElements.GridLinePosition(20, Resco.UIElements.GridLineValueType.Absolute, true));
            this.uiGridPanel4.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel4.TabIndex = 3;
            // 
            // uiLabel4
            // 
            this.uiLabel4.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.uiLabel4.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 121, 17);
            this.uiLabel4.Name = "uiLabel4";
            this.uiLabel4.Text = "Верхний уровень";
            // 
            // ParentText
            // 
            this.ParentText.Buttons.Add(this.AddParentBtn);
            this.ParentText.Buttons.Add(this.DeleteParentBtn);
            this.ParentText.IsFocusable = false;
            this.ParentText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 5, 2, 5, 2, 230, 26);
            this.ParentText.Name = "ParentText";
            this.ParentText.ReadOnly = true;
            this.ParentText.TabIndex = 1;
            // 
            // AddParentBtn
            // 
            this.AddParentBtn.HorizontalAlignment = Resco.UIElements.HAlignment.Right;
            this.AddParentBtn.Name = "AddParentBtn";
            this.AddParentBtn.Size = new System.Drawing.Size(16, 16);
            this.AddParentBtn.Text = "...";
            this.AddParentBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.AddParentBtnClick);
            // 
            // DeleteParentBtn
            // 
            this.DeleteParentBtn.HorizontalAlignment = Resco.UIElements.HAlignment.Right;
            this.DeleteParentBtn.Name = "DeleteParentBtn";
            this.DeleteParentBtn.Size = new System.Drawing.Size(16, 16);
            this.DeleteParentBtn.StateIcon = Resco.UIElements.StateIcon.Delete;
            this.DeleteParentBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.DeleteParentBtnClick);
            // 
            // uiGridPanel5
            // 
            this.uiGridPanel5.Children.Add(this.CancelBtn, 0, 0);
            this.uiGridPanel5.Children.Add(this.AddBtn, 1, 0);
            this.uiGridPanel5.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel5.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel5.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 30);
            this.uiGridPanel5.Name = "uiGridPanel5";
            this.uiGridPanel5.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel5.TabIndex = 4;
            // 
            // CancelBtn
            // 
            this.CancelBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 5, 2, 5, 3, 110, 25);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.RoundedCornerRadius = 3;
            this.CancelBtn.Text = "Отмена";
            this.CancelBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.CancelBtnClick);
            // 
            // AddBtn
            // 
            this.AddBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 5, 2, 5, 3, 110, 25);
            this.AddBtn.Name = "AddBtn";
            this.AddBtn.RoundedCornerRadius = 3;
            this.AddBtn.TabIndex = 1;
            this.AddBtn.Text = "Создать";
            this.AddBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.AddBtnClick);
            // 
            // AddObjectControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.HeaderText = "Создание объекта инвентаризации";
            this.Size = new System.Drawing.Size(240, 320);
            this.StatusText = "Укажите параметры объекта";
            this.ResumeLayout();

        }

        #endregion

        private Resco.UIElements.UIGridPanel AddObjectGridPanel;
        private Resco.UIElements.UIGridPanel uiGridPanel1;
        private Resco.UIElements.UIGridPanel uiGridPanel2;
        private Resco.UIElements.UIGridPanel uiGridPanel3;
        private Resco.UIElements.UIGridPanel uiGridPanel4;
        private Resco.UIElements.UIGridPanel uiGridPanel5;
        private Stock.Controls.StockButton CancelBtn;
        private Stock.Controls.StockButton AddBtn;
        private Resco.UIElements.UILabel uiLabel1;
        private Resco.UIElements.UILabel uiLabel2;
        private Resco.UIElements.UILabel uiLabel3;
        private Resco.UIElements.UILabel uiLabel4;
        private Stock.Controls.StockTextEdit BarcodeText;
        private Resco.UIElements.UITextBoxButton uiTextBoxButton1;
        private Stock.Controls.StockTextEdit CodeText;
        private Resco.UIElements.UITextBoxButton uiTextBoxButton2;
        private Stock.Controls.StockTextEdit NameText;
        private Resco.UIElements.UITextBoxButton uiTextBoxButton3;
        private Stock.Controls.StockTextEdit ParentText;
        private Resco.UIElements.UITextBoxButton AddParentBtn;
        private Resco.UIElements.UITextBoxButton DeleteParentBtn;
    }
}
