﻿using Stock.Controls;

namespace Stock.Views
{
    partial class ObjectParametersControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ParametersGridPanel = new Resco.UIElements.UIGridPanel();
            this.uiLabel1 = new Resco.UIElements.UILabel();
            this.uiLabel2 = new Resco.UIElements.UILabel();
            this.ObjectNameText = new Resco.UIElements.UILabel();
            this.MolNameText = new Resco.UIElements.UILabel();
            this.MolGridPanel = new Resco.UIElements.UIGridPanel();
            this.FindMolBtn = new Stock.Controls.StockButton();
            this.FilterMolText = new Stock.Controls.StockTextEdit();
            this.uiTextBoxButton1 = new Resco.UIElements.UITextBoxButton();
            this.SelectMolBtn = new Stock.Controls.StockButton();
            this.uiLabel5 = new Resco.UIElements.UILabel();
            this.UserNameText = new Resco.UIElements.UILabel();
            this.ItemGridPanel = new Resco.UIElements.UIGridPanel();
            this.FindUserBtn = new Stock.Controls.StockButton();
            this.FilterUserText = new Stock.Controls.StockTextEdit();
            this.uiTextBoxButton2 = new Resco.UIElements.UITextBoxButton();
            this.SelectUserBtn = new Stock.Controls.StockButton();
            this.BottomGridPanel = new Resco.UIElements.UIGridPanel();
            this.CancelBtn = new Stock.Controls.StockButton();
            this.OkBtn = new Stock.Controls.StockButton();
            this.SuspendLayout();
            // 
            // StockGridPanel
            // 
            this.StockGridPanel.Children.Add(this.ParametersGridPanel, 0, 1);
            // 
            // HeaderLabel
            // 
            this.HeaderLabel.Text = "Параметры объекта";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold);
            this.StatusLabel.Text = "Параметры объекта инвентаризации";
            // 
            // ParametersGridPanel
            // 
            this.ParametersGridPanel.Children.Add(this.uiLabel1, 0, 0);
            this.ParametersGridPanel.Children.Add(this.uiLabel2, 0, 3);
            this.ParametersGridPanel.Children.Add(this.ObjectNameText, 0, 1);
            this.ParametersGridPanel.Children.Add(this.MolNameText, 0, 4);
            this.ParametersGridPanel.Children.Add(this.MolGridPanel, 0, 5);
            this.ParametersGridPanel.Children.Add(this.SelectMolBtn, 0, 6);
            this.ParametersGridPanel.Children.Add(this.uiLabel5, 0, 8);
            this.ParametersGridPanel.Children.Add(this.UserNameText, 0, 9);
            this.ParametersGridPanel.Children.Add(this.ItemGridPanel, 0, 10);
            this.ParametersGridPanel.Children.Add(this.SelectUserBtn, 0, 11);
            this.ParametersGridPanel.Children.Add(this.BottomGridPanel, 0, 13);
            this.ParametersGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ParametersGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 290);
            this.ParametersGridPanel.Name = "ParametersGridPanel";
            this.ParametersGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(20, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ParametersGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(20, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ParametersGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(2, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ParametersGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(20, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ParametersGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(20, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ParametersGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(0, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ParametersGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(30, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ParametersGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(2, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ParametersGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(20, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ParametersGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(20, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ParametersGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(0, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ParametersGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(30, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ParametersGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ParametersGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(30, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ParametersGridPanel.TabIndex = 1;
            // 
            // uiLabel1
            // 
            this.uiLabel1.AutoSize = false;
            this.uiLabel1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.uiLabel1.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 20);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Padding = new Resco.UIElements.Thickness(2);
            this.uiLabel1.Text = "Объект:";
            // 
            // uiLabel2
            // 
            this.uiLabel2.AutoSize = false;
            this.uiLabel2.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.uiLabel2.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 20);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Padding = new Resco.UIElements.Thickness(2);
            this.uiLabel2.TabIndex = 1;
            this.uiLabel2.Text = "МОЛ:";
            // 
            // ObjectNameText
            // 
            this.ObjectNameText.AutoSize = false;
            this.ObjectNameText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 2, 2, 2, 2, 236, 16);
            this.ObjectNameText.Name = "ObjectNameText";
            this.ObjectNameText.TabIndex = 2;
            this.ObjectNameText.Text = "не выбран";
            // 
            // MolNameText
            // 
            this.MolNameText.AutoSize = false;
            this.MolNameText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 2, 2, 2, 2, 236, 16);
            this.MolNameText.Name = "MolNameText";
            this.MolNameText.TabIndex = 3;
            this.MolNameText.Text = "не выбран";
            // 
            // MolGridPanel
            // 
            this.MolGridPanel.Children.Add(this.FindMolBtn, 1, 0);
            this.MolGridPanel.Children.Add(this.FilterMolText, 0, 0);
            this.MolGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.MolGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(70, Resco.UIElements.GridLineValueType.Absolute, true));
            this.MolGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 0, 0);
            this.MolGridPanel.Name = "MolGridPanel";
            this.MolGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.MolGridPanel.TabIndex = 4;
            // 
            // FindMolBtn
            // 
            this.FindMolBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 3, 3, 3, 3, -6, -6);
            this.FindMolBtn.Name = "FindMolBtn";
            this.FindMolBtn.RoundedCornerRadius = 3;
            this.FindMolBtn.Text = "Поиск";
            this.FindMolBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.FindMolBtnClick);
            // 
            // FilterMolText
            // 
            this.FilterMolText.Buttons.Add(this.uiTextBoxButton1);
            this.FilterMolText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 3, 3, 3, 3, -6, -6);
            this.FilterMolText.Name = "FilterMolText";
            this.FilterMolText.RoundedCornerRadius = 3;
            this.FilterMolText.TabIndex = 1;
            // 
            // uiTextBoxButton1
            // 
            this.uiTextBoxButton1.Action = Resco.UIElements.TextBoxAction.Clear;
            this.uiTextBoxButton1.BackColor = System.Drawing.Color.Transparent;
            this.uiTextBoxButton1.BorderThickness = 0;
            this.uiTextBoxButton1.HorizontalAlignment = Resco.UIElements.HAlignment.Right;
            this.uiTextBoxButton1.Name = "uiTextBoxButton1";
            this.uiTextBoxButton1.PressedBackground.BackColor = System.Drawing.Color.Transparent;
            this.uiTextBoxButton1.Size = new System.Drawing.Size(18, 18);
            this.uiTextBoxButton1.StateIcon = Resco.UIElements.StateIcon.Delete;
            this.uiTextBoxButton1.VisibleMode = Resco.UIElements.UITextBoxButtonVisibleMode.WhileEditing;
            // 
            // SelectMolBtn
            // 
            this.SelectMolBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 3, 3, 3, 3, 234, 24);
            this.SelectMolBtn.Name = "SelectMolBtn";
            this.SelectMolBtn.RoundedCornerRadius = 3;
            this.SelectMolBtn.TabIndex = 5;
            this.SelectMolBtn.Text = "Выбрать из справочника";
            this.SelectMolBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.SelectMolBtnClick);
            // 
            // uiLabel5
            // 
            this.uiLabel5.AutoSize = false;
            this.uiLabel5.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.uiLabel5.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 2, 2, 2, 2, 236, 16);
            this.uiLabel5.Name = "uiLabel5";
            this.uiLabel5.TabIndex = 6;
            this.uiLabel5.Text = "Пользователь ТМЦ:";
            // 
            // UserNameText
            // 
            this.UserNameText.AutoSize = false;
            this.UserNameText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 2, 2, 2, 2, 236, 16);
            this.UserNameText.Name = "UserNameText";
            this.UserNameText.TabIndex = 7;
            this.UserNameText.Text = "не выбран";
            // 
            // ItemGridPanel
            // 
            this.ItemGridPanel.Children.Add(this.FindUserBtn, 1, 0);
            this.ItemGridPanel.Children.Add(this.FilterUserText, 0, 0);
            this.ItemGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ItemGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(70, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ItemGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 0, 0);
            this.ItemGridPanel.Name = "ItemGridPanel";
            this.ItemGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ItemGridPanel.TabIndex = 8;
            // 
            // FindUserBtn
            // 
            this.FindUserBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 3, 3, 3, 3, -6, -6);
            this.FindUserBtn.Name = "FindUserBtn";
            this.FindUserBtn.RoundedCornerRadius = 3;
            this.FindUserBtn.Text = "Поиск";
            this.FindUserBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.FindUserBtnClick);
            // 
            // FilterUserText
            // 
            this.FilterUserText.Buttons.Add(this.uiTextBoxButton2);
            this.FilterUserText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 3, 3, 3, 3, -6, -6);
            this.FilterUserText.Name = "FilterUserText";
            this.FilterUserText.RoundedCornerRadius = 3;
            this.FilterUserText.TabIndex = 1;
            // 
            // uiTextBoxButton2
            // 
            this.uiTextBoxButton2.Action = Resco.UIElements.TextBoxAction.Clear;
            this.uiTextBoxButton2.BackColor = System.Drawing.Color.Transparent;
            this.uiTextBoxButton2.BorderThickness = 0;
            this.uiTextBoxButton2.HorizontalAlignment = Resco.UIElements.HAlignment.Right;
            this.uiTextBoxButton2.Name = "uiTextBoxButton2";
            this.uiTextBoxButton2.PressedBackground.BackColor = System.Drawing.Color.Transparent;
            this.uiTextBoxButton2.Size = new System.Drawing.Size(18, 18);
            this.uiTextBoxButton2.StateIcon = Resco.UIElements.StateIcon.Delete;
            this.uiTextBoxButton2.VisibleMode = Resco.UIElements.UITextBoxButtonVisibleMode.WhileEditing;
            // 
            // SelectUserBtn
            // 
            this.SelectUserBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 3, 3, 3, 3, 234, 24);
            this.SelectUserBtn.Name = "SelectUserBtn";
            this.SelectUserBtn.RoundedCornerRadius = 3;
            this.SelectUserBtn.TabIndex = 9;
            this.SelectUserBtn.Text = "Выбрать из справочника";
            this.SelectUserBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.SelectUserBtnClick);
            // 
            // BottomGridPanel
            // 
            this.BottomGridPanel.Children.Add(this.CancelBtn, 0, 0);
            this.BottomGridPanel.Children.Add(this.OkBtn, 1, 0);
            this.BottomGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.BottomGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.BottomGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 30);
            this.BottomGridPanel.Name = "BottomGridPanel";
            this.BottomGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.BottomGridPanel.TabIndex = 10;
            // 
            // CancelBtn
            // 
            this.CancelBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 5, 2, 5, 3, 110, 25);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.RoundedCornerRadius = 3;
            this.CancelBtn.Text = "Отмена";
            this.CancelBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.CancelBtnClick);
            // 
            // OkBtn
            // 
            this.OkBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 5, 2, 5, 3, 110, 25);
            this.OkBtn.Name = "OkBtn";
            this.OkBtn.RoundedCornerRadius = 3;
            this.OkBtn.TabIndex = 1;
            this.OkBtn.Text = "Продолжить";
            this.OkBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.OkBtnClick);
            // 
            // ObjectParametersControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.HeaderText = "Параметры объекта";
            this.Size = new System.Drawing.Size(240, 320);
            this.StatusText = "Параметры объекта инвентаризации";
            this.ResumeLayout();

        }

        #endregion

        private Resco.UIElements.UIGridPanel ParametersGridPanel;
        private Resco.UIElements.UILabel uiLabel1;
        private Resco.UIElements.UILabel uiLabel2;
        private Resco.UIElements.UILabel ObjectNameText;
        private Resco.UIElements.UILabel MolNameText;
        private Resco.UIElements.UIGridPanel MolGridPanel;
        private Stock.Controls.StockButton SelectMolBtn;
        private Stock.Controls.StockButton FindMolBtn;
        private Stock.Controls.StockTextEdit FilterMolText;
        private Resco.UIElements.UITextBoxButton uiTextBoxButton1;
        private Resco.UIElements.UILabel uiLabel5;
        private Resco.UIElements.UILabel UserNameText;
        private Resco.UIElements.UIGridPanel ItemGridPanel;
        private Stock.Controls.StockButton FindUserBtn;
        private Stock.Controls.StockButton SelectUserBtn;
        private StockTextEdit FilterUserText;
        private Resco.UIElements.UITextBoxButton uiTextBoxButton2;
        private Resco.UIElements.UIGridPanel BottomGridPanel;
        private Stock.Controls.StockButton CancelBtn;
        private Stock.Controls.StockButton OkBtn;
    }
}
