﻿using System;
using System.Collections.Generic;
using Resco.UIElements;
using Stock.Views.Personals;
using StockCore.Entity;
using StockCore.Interfaces.Views;

namespace Stock.Views
{
    public partial class UsersControl : StockControl, ISelectPersonalView<Users>
    {
        public UsersControl()
        {
            InitializeComponent();
        }

        public void ShowView()
        {
            PersonalListView.DataTemplate = new NormalPersonalControl();
            PersonalListView.SelectedDataTemplate = new SelectPersonalControl();

            if (Show != null)
            {
                Show(this, EventArgs.Empty);
            }

            ShowView<UsersControl>(AnimationEffect.MoveLeft);

            Invoker(() => CancelBtn.Focus());
        }

        public void CloseView()
        {
            if (Close != null)
            {
                Close(this, EventArgs.Empty);
            }
        }

        public void CloseToObjects()
        {
            ShowView<ObjectParametersControl>(AnimationEffect.MoveRight);
        }

        public void CloseToInventory()
        {
            ShowView<InventoryControl>(AnimationEffect.MoveRight);
        }

        public event EventHandler Show;

        public event EventHandler Close;

        public event EventHandler Filter;

        public event EventHandler Select;

        public event EventHandler SelectedChange;

        public string FilterValue
        {
            get { return FilterText.Text; }
            set { Invoker(() => FilterText.Text = value); }
        }

        public Users SelectPersonal { get; protected set; }

        public string PersonalName
        {
            get { return PersonalNameText.Text; }
            set { Invoker(() => PersonalNameText.Text = value); }
        }

        public List<Users> Personals
        {
            get { return PersonalListView.DataSource as List<Users>; }
            set { Invoker(() => PersonalListView.DataSource = value); }
        }

        private void PersonalListViewSelectedIndexChanging(object sender, SelectedIndexChangingEventArgs e)
        {
            if (Personals != null && Personals.Count > 0 && e.NewIndex > -1)
            {
                SelectPersonal = Personals[e.NewIndex];
                PersonalName = SelectPersonal.UserName;
            }
            else
            {
                SelectPersonal = null;
                PersonalName = string.Empty;
            }

            if (SelectedChange != null)
            {
                SelectedChange(sender, e);
            }
        }

        private void CancelBtnClick(object sender, UIMouseEventArgs e)
        {
            CloseView();
        }

        private void FilterBtnClick(object sender, UIMouseEventArgs e)
        {
            if (Filter != null)
            {
                Filter(sender, e);
            }
        }

        private void SelectBtnClick(object sender, UIMouseEventArgs e)
        {
            if(Select != null)
            {
                Select(sender, e);
            }
        }
    }
}
