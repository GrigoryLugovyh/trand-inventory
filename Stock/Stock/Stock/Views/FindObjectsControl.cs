﻿using System;
using System.Collections.Generic;
using Resco.UIElements;
using Stock.Views.InventoryObjects;
using StockCore.Entity;
using StockCore.Interfaces.Views;

namespace Stock.Views
{
    public partial class FindObjectsControl : StockControl, IFindObjectView
    {
        public FindObjectsControl()
        {
            InitializeComponent();
        }

        public event EventHandler Show;

        public event EventHandler Close;

        public event EventHandler Filter;

        public event EventHandler Select;

        public void ShowView()
        {
            ObjectsListView.DataTemplate = new NormalObjectControl();
            ObjectsListView.SelectedDataTemplate = new SelectObjectControl();

            if(Show != null)
            {
                Show(this, EventArgs.Empty);
            }

            ShowView<FindObjectsControl>(AnimationEffect.MoveLeft);
        }

        public void CloseView()
        {
            if(Close != null)
            {
                Close(this, EventArgs.Empty);
            }

            ShowView<AddObjectControl>(AnimationEffect.MoveRight);

            Invoker(() => SelectBtn.Focus());
        }

        public string FilterEdit
        {
            get { return ObjectsFilterText.Text; }
            set { Invoker(() => ObjectsFilterText.Text = value); }
        }

        public Objects SelectedObject
        {
            get { return ObjectsListView.SelectedItem as Objects; }
            set { Invoker(() => ObjectsListView.SelectedItem = value); }
        }

        public List<Objects> Objects
        {
            get { return ObjectsListView.DataSource as List<Objects>; }
            set { Invoker(() => ObjectsListView.DataSource = value); }
        }

        private void FilterBtnClick(object sender, UIMouseEventArgs e)
        {
            if(Filter != null)
            {
                Filter(sender, e);
            }
        }

        private void SelectBtnClick(object sender, UIMouseEventArgs e)
        {
            if(Select != null)
            {
                Select(sender, e);
            }
        }

        private void CancelBtnClick(object sender, UIMouseEventArgs e)
        {
            CloseView();
        }
    }
}