﻿using Resco.UIElements;

namespace Stock.Views.Items
{
    public partial class ItemSelectedControl : UIUserPanel
    {
        public ItemSelectedControl()
        {
            InitializeComponent();
        }

        public bool SelectedVisible
        {
            get { return ItemGridPanel.Columns[0].Value > 0; }
            set { ItemGridPanel.Columns[0].Value = value ? 20 : 0; }
        }
    }
}
