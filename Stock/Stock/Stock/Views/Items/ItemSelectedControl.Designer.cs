﻿using Stock.Controls;

namespace Stock.Views.Items
{
    partial class ItemSelectedControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ItemSelectedGridPanel = new Resco.UIElements.UIGridPanel();
            this.ItemGridPanel = new Resco.UIElements.UIGridPanel();
            this.SelectedCheck = new Stock.Controls.StockCheckBox();
            this.ItemCode = new Resco.UIElements.UILabel();
            this.ItemDetailsGridPanel = new Resco.UIElements.UIGridPanel();
            this.uiLabel1 = new Resco.UIElements.UILabel();
            this.uiLabel2 = new Resco.UIElements.UILabel();
            this.uiLabel3 = new Resco.UIElements.UILabel();
            this.uiLabel4 = new Resco.UIElements.UILabel();
            this.uiTextBox1 = new Stock.Controls.StockTextEdit();
            this.uiTextBox2 = new Stock.Controls.StockTextEdit();
            this.uiTextBox3 = new Stock.Controls.StockTextEdit();
            this.uiTextBox4 = new Stock.Controls.StockTextEdit();
            this.SuspendLayout();
            // 
            // ItemSelectedGridPanel
            // 
            this.ItemSelectedGridPanel.Children.Add(this.ItemGridPanel, 0, 0);
            this.ItemSelectedGridPanel.Children.Add(this.ItemDetailsGridPanel, 0, 1);
            this.ItemSelectedGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ItemSelectedGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 400, 100);
            this.ItemSelectedGridPanel.Name = "ItemSelectedGridPanel";
            this.ItemSelectedGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(25, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ItemSelectedGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            // 
            // ItemGridPanel
            // 
            this.ItemGridPanel.Children.Add(this.SelectedCheck, 0, 0);
            this.ItemGridPanel.Children.Add(this.ItemCode, 1, 0);
            this.ItemGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(20, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ItemGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ItemGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 400, 25);
            this.ItemGridPanel.Name = "ItemGridPanel";
            this.ItemGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            // 
            // SelectedCheck
            // 
            this.SelectedCheck.DataBindings.Add(new Resco.UIElements.ElementBinding("Selected", "Selected", System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, false));
            this.SelectedCheck.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 29, 29);
            this.SelectedCheck.Name = "SelectedCheck";
            this.SelectedCheck.TabIndex = 2;
            // 
            // ItemCode
            // 
            this.ItemCode.AutoSize = false;
            this.ItemCode.DataBindings.Add(new Resco.UIElements.ElementBinding("Text", "Id", System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, false));
            this.ItemCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.ItemCode.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 5, 2, 2, 2, 373, 21);
            this.ItemCode.Name = "ItemCode";
            this.ItemCode.TabIndex = 1;
            this.ItemCode.TextAlignment = Resco.Drawing.Alignment.MiddleLeft;
            // 
            // ItemDetailsGridPanel
            // 
            this.ItemDetailsGridPanel.BackColor = System.Drawing.Color.Gainsboro;
            this.ItemDetailsGridPanel.BorderThickness = 1;
            this.ItemDetailsGridPanel.Children.Add(this.uiLabel1, 0, 0);
            this.ItemDetailsGridPanel.Children.Add(this.uiLabel2, 0, 1);
            this.ItemDetailsGridPanel.Children.Add(this.uiLabel3, 0, 2);
            this.ItemDetailsGridPanel.Children.Add(this.uiLabel4, 0, 3);
            this.ItemDetailsGridPanel.Children.Add(this.uiTextBox1, 1, 0);
            this.ItemDetailsGridPanel.Children.Add(this.uiTextBox2, 1, 1);
            this.ItemDetailsGridPanel.Children.Add(this.uiTextBox3, 1, 2);
            this.ItemDetailsGridPanel.Children.Add(this.uiTextBox4, 1, 3);
            this.ItemDetailsGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(90, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ItemDetailsGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ItemDetailsGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 400, 75);
            this.ItemDetailsGridPanel.Name = "ItemDetailsGridPanel";
            this.ItemDetailsGridPanel.Padding = new Resco.UIElements.Thickness(1);
            this.ItemDetailsGridPanel.RoundedCornerRadius = 3;
            this.ItemDetailsGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ItemDetailsGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ItemDetailsGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ItemDetailsGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ItemDetailsGridPanel.TabIndex = 1;
            // 
            // uiLabel1
            // 
            this.uiLabel1.AutoSize = false;
            this.uiLabel1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.uiLabel1.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 2, 2, 2, 2, 86, 13);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Text = "Наименование";
            this.uiLabel1.TextAlignment = Resco.Drawing.Alignment.MiddleLeft;
            // 
            // uiLabel2
            // 
            this.uiLabel2.AutoSize = false;
            this.uiLabel2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.uiLabel2.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 2, 2, 2, 2, 86, 13);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.TabIndex = 1;
            this.uiLabel2.Text = "Статус";
            this.uiLabel2.TextAlignment = Resco.Drawing.Alignment.MiddleLeft;
            // 
            // uiLabel3
            // 
            this.uiLabel3.AutoSize = false;
            this.uiLabel3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.uiLabel3.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 2, 2, 2, 2, 86, 13);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.TabIndex = 2;
            this.uiLabel3.Text = "Пользователь";
            this.uiLabel3.TextAlignment = Resco.Drawing.Alignment.MiddleLeft;
            // 
            // uiLabel4
            // 
            this.uiLabel4.AutoSize = false;
            this.uiLabel4.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.uiLabel4.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 2, 2, 2, 2, 86, 13);
            this.uiLabel4.Name = "uiLabel4";
            this.uiLabel4.TabIndex = 3;
            this.uiLabel4.Text = "МОЛ";
            this.uiLabel4.TextAlignment = Resco.Drawing.Alignment.MiddleLeft;
            // 
            // uiTextBox1
            // 
            this.uiTextBox1.BorderThickness = 0;
            this.uiTextBox1.DataBindings.Add(new Resco.UIElements.ElementBinding("Text", "Name", System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, false));
            this.uiTextBox1.DefaultTextColor = System.Drawing.Color.Black;
            this.uiTextBox1.IsFocusable = false;
            this.uiTextBox1.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 306, 17);
            this.uiTextBox1.Name = "uiTextBox1";
            this.uiTextBox1.ReadOnly = true;
            this.uiTextBox1.TabIndex = 4;
            // 
            // uiTextBox2
            // 
            this.uiTextBox2.BorderThickness = 0;
            this.uiTextBox2.DataBindings.Add(new Resco.UIElements.ElementBinding("Text", "StatusFriendly", System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, false));
            this.uiTextBox2.IsFocusable = false;
            this.uiTextBox2.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 306, 17);
            this.uiTextBox2.Name = "uiTextBox2";
            this.uiTextBox2.ReadOnly = true;
            this.uiTextBox2.TabIndex = 5;
            // 
            // uiTextBox3
            // 
            this.uiTextBox3.BorderThickness = 0;
            this.uiTextBox3.DataBindings.Add(new Resco.UIElements.ElementBinding("Text", "UserName", System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, false));
            this.uiTextBox3.IsFocusable = false;
            this.uiTextBox3.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 306, 17);
            this.uiTextBox3.Name = "uiTextBox3";
            this.uiTextBox3.ReadOnly = true;
            this.uiTextBox3.TabIndex = 6;
            // 
            // uiTextBox4
            // 
            this.uiTextBox4.BorderThickness = 0;
            this.uiTextBox4.DataBindings.Add(new Resco.UIElements.ElementBinding("Text", "MolName", System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, false));
            this.uiTextBox4.IsFocusable = false;
            this.uiTextBox4.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 306, 17);
            this.uiTextBox4.Name = "uiTextBox4";
            this.uiTextBox4.ReadOnly = true;
            this.uiTextBox4.TabIndex = 7;
            // 
            // ItemSelectedControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.Children.Add(this.ItemSelectedGridPanel);
            this.Size = new System.Drawing.Size(400, 100);
            this.ResumeLayout();

        }

        #endregion

        private Resco.UIElements.UIGridPanel ItemSelectedGridPanel;
        private Resco.UIElements.UIGridPanel ItemGridPanel;
        private StockCheckBox SelectedCheck;
        private Resco.UIElements.UILabel ItemCode;
        private Resco.UIElements.UIGridPanel ItemDetailsGridPanel;
        private Resco.UIElements.UILabel uiLabel1;
        private Resco.UIElements.UILabel uiLabel2;
        private Resco.UIElements.UILabel uiLabel3;
        private Resco.UIElements.UILabel uiLabel4;
        private Stock.Controls.StockTextEdit uiTextBox1;
        private Stock.Controls.StockTextEdit uiTextBox2;
        private Stock.Controls.StockTextEdit uiTextBox3;
        private Stock.Controls.StockTextEdit uiTextBox4;

    }
}
