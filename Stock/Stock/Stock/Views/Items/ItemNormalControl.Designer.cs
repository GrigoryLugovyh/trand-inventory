﻿using Stock.Controls;

namespace Stock.Views.Items
{
    partial class ItemNormalControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ItemNormalGridPanel = new Resco.UIElements.UIGridPanel();
            this.SelectedCheck = new Stock.Controls.StockCheckBox();
            this.ItemCode = new Resco.UIElements.UILabel();
            this.ItemName = new Resco.UIElements.UILabel();
            this.SuspendLayout();
            // 
            // ItemNormalGridPanel
            // 
            this.ItemNormalGridPanel.Children.Add(this.SelectedCheck, 0, 0);
            this.ItemNormalGridPanel.Children.Add(this.ItemCode, 1, 0);
            this.ItemNormalGridPanel.Children.Add(this.ItemName, 2, 0);
            this.ItemNormalGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(20, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ItemNormalGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(120, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ItemNormalGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ItemNormalGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 400, 25);
            this.ItemNormalGridPanel.Name = "ItemNormalGridPanel";
            this.ItemNormalGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            // 
            // SelectedCheck
            // 
            this.SelectedCheck.DataBindings.Add(new Resco.UIElements.ElementBinding("Selected", "Selected", System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, false));
            this.SelectedCheck.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 29, 29);
            this.SelectedCheck.Name = "SelectedCheck";
            // 
            // ItemCode
            // 
            this.ItemCode.AutoSize = false;
            this.ItemCode.DataBindings.Add(new Resco.UIElements.ElementBinding("Text", "Id", System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, false));
            this.ItemCode.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 5, 2, 2, 2, 113, 21);
            this.ItemCode.Name = "ItemCode";
            this.ItemCode.TabIndex = 1;
            this.ItemCode.TextAlignment = Resco.Drawing.Alignment.MiddleLeft;
            // 
            // ItemName
            // 
            this.ItemName.AutoSize = false;
            this.ItemName.DataBindings.Add(new Resco.UIElements.ElementBinding("Text", "Name", System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, false));
            this.ItemName.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 2, 2, 2, 2, 256, 21);
            this.ItemName.Name = "ItemName";
            this.ItemName.TabIndex = 2;
            this.ItemName.TextAlignment = Resco.Drawing.Alignment.MiddleLeft;
            // 
            // ItemNormalControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.Children.Add(this.ItemNormalGridPanel);
            this.Size = new System.Drawing.Size(400, 25);
            this.ResumeLayout();

        }

        #endregion

        private Resco.UIElements.UIGridPanel ItemNormalGridPanel;
        private StockCheckBox SelectedCheck;
        private Resco.UIElements.UILabel ItemCode;
        private Resco.UIElements.UILabel ItemName;
    }
}
