﻿using Resco.UIElements;

namespace Stock.Views.Items
{
    public partial class ItemNormalControl : UIUserPanel
    {
        public ItemNormalControl()
        {
            InitializeComponent();
        }

        public bool SelectedVisible
        {
            get { return ItemNormalGridPanel.Columns[0].Value > 0; }
            set { ItemNormalGridPanel.Columns[0].Value = value ? 20 : 0; }
        }
    }
}
