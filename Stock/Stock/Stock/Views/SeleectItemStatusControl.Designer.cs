﻿namespace Stock.Views
{
    partial class SeleectItemStatusControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SelectStatusGridPanel = new Resco.UIElements.UIGridPanel();
            this.uiLabel1 = new Resco.UIElements.UILabel();
            this.StatusesBox = new Resco.UIElements.UIComboBox();
            this.SelectStatusBottomGridPanel = new Resco.UIElements.UIGridPanel();
            this.ChangeBtn = new Stock.Controls.StockButton();
            this.SuspendLayout();
            // 
            // StockGridPanel
            // 
            this.StockGridPanel.Children.Add(this.SelectStatusGridPanel, 0, 1);
            // 
            // HeaderLabel
            // 
            this.HeaderLabel.Text = "Выберите статус ТМЦ";
            // 
            // SelectStatusGridPanel
            // 
            this.SelectStatusGridPanel.Children.Add(this.uiLabel1, 0, 0);
            this.SelectStatusGridPanel.Children.Add(this.StatusesBox, 0, 1);
            this.SelectStatusGridPanel.Children.Add(this.SelectStatusBottomGridPanel, 0, 3);
            this.SelectStatusGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.SelectStatusGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 290);
            this.SelectStatusGridPanel.Name = "SelectStatusGridPanel";
            this.SelectStatusGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.SelectStatusGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(50, Resco.UIElements.GridLineValueType.Absolute, true));
            this.SelectStatusGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.SelectStatusGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(30, Resco.UIElements.GridLineValueType.Absolute, true));
            this.SelectStatusGridPanel.TabIndex = 1;
            // 
            // uiLabel1
            // 
            this.uiLabel1.AutoSize = false;
            this.uiLabel1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.uiLabel1.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 105);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Text = "Указанная ТМЦ уже была найдена\r\nв текущем месторасположении.\r\nВыберите новый стат" +
                "ус ТМЦ:";
            this.uiLabel1.TextAlignment = Resco.Drawing.Alignment.MiddleCenter;
            // 
            // StatusesBox
            // 
            this.StatusesBox.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 10, 10, 10, 10, 220, 30);
            this.StatusesBox.Name = "StatusesBox";
            this.StatusesBox.TabIndex = 1;
            // 
            // SelectStatusBottomGridPanel
            // 
            this.SelectStatusBottomGridPanel.Children.Add(this.ChangeBtn, 1, 0);
            this.SelectStatusBottomGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.SelectStatusBottomGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Absolute, true));
            this.SelectStatusBottomGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.SelectStatusBottomGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 30);
            this.SelectStatusBottomGridPanel.Name = "SelectStatusBottomGridPanel";
            this.SelectStatusBottomGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.SelectStatusBottomGridPanel.TabIndex = 2;
            // 
            // ChangeBtn
            // 
            this.ChangeBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 5, 2, 5, 3, 90, 25);
            this.ChangeBtn.Name = "ChangeBtn";
            this.ChangeBtn.RoundedCornerRadius = 3;
            this.ChangeBtn.Text = "Изменить";
            this.ChangeBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.ChangeBtnClick);
            // 
            // SeleectItemStatusControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.HeaderText = "Выберите статус ТМЦ";
            this.Size = new System.Drawing.Size(240, 320);
            this.ResumeLayout();

        }

        #endregion

        private Resco.UIElements.UIGridPanel SelectStatusGridPanel;
        private Resco.UIElements.UILabel uiLabel1;
        private Resco.UIElements.UIComboBox StatusesBox;
        private Resco.UIElements.UIGridPanel SelectStatusBottomGridPanel;
        private Stock.Controls.StockButton ChangeBtn;
    }
}
