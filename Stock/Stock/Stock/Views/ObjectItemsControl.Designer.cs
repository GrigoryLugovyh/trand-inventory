﻿using Stock.Controls;

namespace Stock.Views
{
    partial class ObjectItemsControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uiGridPanel1 = new Resco.UIElements.UIGridPanel();
            this.uiGridPanel2 = new Resco.UIElements.UIGridPanel();
            this.CancelBtn = new Stock.Controls.StockButton();
            this.PrintBtn = new Stock.Controls.StockButton();
            this.uiGridPanel3 = new Resco.UIElements.UIGridPanel();
            this.SelectCheck = new Stock.Controls.StockCheckBox();
            this.ObjectNameText = new Resco.UIElements.UILabel();
            this.ItemsListView = new Resco.UIElements.UIListView();
            this.SuspendLayout();
            // 
            // StockGridPanel
            // 
            this.StockGridPanel.Children.Add(this.uiGridPanel1, 0, 1);
            // 
            // HeaderLabel
            // 
            this.HeaderLabel.Text = "Информация по объекту";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold);
            this.StatusLabel.Text = "Все ТМЦ в объекте";
            // 
            // uiGridPanel1
            // 
            this.uiGridPanel1.Children.Add(this.uiGridPanel2, 0, 2);
            this.uiGridPanel1.Children.Add(this.uiGridPanel3, 0, 0);
            this.uiGridPanel1.Children.Add(this.ItemsListView, 0, 1);
            this.uiGridPanel1.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel1.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 290);
            this.uiGridPanel1.Name = "uiGridPanel1";
            this.uiGridPanel1.Rows.Add(new Resco.UIElements.GridLinePosition(20, Resco.UIElements.GridLineValueType.Absolute, true));
            this.uiGridPanel1.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel1.Rows.Add(new Resco.UIElements.GridLinePosition(30, Resco.UIElements.GridLineValueType.Absolute, true));
            this.uiGridPanel1.TabIndex = 1;
            // 
            // uiGridPanel2
            // 
            this.uiGridPanel2.Children.Add(this.CancelBtn, 0, 0);
            this.uiGridPanel2.Children.Add(this.PrintBtn, 1, 0);
            this.uiGridPanel2.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel2.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel2.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 30);
            this.uiGridPanel2.Name = "uiGridPanel2";
            this.uiGridPanel2.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            // 
            // CancelBtn
            // 
            this.CancelBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 5, 3, 5, 2, 110, 25);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.RoundedCornerRadius = 3;
            this.CancelBtn.Text = "Завершить";
            this.CancelBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.CancelBtnClick);
            // 
            // PrintBtn
            // 
            this.PrintBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 5, 3, 5, 2, 110, 25);
            this.PrintBtn.Name = "PrintBtn";
            this.PrintBtn.RoundedCornerRadius = 3;
            this.PrintBtn.TabIndex = 1;
            this.PrintBtn.Text = "Печать";
            this.PrintBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.PrintBtnClick);
            // 
            // uiGridPanel3
            // 
            this.uiGridPanel3.Children.Add(this.SelectCheck, 0, 0);
            this.uiGridPanel3.Children.Add(this.ObjectNameText, 1, 0);
            this.uiGridPanel3.Columns.Add(new Resco.UIElements.GridLinePosition(20, Resco.UIElements.GridLineValueType.Absolute, true));
            this.uiGridPanel3.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel3.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 20);
            this.uiGridPanel3.Name = "uiGridPanel3";
            this.uiGridPanel3.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel3.TabIndex = 1;
            // 
            // SelectCheck
            // 
            this.SelectCheck.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 29, 29);
            this.SelectCheck.Name = "SelectCheck";
            this.SelectCheck.TabIndex = 2;
            this.SelectCheck.CheckedChanged += new System.EventHandler(this.SelectCheckCheckedChanged);
            // 
            // ObjectNameText
            // 
            this.ObjectNameText.AutoSize = false;
            this.ObjectNameText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 5, 0, 0, 0, 215, 20);
            this.ObjectNameText.Name = "ObjectNameText";
            this.ObjectNameText.TabIndex = 1;
            this.ObjectNameText.Text = "Наименование объекта";
            this.ObjectNameText.TextAlignment = Resco.Drawing.Alignment.MiddleLeft;
            // 
            // ItemsListView
            // 
            this.ItemsListView.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Bottom, 0, 0, 0, 0, 240, 240);
            this.ItemsListView.Name = "ItemsListView";
            this.ItemsListView.ScrollBarsMode = Resco.UIElements.ScrollBarsMode.OutOfContent;
            this.ItemsListView.TabIndex = 2;
            // 
            // ObjectItemsControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.HeaderText = "Информация по объекту";
            this.Size = new System.Drawing.Size(240, 320);
            this.StatusText = "Все ТМЦ в объекте";
            this.ResumeLayout();

        }

        #endregion

        private Resco.UIElements.UIGridPanel uiGridPanel1;
        private Resco.UIElements.UIGridPanel uiGridPanel2;
        private Stock.Controls.StockButton CancelBtn;
        private Stock.Controls.StockButton PrintBtn;
        private Resco.UIElements.UIGridPanel uiGridPanel3;
        private StockCheckBox SelectCheck;
        private Resco.UIElements.UILabel ObjectNameText;
        private Resco.UIElements.UIListView ItemsListView;
    }
}
