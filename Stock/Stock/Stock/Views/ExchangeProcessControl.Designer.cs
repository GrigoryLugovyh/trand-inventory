﻿namespace Stock.Views
{
    partial class ExchangeProcessControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ExchangeGridPanel = new Resco.UIElements.UIGridPanel();
            this.ExchangeHeaderText = new Resco.UIElements.UIAnimatedLabel();
            this.AbortBtn = new Stock.Controls.StockButton();
            this.ExchangeProgress = new Resco.UIElements.UIProgressBar();
            this.StatisticGridPanel = new Resco.UIElements.UIGridPanel();
            this.uiLabel1 = new Resco.UIElements.UILabel();
            this.uiLabel2 = new Resco.UIElements.UILabel();
            this.uiLabel3 = new Resco.UIElements.UILabel();
            this.uiLabel4 = new Resco.UIElements.UILabel();
            this.uiLabel5 = new Resco.UIElements.UILabel();
            this.ExchangeReceivedText = new Resco.UIElements.UILabel();
            this.ExchangePreparedText = new Resco.UIElements.UILabel();
            this.ExchangeProcessedText = new Resco.UIElements.UILabel();
            this.ExchangeNotProcessedText = new Resco.UIElements.UILabel();
            this.ExchangeProcessTimeText = new Resco.UIElements.UILabel();
            this.ExchangeProcessText = new Resco.UIElements.UILabel();
            this.uiLabel11 = new Resco.UIElements.UILabel();
            this.SuspendLayout();
            // 
            // StockGridPanel
            // 
            this.StockGridPanel.Children.Add(this.ExchangeGridPanel, 0, 1);
            // 
            // ExchangeGridPanel
            // 
            this.ExchangeGridPanel.Children.Add(this.ExchangeHeaderText, 0, 0);
            this.ExchangeGridPanel.Children.Add(this.AbortBtn, 0, 6);
            this.ExchangeGridPanel.Children.Add(this.ExchangeProgress, 0, 4);
            this.ExchangeGridPanel.Children.Add(this.StatisticGridPanel, 0, 2);
            this.ExchangeGridPanel.Children.Add(this.ExchangeProcessText, 0, 3);
            this.ExchangeGridPanel.Children.Add(this.uiLabel11, 0, 1);
            this.ExchangeGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ExchangeGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 290);
            this.ExchangeGridPanel.Name = "ExchangeGridPanel";
            this.ExchangeGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(30, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ExchangeGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(20, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ExchangeGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ExchangeGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(20, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ExchangeGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(25, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ExchangeGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(5, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ExchangeGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(40, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ExchangeGridPanel.TabIndex = 1;
            // 
            // ExchangeHeaderText
            // 
            this.ExchangeHeaderText.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.ExchangeHeaderText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 30);
            this.ExchangeHeaderText.Name = "ExchangeHeaderText";
            this.ExchangeHeaderText.Text = "Обмен данными";
            this.ExchangeHeaderText.TextAlignment = Resco.Drawing.Alignment.MiddleCenter;
            // 
            // AbortBtn
            // 
            this.AbortBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 30, 5, 30, 5, 180, 30);
            this.AbortBtn.Name = "AbortBtn";
            this.AbortBtn.RoundedCornerRadius = 3;
            this.AbortBtn.TabIndex = 1;
            this.AbortBtn.Text = "Завершить";
            this.AbortBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.AbortBtnClick);
            // 
            // ExchangeProgress
            // 
            this.ExchangeProgress.BackColor = System.Drawing.Color.Transparent;
            this.ExchangeProgress.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 3, 3, 3, 3, 234, 19);
            this.ExchangeProgress.Name = "ExchangeProgress";
            this.ExchangeProgress.ProgressCornerRadius = 3;
            this.ExchangeProgress.ProgressFill.BackColor = System.Drawing.Color.Orange;
            this.ExchangeProgress.TabIndex = 2;
            this.ExchangeProgress.TextFormat = "{0:P0}";
            // 
            // StatisticGridPanel
            // 
            this.StatisticGridPanel.Children.Add(this.uiLabel1, 0, 0);
            this.StatisticGridPanel.Children.Add(this.uiLabel2, 0, 1);
            this.StatisticGridPanel.Children.Add(this.uiLabel3, 0, 2);
            this.StatisticGridPanel.Children.Add(this.uiLabel4, 0, 3);
            this.StatisticGridPanel.Children.Add(this.uiLabel5, 0, 4);
            this.StatisticGridPanel.Children.Add(this.ExchangeReceivedText, 1, 0);
            this.StatisticGridPanel.Children.Add(this.ExchangePreparedText, 1, 1);
            this.StatisticGridPanel.Children.Add(this.ExchangeProcessedText, 1, 2);
            this.StatisticGridPanel.Children.Add(this.ExchangeNotProcessedText, 1, 3);
            this.StatisticGridPanel.Children.Add(this.ExchangeProcessTimeText, 1, 4);
            this.StatisticGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.StatisticGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.StatisticGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 150);
            this.StatisticGridPanel.Name = "StatisticGridPanel";
            this.StatisticGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.StatisticGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.StatisticGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.StatisticGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.StatisticGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.StatisticGridPanel.TabIndex = 3;
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.uiLabel1.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Right, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 73, 17);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Text = "Получено:";
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.uiLabel2.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Right, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 104, 17);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.TabIndex = 1;
            this.uiLabel2.Text = "Подготовлено:";
            // 
            // uiLabel3
            // 
            this.uiLabel3.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.uiLabel3.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Right, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 89, 17);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.TabIndex = 2;
            this.uiLabel3.Text = "Обработано:";
            // 
            // uiLabel4
            // 
            this.uiLabel4.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.uiLabel4.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Right, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 109, 17);
            this.uiLabel4.Name = "uiLabel4";
            this.uiLabel4.TabIndex = 3;
            this.uiLabel4.Text = "Не обработано:";
            // 
            // uiLabel5
            // 
            this.uiLabel5.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.uiLabel5.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Right, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 117, 17);
            this.uiLabel5.Name = "uiLabel5";
            this.uiLabel5.TabIndex = 9;
            this.uiLabel5.Text = "Время процесса:";
            // 
            // ExchangeReceivedText
            // 
            this.ExchangeReceivedText.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.ExchangeReceivedText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 9, 17);
            this.ExchangeReceivedText.Name = "ExchangeReceivedText";
            this.ExchangeReceivedText.TabIndex = 10;
            this.ExchangeReceivedText.Text = "0";
            // 
            // ExchangePreparedText
            // 
            this.ExchangePreparedText.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.ExchangePreparedText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 9, 17);
            this.ExchangePreparedText.Name = "ExchangePreparedText";
            this.ExchangePreparedText.TabIndex = 11;
            this.ExchangePreparedText.Text = "0";
            // 
            // ExchangeProcessedText
            // 
            this.ExchangeProcessedText.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.ExchangeProcessedText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 9, 17);
            this.ExchangeProcessedText.Name = "ExchangeProcessedText";
            this.ExchangeProcessedText.TabIndex = 12;
            this.ExchangeProcessedText.Text = "0";
            // 
            // ExchangeNotProcessedText
            // 
            this.ExchangeNotProcessedText.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.ExchangeNotProcessedText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 9, 17);
            this.ExchangeNotProcessedText.Name = "ExchangeNotProcessedText";
            this.ExchangeNotProcessedText.TabIndex = 13;
            this.ExchangeNotProcessedText.Text = "0";
            // 
            // ExchangeProcessTimeText
            // 
            this.ExchangeProcessTimeText.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.ExchangeProcessTimeText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 9, 17);
            this.ExchangeProcessTimeText.Name = "ExchangeProcessTimeText";
            this.ExchangeProcessTimeText.TabIndex = 8;
            this.ExchangeProcessTimeText.Text = "0";
            // 
            // ExchangeProcessText
            // 
            this.ExchangeProcessText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 79, 15);
            this.ExchangeProcessText.Name = "ExchangeProcessText";
            this.ExchangeProcessText.TabIndex = 4;
            this.ExchangeProcessText.Text = "Подготовка ...";
            this.ExchangeProcessText.TextAlignment = Resco.Drawing.Alignment.MiddleCenter;
            // 
            // uiLabel11
            // 
            this.uiLabel11.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 136, 15);
            this.uiLabel11.Name = "uiLabel11";
            this.uiLabel11.TabIndex = 5;
            this.uiLabel11.Text = "Информация по строкам";
            // 
            // ExchangeProcessControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.Size = new System.Drawing.Size(240, 320);
            this.ResumeLayout();

        }

        #endregion

        private Resco.UIElements.UIGridPanel ExchangeGridPanel;
        public Resco.UIElements.UIAnimatedLabel ExchangeHeaderText;
        private Stock.Controls.StockButton AbortBtn;
        public Resco.UIElements.UIProgressBar ExchangeProgress;
        private Resco.UIElements.UIGridPanel StatisticGridPanel;
        private Resco.UIElements.UILabel uiLabel1;
        private Resco.UIElements.UILabel uiLabel2;
        private Resco.UIElements.UILabel uiLabel3;
        private Resco.UIElements.UILabel uiLabel4;
        public Resco.UIElements.UILabel ExchangeReceivedText;
        public Resco.UIElements.UILabel ExchangePreparedText;
        public Resco.UIElements.UILabel ExchangeProcessedText;
        public Resco.UIElements.UILabel ExchangeNotProcessedText;
        public Resco.UIElements.UILabel ExchangeProcessText;
        private Resco.UIElements.UILabel uiLabel11;
        private Resco.UIElements.UILabel uiLabel5;
        public Resco.UIElements.UILabel ExchangeProcessTimeText;
    }
}
