﻿using System;
using System.Windows.Forms;
using Resco.UIElements;
using StockCore;
using StockCore.Interfaces.Views;

namespace Stock.Views
{
    public partial class SplashControl : StockControl, ISplashView
    {
        public SplashControl()
        {
            InitializeComponent();

            HeaderVisible = false;
        }

        public event EventHandler Show;

        public event EventHandler Close;

        public void ShowView()
        {
            Version = string.Format(Version, Model.ApplicationVersion);
            ShowView<SplashControl>(AnimationEffect.MoveLeft);
            Application.DoEvents();
        }

        public void CloseView()
        {

        }

        public string Version
        {
            get { return StatusLabel.Text; }
            set { Invoker(() => StatusLabel.Text = value); }
        }
    }
}
