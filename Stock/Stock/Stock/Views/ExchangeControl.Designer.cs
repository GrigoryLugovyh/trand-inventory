﻿namespace Stock.Views
{
    partial class ExchangeControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ExchnageCommonGridPanel = new Resco.UIElements.UIGridPanel();
            this.ImportPanel = new Resco.UIElements.UIPanel();
            this.ImportGridPanel = new Resco.UIElements.UIGridPanel();
            this.ImportUpdateCheck = new Resco.UIElements.UICheckBox();
            this.ImportBtn = new Stock.Controls.StockButton();
            this.uiAnimatedLabel1 = new Resco.UIElements.UIAnimatedLabel();
            this.ExportPanel = new Resco.UIElements.UIPanel();
            this.ExportGridPanel = new Resco.UIElements.UIGridPanel();
            this.ExportFoundCheck = new Resco.UIElements.UICheckBox();
            this.ExportBtn = new Stock.Controls.StockButton();
            this.uiAnimatedLabel2 = new Resco.UIElements.UIAnimatedLabel();
            this.ExportNewCheck = new Resco.UIElements.UICheckBox();
            this.ExitBtn = new Stock.Controls.StockButton();
            this.SuspendLayout();
            // 
            // StockGridPanel
            // 
            this.StockGridPanel.Children.Add(this.ExchnageCommonGridPanel, 0, 1);
            // 
            // HeaderLabel
            // 
            this.HeaderLabel.Text = "Инвентаризация: Обмен данными";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Text = "Обмен данными";
            // 
            // ExchnageCommonGridPanel
            // 
            this.ExchnageCommonGridPanel.Children.Add(this.ImportPanel, 0, 0);
            this.ExchnageCommonGridPanel.Children.Add(this.ExportPanel, 0, 1);
            this.ExchnageCommonGridPanel.Children.Add(this.ExitBtn, 0, 2);
            this.ExchnageCommonGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 290);
            this.ExchnageCommonGridPanel.Name = "ExchnageCommonGridPanel";
            this.ExchnageCommonGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ExchnageCommonGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ExchnageCommonGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(50, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ExchnageCommonGridPanel.TabIndex = 1;
            // 
            // ImportPanel
            // 
            this.ImportPanel.BackColor = System.Drawing.Color.Azure;
            this.ImportPanel.Children.Add(this.ImportGridPanel);
            this.ImportPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 5, 5, 5, 5, 230, 110);
            this.ImportPanel.Name = "ImportPanel";
            this.ImportPanel.Padding = new Resco.UIElements.Thickness(1);
            this.ImportPanel.RoundedCornerRadius = 3;
            // 
            // ImportGridPanel
            // 
            this.ImportGridPanel.BorderThickness = 1;
            this.ImportGridPanel.Children.Add(this.ImportUpdateCheck, 0, 1);
            this.ImportGridPanel.Children.Add(this.ImportBtn, 0, 2);
            this.ImportGridPanel.Children.Add(this.uiAnimatedLabel1, 0, 0);
            this.ImportGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 228, 108);
            this.ImportGridPanel.Name = "ImportGridPanel";
            this.ImportGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ImportGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ImportGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            // 
            // ImportUpdateCheck
            // 
            this.ImportUpdateCheck.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Left, Resco.UIElements.VAlignment.Center, 20, 0, 20, 0, 127, 29);
            this.ImportUpdateCheck.Name = "ImportUpdateCheck";
            this.ImportUpdateCheck.TabIndex = 3;
            this.ImportUpdateCheck.Text = "Обновить данные";
            // 
            // ImportBtn
            // 
            this.ImportBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 5, 5, 5, 5, 216, 25);
            this.ImportBtn.Name = "ImportBtn";
            this.ImportBtn.RoundedCornerRadius = 3;
            this.ImportBtn.TabIndex = 1;
            this.ImportBtn.Text = "Загрузить";
            this.ImportBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.ImportBtnClick);
            // 
            // uiAnimatedLabel1
            // 
            this.uiAnimatedLabel1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.uiAnimatedLabel1.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 226, 35);
            this.uiAnimatedLabel1.Name = "uiAnimatedLabel1";
            this.uiAnimatedLabel1.TabIndex = 2;
            this.uiAnimatedLabel1.Text = "Загрузка данных";
            this.uiAnimatedLabel1.TextAlignment = Resco.Drawing.Alignment.MiddleCenter;
            // 
            // ExportPanel
            // 
            this.ExportPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ExportPanel.Children.Add(this.ExportGridPanel);
            this.ExportPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 5, 5, 5, 5, 230, 110);
            this.ExportPanel.Name = "ExportPanel";
            this.ExportPanel.Padding = new Resco.UIElements.Thickness(1);
            this.ExportPanel.RoundedCornerRadius = 3;
            this.ExportPanel.TabIndex = 1;
            // 
            // ExportGridPanel
            // 
            this.ExportGridPanel.BackColor = System.Drawing.Color.Azure;
            this.ExportGridPanel.BorderThickness = 1;
            this.ExportGridPanel.Children.Add(this.ExportFoundCheck, 0, 1);
            this.ExportGridPanel.Children.Add(this.ExportBtn, 0, 3);
            this.ExportGridPanel.Children.Add(this.uiAnimatedLabel2, 0, 0);
            this.ExportGridPanel.Children.Add(this.ExportNewCheck, 0, 2);
            this.ExportGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 228, 108);
            this.ExportGridPanel.Name = "ExportGridPanel";
            this.ExportGridPanel.Padding = new Resco.UIElements.Thickness(1);
            this.ExportGridPanel.RoundedCornerRadius = 3;
            this.ExportGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ExportGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ExportGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ExportGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            // 
            // ExportFoundCheck
            // 
            this.ExportFoundCheck.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Left, Resco.UIElements.VAlignment.Center, 20, 0, 20, 0, 165, 29);
            this.ExportFoundCheck.Name = "ExportFoundCheck";
            this.ExportFoundCheck.TabIndex = 3;
            this.ExportFoundCheck.Text = "Только найденные ТМЦ ";
            this.ExportFoundCheck.CheckedChanged += new System.EventHandler(this.ExportFoundCheckCheckedChanged);
            // 
            // ExportBtn
            // 
            this.ExportBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 5, 1, 5, 0, 214, 25);
            this.ExportBtn.Name = "ExportBtn";
            this.ExportBtn.RoundedCornerRadius = 3;
            this.ExportBtn.TabIndex = 1;
            this.ExportBtn.Text = "Выгрузить";
            this.ExportBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.ExportBtnClick);
            // 
            // uiAnimatedLabel2
            // 
            this.uiAnimatedLabel2.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.uiAnimatedLabel2.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 224, 26);
            this.uiAnimatedLabel2.Name = "uiAnimatedLabel2";
            this.uiAnimatedLabel2.TabIndex = 2;
            this.uiAnimatedLabel2.Text = "Выгрузка данных";
            this.uiAnimatedLabel2.TextAlignment = Resco.Drawing.Alignment.MiddleCenter;
            // 
            // ExportNewCheck
            // 
            this.ExportNewCheck.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Left, Resco.UIElements.VAlignment.Center, 20, 0, 20, 0, 138, 29);
            this.ExportNewCheck.Name = "ExportNewCheck";
            this.ExportNewCheck.TabIndex = 4;
            this.ExportNewCheck.Text = "Только новые ТМЦ ";
            this.ExportNewCheck.CheckedChanged += new System.EventHandler(this.ExportNewCheckCheckedChanged);
            // 
            // ExitBtn
            // 
            this.ExitBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 20, 10, 20, 10, 200, 30);
            this.ExitBtn.Name = "ExitBtn";
            this.ExitBtn.RoundedCornerRadius = 3;
            this.ExitBtn.TabIndex = 2;
            this.ExitBtn.Text = "Вернуться в главное меню";
            this.ExitBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.ExitBtnClick);
            // 
            // ExchangeControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.HeaderText = "Инвентаризация: Обмен данными";
            this.Size = new System.Drawing.Size(240, 320);
            this.StatusText = "Обмен данными";
            this.ResumeLayout();

        }

        #endregion

        private Resco.UIElements.UIGridPanel ExchnageCommonGridPanel;
        private Resco.UIElements.UIPanel ImportPanel;
        private Resco.UIElements.UIGridPanel ImportGridPanel;
        private Resco.UIElements.UICheckBox ImportUpdateCheck;
        private Stock.Controls.StockButton ImportBtn;
        private Resco.UIElements.UIAnimatedLabel uiAnimatedLabel1;
        private Resco.UIElements.UIPanel ExportPanel;
        private Resco.UIElements.UIGridPanel ExportGridPanel;
        private Resco.UIElements.UICheckBox ExportFoundCheck;
        private Stock.Controls.StockButton ExportBtn;
        private Resco.UIElements.UIAnimatedLabel uiAnimatedLabel2;
        private Stock.Controls.StockButton ExitBtn;
        private Resco.UIElements.UICheckBox ExportNewCheck;

    }
}
