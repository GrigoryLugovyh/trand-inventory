﻿namespace Stock.Views.Printers
{
    partial class NormalPrinterControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uiGridPanel1 = new Resco.UIElements.UIGridPanel();
            this.uiLabel1 = new Resco.UIElements.UILabel();
            this.SuspendLayout();
            // 
            // uiGridPanel1
            // 
            this.uiGridPanel1.Children.Add(this.uiLabel1, 0, 0);
            this.uiGridPanel1.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel1.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 25);
            this.uiGridPanel1.Name = "uiGridPanel1";
            this.uiGridPanel1.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            // 
            // uiLabel1
            // 
            this.uiLabel1.AutoSize = false;
            this.uiLabel1.DataBindings.Add(new Resco.UIElements.ElementBinding("Text", "Name", System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, false));
            this.uiLabel1.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 15, 0, 0, 0, 225, 25);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.TextAlignment = Resco.Drawing.Alignment.MiddleLeft;
            // 
            // NormalPrinterControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.Children.Add(this.uiGridPanel1);
            this.Size = new System.Drawing.Size(240, 25);
            this.ResumeLayout();

        }

        #endregion

        private Resco.UIElements.UIGridPanel uiGridPanel1;
        private Resco.UIElements.UILabel uiLabel1;

    }
}
