﻿using System;
using Resco.UIElements;
using StockCore.Interfaces.Views;

namespace Stock.Views
{
    public partial class ExchangeControl : StockControl, IExchangeView
    {
        public ExchangeControl()
        {
            InitializeComponent();
        }

        public void ShowView()
        {
            ShowView<ExchangeControl>(AnimationEffect.MoveLeft);

            if(Show != null)
            {
                Show(this, EventArgs.Empty);
            }
        }

        public void CloseView()
        {
            if (Close != null)
            {
                Close(this, EventArgs.Empty);
            }

            ShowView<MainControl>(AnimationEffect.MoveRight);

            Invoker(() => ImportBtn.Focus());
        }

        public bool ImportUpdate
        {
            get { return ImportUpdateCheck.Checked; }
            set { Invoker(() => ImportUpdateCheck.Checked = value); }
        }

        public bool ExportFound
        {
            get { return ExportFoundCheck.Checked; }
            set { Invoker(() => ExportFoundCheck.Checked = value); }
        }

        public bool ExportNew
        {
            get { return ExportNewCheck.Checked; }
            set { Invoker(() => ExportNewCheck.Checked = value); }
        }

        public event EventHandler Show;

        public event EventHandler Close;

        public event EventHandler Import;

        public event EventHandler Export;

        public event EventHandler Exit;

        private void ImportBtnClick(object sender, UIMouseEventArgs e)
        {
            if(Import != null)
            {
                Import(sender, e);
            }
        }

        private void ExportBtnClick(object sender, UIMouseEventArgs e)
        {
            if(Export != null)
            {
                Export(sender, e);
            }
        }

        private void ExitBtnClick(object sender, UIMouseEventArgs e)
        {
            if(Exit != null)
            {
                Exit(sender, e);
            }
        }

        private void ExportFoundCheckCheckedChanged(object sender, EventArgs e)
        {
            if(ExportFound)
            {
                ExportNew = false;
            }
        }

        private void ExportNewCheckCheckedChanged(object sender, EventArgs e)
        {
            if(ExportNew)
            {
                ExportFound = false;
            }
        }
    }
}