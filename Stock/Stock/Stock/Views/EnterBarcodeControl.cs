﻿using System;
using System.Collections.Generic;
using Resco.UIElements;
using StockCore.Entity;
using StockCore.Handlers.Exchange.Configuration.CSV;
using StockCore.Interfaces.Views;

namespace Stock.Views
{
    public partial class EnterBarcodeControl : StockControl, IEnterBarcodeView
    {
        public EnterBarcodeControl()
        {
            InitializeComponent();
        }

        public event EventHandler Show;

        public event EventHandler Close;

        public event EventHandler Enter;

        public void ShowView()
        {
            if(Show != null)
            {
                Show(this, EventArgs.Empty);
            }

            ShowView<EnterBarcodeControl>(AnimationEffect.MoveLeft);

            Invoker(() => BarcodeText.Focus());
        }

        public void CloseView()
        {
            if (Close != null)
            {
                Close(this, EventArgs.Empty);
            }
        }

        public void CloseToInventory()
        {
            ShowView<InventoryControl>(AnimationEffect.MoveRight);
        }

        public void CloseToInformation()
        {
            ShowView<InformationControl>(AnimationEffect.MoveRight);
        }

        public string Barcode
        {
            get { return BarcodeText.Text; }
            set { Invoker(() => BarcodeText.Text = value); }
        }

        public bool PrefixesVisible
        {
            get { return PrefixesBox.Visible; }
            set
            {
                Invoker(() =>
                {
                    PrefixesBox.Visible = value;
                    BarcodeGridPanel.Columns[0].Visible = value;
                });
            }
        }

        public bool PostfixesVisible
        {
            get { return PostfixesBox.Visible; }
            set
            {
                Invoker(() =>
                {
                    PostfixesBox.Visible = value;
                    BarcodeGridPanel.Columns[2].Visible = value;
                });
            }
        }

        public List<CsvPrefix> Prefixes
        {
            get { return PrefixesBox.DataSource as List<CsvPrefix>; }
            set { Invoker(() => PrefixesBox.DataSource = value); }
        }

        public List<Codes> Postfixes
        {
            get { return PostfixesBox.DataSource as List<Codes>; }
            set { Invoker(() => PostfixesBox.DataSource = value); }
        }

        public CsvPrefix SelectedPrefix
        {
            get { return PrefixesBox.SelectedItem as CsvPrefix; }
        }

        public Codes SelectedPostfix
        {
            get { return PostfixesBox.SelectedItem as Codes; }
        }

        private void EnterBtnClick(object sender, UIMouseEventArgs e)
        {
            if(Enter != null)
            {
                Enter(sender, e);
            }
        }

        private void CancelBtnClick(object sender, UIMouseEventArgs e)
        {
            CloseView();
        }
    }
}
