﻿using System;
using Resco.UIElements;
using StockCore.Interfaces.Views;

namespace Stock.Views
{
    public partial class SerialNumberInputControl : StockControl, ISerialNumberInputView
    {
        public SerialNumberInputControl()
        {
            InitializeComponent();
        }

        public void ShowView()
        {
            if(Show != null)
            {
                Show(this, EventArgs.Empty);
            }

            ShowView<SerialNumberInputControl>(AnimationEffect.MoveLeft);

            Invoker(() => SerialNumberText.Focus());
        }

        public void CloseView()
        {
            if (Close != null)
            {
                Close(this, EventArgs.Empty);
            }

            ShowView<InventoryControl>(AnimationEffect.MoveRight);
        }

        public event EventHandler Show;

        public event EventHandler Close;

        public event EventHandler Input;

        public string SerialNumber
        {
            get { return SerialNumberText.Text; }
            set { Invoker(() => SerialNumberText.Text = value); }
        }

        public string CurrentSerialNumber
        {
            get { return CurrentSerialNumberText.Text; }
            set { Invoker(() => CurrentSerialNumberText.Text = value); }
        }

        private void CancelBtnClick(object sender, UIMouseEventArgs e)
        {
            CloseView();
        }

        private void InputBtnClick(object sender, UIMouseEventArgs e)
        {
            if(Input != null)
            {
                Input(sender, e);
            }
        }
    }
}
