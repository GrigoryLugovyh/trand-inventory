﻿namespace Stock.Views
{
    partial class SplashControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uiAnimatedLabel1 = new Resco.UIElements.UIAnimatedLabel();
            this.LogoImage = new Resco.UIElements.UIImage();
            this.SuspendLayout();
            // 
            // StockGridPanel
            // 
            this.StockGridPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(6)))), ((int)(((byte)(17)))));
            this.StockGridPanel.Children.Add(this.LogoImage, 0, 1);
            // 
            // HeaderLabel
            // 
            this.HeaderLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(6)))), ((int)(((byte)(17)))));
            this.HeaderLabel.ForeColor = System.Drawing.Color.White;
            // 
            // StatusLabel
            // 
            this.StatusLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(6)))), ((int)(((byte)(17)))));
            this.StatusLabel.ForeColor = System.Drawing.Color.White;
            this.StatusLabel.Text = "Версия {0}";
            // 
            // uiAnimatedLabel1
            // 
            this.uiAnimatedLabel1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.uiAnimatedLabel1.ForeColor = System.Drawing.Color.White;
            this.uiAnimatedLabel1.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 24, 280, 24, 12, 192, 28);
            this.uiAnimatedLabel1.Name = "uiAnimatedLabel1";
            this.uiAnimatedLabel1.TabIndex = 1;
            this.uiAnimatedLabel1.Text = "Загрузка ...";
            this.uiAnimatedLabel1.TextAlignment = Resco.Drawing.Alignment.MiddleCenter;
            // 
            // LogoImage
            // 
            this.LogoImage.Image = global::Stock.StockImages.GetImage("Images.Logo");
            this.LogoImage.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 290);
            this.LogoImage.Name = "LogoImage";
            // 
            // SplashControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.Children.Add(this.uiAnimatedLabel1);
            this.Size = new System.Drawing.Size(240, 320);
            this.StatusText = "Версия {0}";
            this.ResumeLayout();

        }

        #endregion

        private Resco.UIElements.UIAnimatedLabel uiAnimatedLabel1;
        private Resco.UIElements.UIImage LogoImage;
    }
}
