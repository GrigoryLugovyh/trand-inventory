﻿namespace Stock.Views
{
    partial class InventoryControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.InventoryPanel = new Resco.UIElements.UITabPanel();
            this.EnterPage = new Resco.UIElements.UITabPage();
            this.InventoryGridPanel = new Resco.UIElements.UIGridPanel();
            this.ButtomGridPanel = new Resco.UIElements.UIGridPanel();
            this.FinishBtn = new Stock.Controls.StockButton();
            this.BarcodesGridPanel = new Resco.UIElements.UIGridPanel();
            this.uiLabel1 = new Resco.UIElements.UILabel();
            this.uiLabel2 = new Resco.UIElements.UILabel();
            this.StatusBox = new Resco.UIElements.UIComboBox();
            this.BarcodeText = new Stock.Controls.StockTextEdit();
            this.EnterBarcodeBtn = new Resco.UIElements.UITextBoxButton();
            this.ItemNameGridPanel = new Resco.UIElements.UIGridPanel();
            this.uiLabel3 = new Resco.UIElements.UILabel();
            this.ItemNameText = new Stock.Controls.StockTextEdit();
            this.uiGridPanel1 = new Resco.UIElements.UIGridPanel();
            this.uiLabel4 = new Resco.UIElements.UILabel();
            this.IbsoText = new Stock.Controls.StockTextEdit();
            this.uiGridPanel2 = new Resco.UIElements.UIGridPanel();
            this.uiLabel5 = new Resco.UIElements.UILabel();
            this.UserNameText = new Stock.Controls.StockTextEdit();
            this.EnterUserBtn = new Resco.UIElements.UITextBoxButton();
            this.MolGridPanel = new Resco.UIElements.UIGridPanel();
            this.uiLabel6 = new Resco.UIElements.UILabel();
            this.MolNameText = new Stock.Controls.StockTextEdit();
            this.EnterMolBtn = new Resco.UIElements.UITextBoxButton();
            this.uiGridPanel3 = new Resco.UIElements.UIGridPanel();
            this.uiLabel7 = new Resco.UIElements.UILabel();
            this.StatusText = new Stock.Controls.StockTextEdit();
            this.EnterSerialBtn = new Resco.UIElements.UITextBoxButton();
            this.FindPage = new Resco.UIElements.UITabPage();
            this.FindItemsGridPanel = new Resco.UIElements.UIGridPanel();
            this.FoundBottomGridPanel = new Resco.UIElements.UIGridPanel();
            this.DeleteBtn = new Stock.Controls.StockButton();
            this.FindItemsListView = new Resco.UIElements.UIListView();
            this.NotFindPage = new Resco.UIElements.UITabPage();
            this.NotFindItemsGridPanel = new Resco.UIElements.UIGridPanel();
            this.NotFoundBottomGridPanel = new Resco.UIElements.UIGridPanel();
            this.PrintBtn = new Stock.Controls.StockButton();
            this.NotFindItemsListView = new Resco.UIElements.UIListView();
            this.SuspendLayout();
            // 
            // StockGridPanel
            // 
            this.StockGridPanel.Children.Add(this.InventoryPanel, 0, 1);
            // 
            // HeaderLabel
            // 
            this.HeaderLabel.Text = "ВСЕГО - 0 / НАЙДЕНО - 0";
            this.HeaderLabel.TextAlignment = Resco.Drawing.Alignment.MiddleCenter;
            // 
            // StatusLabel
            // 
            this.StatusLabel.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold);
            // 
            // InventoryPanel
            // 
            this.InventoryPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 229, 291);
            this.InventoryPanel.Name = "InventoryPanel";
            this.InventoryPanel.TabIndex = 1;
            this.InventoryPanel.TabPages.Add(this.EnterPage);
            this.InventoryPanel.TabPages.Add(this.FindPage);
            this.InventoryPanel.TabPages.Add(this.NotFindPage);
            this.InventoryPanel.TabPagesMode = Resco.UIElements.UITabPanelMode.Top;
            this.InventoryPanel.SelectedIndexChanging += new Resco.UIElements.SelectedIndexChangingEventHandler(this.InventoryPanelSelectedIndexChanging);
            // 
            // EnterPage
            // 
            this.EnterPage.Children.Add(this.InventoryGridPanel);
            this.EnterPage.Name = "EnterPage";
            this.EnterPage.Text = "Ввод ТМЦ";
            // 
            // InventoryGridPanel
            // 
            this.InventoryGridPanel.Children.Add(this.ButtomGridPanel, 0, 6);
            this.InventoryGridPanel.Children.Add(this.BarcodesGridPanel, 0, 0);
            this.InventoryGridPanel.Children.Add(this.ItemNameGridPanel, 0, 1);
            this.InventoryGridPanel.Children.Add(this.uiGridPanel1, 0, 2);
            this.InventoryGridPanel.Children.Add(this.uiGridPanel2, 0, 3);
            this.InventoryGridPanel.Children.Add(this.MolGridPanel, 0, 4);
            this.InventoryGridPanel.Children.Add(this.uiGridPanel3, 0, 5);
            this.InventoryGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.InventoryGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 229, 265);
            this.InventoryGridPanel.Name = "InventoryGridPanel";
            this.InventoryGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(39, Resco.UIElements.GridLineValueType.Absolute, true));
            this.InventoryGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.InventoryGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(39, Resco.UIElements.GridLineValueType.Absolute, true));
            this.InventoryGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(39, Resco.UIElements.GridLineValueType.Absolute, true));
            this.InventoryGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(39, Resco.UIElements.GridLineValueType.Absolute, true));
            this.InventoryGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(39, Resco.UIElements.GridLineValueType.Absolute, true));
            this.InventoryGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(25, Resco.UIElements.GridLineValueType.Absolute, true));
            // 
            // ButtomGridPanel
            // 
            this.ButtomGridPanel.Children.Add(this.FinishBtn, 1, 0);
            this.ButtomGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ButtomGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ButtomGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ButtomGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 229, 25);
            this.ButtomGridPanel.Name = "ButtomGridPanel";
            this.ButtomGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            // 
            // FinishBtn
            // 
            this.FinishBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 3, 2, 3, 1, 94, 22);
            this.FinishBtn.Name = "FinishBtn";
            this.FinishBtn.RoundedCornerRadius = 3;
            this.FinishBtn.Text = "Завершить";
            this.FinishBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.FinishBtnClick);
            // 
            // BarcodesGridPanel
            // 
            this.BarcodesGridPanel.Children.Add(this.uiLabel1, 0, 0);
            this.BarcodesGridPanel.Children.Add(this.uiLabel2, 1, 0);
            this.BarcodesGridPanel.Children.Add(this.StatusBox, 1, 1);
            this.BarcodesGridPanel.Children.Add(this.BarcodeText, 0, 1);
            this.BarcodesGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.BarcodesGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(60, Resco.UIElements.GridLineValueType.Percentage, true));
            this.BarcodesGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 229, 39);
            this.BarcodesGridPanel.Name = "BarcodesGridPanel";
            this.BarcodesGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(15, Resco.UIElements.GridLineValueType.Absolute, true));
            this.BarcodesGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.BarcodesGridPanel.TabIndex = 1;
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.uiLabel1.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 62, 13);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Text = "Штрих-код";
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.uiLabel2.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 37, 13);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.TabIndex = 1;
            this.uiLabel2.Text = "Статус";
            // 
            // StatusBox
            // 
            this.StatusBox.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 2, 2, 2, 2, 81, 20);
            this.StatusBox.Name = "StatusBox";
            this.StatusBox.TabIndex = 2;
            this.StatusBox.SelectedItemChanged += new System.EventHandler(this.StatusBoxSelectedItemChanged);
            // 
            // BarcodeText
            // 
            this.BarcodeText.Buttons.Add(this.EnterBarcodeBtn);
            this.BarcodeText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.BarcodeText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 1, 1, 1, 1, 141, 22);
            this.BarcodeText.Name = "BarcodeText";
            this.BarcodeText.TabIndex = 3;
            this.BarcodeText.KeyUp += new System.Windows.Forms.KeyEventHandler(this.BarcodeTextKeyUp);
            // 
            // EnterBarcodeBtn
            // 
            this.EnterBarcodeBtn.HorizontalAlignment = Resco.UIElements.HAlignment.Right;
            this.EnterBarcodeBtn.Name = "EnterBarcodeBtn";
            this.EnterBarcodeBtn.Size = new System.Drawing.Size(16, 16);
            this.EnterBarcodeBtn.Text = "...";
            this.EnterBarcodeBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.EnterBarcodeBtnClick);
            // 
            // ItemNameGridPanel
            // 
            this.ItemNameGridPanel.Children.Add(this.uiLabel3, 0, 0);
            this.ItemNameGridPanel.Children.Add(this.ItemNameText, 0, 1);
            this.ItemNameGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ItemNameGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 229, 45);
            this.ItemNameGridPanel.Name = "ItemNameGridPanel";
            this.ItemNameGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(15, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ItemNameGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ItemNameGridPanel.TabIndex = 2;
            // 
            // uiLabel3
            // 
            this.uiLabel3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.uiLabel3.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 110, 13);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Text = "Наименование ТМЦ";
            // 
            // ItemNameText
            // 
            this.ItemNameText.IsFocusable = false;
            this.ItemNameText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 1, 1, 1, 1, 227, 28);
            this.ItemNameText.Name = "ItemNameText";
            this.ItemNameText.ReadOnly = true;
            this.ItemNameText.TabIndex = 1;
            // 
            // uiGridPanel1
            // 
            this.uiGridPanel1.Children.Add(this.uiLabel4, 0, 0);
            this.uiGridPanel1.Children.Add(this.IbsoText, 0, 1);
            this.uiGridPanel1.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel1.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 229, 39);
            this.uiGridPanel1.Name = "uiGridPanel1";
            this.uiGridPanel1.Rows.Add(new Resco.UIElements.GridLinePosition(15, Resco.UIElements.GridLineValueType.Absolute, true));
            this.uiGridPanel1.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel1.TabIndex = 3;
            // 
            // uiLabel4
            // 
            this.uiLabel4.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.uiLabel4.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 134, 13);
            this.uiLabel4.Name = "uiLabel4";
            this.uiLabel4.Text = "Местонахождение IBSO";
            // 
            // IbsoText
            // 
            this.IbsoText.IsFocusable = false;
            this.IbsoText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 1, 1, 1, 1, 227, 22);
            this.IbsoText.Name = "IbsoText";
            this.IbsoText.ReadOnly = true;
            this.IbsoText.TabIndex = 1;
            // 
            // uiGridPanel2
            // 
            this.uiGridPanel2.Children.Add(this.uiLabel5, 0, 0);
            this.uiGridPanel2.Children.Add(this.UserNameText, 0, 1);
            this.uiGridPanel2.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel2.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 229, 39);
            this.uiGridPanel2.Name = "uiGridPanel2";
            this.uiGridPanel2.Rows.Add(new Resco.UIElements.GridLinePosition(15, Resco.UIElements.GridLineValueType.Absolute, true));
            this.uiGridPanel2.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel2.TabIndex = 4;
            // 
            // uiLabel5
            // 
            this.uiLabel5.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.uiLabel5.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 78, 13);
            this.uiLabel5.Name = "uiLabel5";
            this.uiLabel5.Text = "Пользователь";
            // 
            // UserNameText
            // 
            this.UserNameText.Buttons.Add(this.EnterUserBtn);
            this.UserNameText.IsFocusable = false;
            this.UserNameText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 1, 1, 1, 1, 227, 22);
            this.UserNameText.Name = "UserNameText";
            this.UserNameText.ReadOnly = true;
            this.UserNameText.TabIndex = 1;
            // 
            // EnterUserBtn
            // 
            this.EnterUserBtn.Enabled = false;
            this.EnterUserBtn.HorizontalAlignment = Resco.UIElements.HAlignment.Right;
            this.EnterUserBtn.Name = "EnterUserBtn";
            this.EnterUserBtn.Size = new System.Drawing.Size(16, 16);
            this.EnterUserBtn.Text = "...";
            this.EnterUserBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.EnterUserBtnClick);
            // 
            // MolGridPanel
            // 
            this.MolGridPanel.Children.Add(this.uiLabel6, 0, 0);
            this.MolGridPanel.Children.Add(this.MolNameText, 0, 1);
            this.MolGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.MolGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 229, 39);
            this.MolGridPanel.Name = "MolGridPanel";
            this.MolGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(15, Resco.UIElements.GridLineValueType.Absolute, true));
            this.MolGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.MolGridPanel.TabIndex = 5;
            // 
            // uiLabel6
            // 
            this.uiLabel6.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.uiLabel6.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 27, 13);
            this.uiLabel6.Name = "uiLabel6";
            this.uiLabel6.Text = "МОЛ";
            // 
            // MolNameText
            // 
            this.MolNameText.Buttons.Add(this.EnterMolBtn);
            this.MolNameText.IsFocusable = false;
            this.MolNameText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 1, 1, 1, 1, 227, 22);
            this.MolNameText.Name = "MolNameText";
            this.MolNameText.ReadOnly = true;
            this.MolNameText.TabIndex = 1;
            // 
            // EnterMolBtn
            // 
            this.EnterMolBtn.Enabled = false;
            this.EnterMolBtn.HorizontalAlignment = Resco.UIElements.HAlignment.Right;
            this.EnterMolBtn.Name = "EnterMolBtn";
            this.EnterMolBtn.Size = new System.Drawing.Size(16, 16);
            this.EnterMolBtn.Text = "...";
            this.EnterMolBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.EnterMolBtnClick);
            // 
            // uiGridPanel3
            // 
            this.uiGridPanel3.Children.Add(this.uiLabel7, 0, 0);
            this.uiGridPanel3.Children.Add(this.StatusText, 0, 1);
            this.uiGridPanel3.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel3.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 229, 39);
            this.uiGridPanel3.Name = "uiGridPanel3";
            this.uiGridPanel3.Rows.Add(new Resco.UIElements.GridLinePosition(15, Resco.UIElements.GridLineValueType.Absolute, true));
            this.uiGridPanel3.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel3.TabIndex = 6;
            // 
            // uiLabel7
            // 
            this.uiLabel7.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.uiLabel7.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 95, 13);
            this.uiLabel7.Name = "uiLabel7";
            this.uiLabel7.Text = "Серийный номер";
            // 
            // StatusText
            // 
            this.StatusText.Buttons.Add(this.EnterSerialBtn);
            this.StatusText.IsFocusable = false;
            this.StatusText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 1, 1, 1, 1, 227, 22);
            this.StatusText.Name = "StatusText";
            this.StatusText.ReadOnly = true;
            this.StatusText.TabIndex = 1;
            // 
            // EnterSerialBtn
            // 
            this.EnterSerialBtn.Enabled = false;
            this.EnterSerialBtn.HorizontalAlignment = Resco.UIElements.HAlignment.Right;
            this.EnterSerialBtn.Name = "EnterSerialBtn";
            this.EnterSerialBtn.Size = new System.Drawing.Size(16, 16);
            this.EnterSerialBtn.Text = "...";
            this.EnterSerialBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.EnterSerialBtnClick);
            // 
            // FindPage
            // 
            this.FindPage.Children.Add(this.FindItemsGridPanel);
            this.FindPage.Name = "FindPage";
            this.FindPage.Text = "Найдено";
            // 
            // FindItemsGridPanel
            // 
            this.FindItemsGridPanel.Children.Add(this.FoundBottomGridPanel, 0, 2);
            this.FindItemsGridPanel.Children.Add(this.FindItemsListView, 0, 1);
            this.FindItemsGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.FindItemsGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 229, 265);
            this.FindItemsGridPanel.Name = "FindItemsGridPanel";
            this.FindItemsGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(15, Resco.UIElements.GridLineValueType.Absolute, true));
            this.FindItemsGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.FindItemsGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(25, Resco.UIElements.GridLineValueType.Absolute, true));
            this.FindItemsGridPanel.ScrollBarsMode = Resco.UIElements.ScrollBarsMode.OutOfContent;
            // 
            // FoundBottomGridPanel
            // 
            this.FoundBottomGridPanel.Children.Add(this.DeleteBtn, 1, 0);
            this.FoundBottomGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.FoundBottomGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Absolute, true));
            this.FoundBottomGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.FoundBottomGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 229, 25);
            this.FoundBottomGridPanel.Name = "FoundBottomGridPanel";
            this.FoundBottomGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            // 
            // DeleteBtn
            // 
            this.DeleteBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 3, 2, 3, 1, 94, 22);
            this.DeleteBtn.Name = "DeleteBtn";
            this.DeleteBtn.RoundedCornerRadius = 3;
            this.DeleteBtn.Text = "Удалить";
            this.DeleteBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.DeleteBtnClick);
            // 
            // FindItemsListView
            // 
            this.FindItemsListView.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 1, 1, 1, 1, 227, 223);
            this.FindItemsListView.Name = "FindItemsListView";
            this.FindItemsListView.TabIndex = 1;
            this.FindItemsListView.SelectedIndexChanging += new Resco.UIElements.SelectedIndexChangingEventHandler(this.FindItemsListViewSelectedIndexChanging);
            // 
            // NotFindPage
            // 
            this.NotFindPage.Children.Add(this.NotFindItemsGridPanel);
            this.NotFindPage.Name = "NotFindPage";
            this.NotFindPage.Text = "Не найдено";
            // 
            // NotFindItemsGridPanel
            // 
            this.NotFindItemsGridPanel.Children.Add(this.NotFoundBottomGridPanel, 0, 2);
            this.NotFindItemsGridPanel.Children.Add(this.NotFindItemsListView, 0, 1);
            this.NotFindItemsGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.NotFindItemsGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 229, 265);
            this.NotFindItemsGridPanel.Name = "NotFindItemsGridPanel";
            this.NotFindItemsGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(15, Resco.UIElements.GridLineValueType.Absolute, true));
            this.NotFindItemsGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.NotFindItemsGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(25, Resco.UIElements.GridLineValueType.Absolute, true));
            this.NotFindItemsGridPanel.ScrollBarsMode = Resco.UIElements.ScrollBarsMode.OutOfContent;
            // 
            // NotFoundBottomGridPanel
            // 
            this.NotFoundBottomGridPanel.Children.Add(this.PrintBtn, 1, 0);
            this.NotFoundBottomGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.NotFoundBottomGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Absolute, true));
            this.NotFoundBottomGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.NotFoundBottomGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 229, 25);
            this.NotFoundBottomGridPanel.Name = "NotFoundBottomGridPanel";
            this.NotFoundBottomGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            // 
            // PrintBtn
            // 
            this.PrintBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 3, 2, 3, 1, 94, 22);
            this.PrintBtn.Name = "PrintBtn";
            this.PrintBtn.RoundedCornerRadius = 3;
            this.PrintBtn.Text = "Печать";
            this.PrintBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.PrintBtnClick);
            // 
            // NotFindItemsListView
            // 
            this.NotFindItemsListView.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 1, 1, 1, 1, 227, 223);
            this.NotFindItemsListView.Name = "NotFindItemsListView";
            this.NotFindItemsListView.TabIndex = 1;
            this.NotFindItemsListView.SelectedIndexChanging += new Resco.UIElements.SelectedIndexChangingEventHandler(this.NotFindItemsListViewSelectedIndexChanging);
            // 
            // InventoryControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.HeaderText = "ВСЕГО - 0 / НАЙДЕНО - 0";
            this.Size = new System.Drawing.Size(229, 321);
            this.ResumeLayout();

        }

        #endregion

        private Resco.UIElements.UITabPanel InventoryPanel;
        private Resco.UIElements.UITabPage EnterPage;
        private Resco.UIElements.UITabPage FindPage;
        private Resco.UIElements.UITabPage NotFindPage;
        private Resco.UIElements.UIGridPanel InventoryGridPanel;
        private Resco.UIElements.UIGridPanel ButtomGridPanel;
        private Stock.Controls.StockButton FinishBtn;
        private Resco.UIElements.UIGridPanel BarcodesGridPanel;
        private Resco.UIElements.UILabel uiLabel1;
        private Resco.UIElements.UILabel uiLabel2;
        private Resco.UIElements.UIComboBox StatusBox;
        private Resco.UIElements.UIGridPanel ItemNameGridPanel;
        private Resco.UIElements.UILabel uiLabel3;
        private Stock.Controls.StockTextEdit ItemNameText;
        private Stock.Controls.StockTextEdit BarcodeText;
        private Resco.UIElements.UITextBoxButton EnterBarcodeBtn;
        private Resco.UIElements.UIGridPanel uiGridPanel1;
        private Resco.UIElements.UILabel uiLabel4;
        private Stock.Controls.StockTextEdit IbsoText;
        private Resco.UIElements.UIGridPanel uiGridPanel2;
        private Resco.UIElements.UILabel uiLabel5;
        private Stock.Controls.StockTextEdit UserNameText;
        private Resco.UIElements.UITextBoxButton EnterUserBtn;
        private Resco.UIElements.UIGridPanel MolGridPanel;
        private Resco.UIElements.UILabel uiLabel6;
        private Stock.Controls.StockTextEdit MolNameText;
        private Resco.UIElements.UITextBoxButton EnterMolBtn;
        private Resco.UIElements.UIGridPanel uiGridPanel3;
        private Resco.UIElements.UILabel uiLabel7;
        private Stock.Controls.StockTextEdit StatusText;
        private Resco.UIElements.UITextBoxButton EnterSerialBtn;
        private Resco.UIElements.UIGridPanel FindItemsGridPanel;
        private Resco.UIElements.UIGridPanel NotFindItemsGridPanel;
        private Resco.UIElements.UIGridPanel FoundBottomGridPanel;
        private Resco.UIElements.UIGridPanel NotFoundBottomGridPanel;
        private Stock.Controls.StockButton DeleteBtn;
        private Stock.Controls.StockButton PrintBtn;
        private Resco.UIElements.UIListView FindItemsListView;
        private Resco.UIElements.UIListView NotFindItemsListView;
    }
}
