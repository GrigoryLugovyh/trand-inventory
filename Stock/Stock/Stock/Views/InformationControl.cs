﻿using System;
using System.Collections.Generic;
using Resco.UIElements;
using Stock.Views.InventoryObjects;
using StockCore.Entity;
using StockCore.Interfaces.Views;

namespace Stock.Views
{
    public partial class InformationControl : StockControl, IInformationView
    {
        public InformationControl()
        {
            InitializeComponent();
        }

        public event EventHandler Show;

        public event EventHandler Close;

        public event EventHandler BarcodeEnterHand;

        public event EventHandler ToInformation;

        public event EventHandler ToAll;

        public List<Objects> Objects
        {
            get { return AllObjectsListView.DataSource as List<Objects>; }
            set { Invoker(() => AllObjectsListView.DataSource = value); }
        }

        public Objects SelectedObject
        {
            get { return AllObjectsListView.SelectedItem as Objects; }
        }

        public void ObjectsRefresh()
        {
            Invoker(() => AllObjectsListView.Refresh());
        }

        public event EventHandler SelectAllChanged;

        public bool SelectAll
        {
            get { return SelectAllCheck.Selected; }
        }

        public string Filter
        {
            get { return AllFilterText.Text; }
            set { Invoker(() => AllFilterText.Text = value); }
        }

        public event EventHandler Find;

        public event EventHandler Open;

        public event EventHandler PrintItem;

        public event EventHandler PrintObjects;

        public void ShowView()
        {
            AllObjectsListView.DataTemplate = new NormalObjectControl {SelectedVisible = true};
            AllObjectsListView.SelectedDataTemplate = new SelectObjectControl {SelectedVisible = true};

            if (Show != null)
            {
                Show(this, EventArgs.Empty);
            }

            ShowView<InformationControl>(AnimationEffect.MoveLeft);

            InventoryPanel.SelectedIndex = 0;

            Invoker(() => EnterBarcodeBtn.Focus());
        }

        public void CloseView()
        {
            if (Close != null)
            {
                Close(this, EventArgs.Empty);
            }

            ShowView<MainControl>(AnimationEffect.MoveRight);
        }

        private void CloseInfoBtnClick(object sender, UIMouseEventArgs e)
        {
            CloseView();
        }

        private void CloseAllBtnClick(object sender, UIMouseEventArgs e)
        {
            CloseView();
        }

        public string Status
        {
            get { return StatusBox.Text; }
            set { Invoker(() => StatusBox.Text = value); }
        }

        public string Barcode
        {
            get { return BarcodeText.Text; }
            set { Invoker(() => BarcodeText.Text = value); }
        }

        public string ItemName
        {
            get { return ItemNameText.Text; }
            set { Invoker(() => ItemNameText.Text = value); }
        }

        public string Ibso
        {
            get { return IbsoText.Text; }
            set { Invoker(() => IbsoText.Text = value); }
        }

        public string UserName
        {
            get { return UserNameText.Text; }
            set { Invoker(() => UserNameText.Text = value); }
        }

        public string MolName
        {
            get { return MolNameText.Text; }
            set { Invoker(() => MolNameText.Text = value); }
        }

        public string Serial
        {
            get { return StatusText.Text; }
            set { Invoker(() => StatusText.Text = value); }
        }

        private void InventoryPanelSelectedIndexChanging(object sender, SelectedIndexChangingEventArgs e)
        {
            switch (e.NewIndex)
            {
                case 0:
                    {
                        if (ToInformation != null)
                        {
                            ToInformation(sender, e);
                        }
                        break;
                    }
                case 1:
                    {
                        if (ToAll != null)
                        {
                            ToAll(sender, e);
                        }
                        break;
                    }
            }
        }

        private void EnterBarcodeBtnClick(object sender, UIMouseEventArgs e)
        {
            if(BarcodeEnterHand != null)
            {
                BarcodeEnterHand(sender, e);
            }
        }

        private void SelectAllCheckCheckedChanged(object sender, EventArgs e)
        {
            if(SelectAllChanged != null)
            {
                SelectAllChanged(sender, e);
            }
        }

        private void AllFilterBtnClick(object sender, UIMouseEventArgs e)
        {
            if(Find != null)
            {
                Find(sender, e);
            }
        }

        private void OpenObjectBtnClick(object sender, UIMouseEventArgs e)
        {
            if(Open != null)
            {
                Open(sender, e);
            }
        }

        private void PrintInfoBtnClick(object sender, UIMouseEventArgs e)
        {
            if(PrintItem != null)
            {
                PrintItem(sender, e);
            }
        }

        private void PrintAllBtnClick(object sender, UIMouseEventArgs e)
        {
            if(PrintObjects != null)
            {
                PrintObjects(sender, e);
            }
        }
    }
}