﻿using Stock.Controls;

namespace Stock.Views
{
    partial class InformationControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.InventoryPanel = new Resco.UIElements.UITabPanel();
            this.InfoPage = new Resco.UIElements.UITabPage();
            this.InventoryGridPanel = new Resco.UIElements.UIGridPanel();
            this.ButtomGridPanel = new Resco.UIElements.UIGridPanel();
            this.CloseInfoBtn = new Stock.Controls.StockButton();
            this.PrintInfoBtn = new Stock.Controls.StockButton();
            this.BarcodesGridPanel = new Resco.UIElements.UIGridPanel();
            this.uiLabel1 = new Resco.UIElements.UILabel();
            this.uiLabel2 = new Resco.UIElements.UILabel();
            this.BarcodeText = new Stock.Controls.StockTextEdit();
            this.EnterBarcodeBtn = new Resco.UIElements.UITextBoxButton();
            this.StatusBox = new Stock.Controls.StockTextEdit();
            this.ItemNameGridPanel = new Resco.UIElements.UIGridPanel();
            this.uiLabel3 = new Resco.UIElements.UILabel();
            this.ItemNameText = new Stock.Controls.StockTextEdit();
            this.uiGridPanel1 = new Resco.UIElements.UIGridPanel();
            this.uiLabel4 = new Resco.UIElements.UILabel();
            this.IbsoText = new Stock.Controls.StockTextEdit();
            this.uiGridPanel2 = new Resco.UIElements.UIGridPanel();
            this.uiLabel5 = new Resco.UIElements.UILabel();
            this.UserNameText = new Stock.Controls.StockTextEdit();
            this.MolGridPanel = new Resco.UIElements.UIGridPanel();
            this.uiLabel6 = new Resco.UIElements.UILabel();
            this.MolNameText = new Stock.Controls.StockTextEdit();
            this.uiGridPanel3 = new Resco.UIElements.UIGridPanel();
            this.uiLabel7 = new Resco.UIElements.UILabel();
            this.StatusText = new Stock.Controls.StockTextEdit();
            this.AllPage = new Resco.UIElements.UITabPage();
            this.FindItemsGridPanel = new Resco.UIElements.UIGridPanel();
            this.FoundBottomGridPanel = new Resco.UIElements.UIGridPanel();
            this.OpenObjectBtn = new Stock.Controls.StockButton();
            this.CloseAllBtn = new Stock.Controls.StockButton();
            this.PrintAllBtn = new Stock.Controls.StockButton();
            this.AllObjectsListView = new Resco.UIElements.UIListView();
            this.uiGridPanel4 = new Resco.UIElements.UIGridPanel();
            this.AllFilterBtn = new Stock.Controls.StockButton();
            this.AllFilterText = new Stock.Controls.StockTextEdit();
            this.uiTextBoxButton1 = new Resco.UIElements.UITextBoxButton();
            this.SelectAllCheck = new Stock.Controls.StockCheckBox();
            this.SuspendLayout();
            // 
            // StockGridPanel
            // 
            this.StockGridPanel.Children.Add(this.InventoryPanel, 0, 1);
            // 
            // HeaderLabel
            // 
            this.HeaderLabel.Text = "Информация о ТМЦ";
            this.HeaderLabel.TextAlignment = Resco.Drawing.Alignment.MiddleCenter;
            // 
            // StatusLabel
            // 
            this.StatusLabel.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold);
            this.StatusLabel.Text = "Просканируйте штрих-код";
            // 
            // InventoryPanel
            // 
            this.InventoryPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 229, 291);
            this.InventoryPanel.Name = "InventoryPanel";
            this.InventoryPanel.TabIndex = 1;
            this.InventoryPanel.TabPages.Add(this.InfoPage);
            this.InventoryPanel.TabPages.Add(this.AllPage);
            this.InventoryPanel.TabPagesMode = Resco.UIElements.UITabPanelMode.Top;
            this.InventoryPanel.SelectedIndexChanging += new Resco.UIElements.SelectedIndexChangingEventHandler(this.InventoryPanelSelectedIndexChanging);
            // 
            // InfoPage
            // 
            this.InfoPage.Children.Add(this.InventoryGridPanel);
            this.InfoPage.Name = "InfoPage";
            this.InfoPage.Text = "Ввод ТМЦ";
            // 
            // InventoryGridPanel
            // 
            this.InventoryGridPanel.Children.Add(this.ButtomGridPanel, 0, 6);
            this.InventoryGridPanel.Children.Add(this.BarcodesGridPanel, 0, 0);
            this.InventoryGridPanel.Children.Add(this.ItemNameGridPanel, 0, 1);
            this.InventoryGridPanel.Children.Add(this.uiGridPanel1, 0, 2);
            this.InventoryGridPanel.Children.Add(this.uiGridPanel2, 0, 3);
            this.InventoryGridPanel.Children.Add(this.MolGridPanel, 0, 4);
            this.InventoryGridPanel.Children.Add(this.uiGridPanel3, 0, 5);
            this.InventoryGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.InventoryGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 229, 265);
            this.InventoryGridPanel.Name = "InventoryGridPanel";
            this.InventoryGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(39, Resco.UIElements.GridLineValueType.Absolute, true));
            this.InventoryGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.InventoryGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(39, Resco.UIElements.GridLineValueType.Absolute, true));
            this.InventoryGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(39, Resco.UIElements.GridLineValueType.Absolute, true));
            this.InventoryGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(39, Resco.UIElements.GridLineValueType.Absolute, true));
            this.InventoryGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(39, Resco.UIElements.GridLineValueType.Absolute, true));
            this.InventoryGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(25, Resco.UIElements.GridLineValueType.Absolute, true));
            // 
            // ButtomGridPanel
            // 
            this.ButtomGridPanel.Children.Add(this.CloseInfoBtn, 0, 0);
            this.ButtomGridPanel.Children.Add(this.PrintInfoBtn, 1, 0);
            this.ButtomGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ButtomGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ButtomGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 229, 25);
            this.ButtomGridPanel.Name = "ButtomGridPanel";
            this.ButtomGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            // 
            // CloseInfoBtn
            // 
            this.CloseInfoBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 3, 2, 3, 1, 108, 22);
            this.CloseInfoBtn.Name = "CloseInfoBtn";
            this.CloseInfoBtn.RoundedCornerRadius = 3;
            this.CloseInfoBtn.Text = "Завершить";
            this.CloseInfoBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.CloseInfoBtnClick);
            // 
            // PrintInfoBtn
            // 
            this.PrintInfoBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 3, 2, 3, 1, 108, 22);
            this.PrintInfoBtn.Name = "PrintInfoBtn";
            this.PrintInfoBtn.RoundedCornerRadius = 3;
            this.PrintInfoBtn.TabIndex = 1;
            this.PrintInfoBtn.Text = "Печать";
            this.PrintInfoBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.PrintInfoBtnClick);
            // 
            // BarcodesGridPanel
            // 
            this.BarcodesGridPanel.Children.Add(this.uiLabel1, 0, 0);
            this.BarcodesGridPanel.Children.Add(this.uiLabel2, 1, 0);
            this.BarcodesGridPanel.Children.Add(this.BarcodeText, 0, 1);
            this.BarcodesGridPanel.Children.Add(this.StatusBox, 1, 1);
            this.BarcodesGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.BarcodesGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(60, Resco.UIElements.GridLineValueType.Percentage, true));
            this.BarcodesGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 229, 39);
            this.BarcodesGridPanel.Name = "BarcodesGridPanel";
            this.BarcodesGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(15, Resco.UIElements.GridLineValueType.Absolute, true));
            this.BarcodesGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.BarcodesGridPanel.TabIndex = 1;
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.uiLabel1.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 62, 13);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Text = "Штрих-код";
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.uiLabel2.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 37, 13);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.TabIndex = 1;
            this.uiLabel2.Text = "Статус";
            // 
            // BarcodeText
            // 
            this.BarcodeText.Buttons.Add(this.EnterBarcodeBtn);
            this.BarcodeText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.BarcodeText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 1, 1, 1, 1, 141, 22);
            this.BarcodeText.Name = "BarcodeText";
            this.BarcodeText.TabIndex = 3;
            // 
            // EnterBarcodeBtn
            // 
            this.EnterBarcodeBtn.HorizontalAlignment = Resco.UIElements.HAlignment.Right;
            this.EnterBarcodeBtn.Name = "EnterBarcodeBtn";
            this.EnterBarcodeBtn.Size = new System.Drawing.Size(16, 16);
            this.EnterBarcodeBtn.Text = "...";
            this.EnterBarcodeBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.EnterBarcodeBtnClick);
            // 
            // StatusBox
            // 
            this.StatusBox.IsFocusable = false;
            this.StatusBox.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 1, 1, 1, 1, 83, 22);
            this.StatusBox.Name = "StatusBox";
            this.StatusBox.ReadOnly = true;
            this.StatusBox.TabIndex = 4;
            // 
            // ItemNameGridPanel
            // 
            this.ItemNameGridPanel.Children.Add(this.uiLabel3, 0, 0);
            this.ItemNameGridPanel.Children.Add(this.ItemNameText, 0, 1);
            this.ItemNameGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ItemNameGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 229, 45);
            this.ItemNameGridPanel.Name = "ItemNameGridPanel";
            this.ItemNameGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(15, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ItemNameGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ItemNameGridPanel.TabIndex = 2;
            // 
            // uiLabel3
            // 
            this.uiLabel3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.uiLabel3.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 110, 13);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Text = "Наименование ТМЦ";
            // 
            // ItemNameText
            // 
            this.ItemNameText.IsFocusable = false;
            this.ItemNameText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 1, 1, 1, 1, 227, 28);
            this.ItemNameText.Name = "ItemNameText";
            this.ItemNameText.ReadOnly = true;
            this.ItemNameText.TabIndex = 1;
            // 
            // uiGridPanel1
            // 
            this.uiGridPanel1.Children.Add(this.uiLabel4, 0, 0);
            this.uiGridPanel1.Children.Add(this.IbsoText, 0, 1);
            this.uiGridPanel1.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel1.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 229, 39);
            this.uiGridPanel1.Name = "uiGridPanel1";
            this.uiGridPanel1.Rows.Add(new Resco.UIElements.GridLinePosition(15, Resco.UIElements.GridLineValueType.Absolute, true));
            this.uiGridPanel1.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel1.TabIndex = 3;
            // 
            // uiLabel4
            // 
            this.uiLabel4.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.uiLabel4.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 134, 13);
            this.uiLabel4.Name = "uiLabel4";
            this.uiLabel4.Text = "Местонахождение IBSO";
            // 
            // IbsoText
            // 
            this.IbsoText.IsFocusable = false;
            this.IbsoText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 1, 1, 1, 1, 227, 22);
            this.IbsoText.Name = "IbsoText";
            this.IbsoText.ReadOnly = true;
            this.IbsoText.TabIndex = 1;
            // 
            // uiGridPanel2
            // 
            this.uiGridPanel2.Children.Add(this.uiLabel5, 0, 0);
            this.uiGridPanel2.Children.Add(this.UserNameText, 0, 1);
            this.uiGridPanel2.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel2.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 229, 39);
            this.uiGridPanel2.Name = "uiGridPanel2";
            this.uiGridPanel2.Rows.Add(new Resco.UIElements.GridLinePosition(15, Resco.UIElements.GridLineValueType.Absolute, true));
            this.uiGridPanel2.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel2.TabIndex = 4;
            // 
            // uiLabel5
            // 
            this.uiLabel5.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.uiLabel5.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 78, 13);
            this.uiLabel5.Name = "uiLabel5";
            this.uiLabel5.Text = "Пользователь";
            // 
            // UserNameText
            // 
            this.UserNameText.IsFocusable = false;
            this.UserNameText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 1, 1, 1, 1, 227, 22);
            this.UserNameText.Name = "UserNameText";
            this.UserNameText.ReadOnly = true;
            this.UserNameText.TabIndex = 1;
            // 
            // MolGridPanel
            // 
            this.MolGridPanel.Children.Add(this.uiLabel6, 0, 0);
            this.MolGridPanel.Children.Add(this.MolNameText, 0, 1);
            this.MolGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.MolGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 229, 39);
            this.MolGridPanel.Name = "MolGridPanel";
            this.MolGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(15, Resco.UIElements.GridLineValueType.Absolute, true));
            this.MolGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.MolGridPanel.TabIndex = 5;
            // 
            // uiLabel6
            // 
            this.uiLabel6.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.uiLabel6.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 27, 13);
            this.uiLabel6.Name = "uiLabel6";
            this.uiLabel6.Text = "МОЛ";
            // 
            // MolNameText
            // 
            this.MolNameText.IsFocusable = false;
            this.MolNameText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 1, 1, 1, 1, 227, 22);
            this.MolNameText.Name = "MolNameText";
            this.MolNameText.ReadOnly = true;
            this.MolNameText.TabIndex = 1;
            // 
            // uiGridPanel3
            // 
            this.uiGridPanel3.Children.Add(this.uiLabel7, 0, 0);
            this.uiGridPanel3.Children.Add(this.StatusText, 0, 1);
            this.uiGridPanel3.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel3.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 229, 39);
            this.uiGridPanel3.Name = "uiGridPanel3";
            this.uiGridPanel3.Rows.Add(new Resco.UIElements.GridLinePosition(15, Resco.UIElements.GridLineValueType.Absolute, true));
            this.uiGridPanel3.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel3.TabIndex = 6;
            // 
            // uiLabel7
            // 
            this.uiLabel7.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.uiLabel7.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 95, 13);
            this.uiLabel7.Name = "uiLabel7";
            this.uiLabel7.Text = "Серийный номер";
            // 
            // StatusText
            // 
            this.StatusText.IsFocusable = false;
            this.StatusText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 1, 1, 1, 1, 227, 22);
            this.StatusText.Name = "StatusText";
            this.StatusText.ReadOnly = true;
            this.StatusText.TabIndex = 1;
            // 
            // AllPage
            // 
            this.AllPage.Children.Add(this.FindItemsGridPanel);
            this.AllPage.Name = "AllPage";
            this.AllPage.Text = "Все ТМЦ";
            // 
            // FindItemsGridPanel
            // 
            this.FindItemsGridPanel.Children.Add(this.FoundBottomGridPanel, 0, 2);
            this.FindItemsGridPanel.Children.Add(this.AllObjectsListView, 0, 1);
            this.FindItemsGridPanel.Children.Add(this.uiGridPanel4, 0, 0);
            this.FindItemsGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.FindItemsGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 229, 265);
            this.FindItemsGridPanel.Name = "FindItemsGridPanel";
            this.FindItemsGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(30, Resco.UIElements.GridLineValueType.Absolute, true));
            this.FindItemsGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.FindItemsGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(25, Resco.UIElements.GridLineValueType.Absolute, true));
            this.FindItemsGridPanel.ScrollBarsMode = Resco.UIElements.ScrollBarsMode.OutOfContent;
            // 
            // FoundBottomGridPanel
            // 
            this.FoundBottomGridPanel.Children.Add(this.OpenObjectBtn, 1, 0);
            this.FoundBottomGridPanel.Children.Add(this.CloseAllBtn, 0, 0);
            this.FoundBottomGridPanel.Children.Add(this.PrintAllBtn, 2, 0);
            this.FoundBottomGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.FoundBottomGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.FoundBottomGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.FoundBottomGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 229, 25);
            this.FoundBottomGridPanel.Name = "FoundBottomGridPanel";
            this.FoundBottomGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            // 
            // OpenObjectBtn
            // 
            this.OpenObjectBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 3, 2, 3, 1, 70, 22);
            this.OpenObjectBtn.Name = "OpenObjectBtn";
            this.OpenObjectBtn.RoundedCornerRadius = 3;
            this.OpenObjectBtn.Text = "Открыть";
            this.OpenObjectBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.OpenObjectBtnClick);
            // 
            // CloseAllBtn
            // 
            this.CloseAllBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 3, 2, 3, 1, 70, 22);
            this.CloseAllBtn.Name = "CloseAllBtn";
            this.CloseAllBtn.RoundedCornerRadius = 3;
            this.CloseAllBtn.TabIndex = 1;
            this.CloseAllBtn.Text = "Завершить";
            this.CloseAllBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.CloseAllBtnClick);
            // 
            // PrintAllBtn
            // 
            this.PrintAllBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 3, 2, 3, 1, 70, 22);
            this.PrintAllBtn.Name = "PrintAllBtn";
            this.PrintAllBtn.RoundedCornerRadius = 3;
            this.PrintAllBtn.TabIndex = 2;
            this.PrintAllBtn.Text = "Печать";
            this.PrintAllBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.PrintAllBtnClick);
            // 
            // AllObjectsListView
            // 
            this.AllObjectsListView.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 1, 1, 1, 1, 227, 208);
            this.AllObjectsListView.Name = "AllObjectsListView";
            this.AllObjectsListView.TabIndex = 1;
            // 
            // uiGridPanel4
            // 
            this.uiGridPanel4.Children.Add(this.AllFilterBtn, 2, 0);
            this.uiGridPanel4.Children.Add(this.AllFilterText, 1, 0);
            this.uiGridPanel4.Children.Add(this.SelectAllCheck, 0, 0);
            this.uiGridPanel4.Columns.Add(new Resco.UIElements.GridLinePosition(22, Resco.UIElements.GridLineValueType.Absolute, true));
            this.uiGridPanel4.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel4.Columns.Add(new Resco.UIElements.GridLinePosition(60, Resco.UIElements.GridLineValueType.Absolute, true));
            this.uiGridPanel4.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 229, 30);
            this.uiGridPanel4.Name = "uiGridPanel4";
            this.uiGridPanel4.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel4.TabIndex = 2;
            // 
            // AllFilterBtn
            // 
            this.AllFilterBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 2, 2, 2, 2, 56, 26);
            this.AllFilterBtn.Name = "AllFilterBtn";
            this.AllFilterBtn.RoundedCornerRadius = 3;
            this.AllFilterBtn.Text = "Фильтр";
            this.AllFilterBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.AllFilterBtnClick);
            // 
            // AllFilterText
            // 
            this.AllFilterText.Buttons.Add(this.uiTextBoxButton1);
            this.AllFilterText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 2, 2, 2, 2, 143, 26);
            this.AllFilterText.Name = "AllFilterText";
            this.AllFilterText.RoundedCornerRadius = 3;
            this.AllFilterText.TabIndex = 1;
            // 
            // uiTextBoxButton1
            // 
            this.uiTextBoxButton1.Action = Resco.UIElements.TextBoxAction.Clear;
            this.uiTextBoxButton1.BackColor = System.Drawing.Color.Transparent;
            this.uiTextBoxButton1.BorderThickness = 0;
            this.uiTextBoxButton1.HorizontalAlignment = Resco.UIElements.HAlignment.Right;
            this.uiTextBoxButton1.Name = "uiTextBoxButton1";
            this.uiTextBoxButton1.PressedBackground.BackColor = System.Drawing.Color.Transparent;
            this.uiTextBoxButton1.Size = new System.Drawing.Size(18, 18);
            this.uiTextBoxButton1.StateIcon = Resco.UIElements.StateIcon.Delete;
            this.uiTextBoxButton1.VisibleMode = Resco.UIElements.UITextBoxButtonVisibleMode.WhileEditing;
            // 
            // SelectAllCheck
            // 
            this.SelectAllCheck.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 29, 29);
            this.SelectAllCheck.Name = "SelectAllCheck";
            this.SelectAllCheck.TabIndex = 2;
            this.SelectAllCheck.CheckedChanged += new System.EventHandler(this.SelectAllCheckCheckedChanged);
            // 
            // InformationControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.HeaderText = "Информация о ТМЦ";
            this.Size = new System.Drawing.Size(229, 321);
            this.ResumeLayout();

        }

        #endregion

        private Resco.UIElements.UITabPanel InventoryPanel;
        private Resco.UIElements.UITabPage InfoPage;
        private Resco.UIElements.UITabPage AllPage;
        private Resco.UIElements.UIGridPanel InventoryGridPanel;
        private Resco.UIElements.UIGridPanel ButtomGridPanel;
        private Stock.Controls.StockButton CloseInfoBtn;
        private Resco.UIElements.UIGridPanel BarcodesGridPanel;
        private Resco.UIElements.UILabel uiLabel1;
        private Resco.UIElements.UILabel uiLabel2;
        private Resco.UIElements.UIGridPanel ItemNameGridPanel;
        private Resco.UIElements.UILabel uiLabel3;
        private Stock.Controls.StockTextEdit ItemNameText;
        private Stock.Controls.StockTextEdit BarcodeText;
        private Resco.UIElements.UITextBoxButton EnterBarcodeBtn;
        private Resco.UIElements.UIGridPanel uiGridPanel1;
        private Resco.UIElements.UILabel uiLabel4;
        private Stock.Controls.StockTextEdit IbsoText;
        private Resco.UIElements.UIGridPanel uiGridPanel2;
        private Resco.UIElements.UILabel uiLabel5;
        private Stock.Controls.StockTextEdit UserNameText;
        private Resco.UIElements.UIGridPanel MolGridPanel;
        private Resco.UIElements.UILabel uiLabel6;
        private Stock.Controls.StockTextEdit MolNameText;
        private Resco.UIElements.UIGridPanel uiGridPanel3;
        private Resco.UIElements.UILabel uiLabel7;
        private Stock.Controls.StockTextEdit StatusText;
        private Resco.UIElements.UIGridPanel FindItemsGridPanel;
        private Resco.UIElements.UIGridPanel FoundBottomGridPanel;
        private Stock.Controls.StockButton OpenObjectBtn;
        private Resco.UIElements.UIListView AllObjectsListView;
        private Stock.Controls.StockButton PrintInfoBtn;
        private Stock.Controls.StockTextEdit StatusBox;
        private Stock.Controls.StockButton CloseAllBtn;
        private Stock.Controls.StockButton PrintAllBtn;
        private Resco.UIElements.UIGridPanel uiGridPanel4;
        private Stock.Controls.StockButton AllFilterBtn;
        private Stock.Controls.StockTextEdit AllFilterText;
        private Resco.UIElements.UITextBoxButton uiTextBoxButton1;
        private StockCheckBox SelectAllCheck;
    }
}
