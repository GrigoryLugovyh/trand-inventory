﻿using System;
using System.Collections.Generic;
using Resco.UIElements;
using StockCore.Entity;
using StockCore.Interfaces.Views;

namespace Stock.Views
{
    public partial class SeleectItemStatusControl : StockControl, ISeleectItemStatusView
    {
        public SeleectItemStatusControl()
        {
            InitializeComponent();
        }

        public event EventHandler Show;

        public event EventHandler Close;

        public event EventHandler Change;

        public void ShowView()
        {
            if(Show != null)
            {
                Show(this, EventArgs.Empty);
            }

            ShowView<SeleectItemStatusControl>(AnimationEffect.MoveLeft);

            Invoker(() => ChangeBtn.Focus());
        }

        public void CloseView()
        {
            if(Close != null)
            {
                Close(this, EventArgs.Empty);
            }

            ShowView<InventoryControl>(AnimationEffect.MoveRight);
        }

        private void ChangeBtnClick(object sender, UIMouseEventArgs e)
        {
            if (Change != null)
            {
                Change(sender, e);
            }
        }

        public List<Statuses> Statuses
        {
            get { return (List<Statuses>) StatusesBox.DataSource; }
            set { Invoker(() => StatusesBox.DataSource = value); }
        }

        public Statuses SelectedStatus
        {
            get { return StatusesBox.SelectedItem as Statuses; }
        }
    }
}
