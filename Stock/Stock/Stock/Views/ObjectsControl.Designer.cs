﻿using Stock.Controls;

namespace Stock.Views
{
    partial class ObjectsControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.InventoryGridPanel = new Resco.UIElements.UIGridPanel();
            this.FilterGridPanel = new Resco.UIElements.UIGridPanel();
            this.FilterBtn = new Stock.Controls.StockButton();
            this.FilterText = new Stock.Controls.StockTextEdit();
            this.uiTextBoxButton1 = new Resco.UIElements.UITextBoxButton();
            this.ObjectNameText = new Resco.UIElements.UILabel();
            this.ManageGridPanel = new Resco.UIElements.UIGridPanel();
            this.SelectBtn = new Stock.Controls.StockButton();
            this.AddBtn = new Stock.Controls.StockButton();
            this.CanselBtn = new Stock.Controls.StockButton();
            this.ObjectsListView = new Resco.UIElements.UIListView();
            this.SuspendLayout();
            // 
            // StockGridPanel
            // 
            this.StockGridPanel.Children.Add(this.InventoryGridPanel, 0, 1);
            // 
            // StatusLabel
            // 
            this.StatusLabel.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold);
            this.StatusLabel.Text = "Просканируйте штрихкод или введите код";
            // 
            // InventoryGridPanel
            // 
            this.InventoryGridPanel.Children.Add(this.FilterGridPanel, 0, 0);
            this.InventoryGridPanel.Children.Add(this.ObjectNameText, 0, 2);
            this.InventoryGridPanel.Children.Add(this.ManageGridPanel, 0, 3);
            this.InventoryGridPanel.Children.Add(this.ObjectsListView, 0, 1);
            this.InventoryGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.InventoryGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 290);
            this.InventoryGridPanel.Name = "InventoryGridPanel";
            this.InventoryGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(30, Resco.UIElements.GridLineValueType.Absolute, true));
            this.InventoryGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.InventoryGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(0, Resco.UIElements.GridLineValueType.Absolute, true));
            this.InventoryGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(30, Resco.UIElements.GridLineValueType.Absolute, true));
            this.InventoryGridPanel.TabIndex = 1;
            // 
            // FilterGridPanel
            // 
            this.FilterGridPanel.Children.Add(this.FilterBtn, 1, 0);
            this.FilterGridPanel.Children.Add(this.FilterText, 0, 0);
            this.FilterGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.FilterGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(60, Resco.UIElements.GridLineValueType.Absolute, true));
            this.FilterGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 30);
            this.FilterGridPanel.Name = "FilterGridPanel";
            this.FilterGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            // 
            // FilterBtn
            // 
            this.FilterBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 2, 2, 2, 2, 56, 26);
            this.FilterBtn.Name = "FilterBtn";
            this.FilterBtn.RoundedCornerRadius = 3;
            this.FilterBtn.Text = "Фильтр";
            this.FilterBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.FilterBtnClick);
            // 
            // FilterText
            // 
            this.FilterText.Buttons.Add(this.uiTextBoxButton1);
            this.FilterText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 2, 2, 2, 2, 176, 26);
            this.FilterText.Name = "FilterText";
            this.FilterText.RoundedCornerRadius = 3;
            this.FilterText.TabIndex = 1;
            // 
            // uiTextBoxButton1
            // 
            this.uiTextBoxButton1.Action = Resco.UIElements.TextBoxAction.Clear;
            this.uiTextBoxButton1.BackColor = System.Drawing.Color.Transparent;
            this.uiTextBoxButton1.BorderThickness = 0;
            this.uiTextBoxButton1.HorizontalAlignment = Resco.UIElements.HAlignment.Right;
            this.uiTextBoxButton1.Name = "uiTextBoxButton1";
            this.uiTextBoxButton1.PressedBackground.BackColor = System.Drawing.Color.Transparent;
            this.uiTextBoxButton1.Size = new System.Drawing.Size(18, 18);
            this.uiTextBoxButton1.StateIcon = Resco.UIElements.StateIcon.Delete;
            this.uiTextBoxButton1.VisibleMode = Resco.UIElements.UITextBoxButtonVisibleMode.WhileEditing;
            // 
            // ObjectNameText
            // 
            this.ObjectNameText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.ObjectNameText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Left, Resco.UIElements.VAlignment.Top, 0, 0, 0, 0, 114, 15);
            this.ObjectNameText.Name = "ObjectNameText";
            this.ObjectNameText.TabIndex = 1;
            this.ObjectNameText.Text = "Название обьекта";
            // 
            // ManageGridPanel
            // 
            this.ManageGridPanel.Children.Add(this.SelectBtn, 3, 0);
            this.ManageGridPanel.Children.Add(this.AddBtn, 2, 0);
            this.ManageGridPanel.Children.Add(this.CanselBtn, 0, 0);
            this.ManageGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(150, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ManageGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(70, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ManageGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(150, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ManageGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(150, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ManageGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 30);
            this.ManageGridPanel.Name = "ManageGridPanel";
            this.ManageGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ManageGridPanel.TabIndex = 2;
            // 
            // SelectBtn
            // 
            this.SelectBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 2, 2, 2, 2, 65, 26);
            this.SelectBtn.Name = "SelectBtn";
            this.SelectBtn.RoundedCornerRadius = 3;
            this.SelectBtn.Text = "Выбор";
            this.SelectBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.SelectBtnClick);
            // 
            // AddBtn
            // 
            this.AddBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 2, 2, 2, 2, 65, 26);
            this.AddBtn.Name = "AddBtn";
            this.AddBtn.RoundedCornerRadius = 3;
            this.AddBtn.TabIndex = 1;
            this.AddBtn.Text = "Добавить";
            this.AddBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.AddBtnClick);
            // 
            // CanselBtn
            // 
            this.CanselBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 2, 2, 2, 2, 65, 26);
            this.CanselBtn.Name = "CanselBtn";
            this.CanselBtn.RoundedCornerRadius = 3;
            this.CanselBtn.TabIndex = 2;
            this.CanselBtn.Text = "Отмена";
            this.CanselBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.CanselBtnClick);
            // 
            // ObjectsListView
            // 
            this.ObjectsListView.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 230);
            this.ObjectsListView.Name = "ObjectsListView";
            this.ObjectsListView.ScrollBarsMode = Resco.UIElements.ScrollBarsMode.OutOfContent;
            this.ObjectsListView.TabIndex = 3;
            this.ObjectsListView.SelectedIndexChanging += new Resco.UIElements.SelectedIndexChangingEventHandler(this.ObjectsListViewSelectedIndexChanging);
            // 
            // ObjectsControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.Size = new System.Drawing.Size(240, 320);
            this.StatusText = "Просканируйте штрихкод или введите код";
            this.ResumeLayout();

        }

        #endregion

        private Resco.UIElements.UIGridPanel InventoryGridPanel;
        private Resco.UIElements.UIGridPanel FilterGridPanel;
        private Resco.UIElements.UILabel ObjectNameText;
        private Stock.Controls.StockButton FilterBtn;
        private Resco.UIElements.UIGridPanel ManageGridPanel;
        private Stock.Controls.StockButton SelectBtn;
        private Stock.Controls.StockButton AddBtn;
        private Stock.Controls.StockButton CanselBtn;
        private StockTextEdit FilterText;
        private Resco.UIElements.UITextBoxButton uiTextBoxButton1;
        private Resco.UIElements.UIListView ObjectsListView;

    }
}
