﻿namespace Stock.Views
{
    partial class EnterBarcodeControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.EnterBarcodeGridPanel = new Resco.UIElements.UIGridPanel();
            this.BottomGridPanel = new Resco.UIElements.UIGridPanel();
            this.CancelBtn = new Stock.Controls.StockButton();
            this.EnterBtn = new Stock.Controls.StockButton();
            this.BarcodeGridPanel = new Resco.UIElements.UIGridPanel();
            this.PrefixesBox = new Resco.UIElements.UIComboBox();
            this.BarcodeText = new Stock.Controls.StockTextEdit();
            this.uiTextBoxButton2 = new Resco.UIElements.UITextBoxButton();
            this.PostfixesBox = new Resco.UIElements.UIComboBox();
            this.uiLabel1 = new Resco.UIElements.UILabel();
            this.uiTextBoxButton1 = new Resco.UIElements.UITextBoxButton();
            this.SuspendLayout();
            // 
            // StockGridPanel
            // 
            this.StockGridPanel.Children.Add(this.EnterBarcodeGridPanel, 0, 1);
            // 
            // HeaderLabel
            // 
            this.HeaderLabel.Text = "Ручной ввод штрих-кода";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold);
            this.StatusLabel.Text = "Введите штрих-код";
            // 
            // EnterBarcodeGridPanel
            // 
            this.EnterBarcodeGridPanel.Children.Add(this.BottomGridPanel, 0, 3);
            this.EnterBarcodeGridPanel.Children.Add(this.BarcodeGridPanel, 0, 1);
            this.EnterBarcodeGridPanel.Children.Add(this.uiLabel1, 0, 0);
            this.EnterBarcodeGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.EnterBarcodeGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 290);
            this.EnterBarcodeGridPanel.Name = "EnterBarcodeGridPanel";
            this.EnterBarcodeGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.EnterBarcodeGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(40, Resco.UIElements.GridLineValueType.Absolute, true));
            this.EnterBarcodeGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.EnterBarcodeGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(30, Resco.UIElements.GridLineValueType.Absolute, true));
            this.EnterBarcodeGridPanel.TabIndex = 1;
            // 
            // BottomGridPanel
            // 
            this.BottomGridPanel.Children.Add(this.CancelBtn, 0, 0);
            this.BottomGridPanel.Children.Add(this.EnterBtn, 1, 0);
            this.BottomGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.BottomGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.BottomGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 30);
            this.BottomGridPanel.Name = "BottomGridPanel";
            this.BottomGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            // 
            // CancelBtn
            // 
            this.CancelBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 5, 2, 5, 3, 110, 25);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.RoundedCornerRadius = 3;
            this.CancelBtn.Text = "Отмена";
            this.CancelBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.CancelBtnClick);
            // 
            // EnterBtn
            // 
            this.EnterBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 5, 2, 5, 3, 110, 25);
            this.EnterBtn.Name = "EnterBtn";
            this.EnterBtn.RoundedCornerRadius = 3;
            this.EnterBtn.TabIndex = 1;
            this.EnterBtn.Text = "Ввод";
            this.EnterBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.EnterBtnClick);
            // 
            // BarcodeGridPanel
            // 
            this.BarcodeGridPanel.Children.Add(this.PrefixesBox, 0, 0);
            this.BarcodeGridPanel.Children.Add(this.BarcodeText, 1, 0);
            this.BarcodeGridPanel.Children.Add(this.PostfixesBox, 2, 0);
            this.BarcodeGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(60, Resco.UIElements.GridLineValueType.Percentage, true));
            this.BarcodeGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(120, Resco.UIElements.GridLineValueType.Percentage, true));
            this.BarcodeGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(80, Resco.UIElements.GridLineValueType.Percentage, true));
            this.BarcodeGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 40);
            this.BarcodeGridPanel.Name = "BarcodeGridPanel";
            this.BarcodeGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.BarcodeGridPanel.TabIndex = 1;
            // 
            // PrefixesBox
            // 
            this.PrefixesBox.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.PrefixesBox.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 55, 40);
            this.PrefixesBox.Name = "PrefixesBox";
            // 
            // BarcodeText
            // 
            this.BarcodeText.Buttons.Add(this.uiTextBoxButton2);
            this.BarcodeText.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.BarcodeText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 110, 40);
            this.BarcodeText.Name = "BarcodeText";
            this.BarcodeText.TabIndex = 1;
            // 
            // uiTextBoxButton2
            // 
            this.uiTextBoxButton2.Action = Resco.UIElements.TextBoxAction.Clear;
            this.uiTextBoxButton2.BackColor = System.Drawing.Color.Transparent;
            this.uiTextBoxButton2.BorderThickness = 0;
            this.uiTextBoxButton2.HorizontalAlignment = Resco.UIElements.HAlignment.Right;
            this.uiTextBoxButton2.Name = "uiTextBoxButton2";
            this.uiTextBoxButton2.PressedBackground.BackColor = System.Drawing.Color.Transparent;
            this.uiTextBoxButton2.Size = new System.Drawing.Size(18, 18);
            this.uiTextBoxButton2.StateIcon = Resco.UIElements.StateIcon.Delete;
            this.uiTextBoxButton2.VisibleMode = Resco.UIElements.UITextBoxButtonVisibleMode.WhileEditing;
            // 
            // PostfixesBox
            // 
            this.PostfixesBox.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.PostfixesBox.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 73, 40);
            this.PostfixesBox.Name = "PostfixesBox";
            this.PostfixesBox.TabIndex = 2;
            // 
            // uiLabel1
            // 
            this.uiLabel1.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 199, 75);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.TabIndex = 2;
            this.uiLabel1.Text = "Пожалуйста, введите новый штрих-код ТМЦ.";
            this.uiLabel1.TextAlignment = Resco.Drawing.Alignment.MiddleCenter;
            // 
            // uiTextBoxButton1
            // 
            this.uiTextBoxButton1.Action = Resco.UIElements.TextBoxAction.Clear;
            this.uiTextBoxButton1.BackColor = System.Drawing.Color.Transparent;
            this.uiTextBoxButton1.BorderThickness = 0;
            this.uiTextBoxButton1.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Right, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 18, 18);
            this.uiTextBoxButton1.Name = "uiTextBoxButton1";
            this.uiTextBoxButton1.PressedBackground.BackColor = System.Drawing.Color.Transparent;
            this.uiTextBoxButton1.StateIcon = Resco.UIElements.StateIcon.Delete;
            this.uiTextBoxButton1.VisibleMode = Resco.UIElements.UITextBoxButtonVisibleMode.WhileEditing;
            // 
            // EnterBarcodeControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.HeaderText = "Ручной ввод штрих-кода";
            this.Size = new System.Drawing.Size(240, 320);
            this.StatusText = "Введите штрих-код";
            this.ResumeLayout();

        }

        #endregion

        private Resco.UIElements.UIGridPanel EnterBarcodeGridPanel;
        private Resco.UIElements.UIGridPanel BottomGridPanel;
        private Stock.Controls.StockButton CancelBtn;
        private Stock.Controls.StockButton EnterBtn;
        private Resco.UIElements.UIGridPanel BarcodeGridPanel;
        private Resco.UIElements.UITextBoxButton uiTextBoxButton1;
        private Resco.UIElements.UIComboBox PrefixesBox;
        private Stock.Controls.StockTextEdit BarcodeText;
        private Resco.UIElements.UITextBoxButton uiTextBoxButton2;
        private Resco.UIElements.UIComboBox PostfixesBox;
        private Resco.UIElements.UILabel uiLabel1;
    }
}
