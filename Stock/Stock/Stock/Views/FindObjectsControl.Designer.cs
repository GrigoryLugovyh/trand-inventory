﻿namespace Stock.Views
{
    partial class FindObjectsControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FindObjectsGridPanel = new Resco.UIElements.UIGridPanel();
            this.uiGridPanel1 = new Resco.UIElements.UIGridPanel();
            this.SelectBtn = new Stock.Controls.StockButton();
            this.uiGridPanel2 = new Resco.UIElements.UIGridPanel();
            this.FilterBtn = new Stock.Controls.StockButton();
            this.ObjectsFilterText = new Stock.Controls.StockTextEdit();
            this.uiTextBoxButton1 = new Resco.UIElements.UITextBoxButton();
            this.ObjectsListView = new Resco.UIElements.UIListView();
            this.SuspendLayout();
            // 
            // StockGridPanel
            // 
            this.StockGridPanel.Children.Add(this.FindObjectsGridPanel, 0, 1);
            // 
            // HeaderLabel
            // 
            this.HeaderLabel.Text = "Выберите объект";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold);
            this.StatusLabel.Text = "Выберите родительский объект";
            // 
            // FindObjectsGridPanel
            // 
            this.FindObjectsGridPanel.Children.Add(this.uiGridPanel1, 0, 2);
            this.FindObjectsGridPanel.Children.Add(this.uiGridPanel2, 0, 0);
            this.FindObjectsGridPanel.Children.Add(this.ObjectsListView, 0, 1);
            this.FindObjectsGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.FindObjectsGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 290);
            this.FindObjectsGridPanel.Name = "FindObjectsGridPanel";
            this.FindObjectsGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(30, Resco.UIElements.GridLineValueType.Absolute, true));
            this.FindObjectsGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.FindObjectsGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(30, Resco.UIElements.GridLineValueType.Absolute, true));
            this.FindObjectsGridPanel.TabIndex = 1;
            // 
            // uiGridPanel1
            // 
            this.uiGridPanel1.Children.Add(this.SelectBtn, 1, 0);
            this.uiGridPanel1.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel1.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Absolute, true));
            this.uiGridPanel1.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel1.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 30);
            this.uiGridPanel1.Name = "uiGridPanel1";
            this.uiGridPanel1.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            // 
            // SelectBtn
            // 
            this.SelectBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 5, 2, 5, 3, 90, 25);
            this.SelectBtn.Name = "SelectBtn";
            this.SelectBtn.RoundedCornerRadius = 3;
            this.SelectBtn.TabIndex = 1;
            this.SelectBtn.Text = "Выбрать";
            this.SelectBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.SelectBtnClick);
            // 
            // uiGridPanel2
            // 
            this.uiGridPanel2.Children.Add(this.FilterBtn, 1, 0);
            this.uiGridPanel2.Children.Add(this.ObjectsFilterText, 0, 0);
            this.uiGridPanel2.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel2.Columns.Add(new Resco.UIElements.GridLinePosition(70, Resco.UIElements.GridLineValueType.Absolute, true));
            this.uiGridPanel2.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 30);
            this.uiGridPanel2.Name = "uiGridPanel2";
            this.uiGridPanel2.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.uiGridPanel2.TabIndex = 1;
            // 
            // FilterBtn
            // 
            this.FilterBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 3, 3, 3, 3, 64, 24);
            this.FilterBtn.Name = "FilterBtn";
            this.FilterBtn.RoundedCornerRadius = 3;
            this.FilterBtn.Text = "Фильтр";
            this.FilterBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.FilterBtnClick);
            // 
            // ObjectsFilterText
            // 
            this.ObjectsFilterText.Buttons.Add(this.uiTextBoxButton1);
            this.ObjectsFilterText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 3, 3, 3, 3, 164, 24);
            this.ObjectsFilterText.Name = "ObjectsFilterText";
            this.ObjectsFilterText.RoundedCornerRadius = 3;
            this.ObjectsFilterText.TabIndex = 1;
            // 
            // uiTextBoxButton1
            // 
            this.uiTextBoxButton1.Action = Resco.UIElements.TextBoxAction.Clear;
            this.uiTextBoxButton1.BackColor = System.Drawing.Color.Transparent;
            this.uiTextBoxButton1.BorderThickness = 0;
            this.uiTextBoxButton1.HorizontalAlignment = Resco.UIElements.HAlignment.Right;
            this.uiTextBoxButton1.Name = "uiTextBoxButton1";
            this.uiTextBoxButton1.PressedBackground.BackColor = System.Drawing.Color.Transparent;
            this.uiTextBoxButton1.Size = new System.Drawing.Size(18, 18);
            this.uiTextBoxButton1.StateIcon = Resco.UIElements.StateIcon.Delete;
            this.uiTextBoxButton1.VisibleMode = Resco.UIElements.UITextBoxButtonVisibleMode.WhileEditing;
            // 
            // ObjectsListView
            // 
            this.ObjectsListView.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 230);
            this.ObjectsListView.Name = "ObjectsListView";
            this.ObjectsListView.ScrollBarsMode = Resco.UIElements.ScrollBarsMode.OutOfContent;
            this.ObjectsListView.TabIndex = 2;
            // 
            // FindObjectsControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.HeaderText = "Выберите объект";
            this.Size = new System.Drawing.Size(240, 320);
            this.StatusText = "Выберите родительский объект";
            this.ResumeLayout();

        }

        #endregion

        private Resco.UIElements.UIGridPanel FindObjectsGridPanel;
        private Resco.UIElements.UIGridPanel uiGridPanel1;
        private Stock.Controls.StockButton SelectBtn;
        private Resco.UIElements.UIGridPanel uiGridPanel2;
        private Stock.Controls.StockButton FilterBtn;
        private Stock.Controls.StockTextEdit ObjectsFilterText;
        private Resco.UIElements.UITextBoxButton uiTextBoxButton1;
        private Resco.UIElements.UIListView ObjectsListView;
    }
}
