﻿namespace Stock.Views
{
    partial class AddItemControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CreateItemGridPanel = new Resco.UIElements.UIGridPanel();
            this.AddItemBottomGridPanel = new Resco.UIElements.UIGridPanel();
            this.CancelBtn = new Stock.Controls.StockButton();
            this.AddBtn = new Stock.Controls.StockButton();
            this.ItemCodeGridPanel = new Resco.UIElements.UIGridPanel();
            this.uiLabel1 = new Resco.UIElements.UILabel();
            this.BarcodeText = new Stock.Controls.StockTextEdit();
            this.ItemNameGridPanel = new Resco.UIElements.UIGridPanel();
            this.uiLabel2 = new Resco.UIElements.UILabel();
            this.ItemNameText = new Stock.Controls.StockTextEdit();
            this.uiTextBoxButton1 = new Resco.UIElements.UITextBoxButton();
            this.ItemStatusGridPanel = new Resco.UIElements.UIGridPanel();
            this.uiLabel3 = new Resco.UIElements.UILabel();
            this.ItemStatusesBox = new Resco.UIElements.UIComboBox();
            this.ItemBegindateGridPanel = new Resco.UIElements.UIGridPanel();
            this.uiLabel4 = new Resco.UIElements.UILabel();
            this.ItemBegindatePiker = new Resco.UIElements.UIDateTimePicker();
            this.SuspendLayout();
            // 
            // StockGridPanel
            // 
            this.StockGridPanel.Children.Add(this.CreateItemGridPanel, 0, 1);
            // 
            // HeaderLabel
            // 
            this.HeaderLabel.Text = "Создание ТМЦ:";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Text = "Введите параметры ТМЦ";
            // 
            // CreateItemGridPanel
            // 
            this.CreateItemGridPanel.Children.Add(this.AddItemBottomGridPanel, 0, 8);
            this.CreateItemGridPanel.Children.Add(this.ItemCodeGridPanel, 0, 0);
            this.CreateItemGridPanel.Children.Add(this.ItemNameGridPanel, 0, 2);
            this.CreateItemGridPanel.Children.Add(this.ItemStatusGridPanel, 0, 4);
            this.CreateItemGridPanel.Children.Add(this.ItemBegindateGridPanel, 0, 6);
            this.CreateItemGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.CreateItemGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 290);
            this.CreateItemGridPanel.Name = "CreateItemGridPanel";
            this.CreateItemGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(50, Resco.UIElements.GridLineValueType.Absolute, true));
            this.CreateItemGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(0, Resco.UIElements.GridLineValueType.Absolute, true));
            this.CreateItemGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(50, Resco.UIElements.GridLineValueType.Absolute, true));
            this.CreateItemGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(0, Resco.UIElements.GridLineValueType.Absolute, true));
            this.CreateItemGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(50, Resco.UIElements.GridLineValueType.Absolute, true));
            this.CreateItemGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(0, Resco.UIElements.GridLineValueType.Absolute, true));
            this.CreateItemGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(50, Resco.UIElements.GridLineValueType.Absolute, true));
            this.CreateItemGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.CreateItemGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(30, Resco.UIElements.GridLineValueType.Absolute, true));
            this.CreateItemGridPanel.TabIndex = 1;
            // 
            // AddItemBottomGridPanel
            // 
            this.AddItemBottomGridPanel.Children.Add(this.CancelBtn, 0, 0);
            this.AddItemBottomGridPanel.Children.Add(this.AddBtn, 1, 0);
            this.AddItemBottomGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.AddItemBottomGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.AddItemBottomGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 30);
            this.AddItemBottomGridPanel.Name = "AddItemBottomGridPanel";
            this.AddItemBottomGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            // 
            // CancelBtn
            // 
            this.CancelBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 3, 2, 3, 1, 114, 27);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.RoundedCornerRadius = 3;
            this.CancelBtn.Text = "Отмена";
            this.CancelBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.CancelBtnClick);
            // 
            // AddBtn
            // 
            this.AddBtn.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 3, 2, 3, 1, 114, 27);
            this.AddBtn.Name = "AddBtn";
            this.AddBtn.RoundedCornerRadius = 3;
            this.AddBtn.TabIndex = 1;
            this.AddBtn.Text = "Добавить";
            this.AddBtn.Click += new Resco.UIElements.UIMouseEventHandler(this.AddBtnClick);
            // 
            // ItemCodeGridPanel
            // 
            this.ItemCodeGridPanel.Children.Add(this.uiLabel1, 0, 0);
            this.ItemCodeGridPanel.Children.Add(this.BarcodeText, 0, 1);
            this.ItemCodeGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ItemCodeGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 50);
            this.ItemCodeGridPanel.Name = "ItemCodeGridPanel";
            this.ItemCodeGridPanel.Padding = new Resco.UIElements.Thickness(1);
            this.ItemCodeGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(20, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ItemCodeGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ItemCodeGridPanel.TabIndex = 1;
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.uiLabel1.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 61, 17);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Text = "Код ТМЦ";
            // 
            // BarcodeText
            // 
            this.BarcodeText.Enabled = false;
            this.BarcodeText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 238, 28);
            this.BarcodeText.Name = "BarcodeText";
            this.BarcodeText.ReadOnly = true;
            this.BarcodeText.TabIndex = 1;
            // 
            // ItemNameGridPanel
            // 
            this.ItemNameGridPanel.Children.Add(this.uiLabel2, 0, 0);
            this.ItemNameGridPanel.Children.Add(this.ItemNameText, 0, 1);
            this.ItemNameGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ItemNameGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 50);
            this.ItemNameGridPanel.Name = "ItemNameGridPanel";
            this.ItemNameGridPanel.Padding = new Resco.UIElements.Thickness(1);
            this.ItemNameGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(20, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ItemNameGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ItemNameGridPanel.TabIndex = 2;
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.uiLabel2.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 138, 17);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Text = "Наименование ТМЦ";
            // 
            // ItemNameText
            // 
            this.ItemNameText.Buttons.Add(this.uiTextBoxButton1);
            this.ItemNameText.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 238, 28);
            this.ItemNameText.Name = "ItemNameText";
            this.ItemNameText.TabIndex = 1;
            // 
            // uiTextBoxButton1
            // 
            this.uiTextBoxButton1.Action = Resco.UIElements.TextBoxAction.Clear;
            this.uiTextBoxButton1.BackColor = System.Drawing.Color.Transparent;
            this.uiTextBoxButton1.BorderThickness = 0;
            this.uiTextBoxButton1.HorizontalAlignment = Resco.UIElements.HAlignment.Right;
            this.uiTextBoxButton1.Name = "uiTextBoxButton1";
            this.uiTextBoxButton1.PressedBackground.BackColor = System.Drawing.Color.Transparent;
            this.uiTextBoxButton1.Size = new System.Drawing.Size(18, 18);
            this.uiTextBoxButton1.StateIcon = Resco.UIElements.StateIcon.Delete;
            this.uiTextBoxButton1.VisibleMode = Resco.UIElements.UITextBoxButtonVisibleMode.WhileEditing;
            // 
            // ItemStatusGridPanel
            // 
            this.ItemStatusGridPanel.Children.Add(this.uiLabel3, 0, 0);
            this.ItemStatusGridPanel.Children.Add(this.ItemStatusesBox, 0, 1);
            this.ItemStatusGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ItemStatusGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 50);
            this.ItemStatusGridPanel.Name = "ItemStatusGridPanel";
            this.ItemStatusGridPanel.Padding = new Resco.UIElements.Thickness(1);
            this.ItemStatusGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(20, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ItemStatusGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ItemStatusGridPanel.TabIndex = 3;
            // 
            // uiLabel3
            // 
            this.uiLabel3.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.uiLabel3.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 80, 17);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Text = "Статус ТМЦ";
            // 
            // ItemStatusesBox
            // 
            this.ItemStatusesBox.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 238, 28);
            this.ItemStatusesBox.Name = "ItemStatusesBox";
            this.ItemStatusesBox.TabIndex = 1;
            // 
            // ItemBegindateGridPanel
            // 
            this.ItemBegindateGridPanel.Children.Add(this.uiLabel4, 0, 0);
            this.ItemBegindateGridPanel.Children.Add(this.ItemBegindatePiker, 0, 1);
            this.ItemBegindateGridPanel.Columns.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ItemBegindateGridPanel.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 240, 50);
            this.ItemBegindateGridPanel.Name = "ItemBegindateGridPanel";
            this.ItemBegindateGridPanel.Padding = new Resco.UIElements.Thickness(1);
            this.ItemBegindateGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(20, Resco.UIElements.GridLineValueType.Absolute, true));
            this.ItemBegindateGridPanel.Rows.Add(new Resco.UIElements.GridLinePosition(100, Resco.UIElements.GridLineValueType.Percentage, true));
            this.ItemBegindateGridPanel.TabIndex = 4;
            // 
            // uiLabel4
            // 
            this.uiLabel4.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.uiLabel4.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Center, Resco.UIElements.VAlignment.Center, 0, 0, 0, 0, 152, 17);
            this.uiLabel4.Name = "uiLabel4";
            this.uiLabel4.Text = "Начало эксплуатации";
            // 
            // ItemBegindatePiker
            // 
            this.ItemBegindatePiker.Layout = new Resco.UIElements.ElementLayout(Resco.UIElements.HAlignment.Stretch, Resco.UIElements.VAlignment.Stretch, 0, 0, 0, 0, 238, 28);
            this.ItemBegindatePiker.Name = "ItemBegindatePiker";
            this.ItemBegindatePiker.TabIndex = 1;
            // 
            // AddItemControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.HeaderText = "Создание ТМЦ:";
            this.Size = new System.Drawing.Size(240, 320);
            this.StatusText = "Введите параметры ТМЦ";
            this.ResumeLayout();

        }

        #endregion

        private Resco.UIElements.UIGridPanel CreateItemGridPanel;
        private Resco.UIElements.UIGridPanel AddItemBottomGridPanel;
        private Stock.Controls.StockButton CancelBtn;
        private Stock.Controls.StockButton AddBtn;
        private Resco.UIElements.UIGridPanel ItemCodeGridPanel;
        private Resco.UIElements.UIGridPanel ItemNameGridPanel;
        private Resco.UIElements.UIGridPanel ItemStatusGridPanel;
        private Resco.UIElements.UIGridPanel ItemBegindateGridPanel;
        private Resco.UIElements.UILabel uiLabel1;
        private Stock.Controls.StockTextEdit BarcodeText;
        private Resco.UIElements.UILabel uiLabel2;
        private Stock.Controls.StockTextEdit ItemNameText;
        private Resco.UIElements.UITextBoxButton uiTextBoxButton1;
        private Resco.UIElements.UILabel uiLabel3;
        private Resco.UIElements.UIComboBox ItemStatusesBox;
        private Resco.UIElements.UILabel uiLabel4;
        private Resco.UIElements.UIDateTimePicker ItemBegindatePiker;
    }
}
