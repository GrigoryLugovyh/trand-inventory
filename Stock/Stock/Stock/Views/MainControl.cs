﻿using System;
using Resco.UIElements;
using StockCore.Interfaces.Views;

namespace Stock.Views
{
    public partial class MainControl : StockControl, IMainView
    {
        public MainControl()
        {
            InitializeComponent();
        }

        public void ShowView()
        {
            if(Show != null)
            {
                Show(this, EventArgs.Empty);
            }

            ShowView<MainControl>(AnimationEffect.MoveLeft);

            Invoker(() => InventoryBtn.Focus());
        }

        public void CloseView()
        {

        }

        public event EventHandler Show;

        public event EventHandler Close;

        public event EventHandler InventoryClick;

        public event EventHandler InformationClick;

        public event EventHandler ObjectsClick;

        public event EventHandler ExchangeClick;

        public string ItemsAll
        {
            get { return AllItems.Text; }
            set { Invoker(() => AllItems.Text = value); }
        }

        public string ObjectsAll
        {
            get { return AllObjects.Text; }
            set { Invoker(() => AllObjects.Text = value); }
        }

        public string ItemsProcessed
        {
            get { return ProcessedItems.Text; }
            set { Invoker(() => ProcessedItems.Text = value); }
        }

        public string ObjectsProcessed
        {
            get { return ProcessedObjects.Text; }
            set { Invoker(() => ProcessedObjects.Text = value); }
        }

        private void InventoryBtnClick(object sender, UIMouseEventArgs e)
        {
            if (InventoryClick != null)
            {
                InventoryClick(sender, e);
            }
        }

        private void InformationBtnClick(object sender, UIMouseEventArgs e)
        {
            if(InformationClick != null)
            {
                InformationClick(sender, e);
            }
        }

        private void ObjectsBtnClick(object sender, UIMouseEventArgs e)
        {
            if(ObjectsClick != null)
            {
                ObjectsClick(sender, e);
            }
        }

        private void ExchangeBtnClick(object sender, UIMouseEventArgs e)
        {
            if(ExchangeClick != null)
            {
                ExchangeClick(sender, e);
            }
        }

        private void ExitBtnClick(object sender, UIMouseEventArgs e)
        {
            Exit();
        }
    }
}
