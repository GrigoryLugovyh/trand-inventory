﻿using System;
using System.Windows.Forms;
using Resco.UIElements;
using Resco.UIElements.Controls;
using Stock.Handlers;
using Stock.Interfaces;

namespace Stock.Views
{
    public partial class StockControl : UIUserPanel, IControl
    {
        public StockControl()
        {
            InitializeComponent();
        }

        public void Initialize()
        {
            InitializeKeys(this);
        }

        public event EventHandler ExitClick;

        public string HeaderText
        {
            get { return HeaderLabel.Text; }
            set { Invoker(() => HeaderLabel.Text = value); }
        }

        public bool HeaderVisible
        {
            get { return HeaderLabel.Visible; }
            set { Invoker(() => HeaderLabel.Visible = value); }
        }

        public string StatusText
        {
            get { return StatusLabel.Text; }
            set { Invoker(() => StatusLabel.Text = value); }
        }

        public bool StatusVisible
        {
            get { return StatusLabel.Visible; }
            set { Invoker(() => StatusLabel.Visible = value); }
        }

        public virtual bool Rememberable
        {
            get { return true; }
        }

        public UIUserPanel Control
        {
            get { return this; }
        }

        public UIElementPanelControl Template
        {
            get
            {
                UIElementPanelControl option = null;
                if (StockView.SendStockViewHandler != null)
                {
                    StockView.SendStockViewHandler(ref option);
                }

                return option;
            }
        }

        public void CloseApplication()
        {
            if(StockView.CloseApplicationHandler != null)
            {
                StockView.CloseApplicationHandler();
            }
        }

        public virtual void BackView()
        {
            BackView(AnimationEffect.MoveRight);
        }

        protected virtual void ShowView<T>(AnimationEffect animation) where T : UIUserPanel
        {
            Invoker(() => ShowView<T>(animation, true));
        }

        protected virtual void BackView(AnimationEffect animation)
        {
            Invoker(() => ControlHandler.Instance.BackView(animation));
        }

        protected virtual void ShowView<T>(AnimationEffect animation, bool update) where T : UIUserPanel
        {
            var detail = ControlHandler.Instance.GetView<T>(Template/*, update*/);
            ControlHandler.Instance.ShowView(detail, animation);
        }

        protected void Invoker(Action action)
        {
            if (!InvokeRequired)
            {
                action.Invoke();
            }
            else
            {
                Invoke(new Action(action.Invoke));
            }
        }

        protected virtual void InitializeKeys(UIPanel panel)
        {
            foreach (var p in panel.Children)
            {
                if (p is UIPanel)
                {
                    var c = p as UIPanel; 

                    if (c.Children.Count > 0)
                    {
                        InitializeKeys(c);
                    }
                }
                else if (p is UIElement)
                {
                    var e = p as UIElement;

                    e.KeyUp += CtrlKeyUp;
                }
            }
        }

        /// <summary>
        /// Обработка нажатия клавиш на клавиатуре ТСД
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CtrlKeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    {
                        KeyEnterHandler();
                        break;
                    }
                case Keys.Escape:
                    {
                        if(KeyboardHandler.Instance.Shown)
                        {
                            KeyboardHandler.Instance.Hide();
                            break;
                        }

                        KeyEscapeHandler();
                        break;
                    }
                case (Keys)125: // левая функ. клавише
                    {
                        KeyLeftFuncHandler();
                        break;
                    }
                case (Keys)126: // правая функ. клавиша
                    {
                        KeyRightFuncHandler();
                        break;
                    }
            }
        }

        protected virtual void KeyEscapeHandler()
        {
            BackView();
        }

        protected virtual void KeyEnterHandler()
        {
        }

        protected virtual void KeyLeftFuncHandler()
        {
        }

        protected virtual void KeyRightFuncHandler()
        {
        }

        protected void Exit()
        {
            if (MessageHandler.Instance.ShowQuestion(MessageBoxIcon.Question, "Завершить работу\r\nприложения?", DialogResult.No, "Да", "Нет") == DialogResult.Yes)
            {
                if (ExitClick != null)
                {
                    ExitClick(this, EventArgs.Empty);
                }

                CloseApplication();
            }
        }
    }
}