﻿using System;
using Resco.UIElements;
using StockCore.Interfaces.Views;

namespace Stock.Views
{
    public partial class AddObjectControl : StockControl, IAddObjectView
    {
        public AddObjectControl()
        {
            InitializeComponent();
        }

        public event EventHandler Show;

        public event EventHandler Close;

        public event EventHandler Add;

        public event EventHandler Create;

        public event EventHandler Delete;

        public void ShowView()
        {
            if(Show != null)
            {
                Show(this, EventArgs.Empty);
            }

            ShowView<AddObjectControl>(AnimationEffect.MoveLeft);

            Invoker(() => BarcodeText.Focus());
        }

        public void CloseView()
        {
            if(Close != null)
            {
                Close(this, EventArgs.Empty);
            }

            ShowView<ObjectsControl>(AnimationEffect.MoveRight);
        }

        public string ObjectBarcode
        {
            get { return BarcodeText.Text; }
            set { Invoker(() => BarcodeText.Text = value); }
        }

        public string ObjectCode
        {
            get { return CodeText.Text; }
            set { Invoker(() => CodeText.Text = value); }
        }

        public string ObjectName
        {
            get { return NameText.Text; }
            set { Invoker(() => NameText.Text = value); }
        }

        public string ObjectParent
        {
            get { return ParentText.Text; }
            set { Invoker(() => ParentText.Text = value); }
        }

        private void AddBtnClick(object sender, UIMouseEventArgs e)
        {
            if (Create != null)
            {
                Create(sender, e);
            }
        }

        private void AddParentBtnClick(object sender, UIMouseEventArgs e)
        {
            if (Add != null)
            {
                Add(sender, e);
            }
        }

        private void CancelBtnClick(object sender, UIMouseEventArgs e)
        {
            CloseView();
        }

        private void DeleteParentBtnClick(object sender, UIMouseEventArgs e)
        {
            if(Delete != null)
            {
                Delete(sender, e);
            }
        }
    }
}