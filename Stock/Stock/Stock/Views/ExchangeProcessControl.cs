﻿using System;
using Resco.UIElements;
using StockCore.Interfaces.Views;

namespace Stock.Views
{
    public partial class ExchangeProcessControl : StockControl, IExchangeProcessView
    {
        public ExchangeProcessControl()
        {
            InitializeComponent();
        }

        public void ShowView()
        {
            ShowView<ExchangeProcessControl>(AnimationEffect.MoveLeft);

            Invoker(() => AbortBtn.Focus());
        }

        public void CloseView()
        {
            ShowView<ExchangeControl>(AnimationEffect.MoveRight);
        }

        public string Header
        {
            get { return ExchangeHeaderText.Text; }
            set { Invoker(() => ExchangeHeaderText.Text = value); }
        }

        public string Received
        {
            get { return ExchangeReceivedText.Text; }
            set { Invoker(() => ExchangeReceivedText.Text = value); }
        }

        public string Prepared
        {
            get { return ExchangePreparedText.Text; }
            set { Invoker(() => ExchangePreparedText.Text = value); }
        }

        public string Processed
        {
            get { return ExchangeProcessedText.Text; }
            set { Invoker(() => ExchangeProcessedText.Text = value); }
        }

        public string NotProcessed
        {
            get { return ExchangeNotProcessedText.Text; }
            set { Invoker(() => ExchangeNotProcessedText.Text = value); }
        }

        public string ExchangeTime
        {
            get { return ExchangeProcessTimeText.Text; }
            set { Invoker(() => ExchangeProcessTimeText.Text = value); }
        }

        public string ProcessText
        {
            get { return ExchangeProcessText.Text; }
            set { Invoker(() => ExchangeProcessText.Text = value); }
        }

        public int Progress
        {
            get { return ExchangeProgress.Value; }
            set { Invoker(() => ExchangeProgress.Value = value); }
        }

        public event EventHandler Show;
        public event EventHandler Close;
        public event EventHandler Abort;

        private void AbortBtnClick(object sender, UIMouseEventArgs e)
        {
            if(Abort != null)
            {
                Abort(sender, e);
            }
        }
    }
}