using System;
using System.Windows.Forms;
using Stock.Handlers;
using StockCore.Enums;
using StockCore.Interfaces.Common;

namespace Stock.Common
{
    public class MessageDistributor : IMessager
    {
        public DialogEnum ShowExclamation(string message, string btn)
        {
            var show = WaitHandler.Instance.IsShow;
            if (show)
            {
                WaitHandler.Instance.Close();
            }

            try
            {
                return (DialogEnum)MessageHandler.Instance.ShowExclamation(MessageBoxIcon.Exclamation, message, btn);
            }
            finally
            {
                if (show)
                {
                    WaitHandler.Instance.Show();
                }
            }
        }

        public DialogEnum ShowQuestion(string message, DialogEnum result, string btnYes, string bntNo)
        {
            var show = WaitHandler.Instance.IsShow;
            if (show)
            {
                WaitHandler.Instance.Close();
            }

            try
            {
                return (DialogEnum)MessageHandler.Instance.ShowQuestion(MessageBoxIcon.Exclamation, message, (DialogResult)result, btnYes, bntNo);
            }
            finally
            {
                if(show)
                {
                    WaitHandler.Instance.Show();
                }
            }
        }
    }
}