using Stock.Handlers;
using StockCore.Interfaces.Common;

namespace Stock.Common
{
    public class WaitDistributor : IWait
    {
        public void ShowWait()
        {
            WaitHandler.Instance.Show();
        }

        public void CloseWait()
        {
            WaitHandler.Instance.Close();
        }
    }
}