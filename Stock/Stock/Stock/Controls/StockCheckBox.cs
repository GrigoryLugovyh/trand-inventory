﻿using Resco.UIElements;

namespace Stock.Controls
{
    public partial class StockCheckBox : UICheckBox
    {
        public StockCheckBox()
        {
            InitializeComponent();
        }

        public bool Selected
        {
            get { return CheckState == CheckState.Checked; }
            set { CheckState = value ? CheckState.Checked : CheckState.Unchecked; }
        }
    }
}