﻿namespace Stock.Controls
{
    partial class StockTextEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            // 
            // StockTextEdit
            // 
            this.DisabledBackground.DoNotScaleBorders = false;
            this.DisabledText.ForeColor = System.Drawing.SystemColors.GrayText;
            this.DisabledText.ForeShadow = System.Drawing.Color.Transparent;
            this.FocusedBackground.BorderColor = System.Drawing.SystemColors.Highlight;
            this.FocusedBackground.BorderThickness = 2;
            this.FocusedBackground.DoNotScaleBorders = false;
            this.FocusedText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.FocusedText.ForeShadow = System.Drawing.Color.Transparent;

        }

        #endregion
    }
}
