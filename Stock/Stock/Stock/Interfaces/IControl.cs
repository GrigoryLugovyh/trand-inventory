using Resco.UIElements;
using Resco.UIElements.Controls;

namespace Stock.Interfaces
{
    public interface IControl
    {
        /// <summary>
        /// ������� - �������������
        /// </summary>
        UIUserPanel Control { get; }

        /// <summary>
        /// ����������
        /// </summary>
        UIElementPanelControl Template { get; }
    }
}