﻿using System;
using System.Windows.Forms;
using Stock.Handlers;
using StockCore;
using StockCore.Enums;

namespace Stock
{
    internal static class Program
    {
        [MTAThread]
        private static void Main()
        {
            if (!MutexHandler.Instance.IsSingle())
            {
                return;
            }

            try
            {
                Application.Run(new StockView());
            }
            catch (Exception ex)
            {
                if(Model.Instance.Status == StartableEnum.Started)
                {
                    var error = string.Format("Возникла необработанная ошибка, приложение будет закрыто:\r\n\"{0}\"", ex.Message);
                    Model.Instance.ErrorLog(error);
                    Model.Instance.Messages.ShowExclamation(error, "Ок");
                }
            }
            finally
            {
                Model.Instance.Stop();
            }
        }
    }
}